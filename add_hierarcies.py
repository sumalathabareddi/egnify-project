import requests
import csv

import redis
import requests
from db.HydraArangoDb import HydraArangoDb
from db.HydraMongoDb import HydraMongoDb
from rq import Queue
from utils.EgnifyException import MultipleValidationExceptions, ValidationException


from app import create_app
from bulkread.BulkRead import HierarchiesBulkRead
from api.models import InstituteHierarchiesSchema

application = create_app()

# mongo_db = HydraMongoDb(application.config['MONGO_URI'])
mongo_db = HydraMongoDb("mongodb://root:5eqcMZQCPXtG8MvU@localhost:27018/?authenticationDatabase=admin")
arango_db = HydraArangoDb(
    application.config["ARANGODB_URI"],
    application.config["ARANGODB_USERNAME"],
    application.config["ARANGODB_PASSWORD"],
)
settings_database = application.config["SETTINGS_DATABASE"]
tenant_registry = application.config["TENANT_REGISTRY_DATABASE"]
# hostname = application.config["HOST_NAME"]
hostname = 'getranks.in'
redis_connection = redis.Redis.from_url(application.config["RQ_REDIS_URL"])
queue = Queue(application.config["RQ_QUEUES"], connection=redis_connection)

url = "https://storage.googleapis.com/vega-demo-cdn/Sriprakash_Clg_Nov_18_Students.csv"

with requests.Session() as s:
    downloaded_data = s.get(url)
    decoded_data = downloaded_data.content.decode("utf-8")
    csv_reader = csv.reader(decoded_data.splitlines(), delimiter=",")
    csv_headers = next(csv_reader)
    print(f'{csv_headers}')
    csv_data = list(csv_reader)
    for row in csv_data:
             print(row)
   
           