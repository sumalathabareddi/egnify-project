
from app import create_app
from db.HydraMongoDb import HydraMongoDb
import csv
from bulkread.BulkRead import AssignmentBulkRead
import requests


application = create_app()

mongodb = HydraMongoDb(application.config['MONGO_URI'])
settings_database = application.config["SETTINGS_DATABASE"]

def bulk_create_assignments(data):
    url = data['url']
    with requests.Session() as s:
        downloaded_data = s.get(url)
        decoded_data = downloaded_data.content.decode("utf-8")
        csv_reader = csv.DictReader(decoded_data.splitlines(), delimiter=",")
        csv_headers = csv_reader.fieldnames
        csv_data = list(csv_reader)
        print('data:size::',len(csv_data))
        textbooks,chapters,klass,branches,subjects = [],[],[],[],[]
        for row in csv_data:
            textbooks.append(row['TEXTBOOK'])
            chapters.append(row['CHAPTER'])
            klass.append(row['CLASS'])
            branches.append(row['BRANCH'])
            subjects.append(row['SUBJECT'])
        print("Completed reading students csv data")
        collection_name = "institutehierarchies"
        query = {'active':True}
        query["levelName"]='Class'
        query['child'] = {"$in":klass}
        classesData = mongodb.find(settings_database,collection_name,query,"child")
        squery = {'active':True}
        squery['refs.class.code']= {"$exists": True}
        squery['subject'] = {"$in":subjects}
        subjectData = mongodb.find(settings_database,"subjects",squery)
        collection_name = "textbook"
        tquery = {'active':True}
        tquery['refs.subject.code'] = {"$exists":True}
        tquery['name'] = {"$in":textbooks}
        textbookData = mongodb.find(settings_database,"textbooks",tquery)
        cquery = {'active':True}
        cquery['levelName'] = 'topic'
        cquery['child'] = {"$in":chapters}
        cquery['refs.textbook.name'] = {"$in":textbooks}
        chaptersData = mongodb.find(settings_database,"concepttaxonomies",cquery)
        print('completed Reading the data from db')
        
        bulkread = AssignmentBulkRead(
                    csv_headers,
                    csv_data,
                    classesData,
                    mapSubjectWithClass(subjectData),
                    mapTextbookWithSubjectAndClass(textbookData),
                    mapChapterWithTextbook(chaptersData))
        errors = bulkread.validate()
        if errors:
            print('errors::',errors)
        else:
            print('no errors')

    
def mapSubjectWithClass(subjectData):
    subjectObj ={}
    for subject in subjectData:
        key = f"{subject['refs']['class']['name']}-{subject['subject']}"
        subjectObj[key]=subject
    return subjectObj

def mapTextbookWithSubjectAndClass(textbookData):
    textbookObj = {}
    for textbook in textbookData:
        key = f"{textbook['refs']['class']['name']}-{textbook['refs']['subject']['name']}-{textbook['name']}"
        textbookObj[key]=textbook
    return textbookObj

def mapChapterWithTextbook(chaptersData):
    chapterObj = {}
    for chapter in chaptersData:
        key = f"{chapter['refs']['textbook']['name']}-{chapter['child']}"  
        chapterObj[key] = chapter
    return chapterObj
     


    
    
data = {
    "url":"https://storage.googleapis.com/vega-demo-cdn/SampleAssignmentDetails-09-04-2021-20188.csv"
}
bulk_create_assignments(data)

