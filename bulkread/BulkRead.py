import time
from functools import cached_property

from utils import md5
from utils.EgnifyException import ValidationException


class HierarchiesBulkRead:
    def __init__(
        self, institute_details, csv_headers, csv_data, system_hierarchies, node_count
    ):
        """Bluk data read for hierarchies.

        :param institute_details: institute meta information.
        :param csv_headers: list of hierarchy level names.
        :param csv_data: list of lists containing hierarchy data.
        :param system_hierarchies: list of system hierarchies for validation
        """
        self.institute_details = institute_details
        self.institute_id = institute_details["_id"]
        self.csv_headers = csv_headers
        self.csv_data = csv_data
        self.system_hierarchies = [
            hierarchy["name"] for hierarchy in system_hierarchies
        ]
        self.node_count = node_count

    @cached_property
    def hierarchy_names(self):
        """Return list of hierarchies for institute."""
        return [hierarchy["name"] for hierarchy in self.institute_details["hierarchy"]]

    @cached_property
    def level_codes(self):
        """Return list of level codes for institute."""
        codes = len(self.institute_details["hierarchy"]) * [None]
        for hierarchy in self.institute_details["hierarchy"]:
            codes[hierarchy["level"] - 1] = hierarchy["code"]
        return codes

    @cached_property
    def hierarchy_tuples(self):
        """Prepares unique set of hierarchy tuples for csv data.

        ("Engineering"),
        ("Engineering", "Class 11"),
        ("Engineering", "Class 12"),
        ("Engineering", "Class 11", "AP"),
        ("Engineering", "Class 11", "AP", "GNT"),
        ("Medical"),
        ("Medical", "Class 11"),
        ("Medical", "Class 11", "TELANGANA"),
        ("Medical", "Class 11", "TELANGANA", "HYD")
        """
        s = set()
        for row in self.csv_data:
            hierarchy_tuple = ()
            for hierarchy in row:
                hierarchy = hierarchy.strip()
                hierarchy_tuple = hierarchy_tuple + (hierarchy,)
                s.add(hierarchy_tuple)
        return sorted(s, key=len)
    def remove_space(self):
        for row in self.csv_data:
            for hierarchy in row:
                hierarchy = hierarchy.strip()

    def validate(self):
        """Run validations on data, raises validation exceptions if any."""
        errors = []
        # print('system::hierarchies::', self.system_hierarchies)
        # headers mismatch validation
        if self.csv_headers != self.hierarchy_names:
            errors.append(ValidationException("E201", 1, None))

        # not empty cells exception
        for row_index, row in enumerate(self.csv_data, 1):
            for col_index, val in enumerate(row, 1):
                val=val.strip()
                if not val:
                    errors.append(ValidationException("E404", row_index, col_index))
                else:
                    # ensure first two column values are system hierarchies
                    if col_index < 3 and val not in self.system_hierarchies:
                        print(f"{str(val)}::{row_index}::{col_index}")
                        errors.append(ValidationException("E403", row_index, col_index))

        return errors

    def create_hierarchies(self):
        """Creates institute hierarchies docs."""
        docs = []
        for hierarchy_tuple in self.hierarchy_tuples:
            # sample hierarchy_tuple ("Engineering", "Class 11", "AP", "GNT").
            leaf_node = len(hierarchy_tuple) == len(self.hierarchy_names)
            # we are using level code because name of level is editable.
            level_code = self.level_codes[len(hierarchy_tuple) - 1]

            *prefix_list, hierarchy_name = [self.institute_id] + list(hierarchy_tuple)
            parent = "".join(prefix_list)
            current = parent + hierarchy_name

            docs.append(
                {
                    "isLeafNode": leaf_node,
                    "instituteId": self.institute_id,
                    "instituteLevelCode": level_code,
                    "hierarchyName": hierarchy_name,
                    "_id": md5(current),
                    "parentCode": md5(parent),
                    "level": len(hierarchy_tuple),
                    "hierarchyTuple": list(hierarchy_tuple),
                }
            )
        for i, doc in enumerate(docs, 1):
            doc["viewId"] = str(10000 + self.node_count + i)
        return docs


class StudentsBulkRead:
    def __init__(self, csv_headers, csv_data, institute_details, system_hierarchies):
        """Bulk students data read ."""
        self.csv_headers = csv_headers
        self.csv_data = csv_data
        self.institute_details = institute_details
        self.institute_id = institute_details["_id"]
        self.system_hierarchies = [
            hierarchy["name"] for hierarchy in system_hierarchies
        ]

    @cached_property
    def hierarchy_names(self):
        """Return list of hierarchies for institute."""
        return [hierarchy["name"] for hierarchy in self.institute_details["hierarchy"]]

    def validate(self):
        """Run validations on data, raises validation exceptions if any."""
        errors = []
        seen = {}
        for row_index, row in enumerate(self.csv_data, 1):
            (
                student_id,
                _,
                _,
                _,
                _,
                _,
                _,
                _,
                *hierarchies,
            ) = row
            if student_id in seen:
                errors.append(
                    ValidationException(
                        "E403", f"{student_id} at row {row_index}", None
                    )
                )
            seen[student_id] = True
            if hierarchies[0] not in self.system_hierarchies:
                errors.append(ValidationException("E403", row_index, 9))
            if hierarchies[1] not in self.system_hierarchies:
                errors.append(ValidationException("E403", row_index, 10))

        return errors

    def create_students(self):
        docs = []
        index =1
        for row in self.csv_data:
            (
                student_id,
                name,
                father,
                phone,
                email,
                dob,
                gender,
                category,
                *hierarchies,
            ) = row
            if not email:
                email = f"{student_id}@getranks.in".lower()
            # print(f"{email} ::{index} ")
            student_id = student_id.upper()
            hierarchies = [self.institute_id] + hierarchies
            parent_hierarchy = md5("".join(hierarchies))
            docs.append(
                {
                    "_id": "_".join([self.institute_id, student_id]),
                    "dob": dob,
                    "email": email,
                    "phone": phone,
                    "gender": gender,
                    "fatherName": father,
                    "category": category,
                    "studentId": student_id,
                    "accessTag": {"hierarchy": parent_hierarchy},
                    "studentName": name,
                    "instituteId": self.institute_id,
                    "egnifyId": "_".join([self.institute_id, student_id]),
                }
            )
            index = index +1
        return docs


class StudentsBulkReadV2:
    def __init__(self, data, headers, institute_id):
        self.data = data
        self.institute_id = institute_id
        self.headers = headers

    @property
    def fields(self):
        return {
            "dob": "DOB",
            "email": "Email",
            "phone": "Phone",
            "gender": "Gender",
            "fatherName": "Father Name",
            "category": "Category",
            "studentId": "Student Id",
            "studentName": "Student Name",
        }

    def validate(self):
        errors = []
        file_headers = [
            "Student Id",
            "Student Name",
            "Father Name",
            "Phone",
            "Email",
            "Gender",
            "DOB",
            "Category",
            "Hierarchy Id",
        ]
        if self.headers != file_headers:
            errors.append(
                "File header mismatch. Please download sample format and upload"
            )
            return errors

        seen = {}
        for row_index, row in enumerate(self.data, 1):
            student_id = row["Student Id"]
            if student_id in seen:
                errors.append(f"Duplicate Student Id : {student_id} at row {row_index}")
            seen[student_id] = True
            if not student_id:
                errors.append("Student Id is required")
            if not row["Student Name"]:
                errors.append("Student Name is required")
            if not row["Hierarchy Id"]:
                errors.append("Hierarchy Id is required")

        return errors

    def read_students(self):
        docs = []
        for row in self.data:
            doc = {}
            for field, header in self.fields.items():
                doc[field] = row[header]
            doc["studentId"] = doc["studentId"].upper()
            doc["accessTag"] = {"hierarchy": row.get("Hierarchy Id")}
            doc["instituteId"] = self.institute_id
            doc["_id"] = "_".join([doc["instituteId"], doc["studentId"]])
            doc["egnifyId"] = "_".join([doc["instituteId"], doc["studentId"]])
            if not doc["email"]:
                doc["email"] = f"{doc['studentId']}@getranks.in".lower()
            docs.append(doc)
        return docs





class AssignmentBulkRead:
    def __init__(self,csv_headers,data,classData,subjectData,textbookData,chaptersData):
        """
            param: csv_headers: headers of csv file
            param: data: csv data
        """
        self.classData = classData
        self.subjectData = subjectData
        self.textbookData = textbookData
        self.chaptersData = chaptersData
        self.data = data
        self.csv_headers = csv_headers
    
    def validate(self):
        
        """Run validations on data, raises validation exceptions if any."""
        errors = []
        file_headers =[
            "NAME",
            "DESCRIPTION",
            "MARKS",
            "START DATE",
            "DUE DATE",
            "PUBLISH TO STUDENT",
            "CLASS",
            "ORIENTATION",
            "BRANCH",
            "SUBJECT",
            "TEXTBOOK",
            "CHAPTER"
        ]
        if  self.csv_headers!=file_headers :
            errors.append(
                "File header mismatch. Please download sample format and upload"
            )
            return errors
        for row_index, row in enumerate(self.data, 1):
            
            klass = row['CLASS']
            branch = f"{row['BRANCH']}"
            subject_key = f"{klass}-{row['SUBJECT']}"
            textbook_key = f"{subject_key}-{row['TEXTBOOK']}"
            chapter_key = f"{row['TEXTBOOK']}-{row['CHAPTER']}"
            orientation = row['ORIENTATION']
            if not row['NAME']:
                errors.append(f"Missing NAME :  at row {row_index}")
            if not row['DESCRIPTION']:
                errors.append(f"Missing Description: at row {row_index}")
            if not klass or klass not in self.classData:
                errors.append(f"Invalid CLASS: at row {row_index}")
            if not row['SUBJECT'] or subject_key not in self.subjectData:
                errors.append(f"Invalid SUBJECT: at row {row_index}")
            if not row['TEXTBOOK'] or textbook_key not in self.textbookData:
                errors.append(f"Invalid TEXTBOOK: at row {row_index}")
            if not row['CHAPTER'] or chapter_key not in self.chaptersData:
                errors.append(f"Invalid CHAPTER: at row {row_index}") 
                
            if orientation and orientation not in self.textbookData[textbook_key]['orientations']:
                errors.append(f"Invalid ORIENTATION: at row {row_index}") 
            if branch and branch not in self.textbookData[textbook_key]['branches']:
                errors.append(f"Invalid BRANCH: at row {row_index}") 
        return errors
        
    
    def create_assignments(self):
        """
        
        """