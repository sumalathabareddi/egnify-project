import csv
import json
import time
from io import BytesIO, StringIO
from collections import defaultdict

import pdfkit
import utils.decorators as decorators
from flask import Blueprint, Markup, current_app, render_template, request
from utils import deep_defaultdict
from utils.StorageHelper import StorageHelper

from db.HydraArangoDb import HydraArangoDb
from db.HydraMongoDb import HydraMongoDb
from rq import Queue
import redis



from app import create_app

application = create_app()

# mongo_db = HydraMongoDb(application.config['MONGO_URI'])
mongo_db = HydraMongoDb("mongodb://root:5eqcMZQCPXtG8MvU@localhost:27018/?authenticationDatabase=admin")
arango_db = HydraArangoDb(
    application.config["ARANGODB_URI"],
    application.config["ARANGODB_USERNAME"],
    application.config["ARANGODB_PASSWORD"],
)
settings_database = application.config["SETTINGS_DATABASE"]
tenant_registry = application.config["TENANT_REGISTRY_DATABASE"]
question_db = application.config["QUESTION_BANK_DB"]

# hostname = application.config["HOST_NAME"]
hostname = 'getranks.in'
redis_connection = redis.Redis.from_url(application.config["RQ_REDIS_URL"])
queue = Queue(application.config["RQ_QUEUES"], connection=redis_connection)
storage_secrets = {}
storage_secrets["GCP_CREDENTIALS_FILE"] = application.config[
        "GCP_CREDENTIALS_FILE"
    ]    


def download_test_docs(institute_id, test_id, download_type):
    # Fetch test details for rendering paper header meta data
    print(f'db::{question_db}')
    test_details = mongo_db.find_one(question_db, "tests", query={"testId": test_id})
    # Fetch institute details for logo and other institute related data
    institute_details = mongo_db.find_one(
        settings_database, "institutes", query={"_id": institute_id},
    )
    # Use questionPaperId for questions
    question_paper_id = test_details.get("questionPaperId")
    # Fetch marking schema for instructions.
    marking_schema_id = test_details.get("markingSchemaId")
    marking_schema = mongo_db.find_one(
        settings_database, "markingschemas", {"_id": marking_schema_id},
    )
    questions = get_questions_data(question_paper_id, marking_schema, test_details)
    show_instructions = download_type == "paper"
    show_solutions = download_type == "solution"
    pdf_type = f" ({download_type.title()})"
    is_parsed = test_details["questionNumberFormat"]
    # Decide which template to render
    template = (
        "question_paper_key.html" if download_type == "key" else "paper_new_layout.html"
    )
    # Test instructions
    instructions = marking_schema.get("instructionsMarkup")
    # Render the template and use template string for pdf.
    paper_html = render_template(
        template,
        questions=questions,
        institute_details=institute_details,
        instructions=instructions,
        test_details=test_details,
        show_instructions=show_instructions,
        show_solutions=show_solutions,
        marking_schema=marking_schema,
        is_parsed = is_parsed,
    )
    # current_app.logger.info("Completed rendering html for question paper pdf")
    paper_string = BytesIO()
    try:
        options = {"--footer-right": "Page [page] of [topage]"}
        config = pdfkit.configuration(wkhtmltopdf="/usr/local/bin/wkhtmltopdf")
        paper_string.write(
            pdfkit.from_string(
                paper_html, output_path=False, configuration=config, options=options
            )
        )
    except OSError as ose:
        print(f'osError{ose}');
        # current_app.logger.info("Received wkhtmlpdf OSError", ose)
    attachment_filename = "".join(
        [
            institute_details["instituteName"],
            "-",
            test_details["testName"],
            pdf_type,
            ".pdf",
        ]
    )
    paper_string.seek(0)
    bucket_names = {
        "GCP": application.config["CLOUD_STORAGE_BUCKET"],
        "MINIO": application.config["MINIO_STORAGE_BUCKET"],
    }
    storage_provider = application.config["STORAGE_PROVIDER"]
    storage_helper = StorageHelper(storage_provider, storage_secrets)
    data = {
        "file_ext": ".pdf",
        "file_name": attachment_filename,
        "data": bytes(paper_string.getvalue()),
        "bucket_name": bucket_names[storage_provider],
        "content_type": "application/pdf",
        "content_disposition": "inline"
    }
    url = storage_helper.upload(data, get_public_url=True)
    if download_type == "paper":
        update_data = {"questionPaperUrl": url}
        mongo_db.find_one_and_upsert(question_db, "tests", {"testId": test_id}, update_data)
    print(f'{url}')
    return {"download_url": url}, 200, {}


def get_questions_data(question_paper_id, marking_schema, test_details):
    """List of question for question paper
    """
    questions_data = list(
        mongo_db.connection[question_db]
        .get_collection("questions")
        .aggregate(
            [
                {"$match": {"questionPaperId": {"$in": question_paper_id}}},
                {
                    "$lookup": {
                        "from": "questions-list",
                        "localField": "questionNumberId",
                        "foreignField": "_id",
                        "as": "question",
                    }
                },
                {"$unwind": {"path": "$question"}},
            ]
        )
    )
    # current_app.logger.info("Fetched questions data")
    print('Fetched questions data');

    # Need to follow the order from test
    subjects_order = [item["subject"] for item in test_details["subjects"]]
    # Group data by subject and question type
    grouped_questions_data = deep_defaultdict(list, depth=2)
    questions_data.sort(key=lambda _: int(_["qno"]))
    for each_question in questions_data:
        subject = each_question["question"]["subject"]
        question_type = each_question["question"]["questionType"]
        grouped_questions_data[subject][question_type].append(each_question["question"])

    sections_data = deep_defaultdict(dict, 2)
    subjects_map = {
        subject["subjectCode"]: subject["subject"]
        for subject in test_details["subjects"]
    }
    for subject in marking_schema["subjects"]:
        for section in subject["marks"]:
            subject_code = subject["subject"]
            subject_name = subjects_map[subject_code]
            question_type = section["egnifyQuestionType"]
            sections_data[subject_name][question_type] = section
            if question_type == "Integer type":
                sections_data[subject_name]["Numeric type"] = section
            elif question_type == "Numeric type":
                sections_data[subject_name]["Integer type"] = section

    data = []
    for subject in subjects_order:
        subject_questions = grouped_questions_data[subject]
        subject_sections = []
        for index, (question_type, question_type_data) in enumerate(
            subject_questions.items(), 65
        ):
            section_data = sections_data[subject][question_type]
            markup = render_template(
                "section_template.html", section_alpha=chr(index), **section_data
            )
            subject_sections.append((markup, question_type_data))

        data.append((subject, subject_sections))

    return data


download_test_docs("5f64c078f9ad6613d9a53a02","1606972685488","solution")