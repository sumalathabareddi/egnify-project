from utils import deep_defaultdict
from functools import lru_cache

from flask import current_app

from stackmachine import Machine
from stackmachine.mark_analysis import Mark
from stackmachine.hierarchy_analysis import HierarchyAnalysis


class StudentsMeta:
    def __init__(self, docs):
        self.docs = docs
        self.student_meta = deep_defaultdict(dict, 3)

    def meta(self):
        db = current_app.config["SETTINGS_DATABASE"]
        egnify_ids = []
        for doc in self.docs:
            egnify_id = doc["egnifyId"]
            # Make a list of student ids for settings query
            egnify_ids.append(egnify_id)
            hierarchy_ids = doc["hierarchyLevels"]
            # Get the list of hierarchy names
            for hierarchy_id in hierarchy_ids:
                node = hierarchy_node(hierarchy_id)
                # Prepare map of level_code and level_name
                hierarchy_name = node["hierarchyName"]
                institute_level_code = node["instituteLevelCode"]
                self.student_meta[egnify_id]["meta"][
                    institute_level_code
                ] = hierarchy_name

        # Query for student settings data
        query = {"egnifyId": {"$in": egnify_ids}}
        students_docs = current_app.mongo_db.find(
            db,
            "studentInfo",
            query,
            projection={"studentName": 1, "egnifyId": 1, "studentId": 1},
        )
        for doc in students_docs:
            egnify_id = doc["egnifyId"]
            self.student_meta[egnify_id].update(doc)
        return self.student_meta


class HierarchyMeta:
    def __init__(self, docs):
        self.docs = docs
        self.hierarchy_meta = deep_defaultdict(dict, 3)

    def meta(self):
        for doc in self.docs:
            hierarchy = doc["hierarchy"]
            node = hierarchy_node(hierarchy)
            name = node["hierarchyName"]
            level_code = node["instituteLevelCode"]
            self.hierarchy_meta[hierarchy]["meta"][level_code] = name
            self.hierarchy_meta[hierarchy]["name"] = name
        return self.hierarchy_meta

    def hierarchy_name(self, hierarchy):
        return self.hierarchy_meta[hierarchy]["name"]


class ReportsHelper:
    def __init__(self, data, fields, precision=2):
        self.data = data
        self.fields = fields
        self.precision = precision

    def prepare_student_meta(self):
        sm = StudentsMeta(self.data)
        students_meta = sm.meta()
        for doc in self.data:
            egnify_id = doc["egnifyId"]
            meta = students_meta[egnify_id]
            doc.update(meta)

    def run_instructions(self):
        data = []
        machine = Machine(self.precision)
        for doc in self.data:
            new_doc = {}
            machine.active_doc = doc
            for header, iset in self.fields.items():
                # evaluate instruction set
                machine.evaluate(iset)
                val = machine.pop()
                new_doc[header] = val
            data.append(new_doc)
        return data

    def hierarchical_data_meta(self, doc_type, docs):
        hm = HierarchyMeta(docs)
        h_meta = hm.meta()
        # Group docs by the hierarchy name as there can be more than one doc
        # for same hierarchy.
        group_by_name = deep_defaultdict(list)
        for doc in docs:
            hierarchy_id = doc["hierarchy"]
            name = hm.hierarchy_name(hierarchy_id)
            group_by_name[name].append(doc)
        # Combine the docs of same hierarchy
        combined_docs = []
        for name, docs_by_name in group_by_name.items():
            new_doc = {}
            hierarchy_id = docs_by_name[0]["hierarchy"]
            new_doc.update(h_meta[hierarchy_id])
            if doc_type == "mark":
                # empty start object for mark analysis
                start = Mark()
                mark_docs = [Mark(doc["markAnalysis"]) for doc in docs_by_name]
                # combine the list of mark analysis objects
                added_doc = sum(mark_docs, start)
                new_doc["mark"] = added_doc
            elif doc_type == "hierarchy":
                # empty start object for hierarchical analysis
                start = HierarchyAnalysis()
                # make list of hierarchy analysis docs for adding up
                hierarchy_docs = [
                    HierarchyAnalysis(
                        x["numberOfStudents"], x["topicAnalysis"], x["questionMap"]
                    )
                    for x in docs_by_name
                ]
                # combine the list of hierarchy analysis objects
                added_doc = sum(hierarchy_docs, start)
                new_doc.update(added_doc.__dict__)
            elif doc_type == "both":
                # empty start object for mark analysis
                mark_start = Mark()
                # empty start object for hierarchical analysis
                hierarchy_start = HierarchyAnalysis()
                # make list of docs for adding up later
                mark_docs = [Mark(doc["markAnalysis"]) for doc in docs_by_name]
                # add the list of mark analysis docs
                mark_docs_addedd = sum(mark_docs, mark_start)
                # make list of hierarchy analysis docs for adding up
                h_docs = [
                    HierarchyAnalysis(
                        x["numberOfStudents"], x["topicAnalysis"], x["questionMap"]
                    )
                    for x in docs_by_name
                ]
                # combine the list of hierarchy analysis docs
                h_docs_added = sum(h_docs, hierarchy_start)
                # create new doc with both mark and hierarchy analysis
                new_doc["mark"] = mark_docs_addedd
                new_doc.update(h_docs_added.__dict__)
            # Add combined docs to list of data for querying
            combined_docs.append(new_doc)
        self.data = combined_docs

    def prepare_snapshot_data(self, docs, test_info):
        for doc in docs:
            mode = doc.get("mode")
            sync_status = doc.get("syncStatus", False)
            student_status = None
            if mode == "online":
                if sync_status is True:
                    student_status = "Completed"
                else:
                    student_status = "Present"
            else:
                student_status = "Absent"
            doc["status"] = student_status
            doc["testStartTime"] = test_info["startTime"]
            doc["testEndTime"] = test_info["endTime"]
            doc["studentStartTime"] = doc.get("startedAt")
            doc["studentEndTime"] = doc.get("endTime")
            if "version" in doc:
                print(doc["version"])
        self.data = docs
        self.prepare_student_meta()


@lru_cache(maxsize=None)
def hierarchy_node(hierarchy_id):
    node = current_app.arango_db.get_node(hierarchy_id)
    return node
