import jwt
import json
import requests

from datetime import datetime
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import as_completed


def get_paired_timing(in_time, out_time):
    """

    :param in_time:
    :param out_time:
    :return:
    """
    return (out_time - in_time).total_seconds()


def isBetween(date, start, end):
    """

    :param date:
    :param start:
    :param end:
    :return:
    """
    if date >= start and date <= end:
        return True
    else:
        return False


def convert_data(data, student, egnify_test_id):
    """

    :param data:
    :param student:
    :param egnify_test_id:
    :return:
    """
    response = {}

    for entry in data:
        qno = entry["studentOnlineExamResultsDTO"]["qno"]
        startDate = entry["studentOnlineExamResultsDTO"]["startDate"]
        endDate = entry["studentOnlineExamResultsDTO"]["endDate"]
        questionId = entry["studentOnlineExamResultsDTO"]["questionId"]
        key = "Q{}".format(qno)
        response[key] = {}
        response[key]["questionId"] = questionId
        response[key]["studentId"] = student
        response[key]["testId"] = egnify_test_id
        if entry["questionTimings"]:
            entry["questionTimings"].sort(key=lambda item: item["id"])
            in_time = None
            out_time = None
            last_attempted_time = None
            total_time_list = []
            paired = True
            for timing in entry["questionTimings"]:
                time_stamp = datetime.strptime(
                    timing["dateandtime"], "%Y-%m-%d %H:%M:%S.%f"
                )
                start_date = datetime.strptime(startDate, "%Y-%m-%d %H:%M:%S.%f")
                end_date = datetime.strptime(endDate, "%Y-%m-%d %H:%M:%S.%f")
                if isBetween(time_stamp, start_date, end_date):
                    if timing["status"] == "IN":
                        paired = False
                        in_time = time_stamp
                    elif timing["status"] == "OUT" and not paired:
                        paired = True
                        out_time = time_stamp
                    elif timing["status"] == "OUT" and paired:
                        paired = False
                    if paired:
                        last_attempted_time = out_time
                        total_interval_time = get_paired_timing(in_time, out_time)
                        total_time_list.append(total_interval_time)
        response[key]["time"] = total_time_list
        totalTimeSpent = sum(int(time) for time in total_time_list)
        visitCount = len(total_time_list)
        response[key]["totalTimeSpent"] = totalTimeSpent
        response[key]["visitCount"] = visitCount
        timestamp = (last_attempted_time - start_date).total_seconds()
        response[key]["datetime"] = timestamp
    return response


def get_behaviour_student_wise_from_api(
    student_behaviour_api, student, test_id, egnify_test_id
):
    """

    :param student_behaviour_api:
    :param student:
    :param test_id:
    :param egnify_test_id:
    :return:
    """
    r = requests.post(
        student_behaviour_api, data={"scheduleId": test_id, "student": student}
    )
    token = r.json()
    exam_attempt = jwt.decode(token["token"], "scaitsqb")
    data = json.loads(json.loads(exam_attempt["exams"])["response"])
    return convert_data(data, student, egnify_test_id)


def get_behaviour_student_wise(
    student_behaviour_api, student_list, test_id, egnify_test_id
):
    """

    :param student_behaviour_api:
    :param student_list:
    :param test_id:
    :param egnify_test_id:
    :return:
    """
    with ThreadPoolExecutor(max_workers=5) as executor:
        future_to_id = {
            executor.submit(
                get_behaviour_student_wise_from_api,
                student_behaviour_api,
                student,
                test_id,
                egnify_test_id,
            ): student
            for student in student_list
        }
        results = []
        for future in as_completed(future_to_id):
            update_obj = {}
            id = future_to_id[future]
            update_obj["testId"] = egnify_test_id
            update_obj["studentId"] = id
            update_obj["_id"] = "_".join((egnify_test_id, id))
            try:
                update_obj["behaviourData"] = future.result()
            except Exception as exc:
                update_obj["behaviourData"] = {}
                print("%r generated an exception: %s" % (id, exc))
            results.append(update_obj)
        return results


def get_intersected_list(student_list_api, test_id, student_list):
    """

    :param student_list_api:
    :param test_id:
    :param student_list:
    :return:
    """
    resp = requests.post(student_list_api, data={"scheduleId": test_id})
    token = resp.json()
    exam_attempt = jwt.decode(token["token"], "scaitsqb")
    response_code = json.loads(exam_attempt["exams"])["responseCode"]
    if response_code:
        response = json.loads(json.loads(exam_attempt["exams"])["response"])
        api_student_list = response["students"]
        intersected_list = list(set(api_student_list) & set(student_list))
        return {"data": intersected_list, "error": {}}
    else:
        return {"data": [], "error": {"ERR404": "No Data for Queried TestId"}}


def get_behaviour(
    student_list_api,
    student_behaviour_api,
    egnify_test_id,
    provided_test_id,
    online_student_list,
):
    """

    :param student_list_api:
    :param student_behaviour_api:
    :param egnify_test_id:
    :param provided_test_id:
    :param online_student_list:
    :return:
    """
    print("Queried for testId: ", provided_test_id)
    result = get_intersected_list(
        student_list_api, provided_test_id, online_student_list
    )
    if result["data"]:
        intersected_list = result["data"]
        return get_behaviour_student_wise(
            student_behaviour_api, intersected_list, provided_test_id, egnify_test_id
        )
    else:
        return result["error"]
