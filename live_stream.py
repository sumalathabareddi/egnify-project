import rq
import redis
from rq import Queue
from app import create_app

import live.medialive as ml
import live.mediapackage as mp

application = create_app()

redis_connection = redis.Redis.from_url(application.config["RQ_REDIS_URL"])
queue = Queue(application.config["RQ_QUEUES"], connection=redis_connection)

"""
Policy needed for the role

{
    "Statement": [
        {
            "Action": [
                "ssm:DescribeParameters",
                "ssm:GetParameter",
                "ssm:GetParameters",
                "ssm:PutParameter"
            ],
            "Resource": [
                "arn:aws:ssm:{REGION}:{ACCOUNT_ID}:parameter/*"
            ],
            "Effect": "Allow"
        },
        {
            "Action": [
                "mediaconnect:ManagedDescribeFlow",
                "mediaconnect:ManagedAddOutput",
                "mediaconnect:ManagedRemoveOutput"
            ],
            "Resource": [
                "arn:aws:mediaconnect:{REGION}:{ACCOUNT_ID}:*"
            ],
            "Effect": "Allow"
        }
    ]
}

Trust relationship : - "medialive.amazonaws.com"

Refer config/media_role.yaml for cloudformation template
"""


def create_stream(data):
    """
    :param data:
    """
    channel_id = data["channelId"]
    current_job = rq.get_current_job()
    resolution = data["resolution"]
    endpoint = data["endpoint"]

    Profile_Path = None
    if resolution == "720":
        Profile_Path = "encoding-profiles/medialive-720p.json"
    elif resolution == "1080":
        Profile_Path = "encoding-profiles/medialive-1080p.json"
    else:
        Profile_Path = "encoding-profiles/medialive-540p.json"

    current_job.meta["result"] = "Creating Media Package"
    current_job.save_meta()
    media_package_channel_data = mp.create_channel({"ChannelId": channel_id})
    # We can use other as well for now endpoint is hardcoded
    current_job.meta["result"] = "Creating Endpoint"
    current_job.save_meta()
    hls_end_point = mp.create_endpoint(
        {"EndPoint": endpoint, "ChannelId": media_package_channel_data["ChannelId"]}
    )
    current_job.meta["result"] = "Creating Live Stream Input"
    current_job.save_meta()
    media_live_input_data = ml.create_input(
        {
            "StreamName": data["name"],
            "Type": "RTMP_PUSH",
            "SecurityGroupId": data["securityGroupId"],
            "RoleArn": data["roleArn"],
        }
    )
    current_job.meta["result"] = "Creating Live Stream Channel"
    current_job.save_meta()
    media_live_channel_data = ml.create_channel(
        {
            "Name": data["name"],
            "Resolution": resolution,
            "Codec": "AVC",
            "Role": data["roleArn"],
            "InputId": media_live_input_data["Id"],
            "Type": "RTMP_PUSH",
            "MediaPackagePriUrl": media_package_channel_data["PrimaryUrl"],
            "MediaPackagePriUser": media_package_channel_data["PrimaryUser"],
            "MediaPackageSecUrl": media_package_channel_data["SecondaryUrl"],
            "MediaPackageSecUser": media_package_channel_data["SecondaryUser"],
            "Profile_Path": Profile_Path,
        }
    )
    current_job.meta["result"] = "Created Live Channel"
    current_job.meta["data"] = {
        "media_package_channel_data": media_package_channel_data,
        "hls_end_point": hls_end_point,
        "media_live_input_data": media_live_input_data,
        "media_live_channel_data": media_live_channel_data,
    }
    current_job.save_meta()


def start_channel(data):
    current_job = rq.get_current_job()
    current_job.meta["result"] = "Starting Media Live Channel"
    current_job.save_meta()
    ml.start_channel(data)
    current_job.meta["result"] = "Media Live Channel Started."
    current_job.save()


def delete_channel(data):
    current_job = rq.get_current_job()
    current_job.meta["result"] = "Deleting Media Live Channel"
    current_job.save_meta()
    ml.delete_channel(data["channelId"])
    current_job.meta["result"] = "Media Live Channel Deleted."
    current_job.save()


def harvest_job(data):
    current_job = rq.get_current_job()
    current_job.meta["result"] = "Harvest Job Started."
    current_job.save_meta()
    res = mp.harvest_job(data)
    current_job.meta["result"] = "Harvest Job Completed."
    current_job.meta["data"] = res
    current_job.save()


def clean_channel(job, exc_type, exc_value, traceback):
    """

    :param job:
    :param exc_type:
    :param exc_value:
    :param traceback:
    """
    # Need code to delete the channel when there is an exception
    (method_args,) = job.args
    channel_id = method_args["channelId"]
