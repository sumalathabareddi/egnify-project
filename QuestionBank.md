## questions-list
```javascript
_id: Mongo unique id
questionId: Unique id from motion side when question is from motion side otherwise generate by us
question: Question text
answer: Answer to the question in user understandable format
difficulty: Difficulty of the question, one of [Easy, Medium, Hard]
duplicate: If set to true it is duplicate question i.e There is another question record with same data
hide: Used to block the question
hint: Hint for the solving question
instituteId: If set this represents the institute which added the question to DB
isExercise: If set this question is reserved to be used in exercises
isFRM, isSheet: Used for question prioritization. Specific to motion
isPractice: Not used anymore. Originally set to filter questions for practice
key: Key for the question used for eval
maskId: Not if they want this anymore. Originally asked to show on front end
optionHash: md5 of options
options: List of options for questions
parsed: If set to true this indicated this is added to db via Paper upload
qdb: Used to distinguish b/w our and motion data
questionHash: md5 of question
questionNumberId: Unique id given to question
questionType: Type of the question. example Single answer type, Numeric type etc..
questionTypeMetaData: Metadata of question
solution: Solution of the question
subject: Subject of the question
topic: Topic of the question
subTopic: Sub Topic of the question
usageMetrics: Generated usage metric data
```