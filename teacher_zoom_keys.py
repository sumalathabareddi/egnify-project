import csv
from app import create_app
application = create_app()
from bulkread.BulkRead import ZoomKeysBulkread
from db.HydraMongoDb import HydraMongoDb

mongo_db = HydraMongoDb(application.config["MONGO_URI"])
tenant_registry = application.config["TENANT_REGISTRY_DATABASE"]


with open('./teacher_zoom_keys_07_07_2021.csv') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=",")
        headers = csv_reader.fieldnames
        csv_data = list(csv_reader)
        zbr = ZoomKeysBulkread(csv_data,headers)
        errors = zbr.validate()
        if errors:
            print(errors)
        else:
            docs = []
            for row in csv_data:
                doc = {
                    'email': row['email'],
                    'zoom': {'ZOOM_API_KEY':row['ZoomApiKey'],
                             'ZOOM_SECRET_KEY': row['ZoomSecretKey'],
                             'email':row['email'],
                             'ZOOM_API_URL':'https://api.zoom.us/v2'
                            }
                }
                docs.append(doc)
            
            res = mongo_db._upsert_many(
                connection=None,
                db_name=tenant_registry,
                collection_name="users",
                obj=docs,
                id_field="email",
                upsert=True,
            )
            print(res)
        
