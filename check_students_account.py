import base64
import csv
from datetime import datetime
from hashlib import pbkdf2_hmac
from os import urandom
from random import randint
from flask import helpers

import redis
from db.HydraArangoDb import HydraArangoDb
from db.HydraMongoDb import HydraMongoDb
from rq import Queue




from api.models.students import StudentSchema
from app import create_app
from bulkread.BulkRead import StudentsBulkRead, StudentsBulkReadV2

application = create_app()

mongo_db = HydraMongoDb(application.config['MONGO_URI'])
# mongo_db = HydraMongoDb("mongodb://root:5eqcMZQCPXtG8MvU@localhost:27018/?authenticationDatabase=admin")
arango_db = HydraArangoDb(
    application.config["ARANGODB_URI"],
    application.config["ARANGODB_USERNAME"],
    application.config["ARANGODB_PASSWORD"],
)
settings_database = application.config["SETTINGS_DATABASE"]
tenant_registry = application.config["TENANT_REGISTRY_DATABASE"]
# hostname = application.config["HOST_NAME"]
hostname = 'getranks.in'
redis_connection = redis.Redis.from_url(application.config["RQ_REDIS_URL"])
queue = Queue(application.config["RQ_QUEUES"], connection=redis_connection)

with open("./students.csv") as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=",")
    # csv_headers = next(csv_reader)
    # print(f'{csv_headers}')
    csv_data = list(csv_reader)
    for student in csv_data:
        query = {
                "studentId":student['Student Id']
            }
        doc = mongo_db.find_one(tenant_registry,"users",query)
        hashed_password = doc["password"]
        salt = doc['salt']
        salt = base64.b64decode(salt)
        password = student['Password']
        byte_password = password.encode()
        pass_hash = pbkdf2_hmac("sha1", byte_password, salt, 10000, 64)
        base64_pass = base64.b64encode(pass_hash)
        print(f'{student["Student Id"]}::::{len(doc)}')
        print(hashed_password == base64_pass.decode())