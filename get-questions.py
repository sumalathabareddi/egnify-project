from db.HydraArangoDb import HydraArangoDb
from db.HydraMongoDb import HydraMongoDb
from bson.objectid import ObjectId
import csv


from app import create_app

import random


application = create_app()

# mongo_db = HydraMongoDb(application.config['MONGO_URI'])
mongo_db = HydraMongoDb("mongodb://root:5eqcMZQCPXtG8MvU@localhost:27018/?authenticationDatabase=admin")
arango_db = HydraArangoDb(
    application.config["ARANGODB_URI"],
    application.config["ARANGODB_USERNAME"],
    application.config["ARANGODB_PASSWORD"],
)
settings_database = application.config["SETTINGS_DATABASE"]
tenant_registry = application.config["TENANT_REGISTRY_DATABASE"]
test_database = application.config["TEST_DATABASE"]
question_db = application.config["QUESTION_BANK_DB"]


def get_questions_from_mongo(
    query_list, difficulty, number_of_questions, subject, question_type
):
    """

    :param query_list:
    :param difficulty:
    :param number_of_questions:
    :param subject:
    :param question_type:
    :return:
    """
    duplicate_query_list = query_list.copy()
    duplicate_query_list.append({"parsed": {"$ne": True}})
    duplicate_query_list.append({"hide": {"$ne": True}})
    # duplicate_query_list.append({"difficulty": difficulty})
    duplicate_query_list.append({"subject": subject})
    duplicate_query_list.append({"questionType": question_type})
    questions = (
        mongo_db.connection[question_db]
        .get_collection("questions-list")
        .aggregate(
            [
                {"$match": {"$and": duplicate_query_list}},
                {"$sample": {"size": number_of_questions}},
                {"$project": {"questionNumberId": 1, "subject": 1, "topic": 1}},
            ]
        )
    )
    return questions

def print_questions(marking_schema_id,topic,test_id):
    questions_list = []
    question_paper_id = f'QP{test_id}'
    and_query = []
    if topic:
        and_query.append({"topic": {"$in": topic}})

    marking_schema = mongo_db.read_by_id(settings_database, "markingschemas",[marking_schema_id])[
        marking_schema_id
    ]
    for subject in marking_schema["subjects"]:
        for question_type in subject["marks"]:
            print(f'{question_type}')
            subject_object = mongo_db.read_by_id(settings_database, "subjecttaxonomies",[subject["subject"]])
            subject_name = subject_object[subject["subject"]]["name"]
            total_questions = get_questions_from_mongo(
                and_query,
                None,
                int(question_type["numberOfQuestions"]),
                subject_name,
                question_type["egnifyQuestionType"],
            )
            questions_to_return = list(total_questions)
            random.shuffle(questions_to_return)
            if len(questions_to_return) < question_type["numberOfQuestions"]:
                print(f"We have insufficient {question_type['egnifyQuestionType']} of questions for selected subject {subject_name}")
            start_index = question_type["start"]
            for index in range(
                start_index, start_index + question_type["numberOfQuestions"]
            ):
                q = questions_to_return[index - start_index]
                questions_list.append(
                    {
                        "_id": "_".join([str(index), question_paper_id]),
                        "questionPaperId": question_paper_id,
                        "qno": str(index),
                        "questionNumberId": q["questionNumberId"],
                    }
                )
    
    # print(f'{questions_list}')

topics= [
        "Carboxylic acid Chemical Properties"
    ]
# print_questions('5ec6846074aba862a1346601',topics,'1608021418909')

ques = [
            "045b0233f4a8122dbca394b326591144", 
            "02c5c0f507c554e0b4f0bf71cf9380b4", 
            "0493bf9a0f8952bb96155b33f7e2a897", 
            "01a5a51c179619bce2f32a99f2e29821", 
            "00b644351d26f008774a12424d71a4c3", 
            "0d8a859fd7500779e207f03ea033d9f3", 
            "0dcf803ac0bce7eb2307831e9c8bf9f2", 
            "0b670945939846a4621ae79a2c4ab718", 
            "0ff4790f6bd1f25fdec0323a89d44bc9", 
            "127e83781e7c903fb06ab191bef3400e", 
            "177a6950a45c9c37db03af568c62533a", 
            "1368341466499fd909ad663566afab21", 
            "1aef9a99e4905ef1cad95b1d91c06add", 
            "1450ca8bcf6bb41833c1a5ff38b22792", 
            "14d82338926f0b01da319b91aa53cc7b", 
            "1f1f01b4927a3bb43b71e96894aed0ed", 
            "1f4f62efa6528c419e4d19f64f43a515", 
            "1c88a2a16c50c2e3304c2a25a1684dad", 
            "23916a4bcdf7f9c97ab6c1be95bbbfa9", 
            "1f630a3ed4d38c47ae88ae46468d3e60", 
            "2786fcb4b26a49903c8b4230e55ffc8a", 
            "24e888b8e93b2c588c6ff1c3fa961a6a", 
            "271308c5788f7490fdff5784690b062f", 
            "2453f7ae50af4944db2107d589b076fb", 
            "26d42352a8dbc40e142d101c14b4cb5a", 
            "29799d6978c5eddd10e291ccc212f2b8", 
            "242a2fad9caede27acbc2cac6789fb00", 
            "2b2d4ca48a293d1f94825d5f93b084b8", 
            "2fbdf584bd9f291ccdba43ad053c2103", 
            "313288cf6c6bbb64661bdc9c13c9fa07", 
            "2e46714bb5275ff5ff6bb3d3decfa189", 
            "2ef5e9e64df3082757efeb42fabf46c2", 
            "30a49aab409ad59d23a40780663d2fef", 
            "3ae93a1d7f1e59824d2197a7ffde5208", 
            "37326b789eb0b8f8dc35f06ac3944a40", 
            "336b636bf4c5ef4aaa00d7958ca467c7", 
            "33fbd3498fef0823359610e2c0f9c510", 
            "33f07b733cfcdb36fb7fb55395fb1271", 
            "3f5ae1062729d03bc10e8e1b293d47d8", 
            "495bd9685ea98ffa65113124added57a"
      ]


print(len(ques))
def get_questions():
    query ={
        "_id":{"$in":ques}
    }
    # skip = 0
    # size = 180
    # kwargs = {
    #     "key": "_id",
    #     "skip": skip,
    #     "size": size,
    #     "sort_key": "qno",
    # }
    # test_status_data = mongo_db.get_results(
    #     test_database,
    #     "questions",
    #     {"userId": "cmFtYWxheG1hbmJvdGFueUBnZXRyYW5rcy5pbg==", "questionPaperId": "QP1609476911571"},
    #     **kwargs,
    # )
    doc = {
            "_id": "5d5e4252bd1ac5cd9a02b84a",
            "questionNumberId": "5d5e4252bd1ac5cd9a02b84a",
            "subject": "Chemistry",
            "questionType": "Single answer type",
            "topic": "topic5e18caf9addb226ad8a6a908",
            "subTopic": "5e18caf9addb226ad8a6a90b",
            "difficulty": "Easy",
            "order": 1
        }
    q_docs=[]
    docs = mongo_db.find(test_database,"questions-list",query)
    for doc in docs:
        new_doc ={
            "_id":str(ObjectId()),
            "questionNumberId":doc["_id"],
            "subject":doc["subject"],
            "questionType":doc["questionType"],
            "questionPaperId":"QP1612677484295",
            "topic":doc["topic"],
            "subTopic":doc["subTopic"],
            "difficulty":doc["difficulty"],
            "order":1,
            "userId":"cmFtYm90YW55MUBnZXRyYW5rcy5pbg=="
        }
        q_docs.append(new_doc)

    print(len(q_docs))
    # res = mongo_db.insert_many(test_database,"questions",q_docs)

get_questions()

def get_mismatch_questions():
    keys = [1,2,3,4]
    answers = ["A","B","C","D"]
    ques = []
    for key in keys:
        for ans in answers:
            if key != answers.index(ans)+1 :
                query = {
                    "key" : {"$in":[key]},
                    "anser": ans,
                    "questionType": "Single answer type"
                }
                docs = mongo_db.find(question_db,"questions-list",query)
                ques = ques+docs
    new_docs =[]
    for doc in ques:
        obj = {
            "questionNumberId": doc["questionNumberId"],
            "key": doc["key"][0],
            "answer": doc["answer"],
            "questionType": doc["questionType"],
            "subject":doc["subject"]
        }
        new_docs.append(obj)

    with open('./questions.csv', mode='w') as csv_file:
        fieldnames = ['questionNumberId','key','answer','questiontype','subject']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(new_docs)

# get_mismatch_questions()

def hide_questions():
    with open('./questions_prod.csv') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=",")
        # csv_headers = next(csv_reader)
        # print(f'{csv_headers}')
        csv_data = list(csv_reader)
        qids = [f'{question["questionNumberId"]}' for question in csv_data]
        print(f'{len(qids)}')
        for qid in qids:
            query = {"_id": qid}
            doc = mongo_db.find_one_and_upsert(question_db,"questions-list",query,{"hide":True},upsert=False)
            print(doc)
        
# hide_questions()

def get_keys(question_paper_id):
    lookup_query = {
        "from": "questions-list",
        "localField": "questionN",
        "foreignField": "_id",
        "as": "test",
    }
    match_query = {"questionPaperId":question_paper_id}
    questions = list(
        mongo_db.connection[question_db]
        .get_collection("questions")
        .aggregate(
            [
                {"$match": match_query},
                {"$lookup": lookup_query},
                {"$project": {"questionNumberId": 1, "subject": 1, "topic": 1}}
            ]
        )
    )


