import base64
import csv
from datetime import datetime
from hashlib import pbkdf2_hmac
from os import urandom
from random import randint
from flask import helpers

import redis
import requests
import rq
from db.HydraArangoDb import HydraArangoDb
from db.HydraMongoDb import HydraMongoDb
from rq import Queue



from api.models.students import StudentSchema
from app import create_app
from bulkread.BulkRead import StudentsBulkRead, StudentsBulkReadV2

application = create_app()

# mongo_db = HydraMongoDb(application.config['MONGO_URI'])
mongo_db = HydraMongoDb("mongodb://root:5eqcMZQCPXtG8MvU@localhost:27018/?authenticationDatabase=admin")
arango_db = HydraArangoDb(
    application.config["ARANGODB_URI"],
    application.config["ARANGODB_USERNAME"],
    application.config["ARANGODB_PASSWORD"],
)
settings_database = application.config["SETTINGS_DATABASE"]
tenant_registry = application.config["TENANT_REGISTRY_DATABASE"]
question_db = application.config["QUESTION_BANK_DB"]
# hostname = application.config["HOST_NAME"]
hostname = 'getranks.in'
redis_connection = redis.Redis.from_url(application.config["RQ_REDIS_URL"])
queue = Queue(application.config["RQ_QUEUES"], connection=redis_connection)


def delete_subject(test_id):
    print(test_id)
    print(question_db)
    query ={
        "questionPaperId": f'QP{test_id}',
        "subject":"Chemistry"
    }
    print(f'{query}')
    docs = mongo_db.find(question_db,"questions",query)
    print(f'{len(docs)}')

    for doc in docs:
        print(f'\n{doc}')



# delete_subject(1606974277103)


