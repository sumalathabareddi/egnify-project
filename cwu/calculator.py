class CWUCalculator:
    def __init__(self, questions_keys, rankguru):
        self.questions_keys = questions_keys
        self.rankguru = rankguru

    def eval_student_key(self, user_key, question_key):
        if not user_key:
            return "U"

        correct_match_count = 0
        user_key = set(user_key)
        question_key = set(question_key)

        if len(question_key) < len(user_key):
            return "W"

        if user_key == question_key:
            return "C"

        for key in user_key:
            if key in question_key:
                correct_match_count = correct_match_count + 1
            else:
                return "W"
        return "P" + str(correct_match_count)

    def eval_question_resp(self, user_key, question_key, q_type):
        if "" in user_key or "-" in user_key:
            return "U"
        if q_type == "Numeric type":
            user_key = [float(k) for k in user_key]
            question_key = [float(k) for k in question_key]
        elif q_type == "Matrix match type":
            res = {}
            answer_key = {}

            for resp in question_key:
                option = resp["sub"]
                answer = [x.upper() for x in resp["answer"]]
                answer_key[option.lower()] = answer

            user_response = {}

            for response in user_key:
                if response:
                    for key in response:
                        user_response[key.lower()] = [x.upper() for x in response[key]]

            for key in answer_key:
                res[key] = "U"
                if key in user_response and user_response[key] and len(user_response[key]):
                    res[key] = self.eval_student_key(user_response[key], answer_key[key])
            return res

        return self.eval_student_key(user_key, question_key)

    def student_result(self, student_response):
        cwu = {}
        for key in self.questions_keys:
            qKey = f"Q{key}"
            student_response[key] = student_response.get(key, [])
            data = self.eval_question_resp(
                student_response[key],
                self.questions_keys[key]["key"],
                self.questions_keys[key]["q_type"],
            )
            if self.questions_keys[key]["q_type"] == "Matrix match type":
                for col in data:
                    if not self.rankguru:
                        cwu[f"{qKey}{col}"] = data[col]
                    else:
                        cwu[f"{key}{col}"] = data[col]
            else:
                if not self.rankguru:
                    cwu[qKey] = data
                else:
                    cwu[key] = data
        return cwu
