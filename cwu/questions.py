def keys_for_paper(question_paper_id, rankguru, mongodb, settings_db, questions_db):
    if rankguru:
        questions_keys = mongodb.get_results(
            settings_db,
            "questions",
            {"questionPaperId": question_paper_id},
            key="qno",
            projection={"key": 1, "qno": 1, "_id": 0, "q_type": 1},
        )
    else:
        questions_keys = mongodb.get_results(
            questions_db,
            "questions",
            {"questionPaperId": question_paper_id},
            key="qno",
            projection={"qno": 1, "_id": 0, "questionNumberId": 1},
        )

        question_numberids = []

        for qno in questions_keys:
            question_numberids.append(questions_keys[qno]["questionNumberId"])

        questions = mongodb.get_results(
            questions_db,
            "questions-list",
            {"questionNumberId": {"$in": question_numberids}},
            key="questionNumberId",
            projection={
                "key": 1,
                "questionNumberId": 1,
                "_id": 0,
                "questionType": 1,
                "answer": 1,
            },
        )
        for qno in questions_keys:
            questionNumberId = questions_keys[qno]["questionNumberId"]
            questions_keys[qno]["key"] = (
                questions[questionNumberId]["key"]
                if "key" in questions[questionNumberId]
                and questions[questionNumberId]["key"]
                else [questions[questionNumberId]["answer"]]
            )
            questions_keys[qno]["q_type"] = questions[questionNumberId]["questionType"]

    return questions_keys
