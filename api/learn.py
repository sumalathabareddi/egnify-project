from flask import Blueprint, current_app, request
from utils.decorators import (
    make_egnify_response,
    roles_required,
    authorization_required,
)
from utils.StorageHelper import StorageHelper
from datetime import datetime, timedelta, timezone
import pandas as pd
from utils import md5
from aggregation import Aggregation
from dataclasses import dataclass
from utils.EgnifyException import MultipleValidationExceptions, ValidationException
from utils.egy_analytics import cache
from utils.TimeHelper import TimeHelper
from bson.objectid import ObjectId
import time

from .models import LearnScheduleSchema


blueprint = Blueprint("learn", __name__, url_prefix="/learn")

mongo_db = arango_db = settings_database = None


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global mongo_db, arango_db, settings_database, storage_secrets, storage_provider
    mongo_db = current_app.mongo_db
    arango_db = current_app.arango_db
    settings_database = current_app.config["SETTINGS_DATABASE"]
    storage_provider = current_app.config["STORAGE_PROVIDER"]
    storage_secrets = {}
    if storage_provider == "GCP":
        storage_secrets["GCP_CREDENTIALS_FILE"] = current_app.config[
            "GCP_CREDENTIALS_FILE"
        ]
    if storage_provider == "MINIO":
        storage_secrets["endpoint"] = current_app.config["MINIO_ENDPOINT"]
        storage_secrets["access_key"] = current_app.config["MINIO_ACCESS_KEY"]
        storage_secrets["secret_key"] = current_app.config["MINIO_SECRET_KEY"]
        storage_secrets["secure"] = True


@blueprint.route("/presigned.url/<institute_id>/<path:storage_key>", methods=["GET"])
@roles_required(["STUDENT"])
@authorization_required()
@make_egnify_response
def launch_request(institute_id, storage_key):
    """Get Temporary url to access files.
    ---
    get:
      description: Get Temporary url for accessing files from cloud storage
      responses:
        200:
          description: Return temporary url
          content:
            application/json:
    """
    storage_key = institute_id + "/" + storage_key
    response_data, status_code, extra_headers = {}, 200, {}
    storage_helper = StorageHelper(storage_provider, storage_secrets)
    bucket_name = current_app.config["LEARN_PRIVATE_BUCKET"]
    extension = storage_key.split(".")[-1].lower()
    if extension == "htm" or extension == "html":
        bucket_name = current_app.config["LEARN_PUBLIC_BUCKET"]
    url = storage_helper.get_signed_url(bucket_name, storage_key, timedelta(minutes=60))
    response_data = {
        "STATUS": "SUCCESS",
        "MESSAGE": "Launched successfully",
        "DATA": url,
    }
    return response_data, status_code, extra_headers


def get_validation_data(institute_id, hierarchy_id):

    # Getting institue data for level1 and level2
    institute_obj = mongo_db.find_one(
        settings_database,
        "institutes",
        {"_id": institute_id},
        projection={"_id": 0, "systemHierarchies": 1},
    )
    systemHierarchies = (
        institute_obj["systemHierarchies"]
        if "systemHierarchies" in institute_obj
        else []
    )
    orientation_ids = [x["_id"] for x in systemHierarchies if x["level"] == 1]
    class_ids = [x["_id"] for x in systemHierarchies if x["level"] == 2]

    testpatterns = mongo_db.find(
        settings_database,
        "testpatterns",
        {"orientationId": {"$in": orientation_ids}, "active": True},
    )
    testpattern_ids = [x["_id"] for x in testpatterns]

    hierarchy_data = arango_db.get_node_data_as_json(hierarchy_id, depth=2)

    subjects = mongo_db.find(settings_database, "subjecttaxonomies", {})

    topic_match_query = {
        "active": True,
        "instituteId": institute_id,
        "orientations": {"$in": orientation_ids},
        "classes": {"$in": class_ids},
        "patterns": {"$in": testpattern_ids},
    }
    topic_project_query = {
        "orientations": 1,
        "classes": 1,
        "patterns": 1,
        "topic": 1,
        "subTopic": 1,
        "subject": 1,
        "subjectCode": 1,
    }

    taxonomies_count = mongo_db.count(
        settings_database, "concepttaxonomies", topic_match_query
    )
    institute_id = institute_id if taxonomies_count else "root"
    topic_match_query["instituteId"] = institute_id
    topics = mongo_db.find(
        settings_database,
        "concepttaxonomies",
        topic_match_query,
        projection=topic_project_query,
    )
    master_obj = {}
    if "nodes" in hierarchy_data:
        hierarchy_data = hierarchy_data["nodes"]
    for obj in [x for x in hierarchy_data if x["level"] == 1]:
        key = obj["hierarchyName"].lower()
        if key not in master_obj:
            orientation_obj = next(
                (item for item in systemHierarchies if item["name"].lower() == key),
                None,
            )
            if not orientation_obj:
                continue
            orientation_id = orientation_obj["_id"]
            master_obj[key] = {
                "hierarchy_id": obj["id"],
                "orientation_id": orientation_id,
            }

            patterns = [x for x in testpatterns if x["orientationId"] == orientation_id]
            for pattern in patterns:
                pattern_key = key + "_" + pattern["pattern"].lower()
                master_obj[pattern_key] = {"pattern_id": pattern["_id"]}
                if "subjects" in pattern and len(pattern["subjects"]):

                    for subject_id in pattern["subjects"]:
                        subject_obj = next(
                            (item for item in subjects if item["_id"] == subject_id),
                            None,
                        )
                        if not subject_obj:
                            continue
                        subject_key = pattern_key + "_" + subject_obj["name"].lower()
                        master_obj[subject_key] = {"name": subject_obj["name"]}

                        topics_list = [
                            x
                            for x in topics
                            if x["subjectCode"] == subject_id
                            and "topic" in x
                            and x["topic"]
                        ]
                        for topic in topics_list:
                            topic_key = subject_key + "_" + topic["topic"].lower()
                            if topic_key in master_obj:
                                continue

                            master_obj[topic_key] = topic

                            sub_topics_list = [
                                x
                                for x in topics_list
                                if x["topic"] == topic["topic"]
                                and "subTopic" in x
                                and x["subTopic"]
                            ]
                            for sub_topic in sub_topics_list:
                                sub_topic_key = (
                                    topic_key + "_" + sub_topic["subTopic"].lower()
                                )
                                if sub_topic_key in master_obj:
                                    continue
                                master_obj[sub_topic_key] = sub_topic

    for obj in [x for x in hierarchy_data if x["level"] == 2]:
        parent = next(
            (item for item in hierarchy_data if item["id"] == obj["parentCode"]), None
        )
        if not parent:
            continue
        key = parent["hierarchyName"].lower() + "_" + obj["hierarchyName"].lower()
        class_obj = next(
            (
                item
                for item in systemHierarchies
                if item["name"].lower() == obj["hierarchyName"].lower()
                and item["level"] == 2
            ),
            None,
        )
        if not class_obj:
            continue
        master_obj[key] = {"hierarchy_id": obj["id"], "class_id": class_obj["_id"]}
    return master_obj


def isEmptyRow(obj):
    return not any(obj.values())


@dataclass
class LearnContent:
    headers: list
    data: list
    mandate_columns: list
    all_columns: list
    error_limit: int
    row_limit: int

    def clean_data(self):

        # Lowercasing keys and trimming strings
        data = [
            {k.lower().strip(): str(v).strip() for k, v in x.items()} for x in self.data
        ]

        # Removing empty rows in the end
        for obj in reversed(data):
            if isEmptyRow(obj):
                data.pop()
            else:
                break

        self.data = data

    def validate_sheet(self):
        errors = []
        for column in self.all_columns:
            if column not in self.headers:
                errors.append(ValidationException("E204", column.upper(), None))

        if len(list(dict.fromkeys(self.headers))) != len(self.headers):
            errors.append(ValidationException("E205", None, None))

        if not len(self.data):
            errors.append(ValidationException("E206", None, None))

        if len(self.data) > self.row_limit:
            errors.append(ValidationException("E207", self.row_limit, None))

        if len(errors):
            return errors

        for index, obj in enumerate(self.data):
            if len(errors) >= self.error_limit:
                return errors
            row = index + 2
            for column in self.mandate_columns:
                if not obj[column]:
                    errors.append(ValidationException("E208", row, column.upper()))
            if obj["view order"]:
                try:
                    obj["view order"] = int(float(obj["view order"]))
                except:
                    errors.append(ValidationException("E209", row, "VIEW ORDER"))

        return errors

    def validate_data(self, validation_obj):
        errors = []
        categories = mongo_db.find(settings_database, "contentcategories", {})

        for index, obj in enumerate(self.data):
            if len(errors) >= self.error_limit:
                return errors
            row = index + 2
            orientation = obj["orientation"]
            _class = obj["class"]
            pattern = obj["test pattern"]
            subject = obj["subject"]
            topic = obj["topic"]
            subtopic = obj["subtopic"]

            # validating category
            category_obj = next(
                (
                    item
                    for item in categories
                    if item["category"].lower() == obj["category"].lower()
                ),
                None,
            )
            if not category_obj:
                errors.append(ValidationException("E209", row, "CATEGORY"))
            else:
                obj["categoryId"] = category_obj["_id"]

            # validating orientation
            if orientation.lower() not in validation_obj:
                errors.append(ValidationException("E209", row, "ORIENTATION"))
                continue
            else:
                orientationId = validation_obj[orientation.lower()]["orientation_id"]

            # validating class
            key = orientation.lower() + "_" + _class.lower()
            if key not in validation_obj:
                errors.append(ValidationException("E209", row, "CLASS"))
                continue
            else:
                obj["hierarchyId"] = validation_obj[key]["hierarchy_id"]
                class_id = validation_obj[key]["class_id"]

            # validating test pattern
            key = orientation.lower() + "_" + pattern.lower()
            if key not in validation_obj:
                errors.append(ValidationException("E209", row, "TEST PATTERN"))
                continue
            else:
                obj["patternId"] = validation_obj[key]["pattern_id"]

            # validating subject
            key = key + "_" + subject.lower()
            if key not in validation_obj:
                errors.append(ValidationException("E209", row, "SUBJECT"))
                continue
            else:
                obj["subject"] = validation_obj[key]["name"]

            # validating topic
            key = key + "_" + topic.lower()
            if (
                key in validation_obj
                and "orientations" in validation_obj[key]
                and orientationId in validation_obj[key]["orientations"]
                and "classes" in validation_obj[key]
                and class_id in validation_obj[key]["classes"]
                and "patterns" in validation_obj[key]
                and obj["patternId"] in validation_obj[key]["patterns"]
            ):
                obj["topic"] = validation_obj[key]["topic"]
            else:
                errors.append(ValidationException("E209", row, "TOPIC"))
                continue

            # validating subTopic
            if obj["subtopic"]:
                key = key + "_" + subtopic.lower()
                if (
                    key in validation_obj
                    and "orientations" in validation_obj[key]
                    and orientationId in validation_obj[key]["orientations"]
                    and "classes" in validation_obj[key]
                    and class_id in validation_obj[key]["classes"]
                    and "patterns" in validation_obj[key]
                    and obj["patternId"] in validation_obj[key]["patterns"]
                ):
                    obj["subTopic"] = validation_obj[key]["subTopic"]
                else:
                    errors.append(ValidationException("E209", row, "SUBTOPIC"))
                    continue

        return errors

    def prepare_documents(self, institute_id):
        final_data = []

        for obj in self.data:
            temp = {}
            temp["_id"] = md5(institute_id + obj["asset id"])
            temp["assetId"] = obj["asset id"]
            temp["name"] = obj["name"]
            temp["filepath"] = obj["file path"]
            temp["categoryId"] = obj["categoryId"]
            temp["coins"] = 0
            temp["viewOrder"] = obj.get("view order", None)
            temp["instituteId"] = institute_id
            temp["hierarchyId"] = [obj["hierarchyId"]]
            temp["patternId"] = [obj["patternId"]]
            temp["subject"] = obj["subject"]
            temp["topic"] = obj["topic"]
            temp["subTopic"] = obj.get("subTopic", None)
            temp["updatedAt"] = datetime.now(timezone.utc)
            temp["meta_info"] = {
                "faculty": obj.get("faculty", None),
                "thumbnail": obj.get("thumbnail", None),
            }
            temp["active"] = True
            final_data.append(temp)
        return final_data


@blueprint.route("/<institute_id>/upload", methods=["POST"])
# @roles_required(["CONTENT_MANAGER"])
@authorization_required()
@make_egnify_response
def upload_content_mapping(institute_id):
    """Uploding the content mapping sheet.
    ---
    get:
      description: Maping the learn assets to system hierachies and concept taxonomy
      responses:
        200:
          description: success message
          content:
            application/json:
        400:
          description: Invalid data with error messages
    """
    hierarchy_id = md5(institute_id)

    file = request.files.get("file")

    errors = []

    if not file:
        errors.append(ValidationException("E202", None, None))
        raise MultipleValidationExceptions(errors)

    try:
        file_data = pd.read_excel(file, skip_blank_lines=False)
        file_data.fillna("", inplace=True)
    except:
        errors.append(ValidationException("E203", None, None))
        raise MultipleValidationExceptions(errors)

    # extracting data
    headers = [x.lower().strip() for x in list(file_data)]
    data = file_data.to_dict("records")

    mandate_columns = [
        "asset id",
        "orientation",
        "class",
        "test pattern",
        "subject",
        "topic",
        "category",
        "name",
        "file path",
    ]
    all_columns = list(mandate_columns) + [
        "subtopic",
        "view order",
        "faculty",
        "thumbnail",
    ]

    error_limit = (
        current_app.config["LEARN_UPLOAD_ERROR_LIMIT"]
        if "LEARN_UPLOAD_ERROR_LIMIT" in current_app.config
        else 1000
    )
    row_limit = (
        current_app.config["LEARN_UPLOAD_ROW_LIMIT"]
        if "LEARN_UPLOAD_ROW_LIMIT" in current_app.config
        else 10000
    )

    lc = LearnContent(
        headers, data, mandate_columns, all_columns, error_limit, row_limit
    )

    # Cleaning data
    lc.clean_data()

    # Validating sheet
    if (errors := lc.validate_sheet()) :
        raise MultipleValidationExceptions(errors)

    # Getting data to validate
    validation_obj = get_validation_data(institute_id, hierarchy_id)

    # Validating data
    if (errors := lc.validate_data(validation_obj)) :
        raise MultipleValidationExceptions(errors)

    # Preparing documents to store
    data = lc.prepare_documents(institute_id)

    # Storing into thte database
    mongo_db._upsert_many(
        connection=None,
        db_name=settings_database,
        collection_name="contentmappings",
        obj=data,
        id_field="_id",
        upsert=True,
    )

    return (
        {"STATUS": "SUCCESS", "DATA": None, "MESSAGE": "Data uploaded successfully"},
        200,
        {},
    )


@blueprint.route("/categories", methods=["GET"])
@cache.cached()
@make_egnify_response
def get_content_categories():
    """Get available content categories.
    ---
    get:
      description: Get available content categories
      responses:
        200:
          description: Return list of categories
          content:
            application/json:
    """
    data = mongo_db.find(settings_database, "contentcategories", {})
    custom_headers = {"Cache-Control": "private, max-age=14400"}
    return {"STATUS": "SUCCESS", "DATA": data, "MESSAGE": ""}, 200, custom_headers


@blueprint.route("/<hierarchy_id>/", methods=["GET"])
@roles_required(["STUDENT"])
@authorization_required()
@cache.cached(timeout=86400, query_string=True)
@make_egnify_response
def get_learn_content_list(hierarchy_id):
    """Get available content list.
    ---
    get:
      description: Get available content list
      responses:
        200:
          description: Return list of content
          content:
            application/json:
    """

    hierarchy_path = arango_db.get_path("root", hierarchy_id)
    query = {"active": True}
    if len(hierarchy_path):
        query["hierarchyId"] = {"$in": hierarchy_path}

    if request.values.get("subject", None):
        query["subject"] = request.values.get("subject", None)

    if request.values.get("topic", None):
        query["topic"] = request.values.get("topic", None)

    if request.values.get("subTopic", None):
        query["subTopic"] = request.values.get("subTopic", None)

    if request.values.get("patternId", None):
        query["patternId"] = {"$in": request.values.get("patternId", None).split(",")}

    if request.values.get("categoryId", None):
        query["categoryId"] = {"$in": request.values.get("categoryId", None).split(",")}

    data = mongo_db.find(settings_database, "contentmappings", query)

    custom_headers = {"Cache-Control": "private, max-age=14400"}

    return {"STATUS": "SUCCESS", "DATA": data, "MESSAGE": ""}, 200, custom_headers

@blueprint.route("/asset", methods=["GET"])
@roles_required(["ASSET_SCHEDULE_CREATOR"])
@authorization_required()
@cache.cached(timeout=86400, query_string=True)
@make_egnify_response
def get_learn_content_list_admin():
    """Get available content list.
    ---
    get:
      description: Get available content list
      responses:
        200:
          description: Return list of content
          content:
            application/json:
    """

    query = {"active": True}
    
    if request.values.get("hierarchyId", None):
        query["hierarchyId"] = {"$in": request.values.get("hierarchyId", None).split(",")}

    if request.values.get("subject", None):
        query["subject"] = request.values.get("subject", None)

    if request.values.get("topic", None):
        query["topic"] = request.values.get("topic", None)

    if request.values.get("subTopic", None):
        query["subTopic"] = request.values.get("subTopic", None)

    if request.values.get("patternId", None):
        query["patternId"] = {"$in": request.values.get("patternId", None).split(",")}

    if request.values.get("categoryId", None):
        query["categoryId"] = {"$in": request.values.get("categoryId", None).split(",")}


    data = mongo_db.find(settings_database, "contentmappings", query, projection={"_id": 1, "name": 1, "meta_info": 1})

    custom_headers = {"Cache-Control": "private, max-age=14400"}

    return {"STATUS": "SUCCESS", "DATA": data, "MESSAGE": ""}, 200, custom_headers


@blueprint.route(
    "/<hierarchy_id>/<any(categoryId, patternId, subject, topic, subTopic):groupby>/count/",
    methods=["GET"],
)
@roles_required(["STUDENT"])
@authorization_required()
@make_egnify_response
def get_learn_resource_count(groupby, hierarchy_id):
    """Get available content asset count.
    ---
    get:
      description: Get available content asset count
      responses:
        200:
          description: Return list asset count
          content:
            application/json:
    """
    hierarchy_path = arango_db.get_path("root", hierarchy_id)

    query = {"active": True}
    if len(hierarchy_path):
        query["hierarchyId"] = {"$in": hierarchy_path}

    if request.values.get("subject", None):
        query["subject"] = request.values.get("subject", None)

    if request.values.get("topic", None):
        query["topic"] = request.values.get("topic", None)

    if request.values.get("subTopic", None):
        query["subTopic"] = request.values.get("subTopic", None)

    if request.values.get("patternId", None):
        query["patternId"] = {"$in": request.values.get("patternId", None).split(",")}

    if request.values.get("categoryId", None):
        query["categoryId"] = {"$in": request.values.get("categoryId", None).split(",")}

    aggregation = Aggregation()

    group_query = {"_id": f"${groupby}", "count": {"$sum": 1}}

    if groupby == "patternId":
        aggregation.match(query).unwind("$patternId").group(group_query)
    else:
        aggregation.match(query).group(group_query)

    docs = list(
        mongo_db.connection[settings_database]
        .get_collection("contentmappings")
        .aggregate(aggregation.pipeline)
    )
    obj = {x["_id"]: x["count"] for x in docs}

    return {"STATUS": "SUCCESS", "DATA": obj, "MESSAGE": ""}, 200, {}


def prepare_schedule_data(sanitized_data):
    """

    :param sanitized_data:
    """
    sanitized_data["accessTag"] = {"hierarchy": sanitized_data["hierarchy"]}
    start_time = sanitized_data["startTime"]
    start_time = datetime.strptime(start_time, "%Y-%m-%d %H:%M")
    sanitized_data["startTime"] = TimeHelper.to_utc(start_time, "Asia/Kolkata")

    end_time = sanitized_data["endTime"]
    end_time = datetime.strptime(end_time, "%Y-%m-%d %H:%M")
    sanitized_data["endTime"] = TimeHelper.to_utc(end_time, "Asia/Kolkata")


@blueprint.route("/schedule/<institute_id>", methods=["POST"])
@roles_required(["LEARN_SCHEDULE_CREATOR"])
@authorization_required()
@make_egnify_response
def create_schedule(institute_id):
    """Scheduling item for learn.
    ---
    post:
      description: Scheduling item for learn
      responses:
        201:
          description: Return created document
          content:
            application/json:
    """

    response_data, status_code, extra_headers = {}, 200, {}

    post_data = request.get_json()  # read the json data from client request
    post_data["instituteId"] = institute_id
    result = LearnScheduleSchema().load(post_data)  # load for validations
    if result.errors:
        response_data = result.errors
        status_code = 400
    else:
        sanitized_data = result.data
        sanitized_data["_id"] = str(ObjectId())
        prepare_schedule_data(sanitized_data)
        mongo_db.insert_many(settings_database, "learnschedule", [sanitized_data])
        response_data = sanitized_data
        status_code = 201

    return response_data, status_code, extra_headers


@blueprint.route("/schedule/<institute_id>/<string:schedule_id>", methods=["PUT"])
@roles_required(["LEARN_SCHEDULE_CREATOR"])
@authorization_required()
@make_egnify_response
def update_schedule(institute_id, schedule_id):
    """Updating Scheduled item for learn.
    ---
    post:
      description: Updatating Scheduled item for learn
      responses:
        200:
          description: Return success object
          content:
            application/json:
    """
    response_data, status_code, extra_headers = {}, 200, {}
    post_data = request.get_json()
    post_data["instituteId"] = institute_id
    result = LearnScheduleSchema().load(post_data)
    if result.errors:
        response_data = result.errors
        status_code = 400
    else:
        sanitized_data = result.data
        prepare_schedule_data(sanitized_data)
        sanitized_data["_id"] = schedule_id

        # Updating into the database
        mongo_db._upsert_many(
            connection=None,
            db_name=settings_database,
            collection_name="learnschedule",
            obj=[sanitized_data],
            id_field="_id",
            upsert=False,
        )

        response_data = {
            "STATUS": "SUCCESS",
            "DATA": None,
            "MESSAGE": "Updated test successfully",
        }
    return response_data, status_code, extra_headers


@blueprint.route("/schedule/<institute_id>/<string:schedule_id>", methods=["DELETE"])
@roles_required(["LEARN_SCHEDULE_CREATOR"])
@authorization_required()
@make_egnify_response
def delete_schedule(institute_id, schedule_id):
    """Delete Schedule.
    ---
    get:
      description: Delete Schedule data
      responses:
        200:
          description: Return success object
          content:
            application/json:
    """
    response_data, status_code, extra_headers = {}, 200, {}
    data = {"_id": schedule_id, "active": False}

    # Soft delete from database
    mongo_db._upsert_many(
        connection=None,
        db_name=settings_database,
        collection_name="learnschedule",
        obj=[data],
        id_field="_id",
        upsert=False,
    )

    response_data = {
        "STATUS": "SUCCESS",
        "MESSAGE": "Deleted successfully",
        "DATA": None,
    }
    return response_data, status_code, extra_headers


@blueprint.route("/schedule/<institute_id>/<string:schedule_id>", methods=["GET"])
# @roles_required(["LEARN_SCHEDULE_CREATOR"])
# @authorization_required()
@make_egnify_response
def get_schedule(institute_id, schedule_id):
    """Get Schedule.
    ---
    get:
      description: Get Schedule data
      responses:
        200:
          description: Return success object
          content:
            application/json:
    """
    response_data, status_code, extra_headers = {}, 200, {}

    query = {"_id": schedule_id, "active": True}

    schedule_data = mongo_db.find_one(settings_database, "learnschedule", query)
    response_data = {
        "STATUS": "SUCCESS",
        "DATA": schedule_data,
    }
    return response_data, status_code, extra_headers


@blueprint.route("/schedule/<hierarchy_id>", methods=["GET"])
@roles_required(["STUDENT"])
@authorization_required()
@make_egnify_response
def hierarchy_wise_schedule_list(hierarchy_id):
    """
    :param hierarchy_id:
    :return:
    """
    parent_nodes = arango_db.get_parent(hierarchy_id, height=20)
    hierachies = list(map(lambda x: x["id"], parent_nodes["vertices"]))
    hierachies.append(hierarchy_id)

    query = {
        "accessTag.hierarchy": {"$in": hierachies},
        "active": True,
    }

    if request.values.get("patternId", None):
        query["patternId"] = {"$in": request.values.get("patternId", None).split(",")}

    if request.values.get("subject", None):
        query["subject"] = request.values.get("subject", None)

    if request.values.get("topic", None):
        query["topic"] = request.values.get("topic", None)

    status = request.values.get("status", None)
    if status:
        if status == "inprogress":
            query["startTime"] = {"$lte": datetime.now(timezone.utc)}
            query["endTime"] = {"$gte": datetime.now(timezone.utc)}
        elif status == "upcoming":
            query["startTime"] = {"$gte": datetime.now(timezone.utc)}
        elif status == "completed":
            query["endTime"] = {"$lt": datetime.now(timezone.utc)}
    projection = {field: 0 for field in ["accessTag"]}
    kwargs = {
        "key": "_id",
        "sort_key": "startTime",
        "ascending": True,
        "projection": projection,
    }
    start_time = time.time()
    collection_name = "learnschedule"
    data = mongo_db.get_results(settings_database, collection_name, query, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return data, 200, {}


def _build_schedule_query():

    query = {"active": True}
    if request.values.get("orientationId", None):
        query["orientationId"] = {
            "$in": request.values.get("orientationId", None).split(",")
        }
    
    if request.values.get("classId", None):
        query["classId"] = {
            "$in": request.values.get("classId", None).split(",")
        }

    if request.values.get("patternId", None):
        query["patternId"] = {"$in": request.values.get("patternId", None).split(",")}

    if request.values.get("subject", None):
        query["subject"] = request.values.get("subject", None)

    if request.values.get("topic", None):
        query["topic"] = request.values.get("topic", None)

    status = request.values.get("status", None)
    if status:
        if status == "inprogress":
            query["startTime"] = {"$lte": datetime.now(timezone.utc)}
            query["endTime"] = {"$gte": datetime.now(timezone.utc)}
        elif status == "upcoming":
            query["startTime"] = {"$gt": datetime.now(timezone.utc)}
        elif status == "completed":
            query["endTime"] = {"$lt": datetime.now(timezone.utc)}

    hierarchies = request.values.get("hierarchies", None)
    if hierarchies:
        childrens = []
        hierarchies = hierarchies.split(",")
        for hierarchy in hierarchies:
            parent_nodes = arango_db.get_parent(hierarchy, height=20)
            node_hierachies = [v["id"] for v in parent_nodes["vertices"]]
            childrens = childrens + node_hierachies
            children_nodes = arango_db.get_children(hierarchy, depth=20)
            node_hierachies = [v["id"] for v in children_nodes["vertices"]]
            childrens = childrens + node_hierachies
        query["accessTag.hierarchy"] = {"$in": childrens}

    return query


@blueprint.route("/schedule", methods=["GET"])
@roles_required(["LEARN_SCHEDULE_CREATOR"])
@authorization_required()
@make_egnify_response
def get_schedules():
    """
        Return list of scheduled items.
    """
    start_time = time.time()
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 0, type=int)
    query = _build_schedule_query()
    collection_name = "learnschedule"
    projection = {field: 0 for field in ["accessTag"]}
    kwargs = {
        "key": "_id",
        "sort_key": "startTime",
        "skip": skip,
        "size": size,
        "ascending": True,
        "projection": projection,
    }
    data = mongo_db.get_results(settings_database, collection_name, query, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return data, 200, {}


@blueprint.route("/schedule/count", methods=["GET"])
@roles_required(["LEARN_SCHEDULE_CREATOR"])
@authorization_required()
@make_egnify_response
def get_schedule_count():
    """"
        Return count of scheduled items.
    """
    start_time = time.time()
    query = _build_schedule_query()
    collection_name = "learnschedule"
    count = mongo_db.count(settings_database, collection_name, query)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return {"count": count}, 200, {}


