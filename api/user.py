from flask import Blueprint, current_app, request
import utils.decorators as decorators
from utils.EgnifyRole import EgnifyRole
from datetime import datetime, timezone

blueprint = Blueprint("user", __name__, url_prefix="/user")

mongo_db = test_database = None


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global mongo_db, test_database
    mongo_db = current_app.mongo_db
    test_database = current_app.config["TEST_DATABASE"]


@blueprint.route("/<string:user_id>/<string:question_paper_id>", methods=["GET"])
@decorators.roles_required([EgnifyRole.TEST_CREATOR, EgnifyRole.TEACHER])
@decorators.make_egnify_response
def get_questions_by_question_paper_id_test_id_user_id(user_id, question_paper_id):
    """
        This returns the test question split information for any user
        This api will be called to get the users assigned for a test id
        and question paper id
    """
    query = {
        "userId": user_id,
        "questionPaperId": question_paper_id,
    }
    user_questions_assigned = mongo_db.get_results(
        test_database, "questions", query, key="_id"
    )
    return user_questions_assigned, 200, {}


@blueprint.route(
    "/<string:test_id>/<string:question_paper_id>/<string:user_id>/data",
    methods=["GET"],
)
@decorators.roles_required([EgnifyRole.TEST_CREATOR, EgnifyRole.TEACHER])
@decorators.make_egnify_response
def get_questions_by_question_paper_id_user_id(test_id, question_paper_id, user_id):
    """
       Get the Question paper schema for a test for an user
       This returns the test question split information for any user
    """

    _id = "_".join([test_id, question_paper_id, user_id])
    question_assigned = mongo_db.read_by_id(
        current_app.config["QUESTION_BANK_DB"], "question_papers", [_id]
    )
    return question_assigned, 200, {}


# TODO: Need to check for the user level authorization for PUT
@blueprint.route(
    "/<string:user_id>/<string:test_id>/<string:question_paper_id>", methods=["PUT"]
)
@decorators.roles_required(["TEST_CREATOR", "TEACHER"])
@decorators.make_egnify_response
def insert_questions_by_question_paper_id_user_id(user_id, test_id, question_paper_id):
    """
        This will be called when we are assigning questions to the {user}
        Json format:
        {
           "subject": [{
                name: physics
                noOfQuestions: 10,
                difficulty: "EASY/MEDIUM/HARD",
                difficultyLevel: 8,   // Optional Value
                questionTypes: [
                     {
                        "type": "singleAnswer"
                        noOfQuestions: 5,
                        difficulty: "EASY/MEDIUM/HARD",
                        difficultyLevel: 6   // Optional Value
                    },
                    {
                        "type": "multipleAnswer"
                        noOfQuestions: 4,
                        difficulty: "EASY/MEDIUM/HARD",
                        difficultyLevel: 6   // Optional Value
                    },
                    {
                        "type": "matrixMatch"
                        noOfQuestions: 1,
                        difficulty: "EASY/MEDIUM/HARD",
                        difficultyLevel: 10   // Optional Value
                    }]
                }
           }],
           "topic": [{
              noOfQuestions: 10,
                difficulty: "EASY/MEDIUM/HARD",
                difficultyLevel: 8,   // Optional Value
                "subject": "Physics"
                questionTypes: [
                     {
                        "type": "singleAnswer"
                        noOfQuestions: 5,
                        difficulty: "EASY/MEDIUM/HARD",
                        difficultyLevel: 6   // Optional Value
                    },
                    {
                        "type": "multipleAnswer"
                        noOfQuestions: 4,
                        difficulty: "EASY/MEDIUM/HARD",
                        difficultyLevel: 6   // Optional Value
                    },
                    {
                        "type": "matrixMatch"
                        noOfQuestions: 1,
                        difficulty: "EASY/MEDIUM/HARD",
                        difficultyLevel: 10   // Optional Value
                    }]
                }
           }],
           "subTopic": [{
              noOfQuestions: 10,
                difficulty: "EASY/MEDIUM/HARD",
                difficultyLevel: 8,   // Optional Value
                "subject": "Physics"
                questionTypes: [
                     {
                        "type": "singleAnswer"
                        noOfQuestions: 5,
                        difficulty: "EASY/MEDIUM/HARD",
                        difficultyLevel: 6   // Optional Value
                    },
                    {
                        "type": "multipleAnswer"
                        noOfQuestions: 4,
                        difficulty: "EASY/MEDIUM/HARD",
                        difficultyLevel: 6   // Optional Value
                    },
                    {
                        "type": "matrixMatch"
                        noOfQuestions: 1,
                        difficulty: "EASY/MEDIUM/HARD",
                        difficultyLevel: 10   // Optional Value
                    }]
                }
           }]
        }

    """
    _id = "_".join([test_id, question_paper_id, user_id])
    questions_assigned = request.get_json()
    subjects_list = questions_assigned["subject"]
    #deleting already assigned teacher if there is no questions/subjects
    if not len(subjects_list):
        response_data = {}
        mongo_db.delete(current_app.config["QUESTION_BANK_DB"], "question_papers", {"_id": _id})
        response_data["STATUS"] = "SUCCESS"
        response_data["DATA"] = None
        return response_data, 200, {}
    else: 
        test_question = {
            "questions": questions_assigned,
            "testId": test_id,
            "questionPaperId": question_paper_id,
            "userId": user_id,
        }
        response = mongo_db.find_one_and_upsert(
            current_app.config["QUESTION_BANK_DB"],
            "question_papers",
            {"_id": _id},
            test_question,
        )
        return response, 200, {}

@blueprint.route("/<string:test_id>/<string:question_paper_id>/users", methods=["GET"])
@decorators.roles_required([EgnifyRole.TEST_CREATOR, EgnifyRole.TEACHER])
@decorators.make_egnify_response
def get_users_by_question_paper_id_test_id(test_id, question_paper_id):
    """
        This returns the test question split information for any user
        This api will be called to get the users assigned for a
        test id and question paper id
    """
    query = {
        "testId": test_id,
        "questionPaperId": question_paper_id,
        "userId":{"$exists":True}
    }
    users_assigned = mongo_db.get_results(
        current_app.config["QUESTION_BANK_DB"],
        "question_papers",
        query,
        key="userId",
        projection={"testId": 1, "questionPaperId": 1, "userId": 1},
    )
    return users_assigned, 200, {}


@blueprint.route("/<string:user_id>/questions", methods=["GET"])
@decorators.roles_required([EgnifyRole.TEST_CREATOR, EgnifyRole.TEACHER])
@decorators.make_egnify_response
def get_questions_by_user_id(user_id):
    """

    :param user_id:
    :return:
    """
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 20, type=int)
    status = request.values.get("status", None)
    marking_schema_id = request.values.get("markingSchemaId", None)
    pattren_id = request.values.get("patternId", None)
    lookup_query = {
        "from": "tests",
        "localField": "testId",
        "foreignField": "_id",
        "as": "test",
    }
    get_last_test = {"$addFields": {"test": {"$arrayElemAt": ["$test", -1]}}}
    sort_query = {"test.startTime": -1}
    projection_query = {"questions": 0, "test": 0}

    pre_match_query = {"userId": user_id}
    post_match_query = {}
    if status:
        if status == "review":
            post_match_query["test.startTime"] = {"$gte": datetime.now(timezone.utc)}
            pre_match_query["$or"] = [
                {"status": "review"},
                {"status": {"$exists": False}},
                {"questions.subject.status": "review"},
                {"questions.subject.status": {"$exists": False}},
            ]
        elif status == "archived":
            pre_match_query["$or"] = [
                {"status": "review"},
                {"status": {"$exists": False}},
                {"questions.subject.status": "review"},
                {"questions.subject.status": {"$exists": False}},
            ]
            post_match_query["test.startTime"] = {"$lt": datetime.now(timezone.utc)}
        else:
            pre_match_query["status"] = status
    if marking_schema_id:
        post_match_query["test.markingSchemaId"] = marking_schema_id
    if pattren_id:
        post_match_query["test.patternId"] = pattren_id
    # TODO: this works only when question bank DB and Test Database are same
    test_status_data = (
        mongo_db.connection[current_app.config["QUESTION_BANK_DB"]]
        .get_collection("question_papers")
        .aggregate(
            [
                {"$match": pre_match_query},
                {"$lookup": lookup_query},
                get_last_test,
                {"$match": post_match_query},
                {"$sort": sort_query},
                {"$skip": skip},
                {"$limit": size},
                {"$project": projection_query},
            ]
        )
    )
    test_status_response = {}
    for test_status in test_status_data:
        test_status_response[test_status["_id"]] = test_status

    return test_status_response, 200, {}


@blueprint.route("/<string:user_id>/questions/count", methods=["GET"])
@decorators.roles_required([EgnifyRole.TEST_CREATOR, EgnifyRole.TEACHER])
@decorators.make_egnify_response
def get_count_questions_by_user_id(user_id):
    """

    :param user_id:
    :return:
    """
    status = request.values.get("status", None)
    marking_schema_id = request.values.get("markingSchemaId", None)
    pattren_id = request.values.get("patternId", None)
    lookup_query = {
        "from": "tests",
        "localField": "testId",
        "foreignField": "_id",
        "as": "test",
    }
    get_last_test = {"$addFields": {"test": {"$arrayElemAt": ["$test", -1]}}}

    pre_match_query = {"userId": user_id}
    post_match_query = {}
    if status:
        if status == "review":
            post_match_query["test.startTime"] = {"$gte": datetime.now(timezone.utc)}
            pre_match_query["$or"] = [
                {"status": "review"},
                {"status": {"$exists": False}},
                {"questions.subject.status": "review"},
                {"questions.subject.status": {"$exists": False}},
            ]
        elif status == "archived":
            pre_match_query["$or"] = [
                {"status": "review"},
                {"status": {"$exists": False}},
                {"questions.subject.status": "review"},
                {"questions.subject.status": {"$exists": False}},
            ]
            post_match_query["test.startTime"] = {"$lt": datetime.now(timezone.utc)}
        else:
            pre_match_query["status"] = status
    if marking_schema_id:
        post_match_query["test.markingSchemaId"] = marking_schema_id
    if pattren_id:
        post_match_query["test.patternId"] = pattren_id
    # TODO: this works only when question bank DB and Test Database are same
    test_status_data = (
        mongo_db.connection[current_app.config["QUESTION_BANK_DB"]]
        .get_collection("question_papers")
        .aggregate(
            [
                {"$match": pre_match_query},
                {"$lookup": lookup_query},
                get_last_test,
                {"$match": post_match_query},
                {"$count": "count"},
            ]
        )
    )
    count_object = {"count": 0}
    for test_status in test_status_data:
        count_object = test_status
    return count_object, 200, {}


@blueprint.route(
    "/<string:user_id>/<string:question_paper_id>/questions", methods=["GET"]
)
@decorators.roles_required([EgnifyRole.TEST_CREATOR, EgnifyRole.TEACHER])
@decorators.make_egnify_response
def get_questions_by_user_question_paper(user_id, question_paper_id):
    """

    :param user_id:
    :param question_paper_id:
    :return:
    """
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 20, type=int)
    kwargs = {
        "key": "_id",
        "skip": skip,
        "size": size,
        "sort_key": "qno",
    }
    test_status_data = mongo_db.get_results(
        current_app.config["QUESTION_BANK_DB"],
        "questions",
        {"userId": user_id, "questionPaperId": question_paper_id},
        **kwargs,
    )
    return test_status_data, 200, {}


@blueprint.route(
    "/<string:user_id>/<string:test_id>/<string:question_paper_id>/draft",
    methods=["PUT"],
)
@decorators.roles_required(["TEACHER"])
@decorators.make_egnify_response
def save_as_draft(user_id, test_id, question_paper_id):
    db, coll = current_app.config["QUESTION_BANK_DB"], "question_papers"
    _id = "_".join([test_id, question_paper_id, user_id])
    doc = request.get_json()
    op_type = "$pull" if doc["type"] == "remove" else "$addToSet"
    item = doc["questionNumberId"]
    subject = doc["subject"]
    update = {op_type: {f"draft.{subject}": item}}

    mongo_db.connection[db].get_collection(coll).update(
        {"_id": _id}, update,
    )
    return {"STATUS": "SUCCESS"}, 200, {}
