import random
import time
from flask import Blueprint, current_app, request
import utils.decorators as decorators
from utils.egy_analytics import cache

blueprint = Blueprint("questions", __name__, url_prefix="/questions")

mongo_db = test_database = encryption_key = None


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global mongo_db, test_database, encryption_key
    mongo_db = current_app.mongo_db
    test_database = current_app.config["TEST_DATABASE"]
    encryption_key = current_app.config["ENCRYPTION_KEY"]


@blueprint.route("")
@cache.cached(timeout=7200, query_string=True)
@decorators.make_egnify_response
def get_questions():
    """

    :return:
    """
    start_time = time.time()
    subject = request.values.get("subject", None)
    topic = request.values.get("topic", None)
    sub_topic = request.values.get("subTopic", None)
    question_type = request.values.get("questionType", None)
    difficulty = request.values.get("difficulty", None)
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 30, type=int)
    query = {}
    if subject:
        query["subject"] = subject
    if topic:
        query["topic"] = topic
    if sub_topic:
        query["subTopic"] = sub_topic
    if question_type:
        query["questionType"] = question_type
    if difficulty:
        query["difficulty"] = difficulty

    mongo_db = current_app.question_bank_mongo_db
    kwargs = {
        "key": None,
        "skip": skip,
        "size": size,
        "sort_key": "questionNumberId",
        "projection": {"questionNumberId": 1},
    }
    questions = mongo_db.get_results(
        current_app.config["QUESTION_BANK_DB"], "questions-list", query, **kwargs
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        "Time taken to fetch the results from {}: {}".format(
            "questions-list", db_duration
        )
    )
    random.shuffle(questions)
    headers = {
        "Cache-Control": "private, max-age=3600",
    }
    return questions, 200, headers


@blueprint.route("/data/<question_number_id>")
@cache.cached(timeout=86400)
@decorators.make_egnify_response
def get_question_details(question_number_id):
    """

    :param question_number_id:
    :return:
    """
    start_time = time.time()
    query = {"questionNumberId": question_number_id}
    mongo_db = current_app.question_bank_mongo_db
    question_details = mongo_db.get_results(
        current_app.config["QUESTION_BANK_DB"],
        "questions-list",
        query,
        key="questionNumberId",
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        "Time taken to fetch the results from {}: {}".format("questions", db_duration)
    )
    headers = {
        "Cache-Control": "private, max-age=14400",
    }
    return question_details, 200, headers


@blueprint.route("/<string:question_paper_id>")
@cache.cached(timeout=3600, query_string=True)
@decorators.make_egnify_response
def get_questions_for_paper(question_paper_id):
    """
        Return list of question items.
    """
    start_time = time.time()
    query = {"questionPaperId": question_paper_id}
    projection = {field: 1 for field in ["qno", "questionNumberId"]}
    collection_name = "questions"
    kwargs = {
        "key": "qno",
        "projection": projection,
    }
    questions = mongo_db.get_results(test_database, collection_name, query, **kwargs)
    custom_headers = {"Cache-Control": "private, max-age=3600"}
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return questions, 200, custom_headers


@blueprint.route("<string:question_paper_id>/<string:question_id>")
@cache.cached(timeout=3600, query_string=True)
@decorators.make_egnify_response
# @decorators.egnify_encryption(password='EHF2AZMKGJUKVCUA8UCZ2U7S5ZJ0MGYY')
def get_question_data(question_paper_id, question_id):
    """
        Return question data.
    """
    review = request.values.get("review", "false")
    start_time = time.time()
    query = {"questionNumberId": question_id, "questionPaperId": question_paper_id}
    projection = {field: 0 for field in ["solution", "key", "skill"]}
    if review == "true":
        projection = {}
    collection_name = "questions-list"
    kwargs = {
        "key": "questionNumberId",
        "projection": projection,
    }
    question = mongo_db.get_results(test_database, collection_name, query, **kwargs)
    custom_headers = {"Cache-Control": "private, max-age=3600"}
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return question, 200, custom_headers

@blueprint.route("/bulk")
@decorators.make_egnify_response
def questions_for_motion():
    subject, collection_name = "Maths", "questions-list"
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 100, type=int)
    query = {
        "subject": subject,
        "qdb": "EQ",
    }
    kwargs = {
        "skip": skip,
        "size": size,
        "projection": {
            "_id": 0,
            "questionId": 0,
            "qdb": 0
        }
    }
    questions = mongo_db.find(test_database, collection_name, query, **kwargs)
    response = {
        "STATUS": "SUCCESS",
        "DATA": questions
    }
    return response, 200, {}
