# from datetime import datetime, timezone

# import utils.decorators as decorators
# from flask import Blueprint, current_app, request

# blueprint = Blueprint("chat", __name__, url_prefix="/chat")

# firestore = None


# @blueprint.before_app_first_request
# def init_bp():
#     """

#     """
#     global firestore
#     firestore = current_app.firestore


# @blueprint.route(
#     "/<hierarchy_id>/student/<student_id>/<live_class_id>/db/create", methods=["PUT"]
# )
# # @decorators.roles_required(["LMS_OTP_ACCESS", "OTP"])
# @decorators.make_egnify_response
# def create_custom_authentication_token_firestore_for_chat(
#     hierarchy_id: str, student_id: str, live_class_id: str
# ):
#     collection_name = "_".join(["db_chat", live_class_id])
#     collection_name = collection_name.replace("-", "").lower()
#     additional_claims = {"db": collection_name, "live_class_id": live_class_id}

#     custom_token = firestore.create_custom_authentication_token_with_claims(
#         student_id, additional_claims
#     )
#     resp = {
#         "username": student_id,
#         "token": custom_token.decode("utf-8"),
#         "databaseUrl": collection_name,
#         "live_class_id": live_class_id,
#     }
#     return resp, 200, {}
