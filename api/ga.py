import importlib
from datetime import datetime, timedelta, timezone

import pytz
import redis
from flask import Blueprint, current_app, request
from rq import Queue
from utils.egy_analytics import cache

import utils.decorators as decorators

blueprint = Blueprint("ga", __name__)

queue = generate_analysis = mongo_db = test_database = None


@blueprint.before_app_first_request
def init_bp():
    """"""
    global queue, generate_analysis, mongo_db, test_database
    redis_connection = redis.Redis.from_url(current_app.config["RQ_REDIS_URL"])
    queue = Queue(current_app.config["RQ_QUEUES"], connection=redis_connection)
    generate_analysis = importlib.import_module(current_app.config["ANALYSIS_FILE"])
    mongo_db = current_app.mongo_db
    test_database = current_app.config["TEST_DATABASE"]


@blueprint.route("/ga", methods=["POST"])
@decorators.roles_required(["TEST_CREATOR"])
@decorators.make_egnify_response
def trigger_ga_process():
    """

    :return:
    """
    return_response = {}
    data = request.json
    access_control_token = request.headers.get("accesscontroltoken")
    authorization = request.headers.get("Authorization")

    headers = {
        "authorization": authorization,
        "accesscontroltoken": access_control_token,
    }

    data["headers"] = headers

    re_generate_analysis = data.get("regenerate", False)
    test_id = data.get("test_id", None)
    query = {
        "testId": test_id,
    }
    test_info = mongo_db.find_one(test_database, "tests", query)
    if test_info:
        grace = test_info.get("gracePeriod", 0)  # 0 mins is no grace period
        start_time = test_info["startTime"]
        end_time = test_info["endTime"]
        duration = test_info["duration"]

        if grace:
            allowed_after = start_time + timedelta(minutes=duration + grace)
        else:
            allowed_after = end_time + timedelta(minutes=duration)
        allowed_after_aware = allowed_after.replace(tzinfo=pytz.UTC)

        if datetime.now(timezone.utc) < allowed_after_aware:
            return_response["STATUS"] = "FAILURE"
            return_response["JOB_ID"] = None
            return_response[
                "MESSAGE"
            ] = "Not allowed to generate analysis before completion time"
            return return_response, 400, {}
    if re_generate_analysis:
        cache.clear()
    job = queue.enqueue(
        generate_analysis.hello_gcs_generic, job_timeout="30m", args=(data,)
    )
    return_response["STATUS"] = "SUCCESS"
    return_response["JOB_ID"] = job.get_id()
    return return_response, 200, {}


# TODO: Make this generic for any task in place of just ga {}
# TODO: its better to do it stage wise.
@blueprint.route("/ga/job/<job_id>", methods=["GET"])
@decorators.roles_required(["TEST_CREATOR"])
@decorators.make_egnify_response
def get_job_status(job_id):
    """

    :param job_id:
    :return:
    """
    return_response = {}
    job = queue.fetch_job(job_id)
    if job:
        return_response["STATUS"] = "SUCCESS"
        return_response["JOB_STATUS"] = job.get_status()
        return_response["DATA"] = job.meta
        return return_response, 200, {}
    else:
        return_response["STATUS"] = "FAILURE"
        return return_response, 404, {}
