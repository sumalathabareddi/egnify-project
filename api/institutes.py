import copy
import json
import requests
from collections import OrderedDict

from bson.objectid import ObjectId
from flask import Blueprint, current_app, request, url_for
from utils.decorators import (
    make_egnify_response,
    make_egnify_data_response,
    roles_required,
    authorization_required,
)
from utils.StorageHelper import StorageHelper
from utils import md5
from utils.egy_analytics import cache


from .models import InstituteSchema, InstituteHierarchiesSchema

blueprint = Blueprint("institutes", __name__, url_prefix="/institutes")

mongo_db = arango_db = settings_database = storage_secrets = None


@blueprint.before_app_first_request
def init_bp():
    """"""
    global mongo_db, arango_db, settings_database, storage_secrets
    mongo_db = current_app.mongo_db
    arango_db = current_app.arango_db
    settings_database = current_app.config["SETTINGS_DATABASE"]
    storage_secrets = {}
    if current_app.config["STORAGE_PROVIDER"] == "GCP":
        storage_secrets["GCP_CREDENTIALS_FILE"] = current_app.config[
            "GCP_CREDENTIALS_FILE"
        ]
    if current_app.config["STORAGE_PROVIDER"] == "MINIO":
        storage_secrets["endpoint"] = current_app.config["MINIO_ENDPOINT"]
        storage_secrets["access_key"] = current_app.config["MINIO_ACCESS_KEY"]
        storage_secrets["secret_key"] = current_app.config["MINIO_SECRET_KEY"]
        storage_secrets["secure"] = True


def make_hierarchies(institute_doc):
    """Orientation hierarchy nodes for institute"""
    _id = md5(institute_doc["_id"])
    orientations = [
        x["name"] for x in institute_doc["systemHierarchies"] if x["level"] == 1
    ]
    level_code = None
    for item in institute_doc["hierarchy"]:
        if item["name"] == "Orientation":
            level_code = item["code"]
    hierarchy = []
    for orientation in orientations:
        doc = {
            "isLeafNode": False,
            "instituteId": institute_doc["_id"],
            "instituteLevelCode": level_code,
            "hierarchyName": orientation,
            "_id": md5(institute_doc["_id"] + orientation),
            "parentCode": _id,
            "level": 1,
            "hierarchyTuple": [institute_doc["instituteName"], orientation],
        }
        if not arango_db.has_node(doc["_id"]):
            arango_db.create_node(doc["_id"], doc)
        arango_db.create_edge(doc["parentCode"], doc["_id"])
        child_code = {"childCode": doc["_id"], "levelCode": level_code, "level": 1}
        hierarchy.append(child_code)
    return hierarchy


@blueprint.route("", methods=["POST"])
@roles_required(["PARTNER_ADMIN"])
@make_egnify_response
def institutes():
    """API endpoint for creating institute.
    ---
    post:
      description: Add institute details
      responses:
        201:
          description: Return institute details
          content:
            application/json:
              schema: InstituteSchema
        400:
          description: Return validation errors
          content:
            application/json:
              schema: InstituteSchema
    """
    response_data, status_code, extra_headers = {}, 201, {}
    collection_name = "institutes"
    institute_data = request.get_json()
    result = InstituteSchema().load(institute_data)
    if result.errors:
        response_data["STATUS"] = "FAILURE"
        response_data["DATA"] = result.errors
        response_data["MESSAGE"] = "Please resolve the validation errors"
        status_code = 400
    else:
        sanitized_data = OrderedDict(result.data)
        mongo_db.insert_one(settings_database, collection_name, sanitized_data)
        response_data["STATUS"] = "SUCCESS"
        response_data["DATA"] = sanitized_data
        # Add institute to graph
        _id = md5(sanitized_data["_id"])
        initial_institute_hierarchy = {
            "instituteId": sanitized_data["_id"],
            "instituteLevelCode": "",
            "hierarchyName": sanitized_data["instituteName"],
            "_id": _id,
            "parentCode": "root",
            "level": 0,
        }
        result = InstituteHierarchiesSchema().load(initial_institute_hierarchy)
        init_h = result.data
        if not arango_db.has_node(init_h["_id"]):
            arango_db.create_node(init_h["_id"], init_h)
        arango_db.create_edge(init_h["parentCode"], init_h["_id"])
        user_hierarchy = make_hierarchies(sanitized_data)
        response_data["hierarchy"] = user_hierarchy
    return response_data, status_code, extra_headers


@blueprint.route("")
@roles_required(["PARTNER_ADMIN"])
@make_egnify_response
def list_institutes():
    collection_name = "institutes"
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 10, type=int)
    search_text = request.values.get("searchText", "")
    query = {"instituteName": {"$regex": search_text, "$options": "i"}}

    institutes = mongo_db.find(
        settings_database, collection_name, query, skip=skip, size=size
    )
    count = mongo_db.count(settings_database, collection_name, query)
    resp = {"STATUS": "SUCCESS", "DATA": {"institutes": institutes, "count": count}}
    return resp, 200, {}


@blueprint.route("/<string:institute_id>", methods=["GET"])
@roles_required(
    [
        "SETTINGS_CREATOR",
        "SETTINGS_READER",
        "TEST_VIEWER",
        "TEST_CREATOR",
        "REPORTS_VIEWER",
        "ANALYSIS_VIEWER",
        "STUDENT",
        "TEACHER",
        "PAPER_CREATOR",
    ]
)
@cache.cached(timeout=7200)
@make_egnify_data_response
def get_institute_info(institute_id):
    """API endpoint to read the institute info.
    ---
    get:
      description: Get institute details
      responses:
        200:
          description: Return institute details
          content:
            application/json:
              schema: InstituteSchema
    """
    status_code, extra_headers = 200, {}
    collection_name = "institutes"
    query = {"_id": institute_id}
    institute_details = mongo_db.find_one(settings_database, collection_name, query)
    if institute_details:
        institute_details["rootCode"] = md5(institute_details["_id"])
    return institute_details, status_code, extra_headers


@blueprint.route("/<string:institute_id>", methods=["PUT"])
@roles_required(["SETTINGS_CREATOR"])
@make_egnify_response
def update_institute_info(institute_id):
    """API endpoint to update the institute info.
    ---
    put:
      description: Update institute details
      responses:
        200:
          description: Return institute details update status
          content:
            application/json:
              schema: InstituteSchema
        400:
          description: Return validation errors
          content:
            application/json:
              schema: InstituteSchema
    """
    response_data, status_code, extra_headers = {}, 200, {}
    collection_name = "institutes"
    query = {"_id": institute_id}

    institute_data = request.get_json()
    result = InstituteSchema().load(institute_data)
    if result.errors:
        response_data["STATUS"] = "FAILURE"
        response_data["DATA"] = result.errors
        status_code = 400
    else:
        sanitized_data = OrderedDict(result.data)
        mongo_db.upsert(settings_database, collection_name, query, sanitized_data)
        url = url_for(
            "institutes.get_institute_info",
            institute_id=institute_id,
        )
        cache.delete(f"view/{url}")
        response_data = {
            "STATUS": "SUCCESS",
            "DATA": None,
            "MESSAGE": "Updated institute successfully",
        }
    return response_data, status_code, extra_headers


@blueprint.route("/<string:institute_id>/logo", methods=["POST"])
@make_egnify_response
@roles_required(["SETTINGS_CREATOR"])
def update_institute_logo(institute_id):
    """Updates institute logo"""
    uploaded_file = request.files.get("file")
    uploaded_file_string = uploaded_file.read()
    storage_provider = current_app.config["STORAGE_PROVIDER"]
    bucket_names = {
        "GCP": current_app.config["CLOUD_STORAGE_BUCKET"],
        "MINIO": current_app.config["MINIO_STORAGE_BUCKET"],
    }
    data = {
        "file_ext": ".png",
        "file_name": f"/institute-logos/{institute_id}",
        "acl_type": "publicRead",
        "data": uploaded_file_string,
        "content_type": uploaded_file.content_type,
        "bucket_name": bucket_names[storage_provider],
    }

    storage_helper = StorageHelper(storage_provider, storage_secrets)

    url = storage_helper.upload(data, get_public_url=True)

    resp = {"STATUS": "SUCCESS", "DATA": url, "MESSAGE": "Uploaded logo successfully"}
    return resp, 200, {}
