from flask import Blueprint, current_app, request

import utils.decorators as decorators
from utils.egy_analytics import cache
from db.HydraCacheMongoDb import HydraCacheMongoDb

blueprint = Blueprint("auth", __name__, url_prefix="/auth")

mongo_db = test_database = cached_mongo_db = None


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global mongo_db, test_database, cached_mongo_db
    mongo_db = current_app.mongo_db
    test_database = current_app.config["TEST_DATABASE"]
    cached_mongo_db = HydraCacheMongoDb(
        mongo_db, test_database, "tests", projection={"accessTag": 1, "testId": 1}
    )


def get_cached_mongo_db():
    """

    :return:
    """
    return cached_mongo_db


@blueprint.route("/<hierarchy_id>/me")
@decorators.authorization_required()
@cache.cached(timeout=86400)
@decorators.make_egnify_response
def has_access(hierarchy_id):
    """
        Returns 200 if the callee has permissions to the hierarchy id provided else return 403/401
        This ignores the hierarchy_id passed it will be used by authorization required decorator
    """
    return {}, 200, {}
