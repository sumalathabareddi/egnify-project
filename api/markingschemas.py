from flask import Blueprint, current_app, request, url_for
from utils.decorators import (
    make_egnify_response,
    make_egnify_data_response,
    roles_required,
    authorization_required,
)
from utils.egy_analytics import cache
from bson.objectid import ObjectId

from .models.markingschemas import MarkingSchema

blueprint = Blueprint("markingschemas", __name__)

mongo_db = settings_database = None


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global mongo_db, settings_database
    mongo_db = current_app.mongo_db
    settings_database = current_app.config["SETTINGS_DATABASE"]


@blueprint.route("/<institute_id>/markingschemas", methods=["POST"])
@roles_required(["SETTINGS_CREATOR"])
@authorization_required()
@make_egnify_response
def insert_marking_schema(institute_id):
    """Add marking schemas.
    ---
    post:
      description: Add marking schemas
      responses:
        201:
          description: Return add marking schema status
          content:
            application/json:
              schema: MarkingSchema
        400:
          description: Return validation errors
          content:
            application/json:
              schema: MarkingSchema
    """
    response_data, status_code, extra_headers = {}, 200, {}
    collection_name = "markingschemas"
    post_data = request.get_json()
    result = MarkingSchema().load(post_data)
    if result.errors:
        response_data["STATUS"] = "FAILURE"
        response_data["DATA"] = result.errors
        status_code = 400
    else:
        new_schema = result.data
        query = {
            "instituteId": institute_id,
            "customSchema": True,
            "active": True,
            "name": new_schema.name,
        }
        name_used_prev = mongo_db.find(settings_database, collection_name, query)
        if name_used_prev:
            response_data = {
                "STATUS": "FAILURE",
                "DATA": None,
                "MESSAGE": f"Name of the marking schema needs to be unique.",
            }
            return response_data, 400, {}

        query = {"instituteId": institute_id, "customSchema": True, "active": True}
        prev_marking_schemas = mongo_db.find(settings_database, collection_name, query)
        for item in prev_marking_schemas:
            schema_item = MarkingSchema().load(item).data
            if new_schema == schema_item:
                response_data = {
                    "STATUS": "FAILURE",
                    "DATA": None,
                    "MESSAGE": f"Duplicate marking schema found with name {item['name']}",
                }
                return response_data, 400, {}
        post_data["instituteId"] = institute_id
        post_data["customSchema"] = True
        post_data["active"] = True
        post_data["_id"] = str(ObjectId())
        mongo_db.insert_one(settings_database, collection_name, post_data)
        url = url_for(
            "markingschemas.markingschemas",
            institute_id=institute_id,
            orientation_id=post_data["orientationId"],
        )
        cache.delete(f"view/{url}")
        response_data = {
            "STATUS": "SUCCESS",
            "DATA": None,
            "MESSAGE": "Created markingschemas successfully",
        }

    return response_data, status_code, extra_headers


@blueprint.route(
    "/<string:institute_id>/markingschemas/<string:schema_id>", methods=["DELETE"]
)
@roles_required(["SETTINGS_CREATOR"])
@authorization_required()
@make_egnify_data_response
def archive_marking_schema(institute_id, schema_id):
    response_data, status_code, extra_headers = {}, 200, {}
    collection_name = "markingschemas"
    query = {"_id": schema_id, "instituteId": institute_id, "customSchema": True}
    update = {"active": False}
    return_document = mongo_db.find_one_and_upsert(
        settings_database, collection_name, query, update, upsert=False
    )
    if return_document:
        url = url_for(
            "markingschemas.markingschemas",
            institute_id=institute_id,
            orientation_id=return_document["orientationId"],
        )
        cache.delete(f"view/{url}")
    response_data["STATUS"] = "SUCCESS"
    return response_data, status_code, extra_headers


@blueprint.route("/<string:institute_id>/markingschemas/<string:orientation_id>")
@roles_required(
    [
        "SETTINGS_READER",
        "SETTINGS_CREATOR",
        "TEST_VIEWER",
        "TEST_CREATOR",
        "ANALYSIS_VIEWER",
        "REPORTS_VIEWER",
        "STUDENT",
    ]
)
# @cache.cached(timeout=604800)
@make_egnify_data_response
def markingschemas(institute_id, orientation_id):
    """GET marking schemas for orientation.
    ---
    get:
      description: Get list of marking schemas for orientation
      responses:
        200:
          description: Return list of marking schemas for orientation
          content:
            application/json:
              schema: MarkingSchema
    """
    collection_name = "markingschemas"
    query = {
        "orientationId": orientation_id,
        "active": True,
        "$or": [{"instituteId": {"$exists": False}}, {"instituteId": institute_id}],
    }
    markingschemas = mongo_db.find(settings_database, collection_name, query)
    print(len(markingschemas))
    return markingschemas, 200, {}


@blueprint.route("/<string:institute_id>/markingschema/<string:schema_id>")
@roles_required(
    [
        "SETTINGS_READER",
        "SETTINGS_CREATOR",
        "TEST_VIEWER",
        "TEST_CREATOR",
        "ANALYSIS_VIEWER",
        "REPORTS_VIEWER",
        "STUDENT",
    ]
)
@cache.cached(timeout=604800)
@make_egnify_data_response
@authorization_required()
def markingschema(institute_id, schema_id):
    """GET marking schemas for pattern.
    ---
    get:
      description: Get list of marking schemas for pattern
      responses:
        200:
          description: Return list of marking schemas for pattern
          content:
            application/json:
              schema: MarkingSchema
    """
    collection_name = "markingschemas"
    query = {"_id": schema_id}
    marking_schema = mongo_db.find_one(settings_database, collection_name, query)
    return marking_schema, 200, {}
