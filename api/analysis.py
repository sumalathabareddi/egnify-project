import time

from flask import Blueprint, current_app, request

import utils.decorators as decorators
from utils.egy_analytics import cache
from db.HydraCacheMongoDb import HydraCacheMongoDb

blueprint = Blueprint("analysis", __name__, url_prefix="/analysis")

mongo_db = ga_database = cached_mongo_db = None


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global mongo_db, ga_database, cached_mongo_db
    mongo_db = current_app.mongo_db
    ga_database = current_app.config["GA_DATABASE"]
    cached_mongo_db = HydraCacheMongoDb(
        mongo_db,
        ga_database,
        current_app.config["MASTER_RESULTS_TABLE"],
        projection={"accessTag": 1, "studentId": 1, "egnifyId": 1},
    )


def should_cache(*args, **kwargs):
    """

    :param args:
    :param kwargs:
    :return:
    """
    return request.method != "GET"


def get_cached_mongo_db():
    """

    :return:
    """
    return cached_mongo_db


@blueprint.route("/<hierarchy_id>/test/<test_id>/mark")
@decorators.roles_required(["ANALYSIS_VIEWER", "REPORTS_VIEWER"])
@decorators.authorization_required()
@cache.cached(timeout=86400, query_string=True)
@decorators.make_egnify_data_response
def get_mark_analysis_for_hierarchy(hierarchy_id, test_id):
    """
        Returns the mark analysis of students for given test and hierarchy.
    """
    _id = "_".join([hierarchy_id, test_id])
    start_time = time.time()
    collection_name = "markAnalysis"
    students = mongo_db.read_by_id(ga_database, collection_name, [_id])
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )

    custom_headers = {"Cache-Control": "private, max-age=14400"}
    return students.get(_id, None), 200, custom_headers


@blueprint.route("/<hierarchy_id>/student/<student_id>/test/<test_id>/rank")
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER", "STUDENT"])
# @decorators.authorization_on_student(cached_mongo_db=get_cached_mongo_db)
@cache.cached(timeout=86400)
@decorators.make_egnify_data_response
def get_rank_analysis_for_student(hierarchy_id, student_id, test_id):
    """
        Returns the rank analysis of a student for given test and hierarchy.
    """
    _id = "_".join([hierarchy_id, test_id, student_id])
    start_time = time.time()
    collection_name = "rankAnalysis"
    student_rank_data = mongo_db.read_by_id(
        ga_database, collection_name, [_id], projection={"hierarchyLevels": 0}
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    custom_headers = {"Cache-Control": "private, max-age=14400"}
    return student_rank_data.get(_id, None), 200, custom_headers


@blueprint.route("/<hierarchy_id>/test/<test_id>/<int:rank>/rank", methods=["GET"])
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER", "STUDENT"])
@decorators.authorization_required(back_propagation=True)
@cache.cached(timeout=86400, query_string=True, unless=should_cache)
@decorators.make_egnify_data_response
def get_rank_analysis_for_top_students(hierarchy_id, test_id, rank):
    """
        Returns the rank analysis of students for given test and hierarchy.
    """
    start_time = time.time()
    subject = request.values.get("subject", "overall")
    collection_name = "rankAnalysis"
    sort_key = ".".join([collection_name, subject, "rank"])
    query = {"testId": test_id, "hierarchy": hierarchy_id, sort_key: rank}
    kwargs = {
        "projection": {"name": 0, "student": 0},
    }
    current_app.logger.info("Query that is being executed : {}".format(query))
    students = mongo_db.find_one(ga_database, collection_name, query, **kwargs)
    kwargs = {
        "projection": {
            "name": 0,
            "studentId": 0,
            "egnifyId": 0,
            "_id": 0,
            "id": 0,
            "hierarchyLevels": 0,
            "accessTag": 0,
            "studentMetaData": 0,
            "rankOrder": 0,
        },
    }
    if students:
        egnify_id = students["egnifyId"]
        _id = "_".join([test_id, egnify_id])
        students = mongo_db.find_one(
            ga_database,
            current_app.config["MASTER_RESULTS_TABLE"],
            {"_id": _id},
            **kwargs,
        )
        behaviour_data = mongo_db.find_one(
            ga_database, "behaviourData", {"_id": _id}, **kwargs
        )
        if behaviour_data:
            students["behaviourData"] = behaviour_data.get("behaviourData", None)
        students["hierarchy"] = hierarchy_id
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    custom_headers = {"Cache-Control": "private, max-age=14400"}
    return students, 200, custom_headers


@blueprint.route("/<hierarchy_id>/test/<test_id>/data")
@decorators.roles_required(
    ["ANALYSIS_VIEWER", "REPORTS_VIEWER", "STUDENT", "CMS_PERFORMANCE_VIEWER"]
)
@decorators.authorization_required(hierarchy_query=True)
@cache.cached(timeout=86400)
@decorators.make_egnify_data_response
def get_hierarchical_analysis_data(hierarchy_id, test_id):
    """
        Returns the hierarchical analysis data for test.
        Topic wise analysis and question wise analysis at given hierarchy.
    """
    _id = "_".join([hierarchy_id, test_id])
    start_time = time.time()
    collection_name = "hierarchicalAnalysis"
    student_rank = mongo_db.read_by_id(ga_database, collection_name, [_id])
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    custom_headers = {"Cache-Control": "private, max-age=14400"}
    return student_rank.get(_id, None), 200, custom_headers


@blueprint.route("/<hierarchy_id>/test/<test_id>/rank", methods=["GET", "POST"])
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_required(hierarchy_query=True)
@cache.cached(timeout=86400, query_string=True, unless=should_cache)
@decorators.make_egnify_data_response
def get_rank_analysis_for_students(hierarchy_id, test_id):
    """
        Returns the rank analysis of students for given test and hierarchy.
    """
    start_time = time.time()
    query = prepare_query_for_rank(hierarchy_id, test_id)
    collection_name = "rankAnalysis"
    size = request.values.get("size", 20, type=int)
    skip = request.values.get("skip", 0, type=int)
    subject = request.values.get("subject", "overall")
    sort_key = ".".join([collection_name, subject, "rank"])
    kwargs = {
        "skip": skip,
        "size": size,
        "sort_key": sort_key,
        "projection": {"_id": 1, "student": 1, "egnifyId": 1, "name": 1, sort_key: 1},
    }
    current_app.logger.info("Query that is being executed : {}".format(query))
    students = mongo_db.find(ga_database, collection_name, query, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    custom_headers = {"Cache-Control": "private, max-age=14400"}
    return students, 200, custom_headers


@blueprint.route("/<hierarchy_id>/test/<test_id>/rank/count", methods=["GET", "POST"])
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_required(hierarchy_query=True)
@cache.cached(timeout=86400, query_string=True, unless=should_cache)
@decorators.make_egnify_data_response
def get_rank_analysis_count_for_students(hierarchy_id, test_id):
    """

    :param hierarchy_id:
    :param test_id:
    :return:
    """
    start_time = time.time()
    collection_name = "rankAnalysis"
    query = prepare_query_for_rank(hierarchy_id, test_id)
    total_entries = mongo_db.count(ga_database, collection_name, query)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    custom_headers = {"Cache-Control": "private, max-age=14400"}
    return total_entries, 200, custom_headers


@blueprint.route("/<hierarchy_id>/test/<test_id>/cwu", methods=["GET", "POST"])
@decorators.roles_required(
    ["REPORTS_VIEWER", "ANALYSIS_VIEWER", "CMS_PERFORMANCE_VIEWER"]
)
@decorators.authorization_required(hierarchy_query=True)
@cache.cached(timeout=86400, query_string=True, unless=should_cache)
@decorators.make_egnify_data_response
def get_cwu_details_for_test(hierarchy_id, test_id):
    """
        Returns the CWU analysis for test.
    """
    hierarchies = request.values.get("hierarchy", None)
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 20, type=int)
    start_time = time.time()
    collection_name = current_app.config["MASTER_RESULTS_TABLE"]

    query = {"testId": test_id}
    hierarchies = hierarchies if hierarchies else hierarchy_id
    if hierarchies:
        h_list = hierarchies.split(",")
        query["hierarchyLevels"] = {"$in": h_list}
    kwargs = {
        "sort_key": "cwuAnalysis.overall.C",
        "skip": skip,
        "size": size,
        "ascending": False,
        "projection": {"_id": 1, "studentId": 1, "egnifyId": 1, "name": 1},
    }
    students = mongo_db.find(ga_database, collection_name, query, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    custom_headers = {"Cache-Control": "private, max-age=14400"}
    return students, 200, custom_headers


@blueprint.route("/<hierarchy_id>/test/<test_id>/error", methods=["GET", "POST"])
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_required(hierarchy_query=True)
@cache.cached(timeout=86400, query_string=True, unless=should_cache)
@decorators.make_egnify_data_response
def get_error_details_for_test(hierarchy_id, test_id):
    """
        Returns the error analysis for test.
    """
    hierarchies = request.values.get("hierarchy", None)
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 20, type=int)
    start_time = time.time()
    collection_name = current_app.config["MASTER_RESULTS_TABLE"]

    query = {"testId": test_id}
    hierarchies = hierarchies if hierarchies else hierarchy_id
    if hierarchies:
        h_list = hierarchies.split(",")
        query["hierarchyLevels"] = {"$in": h_list}
    kwargs = {
        "sort_key": "cwuAnalysis.overall.marks.UW",
        "skip": skip,
        "size": size,
        "ascending": True,
        "projection": {"_id": 1, "studentId": 1, "egnifyId": 1, "name": 1},
    }

    students = mongo_db.find(ga_database, collection_name, query, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    custom_headers = {"Cache-Control": "private, max-age=14400"}
    return students, 200, custom_headers


@blueprint.route("/<hierarchy_id>/test/<test_id>/error/<question_id>", methods=["GET"])
@decorators.roles_required(["ANALYSIS_VIEWER"])
@decorators.authorization_required(hierarchy_query=True)
@cache.cached(timeout=86400, query_string=True)
@decorators.make_egnify_data_response
def get_student_response_for_question_in_test(hierarchy_id, test_id, question_id):
    """"
        Returns the error analysis for test.
    """
    question_response = request.values.get("response", "W")
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 20, type=int)
    start_time = time.time()
    collection_name = current_app.config["MASTER_RESULTS_TABLE"]
    hierarchies = request.values.get("hierarchy", hierarchy_id)
    hierarchies_list = hierarchies.split(",")

    query = {
        "testId": test_id,
        "hierarchyLevels": {"$in": hierarchies_list},
        "$or": [],
    }
    for question_resp in question_response.split(","):
        query_key = ".".join(
            ["responseData", "questionResponse", question_id, question_resp]
        )
        query["$or"].append({query_key: {"$exists": True}})
    kwargs = {
        "sort_key": "cwuAnalysis.overall.marks.UW",
        "skip": skip,
        "size": size,
        "projection": {"testId": 1, "studentId": 1, "egnifyId": 1},
        "ascending": True,
    }

    data = mongo_db.find(ga_database, collection_name, query, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    custom_headers = {"Cache-Control": "private, max-age=14400"}
    return data, 200, custom_headers


@blueprint.route(
    "/<hierarchy_id>/test/<test_id>/error/<question_id>/count", methods=["GET"]
)
@decorators.roles_required(["ANALYSIS_VIEWER"])
@decorators.authorization_required(hierarchy_query=True)
@cache.cached(timeout=86400, query_string=True)
@decorators.make_egnify_data_response
def get_student_response_count_for_question_in_test(hierarchy_id, test_id, question_id):
    """"
        Returns the error analysis count data for test.
    """
    question_response = request.values.get("response", "W")
    start_time = time.time()
    collection_name = current_app.config["MASTER_RESULTS_TABLE"]
    hierarchies = request.values.get("hierarchy", hierarchy_id)
    hierarchies_list = hierarchies.split(",")

    query = {
        "testId": test_id,
        "hierarchyLevels": {"$in": hierarchies_list},
        "$or": [],
    }
    for question_resp in question_response.split(","):
        query_key = ".".join(
            ["responseData", "questionResponse", question_id, question_resp]
        )
        query["$or"].append({query_key: {"$exists": True}})

    student_count = mongo_db.count(ga_database, collection_name, query)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    custom_headers = {"Cache-Control": "private, max-age=14400"}
    return student_count, 200, custom_headers


@blueprint.route("/<hierarchy_id>/students/count", methods=["GET", "POST"])
@decorators.roles_required(["ANALYSIS_VIEWER"])
@decorators.authorization_required(hierarchy_query=True)
@cache.cached(timeout=86400, query_string=True, unless=should_cache)
@decorators.make_egnify_data_response
def get_unique_students_count_for_multi_test(hierarchy_id):
    """
        Return total unique students among multiple tests.
    """
    hierarchies = request.values.get("hierarchy", None)
    test_ids = request.values.get("testIds", None)
    collection_name = "rankAnalysis"

    query = {"hierarchy": hierarchy_id}
    tests_query = {}
    hierarchies_query = {}
    if not test_ids:
        return "", 404, {}
    tests_query["$or"] = []
    for test_id in test_ids.split(","):
        tests_query["$or"].append({"testId": test_id})
    if hierarchies:
        h_list = hierarchies.split(",")
        hierarchies_query["hierarchyLevels"] = {"$in": h_list}
    query["$and"] = [tests_query, hierarchies_query]

    total_entries = mongo_db.distinct(ga_database, collection_name, "student", query)
    custom_headers = {"Cache-Control": "private, max-age=14400"}
    return total_entries, 200, custom_headers


def prepare_query_for_rank(hierarchy_id, test_id):
    """
        Helper function for rank analysis query preparation.
    """

    hierarchies = request.values.get("hierarchy", None)
    minMarks = request.values.get("min", None)
    maxMarks = request.values.get("max", None)
    subject = request.values.get("subject", "overall")
    percentage = request.values.get("percentage", None)
    subjects_percentage = request.values.get("subjectsPercentage", None)
    subjects_order = request.values.get("subjectsOrder", None)
    minPercentage = request.values.get("minPercentage",None)
    maxPercentage = request.values.get("maxPercentage",None)


    collection_name = "rankAnalysis"
    range_query = {}
    subjects_order_query = {}
    hierarchies_query = {}

    if percentage:
        # for filtering students who have scored overall >= x%
        percentage = float(percentage)
        range_query["rankAnalysis.overall.percentage"] = {"$gte": percentage}
    elif minMarks or maxMarks:
        # for filtering students on obtainedMarks
        query_key = ".".join([collection_name, subject, "obtainedMarks"])
        range_query[query_key] = {}
        if minMarks:
            minMarks = int(minMarks)
            range_query[query_key]["$gte"] = minMarks
        if maxMarks:
            maxMarks = int(maxMarks)
            range_query[query_key]["$lte"] = maxMarks

    if subjects_percentage and subjects_order:
        # for filtering students who have scored < x% in either of subjects
        subjects_order_query["$or"] = []
        subjects_percentage = float(subjects_percentage)
        for subject in subjects_order.split(","):
            query_key = ".".join([collection_name, subject, "percentage"])
            subjects_order_query["$or"].append(
                {query_key: {"$lt": subjects_percentage}}
            )
    if minPercentage:
         minPercentage = float(minPercentage)
         range_query["rankAnalysis.overall.percentage"] = {"$gte": minPercentage}
    if maxPercentage:
        maxPercentage = float(maxPercentage)
        range_query["rankAnalysis.overall.percentage"] = {"$lte": maxPercentage}
    if minPercentage and maxPercentage:
        minPercentage = float(minPercentage)
        maxPercentage = float(maxPercentage)
        range_query["rankAnalysis.overall.percentage"] = {"$gte": minPercentage,"$lte": maxPercentage}

    if hierarchies:
        h_list = hierarchies.split(",")
        hierarchies_query["hierarchyLevels"] = {"$in": h_list}

    and_list = []
    if range_query:
        and_list.append(range_query)
    if subjects_order_query:
        and_list.append(subjects_order_query)
    if hierarchies_query:
        and_list.append(hierarchies_query)
    query = {"testId": test_id, "hierarchy": hierarchy_id}
    if len(and_list) >= 2:
        query["$and"] = and_list
    elif and_list:
        query.update(and_list[0])

    return query
