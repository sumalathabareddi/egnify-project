from flask import Blueprint, current_app, request
from utils.decorators import (
    make_egnify_response,
    roles_required,
    authorization_required,
)
from datetime import datetime, timedelta, timezone
from utils.EgnifyException import MultipleValidationExceptions, ValidationException
from utils.egy_analytics import cache
from utils.TimeHelper import TimeHelper
from bson.objectid import ObjectId
import time
from .models import AssetScheduleSchema

# from notifications import pushnotification, util as meteoroid_util


blueprint = Blueprint("learn/asset", __name__, url_prefix="/learn/asset")

mongo_db = arango_db = settings_database = None


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global mongo_db, arango_db, settings_database
    mongo_db = current_app.mongo_db
    arango_db = current_app.arango_db
    settings_database = current_app.config["SETTINGS_DATABASE"]


def prepare_schedule_data(sanitized_data):
    """

    :param sanitized_data:
    """
    sanitized_data["accessTag"] = {"hierarchy": sanitized_data["hierarchy"]}
    start_time = sanitized_data["startTime"]
    start_time = datetime.strptime(start_time, "%Y-%m-%d %H:%M")
    sanitized_data["startTime"] = TimeHelper.to_utc(start_time, "Asia/Kolkata")

    end_time = sanitized_data["endTime"]
    end_time = datetime.strptime(end_time, "%Y-%m-%d %H:%M")
    sanitized_data["endTime"] = TimeHelper.to_utc(end_time, "Asia/Kolkata")


@blueprint.route("/schedule/<institute_id>", methods=["POST"])
@roles_required(["ASSET_SCHEDULE_CREATOR"])
@authorization_required()
@make_egnify_response
def create_schedule(institute_id):
    """Scheduling item for assets.
    ---
    post:
      description: Scheduling item for assets
      responses:
        201:
          description: Return created documents
          content:
            application/json:
    """

    response_data, status_code, extra_headers = {}, 200, {}

    post_array = request.get_json()  # read the json data from client request
    final_data = []
    for post_data in post_array:

        post_data["instituteId"] = institute_id
        if "_id" not in post_data:
            post_data["_id"] = str(ObjectId())
        result = AssetScheduleSchema().load(post_data)  # load for validations

        if result.errors:
            response_data = result.errors
            status_code = 400
            break

        else:
            sanitized_data = result.data
            prepare_schedule_data(sanitized_data)
            final_data.append(sanitized_data)

    mongo_db._upsert_many(
        connection=None,
        db_name=settings_database,
        collection_name="assetschedule",
        obj=final_data,
        id_field="_id",
        upsert=True,
    )

    # mongo_db.insert_many(settings_database, "assetschedule", final_data)
    response_data = final_data

    ##Notification trigger
    # access_control_token = request.headers.get("accesscontroltoken")
    # authorization = request.headers.get("Authorization")
    # headers = {
    #     "authorization": authorization,
    #     "accesscontroltoken": access_control_token,
    # }
    # hierarchies = post_array[0]["hierarchy"]
    # student_ids = meteoroid_util.get_student_ids_for_hierarchies(hierarchies)
    # notification_body = f"A new schedule has been created for you, tap to check it out."
    # notification_obj = {
    #     "header": "Schedule Alert",
    #     "body": notification_body,
    #     "studentIds": student_ids,
    # }
    # pushnotification.sendPushNotification(notification_obj, headers)

    status_code = 201
    return response_data, status_code, extra_headers


@blueprint.route("/schedule/<hierarchy_id>/<date>", methods=["GET"])
@roles_required(["STUDENT"])
@authorization_required()
@make_egnify_response
def hierarchy_wise_schedule_list(hierarchy_id, date):
    """
    """
    parent_nodes = arango_db.get_parent(hierarchy_id, height=20)
    hierachies = list(map(lambda x: x["id"], parent_nodes["vertices"]))
    hierachies.append(hierarchy_id)

    start_time = TimeHelper.to_utc(datetime.strptime(date, "%Y-%m-%d"), "Asia/Kolkata")
    end_time = start_time + timedelta(days=1)

    query = {
        "accessTag.hierarchy": {"$in": hierachies},
        "active": True,
        "startTime": {"$gte": start_time},
        "endTime": {"$lt": end_time},
    }

    if request.values.get("patternId", None):
        query["patternId"] = {"$in": request.values.get("patternId", None).split(",")}

    if request.values.get("subject", None):
        query["subject"] = request.values.get("subject", None)

    if request.values.get("topic", None):
        query["topic"] = request.values.get("topic", None)

    if request.values.get("categoryId", None):
        query["categoryId"] = {"$in": request.values.get("categoryId", None).split(",")}

    projection = {field: 0 for field in ["accessTag"]}
    kwargs = {
        "key": "_id",
        "sort_key": "startTime",
        "ascending": True,
        "projection": projection,
    }
    start_time = time.time()
    collection_name = "assetschedule"
    data = mongo_db.get_results(settings_database, collection_name, query, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return data, 200, {}


def _build_schedule_query(date):

    start_time = TimeHelper.to_utc(datetime.strptime(date, "%Y-%m-%d"), "Asia/Kolkata")
    end_time = start_time + timedelta(days=1)

    query = {
        "active": True,
        "startTime": {"$gte": start_time},
        "endTime": {"$lt": end_time},
        "accessTag.hierarchy": {"$in": []},
    }

    if request.values.get("orientationId", None):
        query["orientationId"] = {
            "$in": request.values.get("orientationId", None).split(",")
        }

    if request.values.get("patternId", None):
        query["patternId"] = {"$in": request.values.get("patternId", None).split(",")}

    if request.values.get("subject", None):
        query["subject"] = request.values.get("subject", None)

    if request.values.get("topic", None):
        query["topic"] = request.values.get("topic", None)

    hierarchies = request.values.get("hierarchies", None)
    if hierarchies:
        childrens = []
        hierarchies = hierarchies.split(",")
        for hierarchy in hierarchies:
            parent_nodes = arango_db.get_parent(hierarchy, height=20)
            node_hierachies = [v["id"] for v in parent_nodes["vertices"]]
            childrens = childrens + node_hierachies
            children_nodes = arango_db.get_children(hierarchy, depth=20)
            node_hierachies = [v["id"] for v in children_nodes["vertices"]]
            childrens = childrens + node_hierachies
        query["accessTag.hierarchy"] = {"$in": childrens}

    return query


@blueprint.route("/schedule/<date>", methods=["GET"])
@roles_required(["ASSET_SCHEDULE_CREATOR"])
@authorization_required()
@make_egnify_response
def get_schedules(date):
    """
        Return list of scheduled items.
    """
    start_time = time.time()
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 10, type=int)
    query = _build_schedule_query(date)
    collection_name = "assetschedule"
    projection = {field: 0 for field in ["accessTag"]}
    kwargs = {
        "key": "_id",
        "sort_key": "startTime",
        "skip": skip,
        "size": size,
        "ascending": True,
        "projection": projection,
    }
    data = mongo_db.get_results(settings_database, collection_name, query, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    resp = {"STATUS": "SUCCESS", "DATA": data, "MESSAGE": ""}
    return resp, 200, {}


@blueprint.route("/schedule/<date>/count", methods=["GET"])
@roles_required(["ASSET_SCHEDULE_CREATOR"])
@authorization_required()
@make_egnify_response
def get_schedule_count(date):
    """"
        Return count of scheduled items.
    """
    start_time = time.time()
    query = _build_schedule_query(date)
    collection_name = "assetschedule"
    count = mongo_db.count(settings_database, collection_name, query)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    resp = {"STATUS": "SUCCESS", "DATA": count, "MESSAGE": ""}
    return resp, 200, {}
