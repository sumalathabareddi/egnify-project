import json
import random
import time
import threading
from collections import OrderedDict
from datetime import datetime, timezone, timedelta
from pytz import timezone as tmz
# from notifications import pushnotification, util as meteoroid_util

from parser.Parser import (
    RequestValidator,
    convert_doc_to_pdf,
    create_questions_un_mapped,
    update_test_details,
)

import pymongo
import requests
import utils.decorators as decorators
from bson.objectid import ObjectId
from db.HydraCacheMongoDb import HydraCacheMongoDb
from flask import Blueprint, current_app, request
from utils import reorder_schema
from utils.EgnifyRole import EgnifyRole
from utils.egy_analytics import cache
from utils.StorageHelper import StorageHelper
from aggregation import Aggregation
from utils import deep_defaultdict
from utils.EgnifyException import MultipleValidationExceptions, ValidationException

from notifications import mail

from .schedule import schedule_task

blueprint = Blueprint("questionsV2", __name__, url_prefix="/questions/v2")

mongo_db = (
    test_database
) = (
    settings_database
) = encryption_key = cached_mongo_db = storage_secrets = arango_db = None


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global mongo_db, test_database, settings_database, encryption_key, cached_mongo_db, storage_secrets, arango_db
    mongo_db = current_app.mongo_db
    test_database = current_app.config["TEST_DATABASE"]
    settings_database = current_app.config["SETTINGS_DATABASE"]
    encryption_key = current_app.config["ENCRYPTION_KEY"]
    cached_mongo_db = HydraCacheMongoDb(mongo_db, test_database, "tests")
    arango_db = current_app.arango_db
    storage_secrets = {}
    if current_app.config["STORAGE_PROVIDER"] == "GCP":
        storage_secrets["GCP_CREDENTIALS_FILE"] = current_app.config[
            "GCP_CREDENTIALS_FILE"
        ]
    if current_app.config["STORAGE_PROVIDER"] == "MINIO":
        storage_secrets["endpoint"] = current_app.config["MINIO_ENDPOINT"]
        storage_secrets["access_key"] = current_app.config["MINIO_ACCESS_KEY"]
        storage_secrets["secret_key"] = current_app.config["MINIO_SECRET_KEY"]
        storage_secrets["secure"] = True


def get_cache():
    """
    :return:
    """
    return cache


def get_cached_mongo_db():
    """

    :return:
    """
    return cached_mongo_db


@blueprint.route("/<string:test_id>/<string:question_paper_id>/questions")
@decorators.roles_required(
    [EgnifyRole.TEST_CREATOR, "PAPER_CREATOR", EgnifyRole.STUDENT, EgnifyRole.TEACHER]
)
@decorators.authorization_required(
    cached_mongo_db=get_cached_mongo_db, back_propagation=True
)
@cache.cached(timeout=7200, query_string=True)
@decorators.make_egnify_response
def get_questions(test_id, question_paper_id):
    """
        This will be called to get the questions
        from the database for a test and question paper
        this will have an authorization of the test
        TODO: We need to remove the earlier used questions
        for a test and question paper id
    """
    start_time = time.time()
    subject = request.values.get("subject", None)
    topic = request.values.get("topic", None)
    sub_topic = request.values.get("subTopic", "")
    question_type = request.values.get("questionType", "")
    difficulty = request.values.get("difficulty", "")
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 30, type=int)
    duplicate = request.values.get("duplicate", False, type=bool)
    sort_key = request.values.get("sort", None)
    sort_type = request.values.get("type", None)

    query = {}
    if subject:
        query["subject"] = subject
    if topic:
        query["topic"] = topic
    if sub_topic:
        query["subTopic"] = {"$in": sub_topic.split("$")}
    if question_type:
        query["questionType"] = {"$in": question_type.split("$")}
    if difficulty:
        query["difficulty"] = {"$in": difficulty.split("$")}

    query["hide"] = {"$ne": True}
    query["parsed"] = {"$ne": True}

    egn_query = {"qdb": "EQ"}
    egn_query.update(query)
    cnt = mongo_db.count(
        current_app.config["QUESTION_BANK_DB"], "questions-list", egn_query
    )
    if cnt > skip:
        query["qdb"] = "EQ"
    else:
        query["qdb"] = {"$ne": "EQ"}
        skip -= cnt

    sort = [
        ("isSheet", pymongo.ASCENDING),
        ("isFRM", pymongo.ASCENDING),
        ("questionTypeMetaData.paragraph", pymongo.ASCENDING),
    ]
    if sort_key and sort_type:
        if sort_type == "asc":
            sort.append((sort_key, pymongo.ASCENDING))
        else:
            sort.append((sort_key, pymongo.DESCENDING))

    # This is a custom mongo query as we need more customization
    if not duplicate:
        questions_list_used_previously = _get_previously_used_questions(test_id)
        query["questionNumberId"] = {"$nin": questions_list_used_previously}
        questions = (
            mongo_db.connection[current_app.config["QUESTION_BANK_DB"]]
            .get_collection("questions-list")
            .find(query, {"questionNumberId": 1})
            .sort(sort)
            .skip(skip)
            .limit(size)
        )
    else:
        questions = (
            mongo_db.connection[current_app.config["QUESTION_BANK_DB"]]
            .get_collection("questions-list")
            .find(query, {"questionNumberId": 1})
            .sort(sort)
            .skip(skip)
            .limit(size)
        )
    db_duration = time.time() - start_time
    current_app.logger.info(
        "Time taken to fetch the results from {}: {}".format(
            "questions-list", db_duration
        )
    )
    questions_returned = [x for x in questions]
    random.shuffle(questions_returned)
    headers = {
        "Cache-Control": "private, max-age=3600",
    }
    return questions_returned, 200, headers


def assign_qnos(test_id, question_paper_id):
    """

    :param test_id:
    :param question_paper_id:
    :return:
    """
    q_list = mongo_db.find(
        test_database, "questions", {"questionPaperId": question_paper_id}
    )
    test_info = mongo_db.find_one(test_database, "tests", {"testId": test_id})

    is_parsed = test_info.get("is_parsed", False)

    if is_parsed:
        return q_list

    marking_schema_id = test_info["markingSchemaId"]
    marking_schema = mongo_db.find_one(
        settings_database, "markingschemas", {"_id": marking_schema_id}
    )
    if len(q_list) != marking_schema.get("totalQuestions", 0):
        raise MultipleValidationExceptions

    new_schema = reorder_schema(marking_schema, test_info["subjects"])

    # Prepare order criterion for test
    orderd_by_subject_and_type = OrderedDict()
    for subject in new_schema["subjects"]:
        name = subject["subjectName"]
        question_types_order = [_["egnifyQuestionType"] for _ in subject["marks"]]
        orderd_by_subject_and_type[name] = question_types_order
    subjects_order = list(orderd_by_subject_and_type.keys())

    # List sort comparator
    def comparator(q):
        subject = q["subject"]
        question_type = q["questionType"]

        return (
            subjects_order.index(subject),
            orderd_by_subject_and_type[subject].index(question_type),
        )

    # Order the list by subject order and question type. Comparator uses (i, j) tuple
    # for ordering list where i is sequence number of subject and j is sequence
    # number of question type  for kth question
    q_list.sort(key=comparator)
    for qno, q in enumerate(q_list, 1):
        q["qno"] = str(qno)
    return q_list


def syllabus_for_test(q_list):
    qnum_ids = [q["questionNumberId"] for q in q_list]
    query = {"questionNumberId": {"$in": qnum_ids}}
    projection = {"subject": 1, "topic": 1}
    questions = mongo_db.find(
        current_app.config["QUESTION_BANK_DB"],
        "questions-list",
        query,
        projection=projection,
    )
    syllabus = deep_defaultdict(set)
    for q in questions:
        subject = q.get("subject")
        topic = q.get("topic")
        if subject and topic:
            syllabus[subject].add(topic)
    lst = []
    for subject, topics in syllabus.items():
        doc = {
            "subject": subject,
            "topics": list(topics),
        }
        lst.append(doc)
    return lst


def schedule_test_tasks(test_id, autoGa):
    coll, query = "tests", {"testId": test_id}
    test = mongo_db.find_one(test_database, coll, query)
    end_time = test["endTime"]
    duration = test["duration"]
    grace = test.get("gracePeriod", 0)
    if grace:
        date = test["startTime"] + timedelta(minutes=duration + grace)
    else:
        date = end_time + timedelta(minutes=duration)
    update = {"isAutoGa": autoGa}
    if autoGa:
        query = {"_id": test["markingSchemaId"]}
        marking_schema = mongo_db.find_one(settings_database, "markingschemas", query)
        data = {
            "module": current_app.config["ANALYSIS_FILE"],
            "method": "hello_gcs_generic",
            "dt": date,
            "depends_on": test["couchSync"]["jobId"],
            "args": {
                "test_id": test_id,
                "questionPaperId": test["questionPaperId"],
                "test_name": test["testName"],
                "marking_schema": marking_schema,
            },
        }

        ga_task = schedule_task(data)
        update["autoGa"] = {"jobId": ga_task.id, "status": "scheduled"}

    mongo_db.upsert(test_database, coll, query, update)


@blueprint.route("/<string:test_id>/<string:question_paper_id>", methods=["POST"])
@decorators.roles_required([EgnifyRole.TEST_CREATOR, EgnifyRole.TEACHER])
@decorators.authorization_required(cached_mongo_db=get_cached_mongo_db)
@decorators.make_egnify_response
def post_questions_for_paper(test_id, question_paper_id):
    """
        Return list of question items for a test authorized on test hierarchies
    """
    start_time = time.time()
    collection_name = "questions"
    submitted_questions = request.get_json()
    questions_list = []
    status = submitted_questions.get("status", None)
    autoGa = submitted_questions.get("autoGa", False)
    update_object = {"testId": test_id, "questionPaperId": question_paper_id}
    user_id = submitted_questions.get("userId", None)
    if user_id:
        update_object["userId"] = user_id

    if submitted_questions.get("questions", None):
        subject = submitted_questions["questions"][0]["subject"]
        count = len(submitted_questions["questions"])
        for question in submitted_questions["questions"]:
            question["_id"] = str(ObjectId())
            question["questionPaperId"] = question_paper_id
            question["userId"] = user_id
            questions_list.append(question)
        # Check if there is mismatch b/w assigned and received count
        query = {
            "testId": test_id,
            "questionPaperId": question_paper_id,
            "userId": user_id,
        }
        qp_entry = mongo_db.find_one(test_database, "question_papers", query)
        errors = []
        for item in qp_entry["questions"]["subject"]:
            if item["name"] == subject and count != item["noOfQuestions"]:
                errors.append(ValidationException("E405", None, None))
        if errors:
            raise MultipleValidationExceptions(errors)
        # Check if submission has been made already
        prev_questions_query = {
            "questionPaperId": question_paper_id,
            "userId": user_id,
            "subject": subject,
        }
        prev_cnt = mongo_db.count(test_database, "questions", prev_questions_query)
        if prev_cnt:
            errors.append(ValidationException("E406", None, None))
        if errors:
            raise MultipleValidationExceptions(errors)

        mongo_db.upsert_many_in_process(test_database, collection_name, questions_list)
        mongo_db.wait_for_termination()
    mongo_db.upsert(
        current_app.config["QUESTION_BANK_DB"],
        "question_papers",
        update_object,
        {"status": status},
    )
    if status == "publish":
        test_query = {
            "testId": test_id,
            "startTime": {"$gt": datetime.now(timezone.utc)},
        }
        test = mongo_db.find_one(test_database, "tests", test_query)
        if not test:
            error_response = {
                "STATUS": "FAILURE",
                "MESSAGE": "Publish failed as test start time has already passed",
            }
            return error_response, 400, {}

        q_list = assign_qnos(test_id, question_paper_id)
        mongo_db.upsert_many_in_process(test_database, collection_name, q_list)
        mongo_db.wait_for_termination()
        syllabus = syllabus_for_test(q_list)
        update_object = {"_id": test_id}
        update_fieds = {
            "status": "inprogress",
            "questionPaperId": [question_paper_id],
            "syllabus": syllabus,
        }
        #get user email to send email
        query = {
            "testId": test_id,
            "questionPaperId": question_paper_id,
        }
        user_docs = mongo_db.find(test_database, "question_papers", query)
        to_emails =[]
        for doc in user_docs:
            obj ={}
            userEmail = doc["questions"]["email"]
            name = userEmail.split("@")[0]
            obj["Email"] = userEmail
            obj["Name"] = name
            to_emails.append(obj)
        headers = {
             "accesscontroltoken":request.headers.get("accesscontroltoken"),
             "authorization": request.headers.get("Authorization")
        }
        subject = "Test f{test['testName']} is published"
        text = "Your test published"
        result = mail.sendEmail(to_emails,subject,text,headers=headers)
        current_app.logger.info(
            f"sendEmail  status {result}"
        )
        ##Headers
        # access_control_token = request.headers.get("accesscontroltoken")
        # authorization = request.headers.get("Authorization")
        # headers = {
        #     "authorization": authorization,
        #     "accesscontroltoken": access_control_token,
        # }

        # ##Notification trigger
        # curr_timestamp = datetime.utcnow().timestamp()
        # test_timestamp = test["startTime"].timestamp()
        # diff_timestamp = (test_timestamp - curr_timestamp) * 1000  ## in ms
        # conversion_fator = 1000 * 60 * 60
        # diff_timestamp = diff_timestamp / conversion_fator  ## in hours

        # bulk_notification = []

        # test_hierarchies = test["accessTag"]["hierarchy"]
        # student_ids = meteoroid_util.get_student_ids_for_hierarchies(test_hierarchies)
        # IST = tmz("Asia/Kolkata")
        # human_readable_date = (
        #     test["startTime"]
        #     .replace(tzinfo=timezone.utc)
        #     .astimezone(tz=IST)
        #     .strftime("%a %d %b %I:%M %p")
        # )

        # notification_body = f"{test['testName']}, {human_readable_date}, duration:{test['duration']} mins, marks:{test['totalMarks']}"

        # notification_obj1 = {
        #     "header": "Upcoming test",
        #     "body": notification_body,
        #     "details": notification_body,
        #     "studentIds": student_ids,
        # }
        # if diff_timestamp > 24:
        #     date_time_to_trigger = meteoroid_util.date_time_utc(
        #         test["startTime"], delta_hr=-24
        #     )
        #     notification_obj1["timeStamp"] = date_time_to_trigger
        # bulk_notification.append(notification_obj1)
        # time_to_trigger_notification_1_hr_before = meteoroid_util.date_time_utc(
        #     test["startTime"], delta_hr=-1
        # )
        # notification_obj2 = {
        #     "header": "1 hour to test",
        #     "body": notification_body,
        #     "details": notification_body,
        #     "studentIds": student_ids,
        #     "timeStamp": time_to_trigger_notification_1_hr_before,
        # }
        # if diff_timestamp > 1:
        #     bulk_notification.append(notification_obj2)

        # notification_resp = pushnotification.sendPushNotification(
        #     {"bulk_array": bulk_notification}, headers
        # )
        # if notification_resp:
        #     update_fieds["notification_ids"] = notification_resp.get(
        #         "notification_ids", []
        #     )

        mongo_db.upsert(
            current_app.config["TEST_DATABASE"], "tests", update_object, update_fieds,
        )
        schedule_test_tasks(test_id, autoGa)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return {"questions": questions_list}, 200, {}


@blueprint.route("/data/<question_number_id>")
@decorators.roles_required(
    [
        EgnifyRole.TEST_CREATOR,
        "ANALYSIS_VIEWER",
        "REPORTS_VIEWER",
        EgnifyRole.STUDENT,
        EgnifyRole.TEACHER,
    ]
)
@cache.cached(timeout=86400, query_string=True)
@decorators.make_egnify_response
# @decorators.egnify_encryption(password='EHF2AZMKGJUKVCUA8UCZ2U7S5ZJ0MGYY')
def get_question_details(question_number_id):
    """
        Returns the question for the question number id
        No Authorization on hierarchy or testid for this API
    """
    start_time = time.time()
    query = {"questionNumberId": question_number_id}
    mongo_db = current_app.question_bank_mongo_db
    question_details = mongo_db.get_results(
        current_app.config["QUESTION_BANK_DB"],
        "questions-list",
        query,
        key="questionNumberId",
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        "Time taken to fetch the results from {}: {}".format("questions", db_duration)
    )
    headers = {
        "Cache-Control": "private, max-age=14400",
    }
    return question_details, 200, headers


@blueprint.route("/data/<question_number_id>", methods=["PUT"])
@decorators.roles_required(["PAPER_EDITOR"])
@decorators.make_egnify_response
def put_question_details(question_number_id):
    """
        Returns the question for the question number id
        No Authorization on hierarchy or testid for this API
    """
    start_time = time.time()
    query = {"questionNumberId": question_number_id}
    updated_question_json = request.get_json()
    question_mongo_db = current_app.question_bank_mongo_db
    question_details = question_mongo_db.find_one_and_upsert(
        current_app.config["QUESTION_BANK_DB"],
        "questions-list",
        query,
        updated_question_json,
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        "Time taken to fetch the results from {}: {}".format("questions", db_duration)
    )
    # Delete the pre existing cache for querystring path use request.full_path
    cache.delete(f"view/{request.path}")
    return question_details, 200, {}


@blueprint.route("/<string:test_id>/<string:question_paper_id>/")
@decorators.roles_required(
    [
        EgnifyRole.TEST_CREATOR,
        "ANALYSIS_VIEWER",
        "REPORTS_VIEWER",
        EgnifyRole.STUDENT,
        EgnifyRole.TEACHER,
    ]
)
@decorators.authorization_required(
    cached_mongo_db=get_cached_mongo_db, back_propagation=True
)
@cache.cached(timeout=3600, query_string=True)
@decorators.make_egnify_response
def get_questions_for_paper(test_id, question_paper_id):
    """
        Return list of question items.
    """
    start_time = time.time()
    query = {"questionPaperId": question_paper_id}
    projection = {field: 1 for field in ["qno", "questionNumberId"]}
    collection_name = "questions"
    kwargs = {
        "key": "qno",
        "projection": projection,
    }
    questions = mongo_db.get_results(test_database, collection_name, query, **kwargs)
    custom_headers = {"Cache-Control": "private, max-age=3600"}
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return questions, 200, custom_headers


@blueprint.route("/<string:test_id>/<string:question_paper_id>/<string:question_id>")
@decorators.roles_required(
    [
        EgnifyRole.TEST_CREATOR,
        "ANALYSIS_VIEWER",
        "REPORTS_VIEWER",
        EgnifyRole.STUDENT,
        EgnifyRole.TEACHER,
    ]
)
@decorators.authorization_required(cached_mongo_db=get_cached_mongo_db)
@cache.cached(timeout=3600)
@decorators.make_egnify_response
# @decorators.egnify_encryption(password='EHF2AZMKGJUKVCUA8UCZ2U7S5ZJ0MGYY')
def get_question_data(test_id, question_paper_id, question_id):
    """
        Return question data.
    """
    start_time = time.time()
    query = {
        "testId": test_id,
        "questionNumberId": question_id,
        "questionPaperId": question_paper_id,
    }
    projection = {field: 0 for field in ["solution", "key", "skill"]}
    collection_name = "questions"
    kwargs = {
        "key": "questionNumberId",
        "projection": projection,
    }
    question = mongo_db.get_results(test_database, collection_name, query, **kwargs)
    custom_headers = {"Cache-Control": "private, max-age=3600"}
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return question, 200, custom_headers


@blueprint.route("/<string:test_id>/<string:question_paper_id>/questions/count")
@decorators.roles_required(
    [EgnifyRole.TEST_CREATOR, EgnifyRole.STUDENT, EgnifyRole.TEACHER]
)
@decorators.authorization_required(
    cached_mongo_db=get_cached_mongo_db, back_propagation=True
)
@decorators.make_egnify_response
def get_count_of_topic_sub_topic(test_id, question_paper_id):
    """
        Return number of questions in
    """
    difficulty = request.values.get("difficulty", "")
    subject = request.values.get("subject", None)
    topic = request.values.get("topic", None)
    sub_topic = request.values.get("subTopic", "")
    question_type = request.values.get("questionType", "")
    duplicate = request.values.get("duplicate", False, type=bool)

    query = {}

    if subject:
        query["subject"] = subject
    if topic:
        query["topic"] = topic
    if sub_topic:
        query["subTopic"] = {"$in": sub_topic.split("$")}
    if question_type:
        query["questionType"] = {"$in": question_type.split("$")}
    if difficulty:
        query["difficulty"] = {"$in": difficulty.split("$")}

    query["hide"] = {"$ne": True}
    query["parsed"] = {"$ne": True}
    if not duplicate:
        semaphore = threading.Semaphore()
        semaphore.acquire()
        questions_list_used_previously = _get_previously_used_questions(test_id)
        semaphore.release()
        query["questionNumberId"] = {"$nin": questions_list_used_previously}
        count = mongo_db.count(
            current_app.config["QUESTION_BANK_DB"], "questions-list", query
        )
    else:
        count = mongo_db.count(
            current_app.config["QUESTION_BANK_DB"], "questions-list", query
        )
    return {"count": count}, 200, {"Cache-Control": "private, max-age=3600"}


@blueprint.route("/<string:question_paper_id>/addquestionmap", methods=["PUT"])
@decorators.roles_required([EgnifyRole.TEST_CREATOR])
@decorators.make_egnify_response
def add_question_map(question_paper_id):
    """
        Return Status of Question Map.
    """
    req_data = request.get_json()
    question_paper = current_app.mongo_db.find(
        test_database, "questions", {"questionPaperId": question_paper_id}
    )
    if list(question_paper):
        questions_list = []
        for question in list(question_paper):
            qno = question["qno"]
            qNumberId = question["questionNumberId"]
            obj = {}
            obj["_id"] = qNumberId
            obj["questionNumberId"] = qNumberId
            obj.update(req_data.get(qno, {}))
            key = obj.get("key")
            obj["hide"] = False if key else True
            cache_key = f"view//questions/v2/data/{qNumberId}"
            cache.delete(cache_key)

            questions_list.append(obj)

        current_app.mongo_db.upsert_many_in_process(
            test_database, "questions-list", questions_list, "questionNumberId"
        )
        current_app.mongo_db.wait_for_termination()
        return (
            {"STATUS": "SUCCESS", "DATA": {"questionPaperId": question_paper_id}},
            200,
            {},
        )
    else:
        return {"STATUS": "FAILURE", "DATA": None}, 400, {}


@blueprint.route("/<string:test_id>/<string:question_paper_id>/parse", methods=["PUT"])
@decorators.roles_required([EgnifyRole.TEST_CREATOR])
@decorators.authorization_required(cached_mongo_db=get_cached_mongo_db)
@decorators.make_egnify_response
def create_question_paper_to_parse(test_id, question_paper_id):
    """
        Return Created Job Id or Converted Url for which parsing is going on.
    """
    req_form_data = request.values
    uploaded_file = request.files.get("file")
    uploaded_file_string = uploaded_file.read()
    bucket_names = {
        "GCP": current_app.config["CLOUD_STORAGE_BUCKET"],
        "MINIO": current_app.config["MINIO_STORAGE_BUCKET"],
    }
    storage_provider = current_app.config["STORAGE_PROVIDER"]
    extention = uploaded_file.filename.split(".")[-1].lower()
    data = {
        "file_ext": f".{extention}",
        "file_name": uploaded_file.filename,
        "data": uploaded_file_string,
        "content_type": uploaded_file.content_type,
        "bucket_name": bucket_names[storage_provider],
        "content_disposition": "inline",
    }

    storage_helper = StorageHelper(storage_provider, storage_secrets)

    url = storage_helper.upload(data, get_public_url=True)

    test_details = current_app.mongo_db.find_one(
        test_database, "tests", {"testId": test_id}
    )
    validate_question_meta(
        test_id, question_paper_id, req_form_data, test_details, url, extention
    )
    response = parse_on_condition(
        test_id,
        question_paper_id,
        req_form_data,
        test_details,
        url,
        uploaded_file,
        len(uploaded_file_string),
        extention,
    )
    if "reason" in response:
        return response["reason"],response["status_code"],{}

    return response, 200, {}


def validate_question_meta(
    test_id, question_paper_id, req_form_data, test_details, url, extention
):
    current_app.logger.info("... Validating ...")
    kwargs = {
        "file_url": url,
        "marking_schema": req_form_data.get("markingSchema", None),
        "test_id": test_id,
        "question_paperid": question_paper_id,
        "question_reg": req_form_data.get("question_reg"),
        "option_reg": req_form_data.get("option_reg"),
        "ext_name": extention,
        "test_details": test_details,
    }
    request_validation = RequestValidator(**kwargs)
    request_validation.validate()


def update_questions(
    questions, upsert_questions, questions_list, upsert_questions_list
):
    if questions:
        if upsert_questions:
            current_app.mongo_db.upsert_many_in_process(
                test_database, "questions", questions, "_id"
            )
            current_app.mongo_db.wait_for_termination()
        else:
            current_app.mongo_db.insert_many(test_database, "questions", questions)
    if questions_list:
        if upsert_questions_list:
            current_app.mongo_db.upsert_many_in_process(
                test_database, "questions-list", questions_list, "questionNumberId"
            )
            current_app.mongo_db.wait_for_termination()
        else:
            current_app.mongo_db.insert_many(
                test_database, "questions-list", questions_list
            )


def parse_on_condition(
    test_id,
    question_paper_id,
    req_form_data,
    test_details,
    url,
    uploaded_file,
    file_size,
    extention,
):
    if extention == "pdf":
        (
            questions,
            upsert_questions,
            questions_list,
            upsert_questions_list,
        ) = create_questions_un_mapped(question_paper_id, req_form_data, test_details)
        test_obj = update_test_details(question_paper_id, req_form_data, url, extention)

        current_app.mongo_db.upsert(
            test_database, "tests", {"testId": test_id}, test_obj
        )

        update_questions(
            questions, upsert_questions, questions_list, upsert_questions_list
        )
        return {"job_id": None, "paper_url": url}
    else:
        resp = convert_doc_to_pdf(
            req_form_data,
            url,
            uploaded_file,
            file_size,
            current_app.config["PARSER_URL"],
        )

        return {"job_id": resp["data"], "paper_url": None}


@blueprint.route("/<string:test_id>/<string:question_paper_id>/parsestatus/<task_id>")
@decorators.roles_required([EgnifyRole.TEST_CREATOR])
@decorators.authorization_required(cached_mongo_db=get_cached_mongo_db)
@decorators.make_egnify_response
def get_status(test_id, question_paper_id, task_id):
    result = requests.get(f"{current_app.config['PARSER_URL']}/taskstatus/{task_id}")
    if not result.ok:
        return result.reason,result.status_code,{}
    result = result.json()
    return result, 200, {}


@blueprint.route(
    "/<string:test_id>/<string:question_paper_id>/questions/store", methods=["POST"]
)
@decorators.roles_required([EgnifyRole.TEST_CREATOR])
@decorators.authorization_required(cached_mongo_db=get_cached_mongo_db)
@decorators.make_egnify_response
def store_parsed_questions(test_id, question_paper_id):
    """

    :param test_id:
    :param question_paper_id:
    :return:
    """
    req_form_data = request.values
    test_details = current_app.mongo_db.find_one(
        test_database, "tests", {"testId": test_id}
    )
    data = req_form_data.get("result", None)
    if data:
        resp = json.loads(data)
        parsed_paper = resp[0]
        parsed_metrics = resp[1]
        metrics = parsed_metrics["metrics"]
        pdf_url = None
        if len(resp) == 3 and "pdf_url" in resp[2]:
            pdf_url = resp[2]["pdf_url"]

        if metrics["total"]["parsingPercentage"] != 100:
            return {"STATUS": "SUCCESS", "DATA": resp}, 200, {}
        else:
            question_paper = current_app.mongo_db.find(
                test_database,
                "questions",
                {"questionPaperId": question_paper_id, "parsed": True},
            )
            (
                questions,
                upsert_questions,
                questions_list,
                upsert_questions_list,
            ) = create_questions_un_mapped(
                question_paper_id,
                req_form_data,
                test_details,
                parsed_paper["data"],
                question_paper,
            )
            update_questions(
                questions, upsert_questions, questions_list, upsert_questions_list
            )
            test_obj = update_test_details(
                question_paper_id, req_form_data, None, "docx", is_parsed=True
            )
            test_obj["questionPaperUrl"] = pdf_url
            current_app.mongo_db.upsert(
                test_database, "tests", {"testId": test_id}, test_obj
            )
            return {"STATUS": "SUCCESS", "DATA": resp}, 200, {}
    return (
        {"STATUS": "FAILURE", "DATA": None, "ERROR": "result expected in form data"},
        400,
        {},
    )


@decorators.cache_with_arguments(get_cache=get_cache)
def _get_previously_used_questions(test_id: str):
    test_info = mongo_db.find_one(test_database, "tests", {"testId": test_id})
    hierarchy_list = test_info["hierarchy"]
    complete_hierarchy_tree = set()
    for hierarchy in hierarchy_list:
        hierarchy_t = _get_arango_path("root", hierarchy)
        complete_hierarchy_tree.update(hierarchy_t)
    complete_hierarchy_list = list(complete_hierarchy_tree)
    aggregation = (
        Aggregation()
        .match(
            {
                "hierarchy": {"$in": complete_hierarchy_list},
                "testFormatType": "ADMIN_TEST_TYPE",
            },
        )
        .lookup(
            {
                "from": "questions",
                "localField": "questionPaperId",
                "foreignField": "questionPaperId",
                "as": "questions",
            }
        )
        .unwind("$questions")
        .group({"_id": "$questions.questionNumberId"})
    )
    questions_used_previously = (
        mongo_db.connection[test_database]
        .get_collection("tests")
        .aggregate(aggregation.pipeline)
    )
    questions_list_used_previously = []
    for question_used in questions_used_previously:
        questions_list_used_previously.append(question_used["_id"])
    return questions_list_used_previously


@decorators.cache_with_arguments(get_cache=get_cache)
def _get_arango_path(src, destination):
    """

    :param src:
    :param destination:
    :return:
    """
    hierarchy_t = arango_db.get_path(src, destination)
    return hierarchy_t


@blueprint.route("/<string:question_number_id>/feedback", methods=["POST"])
@decorators.roles_required(
    [EgnifyRole.STUDENT, EgnifyRole.TEACHER, EgnifyRole.TEST_CREATOR]
)
@decorators.make_egnify_response
def submit_question_feedback(question_number_id):
    return_response = {"STATUS": "SUCCESS", "MESSAGE": "Feedback received successfully"}
    feedback = request.get_json()
    feedback["_id"] = str(ObjectId())
    feedback["createdAt"] = datetime.now(timezone.utc)
    mongo_db.insert_one(
        current_app.config["QUESTION_BANK_DB"], "questions_feedback", feedback
    )
    return return_response, 200, {}


@blueprint.route("/feedback", methods=["POST"])
@decorators.roles_required(
    [EgnifyRole.STUDENT, EgnifyRole.TEACHER, EgnifyRole.TEST_CREATOR]
)
@decorators.make_egnify_response
def submit_feedback():
    return_response = {"STATUS": "SUCCESS", "MESSAGE": "Feedback received successfully"}
    feedback = request.get_json()
    feedback["_id"] = str(ObjectId())
    feedback["createdAt"] = datetime.now(timezone.utc)
    mongo_db.insert_one(current_app.config["QUESTION_BANK_DB"], "feedback", feedback)
    return return_response, 200, {}


@blueprint.route("/data/create", methods=["POST"])
@decorators.roles_required(["PAPER_EDITOR"])
@decorators.make_egnify_response
def create_question():
    """
        No Authorization on hierarchy or testid for this API
    """
    question_number_id = str(ObjectId())
    start_time = time.time()
    question_json = request.get_json()
    question_json["questionNumberId"] = question_number_id
    question_json["_id"] = question_number_id
    question_json["parsed"] = True
    question_mongo_db = current_app.question_bank_mongo_db
    question_details = question_mongo_db.insert_one(
        current_app.config["QUESTION_BANK_DB"], "questions-list", question_json,
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        "Time taken to create a question {}: {}".format("questions", db_duration)
    )
    return question_details, 200, {}


@blueprint.route("/data")
@decorators.roles_required(
    [
        EgnifyRole.TEST_CREATOR,
        "ANALYSIS_VIEWER",
        "REPORTS_VIEWER",
        EgnifyRole.STUDENT,
        EgnifyRole.TEACHER,
    ]
)
@cache.cached(timeout=86400, query_string=True)
@decorators.make_egnify_response
def get_questions_details():
    """
        Returns the question for the question number id
        No Authorization on hierarchy or testid for this API
    """
    start_time = time.time()
    question_number_ids = request.args["question_number_id"]

    if not question_number_ids:
        return "Bad Req", 400, {}

    question_number_id_list = question_number_ids.split(",")
    query = {"questionNumberId": {"$in": question_number_id_list}}
    mongo_db = current_app.question_bank_mongo_db
    projection = {
        "_id": 0,
        "questionNumberId": 1,
        "key": 1,
        "answer": 1,
        "question": 1,
        "questionTypeMetaData": 1,
        "qno": 1,
        "solution": 1,
        "questionType": 1,
        "options": 1,
        "subject": 1,
        "questionId": 1,
    }

    question_details = mongo_db.get_results(
        current_app.config["QUESTION_BANK_DB"],
        "questions-list",
        query,
        key="questionNumberId",
        projection=projection,
    )

    db_duration = time.time() - start_time
    current_app.logger.info(
        "Time taken to fetch the results from {}: {}".format("questions", db_duration)
    )
    headers = {
        "Cache-Control": "private, max-age=14400",
    }
    return question_details, 200, headers


@blueprint.route("/<test_id>/paper/<question_paper_id>", methods=["POST"])
@decorators.roles_required(["TEST_CREATOR"])
@decorators.make_egnify_response
def update_paper_key(test_id, question_paper_id):
    """Updates the question paper for a test
    {
        "questions": [
            {
                "testId": "TEST_ID"
                "qno": "Q1",
                "prevKey": [2],
                "newKey": [4],
                "questionNumberId": "afd3asas4d",
                "editedBy": "kiran@egnify.com",
                "hasAdditionalMarks": true
            }
        ]
    }
    """
    start_time = time.time()
    data = request.get_json()
    question_mongo_db = current_app.question_bank_mongo_db
    update_ops = []
    activity_ops = []
    for q in data["questions"]:
        qnum_id = q["questionNumberId"]
        op = {"_id": qnum_id, "key": q["newKey"]}
        update_ops.append(op)
        # Delete the pre existing cache for question
        cache_key = f"view//questions/v2/data/{qnum_id}"
        cache.delete(cache_key)
        activity_op = q.copy()
        activity_op["_id"] = "_".join([test_id, question_paper_id, activity_op["qno"]])
        activity_ops.append(activity_op)
    question_mongo_db.upsert_many_in_process(
        current_app.config["QUESTION_BANK_DB"], "questions-list", update_ops,
    )
    mongo_db.upsert_many_in_process(test_database, "paper-edit-entries", activity_ops)
    mongo_db.upsert(
        test_database, "tests", {"testId": test_id}, {"regenerate": True}, upsert=False
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        "Time taken to fetch the results from {}: {}".format("questions", db_duration)
    )
    return {"STATUS": "SUCCESS"}, 200, {}


@blueprint.route("/<test_id>/paper/<question_paper_id>")
@decorators.roles_required(["TEST_CREATOR"])
@decorators.make_egnify_response
def updated_paper_key(test_id, question_paper_id):
    query = {"testId": test_id, "questionPaperId": question_paper_id}
    questions = mongo_db.find(test_database, "paper-edit-entries", query)
    resp = {"STATUS": "SUCCESS", "DATA": questions}
    return resp, 200, {}


@blueprint.route("/question/<question_number_id>", methods=["PUT"])
# @decorators.roles_required(["PAPER_EDITOR"])
@decorators.make_egnify_response
def update_unparsed_questions(question_number_id):
    """
        Returns the question for the question number id
        No Authorization on hierarchy or testid for this API
    """
    start_time = time.time()
    query = {"questionNumberId": question_number_id}
    updated_question_json = request.get_json()
    questionType = updated_question_json.get("questionType", None)
    errors = []
    valid_question_types = ["Single answer type", "Numeric type"]
    if not questionType or questionType not in valid_question_types:
        errors.append("Question type is missing or invalid.")
        return errors, 400, {}

    if questionType == "Single answer type":
        if "question" not in updated_question_json:
            errors.append("Question is missing.")

        options = updated_question_json.get("options", [])
        if not options:
            errors.append("Options are missing.")
        else:
            if not isinstance(options, list):
                errors.append("Invalid data type for options.")
            else:
                optionsLength = len(options)
                if optionsLength != 4:
                    errors.append("Options can not be more or less than 4.")
        key = updated_question_json.get("key", [])
        if not key:
            errors.append("Key is missing in question.")
        else:
            if not isinstance(key, list):
                errors.append("Invalid data type for keys.")
            else:
                keysLength = len(key)
                if keysLength > 1:
                    errors.append("Keys can not be more than one.")

    elif questionType == "Numeric type":
        if "question" not in updated_question_json:
            errors.append("Question is missing.")
        if "answer" not in updated_question_json:
            errors.append("Answer is missing")

    if errors:
        return errors, 400, {}

    question_mongo_db = current_app.question_bank_mongo_db
    question_details = question_mongo_db.find_one_and_upsert(
        current_app.config["QUESTION_BANK_DB"],
        "questions-list",
        query,
        updated_question_json,
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        "Time taken to fetch the results from {}: {}".format("questions", db_duration)
    )
    return question_details, 200, {}
