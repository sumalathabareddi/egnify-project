from flask import Blueprint, current_app, request
from utils.decorators import (
    make_egnify_data_response,
    make_egnify_response,
    roles_required,
    authorization_required,
)
from utils.egy_analytics import cache

from .models.concepttaxonomies import ConceptTaxononmySchema

blueprint = Blueprint("concepttaxonomies", __name__)

mongo_db = settings_database = None


@blueprint.before_app_first_request
def init_bp():
    global mongo_db, settings_database
    mongo_db = current_app.mongo_db
    settings_database = current_app.config["SETTINGS_DATABASE"]


@blueprint.route("/<string:institute_id>/concepttaxonomies", methods=["POST"])
@roles_required(["PARTNER_ADMIN", "SETTINGS_CREATOR"])
@authorization_required()
@make_egnify_response
def concepttaxonomies(institute_id):
    """API endpoint to add concept taxonomies.
    ---
    post:
      description: Add concept taxonomies
      responses:
        201:
          description: Return add concept taxonomies status
          content:
            application/json:
              schema: ConceptTaxononmySchema
    """
    response_data, status_code, extra_headers = {}, 200, {}
    collection_name = "concepttaxonomies"
    if request.is_json:
        post_data = request.get_json()
        taxonomies = post_data.get("taxonomies", [])
        result = ConceptTaxononmySchema(many=True).load(taxonomies)
        if result.errors:
            response_data["STATUS"] = "FAILURE"
            response_data["DATA"] = result.errors
            status_code = 400
        else:
            mongo_db.insert_many(settings_database, collection_name, result.data)
            response_data["STATUS"] = "SUCCESS"
            response_data["MESSAGE"] = "Created concept taxonomies successfully"
    else:
        response_data["STATUS"] = "FAILURE"
        response_data["MESSAGE"] = "Invalid mimetype, expected application/json"
        status_code = 400

    return response_data, status_code, extra_headers


@blueprint.route("/<string:institute_id>/concepttaxonomies")
@roles_required(["SETTINGS_READER", "SETTINGS_CREATOR", "TEACHER", "STUDENT"])
@authorization_required()
@cache.cached(timeout=604800, query_string=True)
@make_egnify_data_response
def get_concepttaxonomies_filtered(institute_id):
    """API endpoint to get concept taxonomies.
    ---
    post:
      description: Get list of concept taxonomies
      responses:
        200:
          description: Return list of concept taxonomies
          content:
            application/json:
              schema: ConceptTaxononmySchema
    """
    collection_name = "concepttaxonomies"
    orientations = request.values.get("orientations", None)
    classes = request.values.get("classes", None)
    patterns = request.values.get("patterns", None)
    subjects = request.values.get("subjects", None)

    query = {}
    if orientations:
        query["orientations"] = {"$in": orientations.split(",")}
    if classes:
        query["classes"] = {"$in": classes.split(",")}
    if patterns:
        query["patterns"] = {"$in": patterns.split(",")}
    if subjects:
        query["subject"] = {"$in": subjects.split(",")}

    query["instituteId"] = institute_id

    taxonomies_count = mongo_db.count(settings_database, collection_name, query)
    institute_id = institute_id if taxonomies_count else "root"
    query["instituteId"] = institute_id
    projection = {"orientations": 0, "classes": 0, "patterns": 0}
    concepttaxonomies = mongo_db.find(
        settings_database, collection_name, query, projection=projection
    )
    return concepttaxonomies, 200, {"Cache-Control": "private, max-age=14400"}


@blueprint.route(
    "/<string:institute_id>/concepttaxonomies/<string:orientation_id>/class/<string:class_id>/pattern/<string"
    ":pattern_id>"
)
@roles_required(["SETTINGS_READER", "SETTINGS_CREATOR"])
@authorization_required()
@cache.cached(timeout=604800)
@make_egnify_data_response
def get_concepttaxonomies(institute_id, orientation_id, class_id, pattern_id):
    """API endpoint to get concept taxonomies.
    ---
    post:
      description: Get list of concept taxonomies
      responses:
        200:
          description: Return list of concept taxonomies
          content:
            application/json:
              schema: ConceptTaxononmySchema
    """
    collection_name = "concepttaxonomies"
    query = {
        "orientations": {"$in": [orientation_id]},
        "classes": {"$in": [class_id]},
        "patterns": {"$in": [pattern_id]},
        "instituteId": institute_id,
    }

    taxonomies_count = mongo_db.count(settings_database, collection_name, query)
    institute_id = institute_id if taxonomies_count else "root"
    query["instituteId"] = institute_id
    concept_taxonomies = mongo_db.find(settings_database, collection_name, query)
    return concept_taxonomies, 200, {"Cache-Control": "private, max-age=14400"}
