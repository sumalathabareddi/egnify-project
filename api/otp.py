from datetime import datetime, timezone, timedelta

import utils.decorators as decorators
from flask import Blueprint, current_app, request
from utils import rand_password_string
from utils.egy_analytics import cache
import generate_analysis
from marks.Test import Test
import time
import pytz
# from google.cloud import vision
# import io
# from utils.StorageHelper import StorageHelper

from snapshot.StudentSnapshot import StudentSnapshot


blueprint = Blueprint("otp", __name__)

mongo_db = (
    couch_db
) = (
    ga_database
) = (
    settings_database
) = encryption_key = question_db = rankguru = nova = master_result_collection = None


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global mongo_db, couch_db, ga_database, settings_database, encryption_key
    global rankguru, nova, question_db, master_result_collection,storage_secrets
    mongo_db = current_app.mongo_db
    couch_db = current_app.couch_db
    ga_database = current_app.config["GA_DATABASE"]
    encryption_key = current_app.config["ENCRYPTION_KEY"]
    settings_database = current_app.config["SETTINGS_DATABASE"]
    question_db = current_app.config["QUESTION_BANK_DB"]
    rankguru = current_app.config.get("IS_RANKGURU", False)
    nova = current_app.config.get("IS_NOVA", False)
    master_result_collection = current_app.config.get("MASTER_RESULTS_TABLE")
    storage_secrets = {}
    storage_secrets["GCP_CREDENTIALS_FILE"] = current_app.config[
            "GCP_CREDENTIALS_FILE"
        ]


@blueprint.route(
    "/<hierarchy_id>/student/<student_id>/test/<test_id>/db/create", methods=["PUT"]
)
@decorators.roles_required(["LMS_OTP_ACCESS", "OTP"])
@decorators.make_egnify_response
def create_couchdb_user(hierarchy_id: str, student_id: str, test_id: str):
    password = rand_password_string(20)
    try:
        couch_db.create_user(student_id, password, roles=["own_db_admin"])
    except Exception as e:
        current_app.logger.warn("Exception in creating user " + str(e))
        couch_db.delete_user(student_id)
        couch_db.create_user(student_id, password, roles=["own_db_admin"])
    database_url = "_".join(["db", hierarchy_id, test_id, student_id])
    database_url = database_url.replace("-", "").lower()
    couch_db.create_database(database_url, student_id)
    resp = {
        "username": student_id,
        "password": password,
        "databaseUrl": database_url,
        "key": encryption_key,
    }
    return resp, 200, {}


@blueprint.route(
    "/<hierarchy_id>/student/<student_id>/test/<test_id>/end", methods=["PUT"]
)
@decorators.roles_required(["LMS_OTP_ACCESS", "OTP"])
@decorators.make_egnify_response
def delete_couchdb_user(hierarchy_id: str, student_id: str, test_id: str):
    """

    :param hierarchy_id:
    :param student_id:
    :param test_id:
    :return:
    """
    couch_db.delete_user(student_id)
    database_url = "_".join(["db", hierarchy_id, test_id, student_id])
    database_url = database_url.replace("-", "").lower()
    behaviour_data = couch_db.get_elements(database_url)
    snapshot_id = "_".join([test_id, student_id])
    mongo_db.upsert(
        ga_database,
        "testStudentSnapshot",
        {"_id": snapshot_id},
        {"status": "completed"},
        upsert=False,
    )
    mongo_db.insert_many_in_process(
        ga_database, "testStudentSync", list(behaviour_data.values())
    )
    mongo_db.wait_for_termination()
    return "Sync Success", 200, {}


@blueprint.route(
    "/<hierarchy_id>/student/<student_id>/test/<test_id>/accept", methods=["PUT"]
)
@decorators.roles_required(["LMS_OTP_ACCESS", "OTP"])
@decorators.make_egnify_response
def accept_test_instuctions(hierarchy_id: str, student_id: str, test_id: str):
    """

    :param hierarchy_id:
    :param student_id:
    :param test_id:
    :return:
    """
    student = request.json
    database_url = "_".join(["db", hierarchy_id, test_id, student_id])
    student["_id"] = "_".join([test_id, student_id])
    database_url = database_url.replace("-", "").lower()
    student["hierarchyId"] = hierarchy_id
    student["testId"] = test_id
    student["databaseUrl"] = database_url
    student["key"] = encryption_key
    student["mode"] = "online"
    student["egnifyId"] = student_id
    if rankguru:
        student["studentId"] = student_id
    test_query = {"testId": test_id, "endTime": {"$gte": datetime.now(timezone.utc)}}
    test = mongo_db.find_one(ga_database, "tests", test_query)
    if not test:
        return "Test Already Completed", 201, {}

    grace = test.get("gracePeriod", 0) # 0 mins is no grace period
    allowed_till = test['startTime'] + timedelta(minutes=grace)
    allowed_till_aware = allowed_till.replace(tzinfo=pytz.UTC)
    if grace and datetime.now(timezone.utc) > allowed_till_aware:
        return "You are not allowed to start test after grace time.", 201, {}

    try:
        snap_rec = prepare_snapshot_record(
            student_id, test_id, cwu={}, behaviour_data={}, sync_status=False
        )
        student.update(snap_rec)
        res = mongo_db.find_one_and_upsert(
            ga_database,
            "testStudentSnapshot",
            {"_id": student["_id"], "status": {"$ne": "completed"}},
            student,
            upsert=True,
        )
    except Exception:
        res = mongo_db.find_one(
            ga_database,
            "testStudentSnapshot",
            {"_id": student["_id"]},
            projection={"_id": 0, "status": 1},
        )
    if "status" in res and res["status"] == "completed":
        return "Test Already Completed", 201, {}
    return res, 200, {}


def prepare_snapshot_record(
    student_id, test_id, cwu, behaviour_data, sync_status=False, end_time=None
):
    start_time = time.time()
    students_coll_name = "studentInfo"
    query = {"_id": student_id}
    projection = {"studentId": 1, "studentName": 1, "instituteId": 1, "egnifyId": 1, "accessTag": 1}
    student_info = mongo_db.find_one(
        settings_database, students_coll_name, query, projection=projection
    )
    student_snapshot = StudentSnapshot(
        list(cwu.keys()), None, list(cwu.values()), test_id, None, None, None
    )
    student_snapshot.fill_student_details(student_info, syncStatus=sync_status)
    parent_hierarchy = student_info["accessTag"]["hierarchy"]

    hierarchy_levels = cache.get(f"parent_hierarchies_{parent_hierarchy}")
    if not hierarchy_levels:
        path = current_app.arango_db.get_path("root", parent_hierarchy)
        # Exclude root
        root_excluded = path[1:]
        hierarchy_levels = root_excluded[::-1]
        cache.set(f"parent_hierarchies_{parent_hierarchy}", hierarchy_levels, 86400)

    current_app.logger.info(f"Time taken for hierarchy levels {time.time()-start_time}")
    student_snapshot.hierarchyLevels = hierarchy_levels

    student_snapshot.endTime = end_time
    student_snapshot.behaviourData = behaviour_data

    return vars(student_snapshot)


@blueprint.route("/<hierarchy_id>/test/<test_id>/<questionPaperId>", methods=["POST"])
@decorators.roles_required(["LMS_OTP_ACCESS", "OTP"])
@decorators.make_egnify_response
def end_test(hierarchy_id, test_id, questionPaperId):
    """

    :param hierarchy_id:
    :param test_id:
    :param questionPaperId:
    :return:
    """
    data = request.json
    student_id = data.get("studentId", None)
    pouchDBData = data.get("pouchDbData", [])
    (
        raw_response,
        behaviour_data,
        startedAt,
        pouchToJson,
    ) = getRawResponseFromPouchDBData(pouchDBData)

    if not student_id:
        return "Bad_Args", 400, {}

    _id = "_".join([test_id, student_id])
    redis_key = f"cwu_{_id}"

    pre_calculated_cwu = cache.get(redis_key)

    if pre_calculated_cwu:
        return pre_calculated_cwu, 200, {}

    query = {"_id": _id}

    question_keys = getQuestionKeys(questionPaperId)
    cwu = calculateCWU(question_keys, raw_response)

    cache.set(redis_key, cwu, 36000)

    student_snap = prepare_snapshot_record(
        student_id, test_id, cwu, behaviour_data, sync_status=True
    )
    student_snap["status"] = "completed"
    student_snap["endTime"] = datetime.now(timezone.utc)

    snapshot = mongo_db.find_one_and_upsert(
        ga_database, "testStudentSnapshot", query, student_snap, upsert=False,
    )

    if not snapshot:
        return "Invalid test id or student id", 400, {}

    test_obj = mongo_db.find_one(ga_database, "tests", {"_id": test_id})

    if test_obj["testFormatType"] in [
        "EXERCISE_TEST_TYPE",
        "DPP_TEST_TYPE",
        "PRACTICE_TEST_TYPE",
    ]:
        mongo_db.find_one_and_upsert(
            ga_database,
            "tests",
            {"testId": test_id},
            {"status": "completed"},
            new=False,
        )
    if test_obj["testFormatType"] in ["EXERCISE_TEST_TYPE", "DPP_TEST_TYPE"]:
        return {}, 200, {}
    new_test_obj = Test(test_id, test_obj["testName"], [])
    marking_schema = mongo_db.find_one(
        settings_database, "markingschemas", {"_id": test_obj["markingSchemaId"]}
    )
    restructed_marking_schema = generate_analysis.re_order_marking_schema_subjects(
        marking_schema, test_obj["subjects"]
    )
    mongodb_question_details = generate_analysis.get_mongodb_question_details(
        mongo_db, question_db, test_id, questionPaperId, restructed_marking_schema
    )
    error_queue = []
    master_results = generate_analysis.caliculate_marks_for_a_student(
        (student_id, snapshot, raw_response),
        new_test_obj,
        marking_schema,
        mongodb_question_details,
        error_queue,
    )
    if not error_queue:
        master_res = vars(master_results)
        master_res["_id"] = master_res["id"]
        master_res["studentId"] = snapshot.get("studentId", student_id)
        snapshot["behaviourData"] = pouchToJson
        mongo_db.insert_one(ga_database, master_result_collection, master_res)
        mongo_db.increment_key(
            ga_database,
            "uploadprogress",
            {"testId": test_id, "hierarchy": {"$in": snapshot["hierarchyLevels"]}},
            "resultsUploaded",
            multi=True,
        )
        mongo_db.insert_one(ga_database, "behaviourData", snapshot)
        return cwu, 200, {}
    else:
        return "Error in caliculating the results", 400, {}


@blueprint.route(
    "/<hierarchy_id>/test/<test_id>/<string:student_id><questionPaperId>/v2",
    methods=["POST"],
)
@decorators.roles_required(["LMS_OTP_ACCESS", "OTP"])
@decorators.make_egnify_response
def end_test_v2(hierarchy_id, test_id, questionPaperId):
    """

    :param hierarchy_id:
    :param test_id:
    :param questionPaperId:
    :return:
    """
    data = request.json
    student_id = data.get("studentId", None)

    if not student_id:
        return "Bad_Args", 400, {}

    _id = "_".join([test_id, student_id])
    redis_key = f"cwu_{_id}"

    pre_calculated_cwu = cache.get(redis_key)
    if pre_calculated_cwu:
        return pre_calculated_cwu, 200, {}

    student_response_query = {
        "testId": test_id,
        "studentId": student_id,
    }
    student_response_data = mongo_db.find(
        ga_database, "user_responses", student_response_query
    )
    pouchDBData = []

    for response in student_response_data:
        pouchDBData.append(response["raw_response"])

    (
        raw_response,
        behaviour_data,
        startedAt,
        pouchToJson,
    ) = getRawResponseFromPouchDBData(pouchDBData)

    query = {"_id": _id}

    question_keys = getQuestionKeys(questionPaperId)
    cwu = calculateCWU(question_keys, raw_response)

    cache.set(redis_key, cwu, 36000)

    student_snap = prepare_snapshot_record(
        student_id, test_id, cwu, behaviour_data, sync_status=True
    )
    student_snap["status"] = "completed"

    snapshot = mongo_db.find_one_and_upsert(
        ga_database, "testStudentSnapshot", query, student_snap, upsert=False,
    )

    if not snapshot:
        return "Invalid test id or student id", 400, {}

    test_obj = mongo_db.find_one(ga_database, "tests", {"_id": test_id})

    if test_obj["testFormatType"] in [
        "EXERCISE_TEST_TYPE",
        "DPP_TEST_TYPE",
        "PRACTICE_TEST_TYPE",
    ]:
        mongo_db.find_one_and_upsert(
            ga_database,
            "tests",
            {"testId": test_id},
            {"status": "completed"},
            new=False,
        )
    if test_obj["testFormatType"] in ["EXERCISE_TEST_TYPE", "DPP_TEST_TYPE"]:
        return {}, 200, {}
    new_test_obj = Test(test_id, test_obj["testName"], [])
    marking_schema = mongo_db.find_one(
        settings_database, "markingschemas", {"_id": test_obj["markingSchemaId"]}
    )
    restructed_marking_schema = generate_analysis.re_order_marking_schema_subjects(
        marking_schema, test_obj["subjects"]
    )
    mongodb_question_details = generate_analysis.get_mongodb_question_details(
        mongo_db, question_db, test_id, questionPaperId, restructed_marking_schema
    )
    error_queue = []
    master_results = generate_analysis.caliculate_marks_for_a_student(
        (student_id, snapshot, raw_response),
        new_test_obj,
        marking_schema,
        mongodb_question_details,
        error_queue,
    )
    if not error_queue:
        master_res = vars(master_results)
        master_res["_id"] = master_res["id"]
        master_res["studentId"] = snapshot.get("studentId", student_id)
        snapshot["behaviourData"] = pouchToJson
        mongo_db.insert_one(ga_database, master_result_collection, master_res)
        mongo_db.increment_key(
            ga_database,
            "uploadprogress",
            {"testId": test_id, "hierarchy": {"$in": snapshot["hierarchyLevels"]}},
            "resultsUploaded",
            multi=True,
        )
        mongo_db.insert_one(ga_database, "behaviourData", snapshot)
        mongo_db.delete(ga_database, "user_responses", student_response_query)
        return cwu, 200, {}
    else:
        return "Error in caliculating the results", 400, {}


# @blueprint.route(
#     "/<hierarchy_id>/test/<test_id>/student/<student_id>/face", methods=["POST"]
# )
# @decorators.roles_required(["LMS_OTP_ACCESS", "OTP"])
# @decorators.make_egnify_data_response
# def detect_student_face(hierarchy_id: str, test_id: str, student_id: str):
#     """Detects faces in an image."""
#     _id = "_".join([test_id, student_id])
#     client = vision.ImageAnnotatorClient()
#     uploaded_file = request.files.get("file")
#     uploaded_file_string = uploaded_file.read()
#     storage_provider = current_app.config["STORAGE_PROVIDER"]
#     bucket_names = {
#         "GCP": current_app.config["CLOUD_STORAGE_BUCKET"],
#     }
#     data = {
#         "file_ext": ".png",
#         "file_name": f"/test-student-img/{_id}",
#         "acl_type": "publicRead",
#         "data": uploaded_file_string,
#         "content_type": uploaded_file.content_type,
#         "bucket_name": bucket_names[storage_provider],
#     }
#     storage_helper = StorageHelper(storage_provider, storage_secrets)
#     url = storage_helper.upload(data, get_public_url=True)
#     client = vision.ImageAnnotatorClient()
#     image = vision.Image()
#     image.source.image_uri = url
#     response = client.face_detection(image=image)
#     faces = response.face_annotations
#     print('faces::',len(faces))
#     data = {
#         "_id":_id,
#         "url":url,
#         "testId":test_id,
#         "accessTag.hierarchy":[hierarchy_id]
#     }
#     mongo_db.insert_one(ga_database,"test-people",data)
#     if response.error.message:
#         return response.error.message,400,{}
#     return len(faces),200,{}

def getRawResponseFromPouchDBData(pouchDbData):
    """

    :params pouchDbData
    :return raw response
    """
    raw_response = {}
    behaviour_data = {}
    jsonFormat = {}
    startedAt = None
    for obj in pouchDbData:
        key = obj["_id"]
        jsonFormat[key] = obj
        if key != "metaData":
            raw_response[key] = obj.get("response", [])
            behaviour_data[f"Q{key}"] = {
                "time": obj.get("time", []),
                "datetime": obj.get("dateTime", None),
            }
        else:
            startedAt = obj.get("startTime", None)
    return raw_response, behaviour_data, startedAt, jsonFormat


def getQuestionKeys(questionPaperId):
    """

    :param questionPaperId:
    :return:
    """
    if rankguru:
        questions_keys = mongo_db.get_results(
            settings_database,
            "questions",
            {"questionPaperId": questionPaperId},
            key="qno",
            projection={"key": 1, "qno": 1, "_id": 0, "q_type": 1},
        )
    else:
        cache_key = f"questions_keys_{questionPaperId}"
        questions_keys = cache.get(cache_key)
        if questions_keys:
            return questions_keys

        questions_keys = mongo_db.get_results(
            question_db,
            "questions",
            {"questionPaperId": questionPaperId},
            key="qno",
            projection={"qno": 1, "_id": 0, "questionNumberId": 1},
        )

        question_numberids = []

        for qno in questions_keys:
            question_numberids.append(questions_keys[qno]["questionNumberId"])
        questions = mongo_db.get_results(
            question_db,
            "questions-list",
            {"questionNumberId": {"$in": question_numberids}},
            key="questionNumberId",
            projection={
                "key": 1,
                "questionNumberId": 1,
                "_id": 0,
                "questionType": 1,
                "answer": 1,
            },
        )
        for qno in questions_keys:
            questionNumberId = questions_keys[qno]["questionNumberId"]
            questions_keys[qno]["key"] = (
                questions[questionNumberId]["key"]
                if "key" in questions[questionNumberId]
                and questions[questionNumberId]["key"]
                else [questions[questionNumberId]["answer"]]
            )
            questions_keys[qno]["q_type"] = questions[questionNumberId]["questionType"]

        cache.set(cache_key, questions_keys, 3600)

    return questions_keys


def calculateCWU(questions_keys, student_response):
    """

    :param student_response:
    :param question_key:
    :return:
    """
    cwu = {}
    for key in questions_keys:
        qKey = f"Q{key}"
        student_response[key] = student_response.get(key, [])
        data = hadleQuestionTypes(
            student_response[key],
            questions_keys[key]["key"],
            questions_keys[key]["q_type"],
            questions_keys[key]["qno"],
        )
        if questions_keys[key]["q_type"] == "Matrix match type":
            for col in data:
                if not rankguru:
                    cwu[f"{qKey}{col}"] = data[col]
                else:
                    cwu[f"{key}{col}"] = data[col]
        else:
            if not rankguru:
                cwu[qKey] = data
            else:
                cwu[key] = data
    return cwu


def hadleQuestionTypes(user_key, question_key, q_type, qno):
    if "" in user_key or "-" in user_key:
        return "U"
    if q_type == "Numeric type":
        user_key = [float(k) for k in user_key]
        question_key = [float(k) for k in question_key]

    elif q_type == "Matrix match type":
        res = {}
        answer_key = {}

        for resp in question_key:
            option = resp["sub"]
            answer = [x.upper() for x in resp["answer"]]
            answer_key[option.lower()] = answer

        user_response = {}

        for response in user_key:
            if response:
                for key in response:
                    user_response[key.lower()] = [x.upper() for x in response[key]]

        for key in answer_key:
            res[key] = "U"
            if key in user_response and user_response[key] and len(user_response[key]):
                res[key] = comparetwoKeys(user_response[key], answer_key[key])
        return res

    return comparetwoKeys(user_key, question_key)


def comparetwoKeys(user_key, question_key):
    if not user_key:
        return "U"

    correctMatchCount = 0

    user_key = set(user_key)
    question_key = set(question_key)

    if len(question_key) < len(user_key):
        return "W"

    if user_key == question_key:
        return "C"

    for key in user_key:
        if key in question_key:
            correctMatchCount = correctMatchCount + 1
        else:
            return "W"

    return "P" + str(correctMatchCount)


@blueprint.route("/<hierarchy_id>/test/<test_id>/time", methods=["GET"])
@decorators.roles_required(["LMS_OTP_ACCESS", "OTP"])
@cache.cached(timeout=14400)
@decorators.make_egnify_response
def fetch_test_timings(hierarchy_id, test_id):
    """
        hierarchyId is the branch id for Rankguru
        model:{
            _id: hierarchyId_testId,
            testId: "*****",
            hierarchyId: "*****",
            class: "******",
            orientation: "*****",
            startTime: "****",
            endTime: "*****",
            duration: "******"
        }
    """
    collectionName = "testTimings"
    _id = "_".join([hierarchy_id, test_id])
    timings = mongo_db.find_one(settings_database, collectionName, {"_id": _id})
    responseHeaders = {"Cache-Control": "private, max-age=14400"}
    return timings, 200, responseHeaders


@blueprint.route(
    "/<hierarchy_id>/student/<student_id>/test/<test_id>/start", methods=["POST"]
)
@decorators.roles_required(["LMS_OTP_ACCESS", "OTP"])
@decorators.make_egnify_data_response
def record_start_time(hierarchy_id: str, student_id: str, test_id: str):
    started = request.values.get("startTime")
    _id = "_".join([test_id, student_id])
    start_time = {"startedAt": started}
    mongo_db.find_one_and_upsert(
        ga_database,
        "testStudentSnapshot",
        {"_id": _id},
        start_time,
        upsert=False,
    )
    return "Updated successfully", 200, {}

@blueprint.route("/<string:institute_id>/markup/<string:test_type>")
@decorators.roles_required(["STUDENT"])
@cache.cached(timeout=604800)
@decorators.make_egnify_data_response
def instructions_markup(institute_id, test_type):
    collection_name = "markingschemas"
    query = {"_id": test_type}
    marking_schema = mongo_db.find_one(settings_database, collection_name, query)
    return marking_schema, 200, {}


@blueprint.route(
    "/<string:test_id>/<string:student_id>/<string:question_number_id>",
    methods=["POST"],
)
@decorators.roles_required(["LMS_OTP_ACCESS", "OTP"])
@decorators.make_egnify_data_response
def question_response(test_id, student_id, question_number_id):

    pouchData = request.json.get("pouchData", {})
    if not pouchData:
        return "Empty response can't be saved.", 400, {}

    collection_name = "user_responses"

    _id = "_".join([test_id, student_id, question_number_id])

    obj = {
        "_id": _id,
        "raw_response": pouchData,
        "testId": test_id,
        "studentId": student_id,
    }

    mongo_db.upsert(
        ga_database, collection_name, {"_id": _id}, obj, upsert=True,
    )

    return "Success", 200, {}
