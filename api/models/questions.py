from bson.objectid import ObjectId
from marshmallow import Schema, fields


class Questions(Schema):
    _id = fields.Str(missing=lambda: str(ObjectId()))
    qno = fields.Str(missing=None)
    questionNumberId = fields.Str(missing=None)
    questionPaperId = fields.Str(missing=None)
    userId = fields.Str(missing=None)
    parsed = fields.Boolean(missing=False)
    subject = fields.Str(required=True)
    questionType = fields.Str(required=True)
