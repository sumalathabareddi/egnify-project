import time
from marshmallow import Schema, fields, validate


class TestGroupSchema(Schema):
    _id = fields.Str(required=True)
    name = fields.Str(required=True)
    groupCode = fields.Str(required=True)
    description = fields.Str()


class SubjectsSchema(Schema):
    code = fields.Str(required=True)
    parentCode = fields.Str(required=True)
    subject = fields.Str(required=True)
    subjectCode = fields.Str(required=True)


class TestSchema(Schema):
    startTime = fields.Str(
        required=True,
        validate=validate.Regexp(r"[0-2][0-9]:[0-5][0-9]", 0, "Invalid start time"),
    )
    endTime = fields.Str(
        required=True,
        validate=validate.Regexp(r"[0-2][0-9]:[0-5][0-9]", 0, "Invalid end time"),
    )
    testName = fields.Str(required=True)
    modeOfConduct = fields.Str(required=True)
    questionPaperUrl = fields.Str(missing=None)
    questionPaperId = fields.List(fields.Str(), missing=[])
    status = fields.Str(missing="draft")
    gaStatus = fields.Str(missing="not_started")
    docxurl = fields.Str(missing=None)
    questionNumberFormat = fields.Str(missing=None)
    optionNumberFormat = fields.Str(missing=None)
    matrixQuestionListFormat = fields.Str(missing=None)
    matrixAnswerListFormat = fields.Str(missing=None)
    testStudentSnapshotStatus = fields.Str(missing="not-started")
    testFormatType = fields.Str(missing="ADMIN_TEST_TYPE")
    testLink = fields.Str(missing=None)
    testId = fields.Str(missing=lambda: str(int(time.time() * 1000)))
    practiceTestId = fields.Str(missing=None)
    egnifyId = fields.Str(missing=None)
    academicYear = fields.Str()

    totalMarks = fields.Int(required=True, validate=validate.Range(min=1))
    duration = fields.Int(required=True, validate=validate.Range(min=1))
    totalStudents = fields.Int(missing=0)
    studentsUploaded = fields.Int(missing=0)
    resultsUploaded = fields.Int(missing=0)
    resultsUploadedPercentage = fields.Float(missing=0)
    stepsCompleted = fields.Int(missing=1)
    totalSteps = fields.Int(missing=4)
    gracePeriod = fields.Int(missing=0)

    startDate = fields.Date(required=True)
    endDate = fields.Date(required=True)

    active = fields.Bool(missing=True)

    testGroup = fields.Nested(TestGroupSchema(), required=True)
    markingSchemaId = fields.Str(required=True)
    patternId = fields.Str(missing=None)
    orientationId = fields.Str(missing=None)
    markingSchemaType = fields.Str(missing=None)

    subjects = fields.List(fields.Nested(SubjectsSchema()), required=True)
    # List of hierarchies test created on
    hierarchy = fields.List(fields.Str(required=True), required=True)
    classes = fields.List(fields.Str(), missing=[])
