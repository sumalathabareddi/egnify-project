import time
from marshmallow import Schema, fields, validate

class AssetScheduleSchema(Schema):
    _id = fields.Str(required=True)
    instituteId = fields.Str(required=True)
    academicYear = fields.Str()
    orientationId = fields.List(fields.Str(required=True), required=True)
    patternId = fields.List(fields.Str(required=True), required=True)
    startTime = fields.Str(required=True)
    endTime = fields.Str(required=True)
    hierarchy = fields.List(fields.Str(required=True), required=True)
    categoryId = fields.Str(required=True)
    subject = fields.Str(required=True)
    topic = fields.Str(required=True)
    assetId = fields.Str(required=True)
    name = fields.Str(required=True)
    thumbnail = fields.Str(missing=None)
    active = fields.Bool(missing=True)
    link = fields.Str(missing=None)
    comment = fields.Str(missing=None)
