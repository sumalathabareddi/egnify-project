import toastedmarshmallow
from bson.objectid import ObjectId
from marshmallow import Schema, fields


class TestGroupsSchema(Schema):
    _id = fields.Str(missing=lambda: str(ObjectId()))
    instituteId = fields.Str(required=True)
    name = fields.Str(required=True)
    groupCode = fields.Str(required=True)
    description = fields.Str(missing=None)
    active = fields.Bool(missing=True)
