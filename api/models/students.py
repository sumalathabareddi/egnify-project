import toastedmarshmallow
from bson.objectid import ObjectId
from marshmallow import Schema, fields, validates, ValidationError


class AccessTagSchema(Schema):
    hierarchy = fields.Str(required=True)


class StudentSchema(Schema):
    _id = fields.Str(missing=lambda: str(ObjectId))
    instituteId = fields.Str(required=True)
    egnifyId = fields.Str(required=True)
    accessTag = fields.Nested(AccessTagSchema(), required=True)
    studentId = fields.Str(required=True)
    studentName = fields.Str(required=True)
    fatherName = fields.Str(missing=None)
    phone = fields.Str(missing=None)
    email = fields.Email(missing=None)
    dob = fields.Str(missing=None)
    gender = fields.Str(missing=None)
    category = fields.Str(missing=None)
    active = fields.Bool(missing=True)
    userCreated = fields.Bool(missing=True)
    block = fields.Bool(missing=False)

    @validates
    def validate_gender(self, value):
        if value and value not in ["M", "F", "Others"]:
            raise ValidationError("Allowed choices ['M', 'F', 'Others']")

    @validates
    def validate_category(self, value):
        if value and value not in ["SC", "ST", "OBC", "PWD", "Open"]:
            raise ValidationError("Allowed choices ['SC', 'ST', 'OBC', 'PWD', 'Open']")
