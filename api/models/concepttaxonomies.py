import toastedmarshmallow
from bson.objectid import ObjectId
from marshmallow import Schema, fields


class ConceptTaxononmySchema(Schema):
    _id = fields.Str(missing=lambda: str(ObjectId()))
    instituteId = fields.Str(missing=None)
    subject = fields.Str(required=True)
    subjectCode = fields.Str(required=True)
    topic = fields.Str(required=True)
    subTopic = fields.Str(required=True)
    code = fields.Str(required=True)
    active = fields.Bool(missing=True)
    orientations = fields.List(fields.Str(required=True), required=True)
    classes = fields.List(fields.Str(required=True), required=True)
    patterns = fields.List(fields.Str(required=True), required=True)
