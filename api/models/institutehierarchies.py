import time

import toastedmarshmallow
from bson.objectid import ObjectId
from marshmallow import Schema, fields


class InstituteHierarchiesSchema(Schema):
    active = fields.Bool(missing=True)
    isLeafNode = fields.Bool(missing=False)

    instituteId = fields.Str(required=True)
    instituteLevelCode = fields.Str(required=True)
    hierarchyName = fields.Str(required=True)
    _id = fields.Str(missing=lambda: str(ObjectId()))
    parentCode = fields.Str(missing=None)
    viewId = fields.Str(required=True)

    level = fields.Int(required=True)

    hierarchyTuple = fields.List(fields.Str(), missing=[])
