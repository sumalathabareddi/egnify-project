import time
from marshmallow import Schema, fields, validate

class LearnScheduleSchema(Schema):
    instituteId = fields.Str(required=True)
    academicYear = fields.Str()
    name = fields.Str(required=True)
    startTime = fields.Str(required=True)
    endTime = fields.Str(required=True)
    duration = fields.Int(required=True, validate=validate.Range(min=1))
    thumbnail = fields.Str(missing=None)
    orientationId = fields.List(fields.Str(required=True), required=True)
    classId = fields.List(fields.Str(required=True), required=True)
    patternId = fields.List(fields.Str(required=True), required=True)
    subject = fields.Str(required=True)
    faculty = fields.Str(required=True)
    topic = fields.Str(required=True)
    hierarchy = fields.List(fields.Str(required=True), required=True)
    streamData = fields.Dict(missing={})
    active = fields.Bool(missing=True)
