from marshmallow import Schema, fields
from random import randint


class AttactmentSchema(Schema):
    url = fields.Str(required=True)
    fileType = fields.Str(required=True)


class MessageSchema(Schema):
    isStudent = fields.Bool(missing=True)
    attachments = fields.List(fields.Nested(AttactmentSchema()), missing=[])
    message = fields.Str(missing=None)
    time = fields.Date(required=True)


class DoubtsSchema(Schema):
    _id = fields.Str(missing=lambda: str(randint(100000, 999999)))
    instituteId = fields.Str(required=True)
    egnifyId = fields.Str(required=True)
    studentName = fields.Str(required=True)
    hierarchy = fields.List(fields.Str(required=True), required=True)
    classes = fields.List(fields.Str(required=True), required=True)
    subject = fields.Str(required=True)
    topic = fields.Str(missing=None)
    messages = fields.List(fields.Nested(MessageSchema(), required=True), required=True)
    teacher = fields.Str(required=True)
    status = fields.Str(missing="pending")
    startDate = fields.Date(required=True)
    endDate = fields.Date(missing=None)
    active = fields.Bool(missing=True)
    isClear = fields.Bool(missing=False)
    bookmark_by_student = fields.Bool(missing=False)
    bookmark_by_teacher = fields.Bool(missing=False)
