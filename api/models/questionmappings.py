import uuid
import toastedmarshmallow
from marshmallow import Schema, fields, validate, validates_schema, ValidationError


class QuestionMappingSchema(Schema):
    topic = fields.Str(missing=None)
    topicCode = fields.Str(missing=None)
    subTopic = fields.Str(missing=None)
    subTopicCode = fields.Str(missing=None)
    questionNumberId = fields.Str(missing=None)
    subject = fields.Str()
    testId = fields.Str(required=True)
    questionNumber = fields.Str(required=True)
    qid = fields.Str(required=True)
    questionType = fields.Str()
    egnifyQuestionType = fields.Str()

    key = fields.List(fields.Str(), missing=[])

    noOfOptions = fields.Int(missing=4)
    C = fields.Int()
    W = fields.Int()
    U = fields.Int()
    ADD = fields.Int()
    P = fields.Int()

    active = fields.Bool(missing=True)
