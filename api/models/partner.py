import toastedmarshmallow
from bson.objectid import ObjectId
from marshmallow import Schema, fields


class PartnerSchema(Schema):
    _id = fields.Str(missing=lambda: str(ObjectId()))
    name = fields.Str(required=True)
    email = fields.Email(required=True)
