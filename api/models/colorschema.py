from marshmallow import Schema, fields


class ColorSchema(Schema):
    testId = fields.Str(required=True)
    instituteId = fields.Str(required=True)
    colorSchema = fields.List(fields.Dict(), missing=[])
