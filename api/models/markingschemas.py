import toastedmarshmallow
from marshmallow import Schema, fields, post_load


class SubjectMarkingSchema(Schema):
    numberOfQuestions = fields.Int(required=True)
    totalMarks = fields.Int(required=True)
    C = fields.Int(required=True)
    W = fields.Int(required=True)
    U = fields.Int(required=True)
    P = fields.Int(missing=0)
    ADD = fields.Int(missing=0)
    marksPerQuestion = fields.Int(required=True)
    noOfOptions = fields.Int(required=True)
    numberOfSubQuestions = fields.Int(required=True)
    paragraphQuestionDivision = fields.List(fields.Int(), missing=[])

    egnifyQuestionType = fields.Str(required=True)
    questionType = fields.Str(required=True)


class SubjectSchema(Schema):
    totalMarks = fields.Int(required=True)
    totalQuestions = fields.Int(required=True)
    tieBreaker = fields.Int(required=True)

    subject = fields.Str(required=True)

    marks = fields.List(
        fields.Nested(SubjectMarkingSchema(), required=True), required=True
    )


class MarkingSchema(Schema):
    name = fields.Str(required=True)
    orientationId = fields.Str(required=True)
    patternId = fields.Str(required=True)

    active = fields.Bool(missing=True)

    totalMarks = fields.Int(required=True)
    totalQuestions = fields.Int(required=True)

    subjects = fields.List(fields.Nested(SubjectSchema(), required=True), required=True)

    @post_load
    def make_schema(self, data):
        return _MarkingSchema(**data)


class QuestionTypeItem:
    def __init__(
        self,
        numberOfQuestions,
        totalMarks,
        C,
        W,
        U,
        P,
        ADD,
        marksPerQuestion,
        noOfOptions,
        numberOfSubQuestions,
        paragraphQuestionDivision,
        egnifyQuestionType,
        questionType,
    ):
        self.numberOfQuestions = numberOfQuestions
        self.totalMarks = totalMarks
        self.C = C
        self.W = W
        self.U = U
        self.P = P
        self.ADD = ADD
        self.marksPerQuestion = marksPerQuestion
        self.noOfOptions = noOfOptions
        self.numberOfSubQuestions = numberOfSubQuestions
        self.paragraphQuestionDivision = paragraphQuestionDivision
        self.egnifyQuestionType = egnifyQuestionType

    def __repr__(self):
        return (
            f"QuestionTypeItem <C:{self.C}, W:{self.W}, U:{self.U}, P:{self.P}, "
            f"Num of options:{self.noOfOptions}, "
            f"Question type:{self.egnifyQuestionType}> "
        )

    def __eq__(self, other):
        for attr in self.__dict__.keys():
            if getattr(self, attr) != getattr(other, attr):
                return False
        return True


class Subject:
    def __init__(self, totalMarks, totalQuestions, tieBreaker, subject, marks):
        self.totalMarks = totalMarks
        self.totalQuestions = totalQuestions
        self.tieBreaker = tieBreaker

        self.subject = subject

        self.marks = [QuestionTypeItem(**item) for item in marks]

    def __repr__(self):
        return (
            f"Subject <Marks: {self.totalMarks}, Questions: {self.totalQuestions}, "
            f"Question types: {self.marks}>"
        )

    def __eq__(self, other):
        for attr in self.__dict__.keys():
            if getattr(self, attr) != getattr(other, attr):
                return False
        return True


class _MarkingSchema:
    def __init__(
        self,
        name,
        active,
        orientationId,
        patternId,
        totalMarks,
        totalQuestions,
        subjects,
    ):
        self.name = name
        self.active = active
        self.orientationId = orientationId
        self.patternId = patternId
        self.totalMarks = totalMarks
        self.totalQuestions = totalQuestions
        self.subjects = [Subject(**item) for item in subjects]

    def __repr__(self):
        return (
            f"MarkingSchema <Marks: {self.totalMarks}, Questions: {self.totalQuestions}, "
            f"Subjects: {self.subjects}>"
        )

    def __eq__(self, other):
        return (
            self.orientationId == other.orientationId
            and self.patternId == other.patternId
            and self.totalMarks == other.totalMarks
            and self.totalQuestions == other.totalQuestions
            and len(self.subjects) == len(other.subjects)
            and all([s in other.subjects for s in self.subjects])
        )
