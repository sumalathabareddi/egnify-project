import secrets
from bson.objectid import ObjectId
from marshmallow import Schema, fields


def mask_str(length):
    allowed_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    return "PQ" + "".join(secrets.choice(allowed_chars) for i in range(length))


class QuestionsList(Schema):
    _id = fields.Str(missing=lambda: str(ObjectId()))
    questionId = fields.Integer(missing=0)
    q_name = fields.Str(missing=None)
    question = fields.Str(missing=None)
    answer = fields.Str(missing=None)
    questionType = fields.Str(missing=None)
    solution = fields.Str(missing=None)
    subTopic = fields.Str(missing=None)
    topic = fields.Str(missing=None)
    subject = fields.Str(missing=None)
    difficulty = fields.Str(missing=None)
    hint = fields.Str(missing=None)
    options = fields.List(fields.Str(), missing=[])
    questionHash = fields.Str(missing=None)
    key = fields.List(fields.Str(), missing=[])
    optionHash = fields.Str(missing=None)
    questionNumberId = fields.Str(missing=lambda: str(ObjectId()))
    instituteId = fields.List(fields.Str(), missing=[])
    questionTypeMetaData = fields.Dict(missing={})
    parsed = fields.Boolean(missing=False)
    maskId = fields.Str(missing=lambda: mask_str(8))
