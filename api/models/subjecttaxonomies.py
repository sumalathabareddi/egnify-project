import toastedmarshmallow
from bson.objectid import ObjectId
from marshmallow import Schema, fields


class SubjectTaxonomySchema(Schema):
    _id = fields.Str(missing=lambda: str(ObjectId()))
    code = fields.Str(required=True)
    name = fields.Str(required=True)
    color = fields.Str(missing=None)
    icon = fields.Str(missing=None)
