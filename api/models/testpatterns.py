import toastedmarshmallow
from bson.objectid import ObjectId
from marshmallow import Schema, fields


class TestPatternSchema(Schema):
    orientationId = fields.Str(required=True)
    pattern = fields.Str(required=True)
    _id = fields.Str(missing=lambda: str(ObjectId()))
    subjects = fields.List(fields.Str(required=True), required=True)
    active = fields.Bool(missing=True)
