import toastedmarshmallow
from bson.objectid import ObjectId
from marshmallow import Schema, fields


class HierarchySchema(Schema):
    parent = fields.Str(missing=None)
    name = fields.Str(required=True)
    code = fields.Str(required=True)
    level = fields.Int(required=True)


class AcademicScheduleSchema(Schema):
    start = fields.Dict()
    end = fields.Dict()


class InstituteSchema(Schema):
    _id = fields.Str(missing=lambda: str(ObjectId()))
    partnerId = fields.Str(missing=None)
    allowedSystemModules = fields.List(fields.Str(required=True), missing=[])
    url = fields.Str(missing=None)
    subdomain = fields.Str(missing=None)
    email = fields.Email(required=True)
    adminName = fields.Str(required=True)
    adminPhone = fields.Str(required=True)
    instituteName = fields.Str(required=True)
    logoUrl = fields.Str(missing=None)
    establishmentYear = fields.Str(missing=None)
    academicSchedule = fields.Nested(AcademicScheduleSchema(), missing={})
    hierarchy = fields.List(fields.Nested(HierarchySchema()), required=True)
    systemHierarchies = fields.List(fields.Dict(), missing=[])
    registrationStatus = fields.Bool(missing=True)
    questionRefs = fields.List(fields.Str(), missing=[])
    questionTags = fields.List(fields.Str(), missing=[])
    allowedTestSeries = fields.List(fields.Dict(), missing=[])
    salesStudentsCount = fields.Int(missing=None)
    plan = fields.Str(missing=None)
