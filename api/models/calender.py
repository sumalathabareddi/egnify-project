from marshmallow import Schema, fields, validate
from bson.objectid import ObjectId
from datetime import datetime


class CalenderSchedule(Schema):
    _id = fields.Str(missing=lambda: str(ObjectId()))
    type = fields.Str(required=True)  # live/test/assignment
    schedule_id = fields.Str(required=True)
    name = fields.Str(required=True)
    startTime = fields.Date(required=True)
    endTime = fields.Date(missing=None)
    active = fields.Boolean(missing=True)
    hierarchy = fields.List(fields.Str(required=True), required=True)
    instituteId = fields.Str(missing=None)
