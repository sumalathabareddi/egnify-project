import importlib
import uuid
import string
import random
import time
import copy



from flask import Blueprint, current_app, request
import redis
from datetime import datetime, timezone
from collections import defaultdict
from snapshot.StudentScheduleSnapshot import StudentScheduleSnapshot
from datetime import datetime,timedelta,timezone
from pytz import timezone as tz


from rq import Queue
from zoom.apiclient import ApiClient
import utils.decorators as decorators
from utils.egy_analytics import cache

from .schedule import schedule_task


blueprint = Blueprint("live", __name__, url_prefix="/live")

security_group_id = (
    role_arn
) = live_stream = queue = harvest_job_arn = harvest_bucket = mongo_db =arango_db= None


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global security_group_id, role_arn, queue, live_stream, harvest_job_arn, harvest_bucket, mongo_db, settings_database,arango_db,ga_database
    role_arn = current_app.config["ROLE_ARN"]
    security_group_id = current_app.config["SECURITY_GROUP_ID"]
    redis_connection = redis.Redis.from_url(current_app.config["RQ_REDIS_URL"])
    queue = Queue(current_app.config["RQ_QUEUES"], connection=redis_connection)
    live_stream = importlib.import_module(current_app.config["LIVE_STREAM_FILE"])
    harvest_job_arn = current_app.config["HARVEST_JOB_ROLE_ARN"]
    harvest_bucket = current_app.config["HARVEST_JOB_BUCKET"]
    settings_database = current_app.config["SETTINGS_DATABASE"]
    mongo_db = current_app.mongo_db
    arango_db = current_app.arango_db
    ga_database = current_app.config["GA_DATABASE"]



@blueprint.route("/link/<institute_id>", methods=["POST"])
@decorators.make_egnify_response
def stream_links(institute_id):
    """

    :param institute_id:
    :return:
    """

    form_data = request.json
    name = form_data.get("name", None)
    if not name:
        return "Bad Req.", 400, {}
    name = form_data["name"]
    identifier = name.lower().replace(" ", "_")
    channel_id = f"{institute_id}_{identifier}_{uuid.uuid1()}"
    data = {
        "channelId": channel_id,
        "resolution": form_data.get("resolution", "720"),
        "endpoint": form_data.get("endpoint", "HLS"),
        "name": f"{institute_id}_{name}",
        "securityGroupId": security_group_id,
        "roleArn": role_arn,
    }

    return_response = {}
    job = queue.enqueue(live_stream.create_stream, job_timeout="30m", args=(data,))
    return_response["STATUS"] = "SUCCESS"
    return_response["JOB_ID"] = job.get_id()
    return return_response, 200, {}


@blueprint.route("/zoomlink/<string:institute_id>", methods=["POST"])
@decorators.make_egnify_response
def zoom_links(institute_id):
    """
        zoom credential is stored institute wise in institute collection in settings db.
        data structure: 
        zoom:{
            ZOOM_API_KEY: xxxxxx,
            ZOOM_SECRET_KEY: xxxxxx,
            ZOOM_API_URL: xxxxxx
        }
    """
    post_data = request.get_json()
    scheduled_for = post_data.get("scheduled_for", None)
    topicName = post_data.get("topic", None)
    duration = post_data.get("duration", None)

    if not scheduled_for or not topicName or not duration:
        return "Bad_Args", 400, {}

    rand_str = "".join(random.choices(string.ascii_uppercase + string.digits, k=8))

    post_data["password"] = rand_str
    redis_key = f"{institute_id}_zoom_key"

    zoom_cred = cache.get(redis_key)

    if not zoom_cred:
        db = current_app.config["SETTINGS_DATABASE"]
        collection = "institutes"

        query = {"_id": institute_id}
        project = {"_id": 0, "zoom": 1}

        institute_details = mongo_db.find_one(db, collection, query, project)
        zoom_cred = institute_details.get("zoom", None)
        if not zoom_cred:
            return "Please contact administrator.", 400, {}

        cache.set(redis_key, zoom_cred, 36000)

    zoomClient = ApiClient(
        zoom_cred["ZOOM_API_KEY"],
        zoom_cred["ZOOM_SECRET_KEY"],
        zoom_cred["ZOOM_API_URL"],
    )

    data = zoomClient.create_meeting(post_data)

    return data, 200, {}



@blueprint.route("/<string:schedule_id>/snapshot", methods=["POST"])
@decorators.roles_required(["LEARN_SCHEDULE_CREATOR"])
@decorators.make_egnify_response
def create_schedule_snapshot(schedule_id):
    """
        Returns the schedule snapshot creation status.
    """
    schedule_collection_name = "learnschedule"
    snapshot_collection = "scheduleStudentSnapshot"

    query = {"scheduleId": schedule_id}
    # clean the previous data if exists
    mongo_db.delete(ga_database, snapshot_collection, query)
    # fetch the hierarchies for which schedule has been created.
    schedule_query = {"_id": schedule_id}
    schedule_data = mongo_db.find(settings_database, schedule_collection_name, schedule_query, key="_id")
    print('name:::',schedule_data[schedule_id]['name'])
    schedule_hierarchies = schedule_data[schedule_id]["accessTag"]["hierarchy"]
    # fetch the leaf level nodes for student snapshot creation
    leaf_nodes = []
    for each_hierarchy in schedule_hierarchies:
        hierarchy_leaf_nodes = arango_db.get_leaf_nodes(each_hierarchy)
        hierarchy_leaf_nodes = [node["id"] for node in hierarchy_leaf_nodes]
        leaf_nodes.extend(hierarchy_leaf_nodes)
    # use the test least level nodes to fetch students

    start_time = time.time()
    students_query = {"accessTag.hierarchy": {"$in": leaf_nodes}, "active": True}
    students_coll_name = "studentInfo"
    students_kwargs = {"key": "_id"}
    students = mongo_db.find(
        settings_database, students_coll_name, students_query, **students_kwargs
    )
    print('from:db::',len(students))

    current_app.logger.info(
        f"Time taken to read from database for students {time.time() - start_time}"
    )
    start_time = time.time()

    lookup = {}
    # create snapshot for all the students
    snapshot_list = []
    for student_info in students.values():
        student_snapshot = StudentScheduleSnapshot(
           schedule_id, schedule_data[schedule_id]["name"]
        )
        student_snapshot.fill_student_details(student_info, False) #initially joined = False
        hierarchy_levels = []
        parent_hierarchy = student_info["accessTag"]["hierarchy"]
        while parent_hierarchy and parent_hierarchy != "root":
            hierarchy_levels.append(parent_hierarchy)
            if parent_hierarchy not in lookup:
                parent_node = arango_db.get_node(parent_hierarchy)
                lookup[parent_hierarchy] = parent_node
            parent_node = lookup[parent_hierarchy]
            parent_hierarchy = parent_node["parentCode"]
        student_snapshot.hierarchyLevels = hierarchy_levels
        snapshot_list.append(vars(student_snapshot))
    current_app.logger.info(
        f"Time taken to create student snapshot list {time.time() - start_time}"
    )
    print('check::',len(snapshot_list))
    mongo_db.insert_many_in_process(ga_database, snapshot_collection, snapshot_list)
    mongo_db.wait_for_termination()
    current_app.logger.info(
        f"Time taken to write to database {time.time() - start_time}"
    )
    return_response = {
        "STATUS": "SUCCESS",
        "DATA": None,
        "MESSAGE": f"Created schedule snapshot for {len(snapshot_list)} students",
    }
    return return_response, 200, {}


@blueprint.route("/<string:institute_id>/<string:schedule_id>/attendance", methods=["GET", "POST"])
@decorators.make_egnify_response
@decorators.authorization_required()
def get_schedule_attendance(institute_id,schedule_id):
    start_time, coll = time.time(), "scheduleStudentSnapshot"
    schedule_collection_name = "learnschedule"
    hierarchy = request.values.get("hierarchy", "")
    status = request.values.get("status")
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 20, type=int)
    search_text = request.values.get("searchText", "")
    hierarchies = hierarchy.split(",")
    redis_key = f"{institute_id}_zoom_key"
    
    query = {"_id": schedule_id}
    query["startTime"] = {"$lte": datetime.now(timezone.utc)}
    query["endTime"] = {"$gte": datetime.now(timezone.utc)}
    schedule_data = mongo_db.find(settings_database, schedule_collection_name, query, key="_id")
    if schedule_data:
        meeting_id = schedule_data[schedule_id]["streamData"]["uuid"]
        next_page_token = schedule_data[schedule_id].get("next_page_token","")
        print('meeting_id::',meeting_id)
        zoom_cred = cache.get(redis_key)

        if not zoom_cred:
            db = current_app.config["SETTINGS_DATABASE"]
            collection = "institutes"

            query = {"_id": institute_id}
            project = {"_id": 0, "zoom": 1}

            institute_details = mongo_db.find_one(db, collection, query, project)
            zoom_cred = institute_details.get("zoom", None)
            if not zoom_cred:
                return "Please contact administrator.", 400, {}

            cache.set(redis_key, zoom_cred, 36000)

        zoomClient = ApiClient(
            zoom_cred["ZOOM_API_KEY"],
            zoom_cred["ZOOM_SECRET_KEY"],
            zoom_cred["ZOOM_API_URL"],
        )
        params = {
            "type": "live",
            "paze_size":size,
            "next_page_token": next_page_token
        }
        data = zoomClient.get_meeting_attendance(meeting_id,params)
        if "next_page_token" in data:
            update_data = {"next_page_token": data["next_page_token"]}
            mongo_db.find_one_and_upsert(settings_database,schedule_collection_name, query, update_data)
        
        participants = data.get("participants",[])
        students_query = {
            "scheduleId":schedule_id,
            "joined": False
        }
        students_list = mongo_db.get_results(ga_database,coll,students_query, key="_id")
        for participant in participants:
            for _id in  students_list:
                if students_list[_id]["email"] == participant['user_email']:
                    students_list[_id]["joined"] = True
                    students_list[_id].update(participant)
        snapshot_docs = list(students_list.values())
        if snapshot_docs:
            mongo_db.update_many_in_process(
                ga_database, "scheduleStudentSnapshot", snapshot_docs, id_field="_id",
            )
    query = {"scheduleId": schedule_id, "hierarchyLevels": {"$in": hierarchies}}
    if status and status == "joined":
        query['joined'] = True
    if status and status == "absent":
        query['joined'] = False
    kwargs = {
        "skip": skip,
        "size": size,
    }
    if search_text:
        query["$or"] = [
            {"studentName": {"$regex": search_text, "$options": "i"}},
            {"studentId": {"$regex": search_text, "$options": "i"}},
        ]
    data = mongo_db.find(ga_database, coll, query, **kwargs)
    count = mongo_db.count(ga_database, coll, query)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {coll}: {db_duration}"
    )
    resp = {"STATUS": "SUCCESS", "DATA": {"students": data, "count": count}}
    return resp, 200, {}
    

@blueprint.route("/<string:schedule_id>/stats", methods=["GET", "POST"])
@decorators.make_egnify_response
def get_schedule_stats(schedule_id):
    """
    Returns test taking numbers
    """
    coll = "scheduleStudentSnapshot"
    # Used for filtering results for user
    hierarchies = request.values.get("hierarchy", "").split(",")
    leaf_nodes = []
    for hierarchy in hierarchies:
        nodes = cache.get(f"leaf_nodes_{hierarchy}")
        if not nodes:
            nodes = arango_db.get_leaf_nodes(hierarchy)
            cache.set(f"leaf_nodes_{hierarchy}", nodes, 1800)
        leaf_nodes.extend([node["id"] for node in nodes])
    # Query the the test with id and leaf hierarchies
    query = {"scheduleId": schedule_id, "hierarchyLevels": {"$in": leaf_nodes}}
    joined_query = copy.deepcopy(query)
    joined_query.update({"joined": True})
    not_joined_query = copy.deepcopy(query)
    not_joined_query.update({"joined": False})
    count = mongo_db.count(ga_database, coll, query)
    online = mongo_db.count(ga_database, coll, joined_query)
    not_started = mongo_db.count(ga_database, coll, not_joined_query)
    resp = {
        "STATUS": "SUCCESS",
        "DATA": {"total": count, "started": online, "not_started": not_started},
    }
    return resp, 200, {}


@blueprint.route("/job/<job_id>", methods=["GET"])
@decorators.make_egnify_response
def get_job_status(job_id):
    """

    :param job_id:
    :return:
    """
    return_response = {}
    job = queue.fetch_job(job_id)
    if job:
        return_response["STATUS"] = "SUCCESS"
        return_response["JOB_STATUS"] = job.get_status()
        return_response["DATA"] = job.meta
        return return_response, 200, {}
    else:
        return_response["STATUS"] = "FAILURE"
        return return_response, 404, {}


@blueprint.route("/start/<channel_id>/channel", methods=["POST"])
@decorators.make_egnify_response
def start_channel(channel_id):
    data = {
        "ChannelId": channel_id,
    }
    return_response = {}
    job = queue.enqueue(live_stream.start_channel, job_timeout="30m", args=(data,))
    return_response["STATUS"] = "SUCCESS"
    return_response["JOB_ID"] = job.get_id()
    return return_response, 200, {}


@blueprint.route("/delete/<channel_id>/channel", methods=["POST"])
@decorators.make_egnify_response
def delete_channel(channel_id):
    data = {
        "channelId": channel_id,
    }
    return_response = {}
    job = queue.enqueue(live_stream.delete_channel, job_timeout="30m", args=(data,))
    return_response["STATUS"] = "SUCCESS"
    return_response["JOB_ID"] = job.get_id()
    return return_response, 200, {}


@blueprint.route("/harvest/<institute_id>", methods=["POST"])
@decorators.make_egnify_response
def harvest_job(institute_id):
    data = request.json
    Id = f"{institute_id}_{uuid.uuid1()}"
    config = {
        "end_time": data.get("endTime"),
        "end_point_id": data.get("endPointId"),
        "start_time": data.get("startTime"),
        "bucket_name": harvest_bucket,
        "role_arn": harvest_job_arn,
        "id": Id,
        "manifest_key": data.get("manifestKey"),
    }

    return_response = {}
    job = queue.enqueue(live_stream.harvest_job, job_timeout="10m", args=(config,))
    return_response["STATUS"] = "SUCCESS"
    return_response["JOB_ID"] = job.get_id()
    return return_response, 200, {}


@blueprint.route("/vod/zoom/<institute_id>/<live_class_id>", methods=["POST"])
@decorators.make_egnify_response
def vod_zoom(institute_id, live_class_id):
    data = request.get_json()
    end_datetime = data["end_date"]
    date = datetime.strptime(end_datetime, "%Y/%m/%d %H:%M:%S")
    data = {
        "module": "zoom_to_vod",
        "method": "on_schedule",
        "dt": date,
        "args": {"live_class_id": live_class_id, "institute_id": institute_id,},
    }
    sheduled_task = schedule_task(data)
    return schedule_task, 200, {}


@blueprint.route("/<string:institute_id>/zoom/timeslots",methods=["POST"])
@decorators.make_egnify_response
def get_time_slots(institute_id):
    """
        :param institute_id
        :param date
        :param duration
        :return list[{"startTime": "", "endTime":""}]
    """
    post_data = request.get_json()
    start_time = post_data.get("start_time",None)
    duration = post_data.get("duration",None)
    faculty = post_data.get("teacher",None)
    if not start_time or not duration or not faculty:
        return "Bad Args", 400, {}
    slots = time_slots(duration)
    query = {
        "instituteId":institute_id,
        "startTime": {"$gte":get_day_start_date()},
        "endTime": {"$lte": get_day_end_date()},
        "faculty": base64.b64encode(faculty.encode()).decode()
    }
    schedule_data = mongo_db.find(current_app.config["SETTINGS_DATABASE"], "learnschedule", query)
    if schedule_data:
        for schedule in schedule_data:
            start_time = schedule['startTime'].replace(tzinfo=timezone.utc).astimezone(tz=tz('Asia/Kolkata'))
            end_time = schedule['endTime'].replace(tzinfo=timezone.utc).astimezone(tz=tz('Asia/Kolkata'))
            booked_slot = (start_time,end_time)
            if booked_slot in slots:
                slots.remove(booked_slot)
    return slots, 200, {}

def get_day_start_date():
    dt = datetime.now(timezone.utc)
    dt = dt.replace(hour=0, minute=0, second=0, microsecond=0)
    return dt

def get_day_start_date():
    dt = datetime.utcnow()
    dt = dt.replace(hour=0, minute=0, second=0, microsecond=0)
    return dt
def get_day_end_date():
    return get_day_start_date()+ timedelta(1)
def time_slots(duration):
    start_date = get_day_start_date()
    end_date = get_day_end_date()
    slots =[]
    while (start_date+timedelta(minutes=duration)<=end_date):
        start = start_date.replace(tzinfo=timezone.utc).astimezone(tz=tz('Asia/Kolkata'))
        end = start_date+timedelta(minutes=duration)
        end = end.replace(tzinfo=timezone.utc).astimezone(tz=tz('Asia/Kolkata'))
        slots.append((start,end))
        start_date = start_date+ timedelta(minutes=duration)
    return slots