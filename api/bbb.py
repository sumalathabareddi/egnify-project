import time
from flask import Blueprint, current_app, request
import utils.decorators as decorators
from utils.egy_analytics import cache
import string
import json
from utils import rand_password_string

from bigbluebutton import apiclient as bbb

blueprint = Blueprint("bigbluebutton", __name__, url_prefix="/virtual-classroom")

settings_database = mongo_db = tenant_registry_database = None


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global mongo_db, settings_database, tenant_registry_database
    mongo_db = current_app.mongo_db
    settings_database = current_app.config["SETTINGS_DATABASE"]
    tenant_registry_database = current_app.config["TENANT_REGISTRY_DATABASE"]


@blueprint.route(
    "/<string:institute_id>/<string:live_class_id>/create", methods=["POST"]
)
@decorators.make_egnify_response
def create_metting(institute_id, live_class_id):
    data = request.json
    if not data["facultyId"]:
        return "Faculty id is mandatory", 400, {}

    server_cred = cache.get(f"bbb_cred{institute_id}")
    if not server_cred:
        server_cred = mongo_db.find_one(
            settings_database,
            "institutes",
            {"_id": institute_id},
            {"_id": 0, "bigbluebutton": 1},
        )
        if not server_cred:
            return "Please contact administrator.", 400, {}
        server_cred = server_cred["bigbluebutton"]
        cache.set(f"bbb_cred{institute_id}", server_cred, 86400)

    option = {}
    option["allowStartStopRecording"] = data.get("allowStartStopRecording", "false")
    option["attendeePW"] = data.get("attendeePW", rand_password_string(8))
    option["autoStartRecording"] = data.get("autoStartRecording", "false")
    option["meetingID"] = data.get("meetingID", rand_password_string(8))
    option["moderatorPW"] = data.get("moderatorPW", rand_password_string(8))
    option["name"] = data.get("name", f"random-{rand_password_string(8)}")
    meeting_name = option["name"]
    meeting_id = option["meetingID"]
    option["meetingId"] = f"{meeting_id}_{institute_id}"
    option["record"] = data.get("record", "false")
    option["welcome"] = data.get(
        "welcome", "%3Cbr%3EWelcome+to+%3Cb%3E%25%25CONFNAME%25%25%3C%2Fb%3E%21"
    )
    option["bbbSharedSecret"] = server_cred.get("bbbSharedSecret")
    option["bbbServerUrl"] = server_cred.get("bbbServerUrl")
    meeting_info = bbb.create_meeting(option)
    meeting_info = json.loads(meeting_info)
    if meeting_info["response"]["returncode"] != "SUCCESS":
        return "Failure while creating meeting", 500, {}

    update_live_class = mongo_db.find_one_and_upsert(
        settings_database,
        "learnschedule",
        {"_id": live_class_id},
        {"streamData": meeting_info["response"], "facultyId": data["facultyId"]},
    )
    if not update_live_class:
        return "Invalid live class id", 400, {}

    return "Success", 200, {}


@blueprint.route(
    "/<string:institute_id>/<string:live_class_id>/<string:user_id>/link",
    methods=["GET"],
)
@decorators.make_egnify_response
def generate_metting_url(institute_id, live_class_id, user_id):
    server_cred = cache.get(f"bbb_cred{institute_id}")

    if not server_cred:
        return "Please contact administrator.", 400, {}

    meeting_info = cache.get(f"bbb_meeting_info_{live_class_id}")
    if not meeting_info:
        meeting_info = mongo_db.find_one(
            settings_database,
            "learnschedule",
            {"_id": live_class_id},
            {"_id": 0, "streamData": 1, "duration": 1, "facultyId": 1},
        )
        if not meeting_info:
            return "No Such meeting running", 400, {}
        duration = meeting_info["duration"] * 60
        cache.set(f"bbb_meeting_info_{live_class_id}", meeting_info, duration)
    userInfo = mongo_db.find_one(tenant_registry_database, "users", {"_id": user_id})
    if not userInfo:
        return "Invalid userid.", 400, {}

    option = {}
    option["fullName"] = userInfo["username"].replace(" ", "+")
    option["redirect"] = "true"
    option["meetingID"] = meeting_info["streamData"]["meetingID"]
    option["bbbSharedSecret"] = server_cred.get("bbbSharedSecret")
    option["bbbServerUrl"] = server_cred.get("bbbServerUrl")
    if user_id == meeting_info["facultyId"]:
        option["password"] = meeting_info["streamData"]["moderatorPW"]
    else:
        option["password"] = meeting_info["streamData"]["attendeePW"]

    url = bbb.join_url(option)
    response = {"STATUS": "SUCCESS", "DATA": url}
    return response, 200, {}


@blueprint.route("/<string:institute_id>/<string:live_class_id>/info", methods=["GET"])
@decorators.make_egnify_response
def vc_class_info(institute_id, live_class_id):
    server_cred = cache.get(f"bbb_cred{institute_id}")

    if not server_cred:
        return "Please contact administrator.", 400, {}

    meeting_info = cache.get(f"bbb_meeting_info_{live_class_id}")
    if not meeting_info:
        return "No such Class Running", 400, {}
    options = {}
    options["meetingID"] = meeting_info["streamData"]["meetingID"]
    options["password"] = meeting_info["streamData"]["moderatorPW"]
    options["bbbSharedSecret"] = server_cred.get("bbbSharedSecret")
    options["bbbServerUrl"] = server_cred.get("bbbServerUrl")

    meeting_details = bbb.meeting_info(options)
    meeting_details = json.loads(meeting_details)

    del meeting_details["response"]["moderatorPW"]
    del meeting_details["response"]["attendeePW"]

    return meeting_details, 200, {}
