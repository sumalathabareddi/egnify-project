from flask import Blueprint, current_app, request
from utils.decorators import make_egnify_response
from utils.StorageHelper import StorageHelper

blueprint = Blueprint("util", __name__, url_prefix="/util")


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global storage_secrets, storage_provider
    storage_provider = current_app.config["STORAGE_PROVIDER"]
    storage_secrets = {}
    if storage_provider == "GCP":
        storage_secrets["GCP_CREDENTIALS_FILE"] = current_app.config[
            "GCP_CREDENTIALS_FILE"
        ]
    if storage_provider == "MINIO":
        storage_secrets["endpoint"] = current_app.config["MINIO_ENDPOINT"]
        storage_secrets["access_key"] = current_app.config["MINIO_ACCESS_KEY"]
        storage_secrets["secret_key"] = current_app.config["MINIO_SECRET_KEY"]
        storage_secrets["secure"] = True


@blueprint.route("/public_url", methods=["POST"])
@make_egnify_response
def get_public_url():
    response_data, status_code, extra_headers = {}, 200, {}

    uploaded_file = request.files.get("file")
    uploaded_file_string = uploaded_file.read()
    bucket_name = ""
    if request.values.get("type", None) == "learn":
        bucket_name = current_app.config["LEARN_PUBLIC_BUCKET"]
    elif storage_provider == "GCP":
        bucket_name = current_app.config["CLOUD_STORAGE_BUCKET"]
    elif storage_provider == "MINIO":
        bucket_name = current_app.config["MINIO_STORAGE_BUCKET"]

    storage_helper = StorageHelper(storage_provider, storage_secrets)
    extention = uploaded_file.filename.split(".")[-1].lower()
    data = {
        "file_ext": f".{extention}",
        "file_name": uploaded_file.filename,
        "data": uploaded_file_string,
        "content_type": uploaded_file.content_type,
        "bucket_name": bucket_name,
        "content_disposition": "inline",
    }

    storage_helper = StorageHelper(storage_provider, storage_secrets)

    url = storage_helper.upload(data, get_public_url=True)

    response_data = {
        "STATUS": "SUCCESS",
        "MESSAGE": "Url fetched successfully",
        "DATA": url,
    }

    return response_data, status_code, extra_headers
