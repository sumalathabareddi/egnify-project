import csv
import time

import requests
import utils.decorators as decorators
from flask import Blueprint, current_app, request
from utils.EgnifyException import MultipleValidationExceptions, ValidationException
from utils.egy_analytics import cache
from utils import md5

from bulkread.BulkRead import HierarchiesBulkRead

from .models.institutehierarchies import InstituteHierarchiesSchema

blueprint = Blueprint("hierarchies", __name__, url_prefix="/hierarchies")

mongo_db = arango_db = ga_database = settings_database = None


@blueprint.before_app_first_request
def init_bp():
    """"""
    global mongo_db, arango_db, ga_database, settings_database
    mongo_db = current_app.mongo_db
    arango_db = current_app.arango_db
    ga_database = current_app.config["GA_DATABASE"]
    settings_database = current_app.config["SETTINGS_DATABASE"]


@blueprint.route("")
@decorators.roles_required(
    [
        "SETTINGS_CREATOR",
        "SETTINGS_READER",
        "TEST_CREATOR",
        "TEST_VIEWER",
        "REPORTS_VIEWER",
        "ANALYSIS_VIEWER",
    ]
)
@cache.cached(timeout=86400)
@decorators.make_egnify_response
def get_old_institute_hierarchies():
    """

    :return:
    """
    start_time = time.time()
    collection_name = "institutes"
    query = {}
    hierarchies = mongo_db.get_results(
        settings_database, collection_name, query, key="instituteId"
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        "Time taken to fetch the results from {}: {}".format(
            collection_name, db_duration
        )
    )
    custom_headers = {"Cache-Control": "private, max-age=14400"}
    return hierarchies, 200, custom_headers


@blueprint.route("/<hierarchy_id>/children")
@decorators.roles_required(
    [
        "SETTINGS_CREATOR",
        "SETTINGS_READER",
        "TEST_CREATOR",
        "TEST_VIEWER",
        "REPORTS_VIEWER",
        "ANALYSIS_VIEWER",
        "STUDENT",
    ]
)
@decorators.authorization_required()
# @cache.cached(timeout=86400, query_string=True)
@decorators.make_egnify_response
def get_children_by_hierarchy(hierarchy_id):
    """
    Returns the institute hierarchies at level `level`.
    """
    institute_id = request.values.get("institute", None)
    if institute_id:
        institute_id = md5(institute_id)
    hierarchy_sub_graph = arango_db.get_node_data_as_json(
        hierarchy_id, stop_node=institute_id
    )
    # custom_headers = {"Cache-Control": "private, max-age=14400"}
    if hierarchy_sub_graph:
        return hierarchy_sub_graph, 200, {}
    else:
        return {}, 200, {}


@blueprint.route("/<string:hierarchy_id>/<string:test_id>")
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@cache.cached(timeout=86400)
@decorators.make_egnify_data_response
def get_hierarchies_for_test(hierarchy_id, test_id):
    """
    Return hierarchies data for test at given level
    """
    start_time = time.time()
    hierarchy_ids = current_app.arango_db.get_descendants(hierarchy_id)
    query = {"testId": test_id, "childCode": {}}
    query["childCode"]["$in"] = hierarchy_ids
    collection_name = "hierarchicalAnalysis"
    no_projection = ["topicAnalysis", "questionMap"]
    kwargs = {"projection": {field: 0 for field in no_projection}}
    hierarchies = mongo_db.get_results(ga_database, collection_name, query, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    custom_headers = {"Cache-Control": "private, max-age=14400"}
    return hierarchies, 200, custom_headers


@blueprint.route("/<institute_id>", methods=["POST"])
@decorators.roles_required(["SETTINGS_CREATOR"])
@decorators.make_egnify_response
def add_hierarchies(institute_id):
    """API endpoint to add institute hierarchies.
    ---
    post:
      description: Add institute hierarchies
      responses:
        201:
          description: Return add institute hierarchies status
        consumes:
            - 'multipart/form-data'
    """
    response_data, status_code, extra_headers = {}, 200, {}
    url = request.values.get("fileUrl")

    institute_details = mongo_db.find_one(
        settings_database, "institutes", {"_id": institute_id}
    )
    # Add hierarchies through file upload
    if url:
        with requests.Session() as s:
            downloaded_data = s.get(url)
            decoded_data = downloaded_data.content.decode("utf-8-sig")

            csv_reader = csv.reader(decoded_data.splitlines(), delimiter=",")
            csv_headers = next(csv_reader)
            csv_data = list(csv_reader)
    else:
        csv_headers = request.values.get("headers", [])
        csv_data = request.values.get("data", [])
    current_app.logger.info("Completed reading hierarchies csv data")
    system_hierarchies = mongo_db.find(settings_database, "systemhierarchies", query={})
    node_count = len(arango_db.find({"instituteId": institute_id}))
    print('node_count',node_count)
    current_app.logger.info("Completed fetching node count")
    hbr = HierarchiesBulkRead(
        institute_details, csv_headers, csv_data, system_hierarchies, node_count
    )
    errors = hbr.validate()
    if errors:
        raise MultipleValidationExceptions(errors)
    docs = hbr.create_hierarchies()
    result = InstituteHierarchiesSchema(many=True).load(docs)
    if result.errors:
        response_data = result.errors
        status_code = 400
    else:
        total_hierarchies, insert_error_count, errors = len(result.data), 0, []
        for row, hierarchy in enumerate(result.data, 1):
            # Add node if doesn't already exists
            if not arango_db.has_node(hierarchy["_id"]):
                arango_db.create_node(hierarchy["_id"], hierarchy)
            # Make sure node with hierarchy["parentCode"] exists,
            # raise error if it doesn't exists
            if not arango_db.has_node(hierarchy["parentCode"]):
                errors.append(ValidationException("E403", row, None))
            else:
                if not arango_db.has_edge(hierarchy["parentCode"], hierarchy["_id"]):
                    arango_db.create_edge(hierarchy["parentCode"], hierarchy["_id"])
        if errors:
            raise MultipleValidationExceptions(errors)
        else:
            cache.clear()
            response_data = {
                "MESSAGE": f"Added {total_hierarchies-insert_error_count} "
                f"hierarchies successfully",
                "STATUS": "SUCCESS",
                "DATA": None,
            }
    return response_data, status_code, extra_headers


@blueprint.route("/system")
@decorators.roles_required(["PARTNER_ADMIN", "SETTINGS_CREATOR"])
@decorators.make_egnify_response
def system_hierarchies():
    hierarchies = mongo_db.find(ga_database, "systemhierarchies", {})
    response_data = {"STATUS": "STATUS", "DATA": hierarchies}
    return response_data, 200, {}
