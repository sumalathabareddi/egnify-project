import csv
import json
import time
from io import BytesIO, StringIO
from collections import defaultdict

import pdfkit
import utils.decorators as decorators
from flask import Blueprint, Markup, current_app, render_template, request
from utils import deep_defaultdict
from utils.StorageHelper import StorageHelper

from reports import ReportsHelper
from aggregation import Aggregation
from stackmachine.hierarchy_analysis import HierarchyAnalysis
from .analysis import prepare_query_for_rank

blueprint = Blueprint("downloads", __name__, url_prefix="/downloads")

mongo_db = ga_database = settings_database = storage_secrets = None


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global mongo_db, ga_database, settings_database, storage_secrets
    mongo_db = current_app.mongo_db
    ga_database = current_app.config["GA_DATABASE"]
    settings_database = current_app.config["SETTINGS_DATABASE"]
    storage_secrets = {}
    if current_app.config["STORAGE_PROVIDER"] == "GCP":
        storage_secrets["GCP_CREDENTIALS_FILE"] = current_app.config[
            "GCP_CREDENTIALS_FILE"
        ]
    if current_app.config["STORAGE_PROVIDER"] == "MINIO":
        storage_secrets["endpoint"] = current_app.config["MINIO_ENDPOINT"]
        storage_secrets["access_key"] = current_app.config["MINIO_ACCESS_KEY"]
        storage_secrets["secret_key"] = current_app.config["MINIO_SECRET_KEY"]
        storage_secrets["secure"] = True


@blueprint.route(
    "/<hierarchy_id>/test/<test_id>/rank/download", methods=["GET", "POST"]
)
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_required(hierarchy_query=True)
@decorators.make_egnify_response
def download_rank_report(hierarchy_id, test_id):
    """Rank analysis data download.
    """
    file_name = request.values.get("filename", "Rank_Analysis_{}.csv".format(test_id))
    # List of instructions from client for preparing data.
    fields = json.loads(request.values.get("fields", {}))
    # Optional skip and limit args.
    skip = request.values.get("skip", 0, type=int)
    limit = request.values.get("size", 0, type=int)
    percentage = request.values.get("percentage",None)
    start_time = time.time()
    collection_name = "rankAnalysis"
    match = prepare_query_for_rank(hierarchy_id, test_id)
    aggregation = Aggregation()

    # testIds param for querying more than one test
    tests = request.values.get("testIds", None)
    test_ids = tests.split(",") if tests else [test_id]
    match["testId"] = {"$in": test_ids}

    # Group students by enifyId and prepare a list of tests
    group = students_group_query(collection="rankAnalysis")

    # Rule out all the students who doesn't belong to current test
    second_match = {"tests.testId": {"$in": [test_id]}}

    # Convert tests array to object by key `testId`
    project = array_to_object("tests", key="testId")

    new_root = {"$mergeObjects": ["$$ROOT", f"$tests.{test_id}"]}

    # Prepare aggregation pipeline
    aggregation.match(match).group(group).match(second_match).project(project).sort(
        f"tests.{test_id}.rankAnalysis.overall.rank"
    ).skip(skip).limit(limit).replace_root(new_root)

    # Run the pipeline
    students = list(
        mongo_db.connection[ga_database]
        .get_collection(collection_name)
        .aggregate(aggregation.pipeline, allowDiskUse=True)
    )

    current_app.logger.info(
        f"Time take to fetch analysis docs: {time.time() - start_time}"
    )
    start_time = time.time()

    r = ReportsHelper(students, fields)
    # The `.prepare_student_meta()` used for fetching list of all hierarchy names
    # and student name from settings, etc..
    r.prepare_student_meta()
    db_duration = time.time() - start_time
    current_app.logger.info(f"Time taken to prepare final data: {db_duration}")

    start_time = time.time()
    # Run the sequential instructions
    csv_data = r.run_instructions()
    db_duration = time.time() - start_time
    current_app.logger.info(f"Time taken to calculate results : {db_duration}")
    string_to_be_returned = StringIO()
    keys = fields.keys()
    dict_writer = csv.DictWriter(string_to_be_returned, keys)
    dict_writer.writeheader()
    dict_writer.writerows(csv_data)
    storage_provider = current_app.config["STORAGE_PROVIDER"]
    storage_helper = StorageHelper(storage_provider, storage_secrets)

    bucket_names = {
        "GCP": current_app.config["CLOUD_STORAGE_BUCKET"],
        "MINIO": current_app.config["MINIO_STORAGE_BUCKET"],
    }

    rank_data = string_to_be_returned.getvalue()
    data = {
        "file_ext": ".csv",
        "file_name": file_name,
        "data": rank_data.encode("utf-8"),
        "bucket_name": bucket_names[storage_provider],
        "content_type": "text/csv",
    }
    url = storage_helper.upload(data, get_public_url=True)
    return {"download_url": url}, 200, {}


@blueprint.route(
    "/<hierarchy_id>/test/<test_id>/data/download", methods=["GET", "POST"]
)
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_required(hierarchy_query=True)
@decorators.make_egnify_response
def download_test_details_for_hierarchy(hierarchy_id, test_id):
    """Hierarchical analysis data download.
    """
    file_name = request.values.get(
        "filename", "Hierarchical_Analysis_{}.csv".format(test_id)
    )
    # List of instructions from client for preparing hierarchy data.
    fields = json.loads(request.values.get("fields", {}))
    # Load all the docs for test in hierarchy `hierarchies`
    query = {"testId": test_id}
    hierarchies = request.values.get("hierarchy", hierarchy_id)
    query["childCode"] = {"$in": hierarchies.split(",")}

    start_time = time.time()
    collection_name = "hierarchicalAnalysis"
    hierarchy_docs = mongo_db.find(ga_database, collection_name, query)
    for doc in hierarchy_docs:
        doc["hierarchy"] = doc["childCode"]

    r = ReportsHelper([], fields)
    r.hierarchical_data_meta(doc_type="hierarchy", docs=hierarchy_docs)

    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    start_time = time.time()
    csv_data = r.run_instructions()

    cal_duration = time.time() - start_time
    current_app.logger.info(f"Time taken to run instructions {cal_duration}")

    string_to_be_returned = StringIO()
    keys = fields.keys()
    dict_writer = csv.DictWriter(string_to_be_returned, keys)
    dict_writer.writeheader()
    dict_writer.writerows(csv_data)
    storage_provider = current_app.config["STORAGE_PROVIDER"]
    storage_helper = StorageHelper(storage_provider, storage_secrets)

    bucket_names = {
        "GCP": current_app.config["CLOUD_STORAGE_BUCKET"],
        "MINIO": current_app.config["MINIO_STORAGE_BUCKET"],
    }

    hierarchy_data = string_to_be_returned.getvalue()
    data = {
        "file_ext": ".csv",
        "file_name": file_name,
        "data": hierarchy_data.encode("utf-8"),
        "bucket_name": bucket_names[storage_provider],
        "content_type": "text/csv",
    }
    url = storage_helper.upload(data, get_public_url=True)
    return {"download_url": url}, 200, {}


@blueprint.route("/<hierarchy_id>/test/<test_id>/cwu/download", methods=["GET", "POST"])
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_required(hierarchy_query=True)
@decorators.make_egnify_response
def download_cwu_details_for_test(hierarchy_id, test_id):
    """CWU analysis data download.
    """
    hierarchies = request.values.get("hierarchy", None)
    file_name = request.values.get("filename", "CWU_Analysis_{}.csv".format(test_id))
    # List of instructions issued by client for cwu data
    fields = json.loads(request.values.get("fields", {}))
    # Optional skip and limit args.
    skip = request.values.get("skip", 0, type=int)
    limit = request.values.get("size", 0, type=int)
    start_time = time.time()
    collection_name = current_app.config["MASTER_RESULTS_TABLE"]
    query = {"testId": test_id}
    if hierarchies:
        query["hierarchyLevels"] = {"$in": hierarchies.split(",")}

    kwargs = {
        "sort_key": "cwuAnalysis.overall.C",
        "key": "egnifyId",
        "skip": skip,
        "size": limit,
        "ascending": False,
        "projection": {"responseData": 0},
    }

    test_docs = mongo_db.find(ga_database, collection_name, query, **kwargs)
    for doc in test_docs.values():
        doc["tests"] = defaultdict(dict)

    # testIds param for querying more than one test
    tests = request.values.get("testIds", None)
    test_ids = tests.split(",") if tests else [test_id]
    query["testId"] = {"$in": test_ids}

    remaining_docs = mongo_db.find(
        ga_database, collection_name, query, projection={"responseData": 0}
    )

    for doc in remaining_docs:
        egnify_id = doc["egnifyId"]
        test_id = doc["testId"]
        test_docs[egnify_id]["tests"][test_id] = doc

    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {time.time()-start_time}"
    )

    students = list(test_docs.values())
    r = ReportsHelper(students, fields)
    r.prepare_student_meta()

    current_app.logger.info(
        f"Time taken to prepare required data: {time.time()-start_time}"
    )

    csv_data = r.run_instructions()

    string_to_be_returned = StringIO()
    keys = fields.keys()
    dict_writer = csv.DictWriter(string_to_be_returned, keys)
    dict_writer.writeheader()
    dict_writer.writerows(csv_data)
    storage_provider = current_app.config["STORAGE_PROVIDER"]
    storage_helper = StorageHelper(storage_provider, storage_secrets)
    cwu_data = string_to_be_returned.getvalue()

    bucket_names = {
        "GCP": current_app.config["CLOUD_STORAGE_BUCKET"],
        "MINIO": current_app.config["MINIO_STORAGE_BUCKET"],
    }

    data = {
        "file_ext": ".csv",
        "file_name": file_name,
        "data": cwu_data.encode("utf-8"),
        "bucket_name": bucket_names[storage_provider],
        "content_type": "text/csv",
    }
    url = storage_helper.upload(data, get_public_url=True)
    return {"download_url": url}, 200, {}


@blueprint.route(
    "/<hierarchy_id>/test/<test_id>/error/download", methods=["GET", "POST"]
)
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_required(hierarchy_query=True)
@decorators.make_egnify_response
def download_error_details_for_test(hierarchy_id, test_id):
    """"
        Returns the error analysis for test.
    """
    fields = json.loads(request.values.get("fields", {}))
    file_name = request.values.get("filename", "Error_Analysis_{}.csv".format(test_id))
    hierarchies = request.values.get("hierarchy", None)
    # Optional skip and limit args.
    skip = request.values.get("skip", 0, type=int)
    limit = request.values.get("size", 0, type=int)
    start_time = time.time()
    collection_name = current_app.config["MASTER_RESULTS_TABLE"]
    query = {"testId": test_id}
    if hierarchies:
        query["hierarchyLevels"] = {"$in": hierarchies.split(",")}

    kwargs = {
        "sort_key": "cwuAnalysis.overall.marks.UW",
        "key": "egnifyId",
        "skip": skip,
        "size": limit,
        "projection": {"topicAnalysis": 0},
    }

    test_docs = mongo_db.find(ga_database, collection_name, query, **kwargs)
    for doc in test_docs.values():
        doc["tests"] = defaultdict(dict)

    # testIds param for querying more than one test
    tests = request.values.get("testIds", None)
    test_ids = tests.split(",") if tests else [test_id]
    query["testId"] = {"$in": test_ids}

    remaining_docs = mongo_db.find(
        ga_database, collection_name, query, projection={"topicAnalysis": 0}
    )

    for doc in remaining_docs:
        egnify_id = doc["egnifyId"]
        test_id = doc["testId"]
        test_docs[egnify_id]["tests"][test_id] = doc

    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {time.time()-start_time}"
    )

    students = list(test_docs.values())

    r = ReportsHelper(students, fields)
    r.prepare_student_meta()
    db_duration = time.time() - start_time
    current_app.logger.info(f"Time taken to fetch the results from DB: {db_duration}")

    csv_data = r.run_instructions()

    string_to_be_returned = StringIO()
    keys = fields.keys()
    dict_writer = csv.DictWriter(string_to_be_returned, keys)
    dict_writer.writeheader()
    dict_writer.writerows(csv_data)
    storage_provider = current_app.config["STORAGE_PROVIDER"]
    storage_helper = StorageHelper(storage_provider, storage_secrets)

    bucket_names = {
        "GCP": current_app.config["CLOUD_STORAGE_BUCKET"],
        "MINIO": current_app.config["MINIO_STORAGE_BUCKET"],
    }

    error_data = string_to_be_returned.getvalue()
    data = {
        "file_ext": ".csv",
        "file_name": file_name,
        "data": error_data.encode("utf-8"),
        "bucket_name": bucket_names[storage_provider],
        "content_type": "text/csv",
    }
    url = storage_helper.upload(data, get_public_url=True)
    return {"download_url": url}, 200, {}


@blueprint.route(
    "/<string:institute_id>/<string:test_id>/<any(paper, key, solution):download_type>"
)
@decorators.make_egnify_response
def download_test_docs(institute_id, test_id, download_type):
    # Fetch test details for rendering paper header meta data
    test_details = mongo_db.find_one(ga_database, "tests", query={"testId": test_id})
    # Fetch institute details for logo and other institute related data
    institute_details = mongo_db.find_one(
        settings_database, "institutes", query={"_id": institute_id},
    )
    # Use questionPaperId for questions
    question_paper_id = test_details.get("questionPaperId")
    # Fetch marking schema for instructions.
    marking_schema_id = test_details.get("markingSchemaId")
    marking_schema = mongo_db.find_one(
        settings_database, "markingschemas", {"_id": marking_schema_id},
    )
    questions = get_questions_data(question_paper_id, marking_schema, test_details)
    show_instructions = download_type == "paper"
    show_solutions = download_type == "solution"
    pdf_type = f" ({download_type.title()})"
    # Decide which template to render
    template = (
        "question_paper_key.html" if download_type == "key" else "paper_new_layout.html"
    )
    # Test instructions
    instructions = marking_schema.get("instructionsMarkup")
    for i,q in questions:
        print(q);
        print(i)
        print('##########')
        # print('q:', q["_id"])
        # print('que',q["question"])
    
    # Render the template and use template string for pdf.
    paper_html = render_template(
        template,
        questions=questions,
        institute_details=institute_details,
        instructions=instructions,
        test_details=test_details,
        show_instructions=show_instructions,
        show_solutions=show_solutions,
        marking_schema=marking_schema,
    
    )
    current_app.logger.info("Completed rendering html for question paper pdf")
    paper_string = BytesIO()
    try:
        options = {"--footer-right": "Page [page] of [topage]"}
        config = pdfkit.configuration(wkhtmltopdf="/usr/local/bin/wkhtmltopdf")
        paper_string.write(
            pdfkit.from_string(
                paper_html, output_path=False, configuration=config, options=options
            )
        )
    except OSError as ose:
        current_app.logger.info("Received wkhtmlpdf OSError", ose)
        print(ose)
    attachment_filename = "".join(
        [
            institute_details["instituteName"],
            "-",
            test_details["testName"],
            pdf_type,
            ".pdf",
        ]
    )
    paper_string.seek(0)
    bucket_names = {
        "GCP": current_app.config["CLOUD_STORAGE_BUCKET"],
        "MINIO": current_app.config["MINIO_STORAGE_BUCKET"],
    }
    storage_provider = current_app.config["STORAGE_PROVIDER"]
    storage_helper = StorageHelper(storage_provider, storage_secrets)
    data = {
        "file_ext": ".pdf",
        "file_name": attachment_filename,
        "data": bytes(paper_string.getvalue()),
        "bucket_name": bucket_names[storage_provider],
        "content_type": "application/pdf",
        "content_disposition": "inline"
    }
    url = storage_helper.upload(data, get_public_url=True)
    if download_type == "paper":
        update_data = {"questionPaperUrl": url}
        mongo_db.find_one_and_upsert(ga_database, "tests", {"testId": test_id}, update_data)
    return {"download_url": url}, 200, {}


def get_questions_data(question_paper_id, marking_schema, test_details):
    """List of question for question paper
    """
    questions_data = list(
        mongo_db.connection[ga_database]
        .get_collection("questions")
        .aggregate(
            [
                {"$match": {"questionPaperId": {"$in": question_paper_id}}},
                {
                    "$lookup": {
                        "from": "questions-list",
                        "localField": "questionNumberId",
                        "foreignField": "_id",
                        "as": "question",
                    }
                },
                {"$unwind": {"path": "$question"}},
            ]
        )
    )
    current_app.logger.info("Fetched questions data")
    # Need to follow the order from test
    subjects_order = [item["subject"] for item in test_details["subjects"]]
    # Group data by subject and question type
    grouped_questions_data = deep_defaultdict(list, depth=2)
    questions_data.sort(key=lambda _: int(_["qno"]))
    for each_question in questions_data:
        print(each_question["question"]["_id"])
        subject = each_question["question"]["subject"]
        question_type = each_question["question"]["questionType"]
        grouped_questions_data[subject][question_type].append(each_question["question"])

    sections_data = deep_defaultdict(dict, 2)
    subjects_map = {
        subject["subjectCode"]: subject["subject"]
        for subject in test_details["subjects"]
    }
    for subject in marking_schema["subjects"]:
        for section in subject["marks"]:
            subject_code = subject["subject"]
            subject_name = subjects_map[subject_code]
            question_type = section["egnifyQuestionType"]
            sections_data[subject_name][question_type] = section
            if question_type == "Integer type":
                sections_data[subject_name]["Numeric type"] = section
            elif question_type == "Numeric type":
                sections_data[subject_name]["Integer type"] = section

    data = []
    for subject in subjects_order:
        subject_questions = grouped_questions_data[subject]
        subject_sections = []
        for index, (question_type, question_type_data) in enumerate(
            subject_questions.items(), 65
        ):
            section_data = sections_data[subject][question_type]
            markup = render_template(
                "section_template.html", section_alpha=chr(index), **section_data
            )
            subject_sections.append((markup, question_type_data))

        data.append((subject, subject_sections))

    return data


@blueprint.route("/<hierarchy_id>/test/<test_id>/download", methods=["GET", "POST"])
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_required(hierarchy_query=True)
@decorators.make_egnify_response
def download_full_test_details(hierarchy_id, test_id):
    """"
        Returns the analysis for test.
    """
    fields = json.loads(request.values.get("fields", {}))
    file_name = request.values.get("filename", "Test_Analysis_{}.csv".format(test_id))
    # Optional skip and limit args.
    skip = request.values.get("skip", 0, type=int)
    limit = request.values.get("size", 0, type=int)

    start_time = time.time()
    collection_name = "rankAnalysis"
    query = prepare_query_for_rank(hierarchy_id, test_id)

    # Join masterresults with rank analysis for combined test results.
    lookup_query = {
        "from": "masterresults",
        "localField": "testStudentId",
        "foreignField": "_id",
        "as": "tests",
    }
    project_query = {
        "studentId": 1,
        "egnifyId": 1,
        "hierarchyLevels": 1,
    }
    project_query.update(array_to_object("tests", key="testId", rank=True))

    aggregation = Aggregation()
    aggregation.match(query).skip(skip).limit(limit).add_fields(
        field_name="testStudentId", fields=["$testId", "_", "$egnifyId"]
    ).lookup(lookup_query).project(project_query)

    students = list(
        mongo_db.connection[ga_database]
        .get_collection(collection_name)
        .aggregate(aggregation.pipeline)
    )
    r = ReportsHelper(students, fields)
    r.prepare_student_meta()
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    csv_data = r.run_instructions()

    string_to_be_returned = StringIO()
    keys = fields.keys()
    dict_writer = csv.DictWriter(string_to_be_returned, keys)
    dict_writer.writeheader()
    dict_writer.writerows(csv_data)
    storage_provider = current_app.config["STORAGE_PROVIDER"]
    storage_helper = StorageHelper(storage_provider, storage_secrets)

    bucket_names = {
        "GCP": current_app.config["CLOUD_STORAGE_BUCKET"],
        "MINIO": current_app.config["MINIO_STORAGE_BUCKET"],
    }

    error_data = string_to_be_returned.getvalue()
    data = {
        "file_ext": ".csv",
        "file_name": file_name,
        "data": error_data.encode("utf-8"),
        "bucket_name": bucket_names[storage_provider],
        "content_type": "text/csv",
    }
    url = storage_helper.upload(data, get_public_url=True)
    return {"download_url": url}, 200, {}


@blueprint.route(
    "/<hierarchy_id>/test/<test_id>/mark/download", methods=["GET", "POST"]
)
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_required(hierarchy_query=True)
@decorators.make_egnify_response
def mark_analysis(hierarchy_id, test_id):
    start_time = time.time()
    collection_name = "markAnalysis"
    fields = json.loads(request.values.get("fields", {}))
    file_name = request.values.get("filename", "Mark_Analysis_{}.csv".format(test_id))
    query = {"testId": test_id}
    hierarchies = request.values.get("hierarchy", None)
    if hierarchies:
        query["hierarchy"] = {"$in": hierarchies.split(",")}

    mark_docs = mongo_db.find(ga_database, collection_name, query)

    r = ReportsHelper([], fields)
    r.hierarchical_data_meta(doc_type="mark", docs=mark_docs)

    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )

    csv_data = r.run_instructions()
    string_to_be_returned = StringIO()
    keys = fields.keys()
    dict_writer = csv.DictWriter(string_to_be_returned, keys)
    dict_writer.writeheader()
    dict_writer.writerows(csv_data)
    storage_provider = current_app.config["STORAGE_PROVIDER"]
    storage_helper = StorageHelper(storage_provider, storage_secrets)

    bucket_names = {
        "GCP": current_app.config["CLOUD_STORAGE_BUCKET"],
        "MINIO": current_app.config["MINIO_STORAGE_BUCKET"],
    }

    error_data = string_to_be_returned.getvalue()
    data = {
        "file_ext": ".csv",
        "file_name": file_name,
        "data": error_data.encode("utf-8"),
        "bucket_name": bucket_names[storage_provider],
        "content_type": "text/csv",
    }
    url = storage_helper.upload(data, get_public_url=True)
    return {"download_url": url}, 200, {}


@blueprint.route(
    "/<hierarchy_id>/test/<test_id>/hierarchy/download", methods=["GET", "POST"]
)
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_required(hierarchy_query=True)
@decorators.make_egnify_response
def download_combined_hierarchy_data(hierarchy_id, test_id):
    file_name = request.values.get(
        "filename", "Hierarchical_Analysis_{}.csv".format(test_id)
    )
    fields = json.loads(request.values.get("fields", {}))
    # Optional skip and limit args.
    skip = request.values.get("skip", 0, type=int)
    limit = request.values.get("size", 0, type=int)

    query = {"testId": test_id}
    hierarchies = request.values.get("hierarchy", hierarchy_id)
    query["childCode"] = {"$in": hierarchies.split(",")}

    start_time = time.time()
    collection_name = "hierarchicalAnalysis"

    # Join mark analysis with hierarchy analysis for combined test results.
    lookup_query = {
        "from": "markAnalysis",
        "localField": "testHierarchyId",
        "foreignField": "_id",
        "as": "markAnalysis",
    }

    new_root = {"$mergeObjects": ["$$ROOT", {"$arrayElemAt": ["$markAnalysis", 0]}]}

    aggregation = Aggregation()
    aggregation.match(query).skip(skip).limit(limit).add_fields(
        field_name="hierarchy", fields=["$childCode"]
    ).add_fields(
        field_name="testHierarchyId", fields=["$childCode", "_", "$testId"]
    ).lookup(
        lookup_query
    ).replace_root(
        new_root
    )

    docs = list(
        mongo_db.connection[ga_database]
        .get_collection(collection_name)
        .aggregate(aggregation.pipeline)
    )

    r = ReportsHelper([], fields)
    r.hierarchical_data_meta(doc_type="both", docs=docs)

    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    csv_data = r.run_instructions()

    db_duration = time.time() - start_time
    current_app.logger.info(
        "Time taken to fetch the results from {}: {}".format(
            collection_name, db_duration
        )
    )

    string_to_be_returned = StringIO()
    keys = fields.keys()
    dict_writer = csv.DictWriter(string_to_be_returned, keys)
    dict_writer.writeheader()
    dict_writer.writerows(csv_data)
    storage_provider = current_app.config["STORAGE_PROVIDER"]
    storage_helper = StorageHelper(storage_provider, storage_secrets)

    bucket_names = {
        "GCP": current_app.config["CLOUD_STORAGE_BUCKET"],
        "MINIO": current_app.config["MINIO_STORAGE_BUCKET"],
    }

    hierarchy_data = string_to_be_returned.getvalue()
    data = {
        "file_ext": ".csv",
        "file_name": file_name,
        "data": hierarchy_data.encode("utf-8"),
        "bucket_name": bucket_names[storage_provider],
        "content_type": "text/csv",
    }
    url = storage_helper.upload(data, get_public_url=True)
    return {"download_url": url}, 200, {}


@blueprint.route(
    "/<hierarchy_id>/test/<test_id>/highest/download", methods=["GET", "POST"]
)
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_required(hierarchy_query=True)
@decorators.make_egnify_response
def highest_marks(hierarchy_id, test_id):
    start_time = time.time()
    collection_name = "rankAnalysis"
    fields = json.loads(request.values.get("fields", {}))
    file_name = request.values.get("filename", "Highest_Marks_{}.csv".format(test_id))
    hierarchies = request.values.get("hierarchy", hierarchy_id).split(",")
    # Group the hierarchies by name for query later
    group_by_name = deep_defaultdict(dict, 2)
    for hierarchy in hierarchies:
        node = current_app.arango_db.get_node(hierarchy)
        name = node["hierarchyName"]
        code = node["instituteLevelCode"]
        if group_by_name[name]["ids"]:
            group_by_name[name]["ids"].append(hierarchy)
        else:
            group_by_name[name]["ids"] = [hierarchy]
        group_by_name[name]["code"] = code

    docs = []

    # For each hierarchy prepare topper and hierarchy max data
    for name, item in group_by_name.items():
        group_ids = item["ids"]
        code = item["code"]
        # Get the topper dataa
        rank_query = {"testId": test_id, "hierarchy": {"$in": group_ids}}
        top = mongo_db.find(ga_database, collection_name, rank_query, size=1)[0]
        # Get the hierarchical analysis
        hierarchy_query = {"testId": test_id, "childCode": {"$in": group_ids}}
        db_docs = mongo_db.find(ga_database, "hierarchicalAnalysis", hierarchy_query)
        start = HierarchyAnalysis()
        h_docs = [
            HierarchyAnalysis(
                x["numberOfStudents"], x["topicAnalysis"], x["questionMap"]
            )
            for x in db_docs
        ]
        # Combine the hierarchical analysis docs
        added_doc = sum(h_docs, start)
        new_doc = {}
        new_doc.update(added_doc.__dict__)
        new_doc.update(top)
        new_doc["meta"] = {code: name}
        docs.append(new_doc)

    db_duration = time.time() - start_time
    current_app.logger.info(f"Time taken to fetch the results from DB: {db_duration}")

    r = ReportsHelper(docs, fields)

    csv_data = r.run_instructions()

    string_to_be_returned = StringIO()
    keys = fields.keys()
    dict_writer = csv.DictWriter(string_to_be_returned, keys)
    dict_writer.writeheader()
    dict_writer.writerows(csv_data)
    storage_provider = current_app.config["STORAGE_PROVIDER"]
    storage_helper = StorageHelper(storage_provider, storage_secrets)

    bucket_names = {
        "GCP": current_app.config["CLOUD_STORAGE_BUCKET"],
        "MINIO": current_app.config["MINIO_STORAGE_BUCKET"],
    }

    hierarchy_data = string_to_be_returned.getvalue()
    data = {
        "file_ext": ".csv",
        "file_name": file_name,
        "data": hierarchy_data.encode("utf-8"),
        "bucket_name": bucket_names[storage_provider],
        "content_type": "text/csv",
    }
    url = storage_helper.upload(data, get_public_url=True)
    return {"download_url": url}, 200, {}


@blueprint.route("/<hierarchy_id>/test/<test_id>/snapshot", methods=["GET", "POST"])
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_required(hierarchy_query=True)
@decorators.make_egnify_response
def download_snapshot_info(hierarchy_id, test_id):
    start_time, coll = time.time(), "testStudentSnapshot"
    hierarchy = request.values.get("hierarchy", "")
    status = request.values.get("status")
    fields = json.loads(request.values.get("fields", {}))
    file_name = request.values.get(
        "filename", "Test_Attendance_Data{}.csv".format(test_id)
    )

    hierarchies = hierarchy.split(",")
    query = {"testId": test_id, "hierarchyLevels": {"$in": hierarchies}}
    if status and status == "completed":
        query["status"] = status
    if status and status == "absent":
        query["mode"] = {"$exists": False}
    if status and status == "inprogress":
        query["mode"] = "online"
        query["status"] = {"$ne": "completed"}
    docs = mongo_db.find(ga_database, coll, query)

    test_info = mongo_db.find_one(ga_database, "tests", {"testId": test_id})

    db_duration = time.time() - start_time
    current_app.logger.info(f"Time taken to fetch the results from DB: {db_duration}")

    r = ReportsHelper([], fields)

    r.prepare_snapshot_data(docs, test_info)

    csv_data = r.run_instructions()

    string_to_be_returned = StringIO()
    keys = fields.keys()
    dict_writer = csv.DictWriter(string_to_be_returned, keys)
    dict_writer.writeheader()
    dict_writer.writerows(csv_data)
    storage_provider = current_app.config["STORAGE_PROVIDER"]
    storage_helper = StorageHelper(storage_provider, storage_secrets)

    bucket_names = {
        "GCP": current_app.config["CLOUD_STORAGE_BUCKET"],
        "MINIO": current_app.config["MINIO_STORAGE_BUCKET"],
    }

    file_data = string_to_be_returned.getvalue()
    data = {
        "file_ext": ".csv",
        "file_name": file_name,
        "data": file_data.encode("utf-8"),
        "bucket_name": bucket_names[storage_provider],
        "content_type": "text/csv",
    }
    url = storage_helper.upload(data, get_public_url=True)
    return {"download_url": url}, 200, {}


@blueprint.route("/<institute_id>/students", methods=["GET", "POST"])
@decorators.roles_required(["SETTINGS_READER", "SETTINGS_CREATOR"])
@decorators.make_egnify_response
def download_student_report(institute_id):
    start_time = time.time()
    query = {"instituteId": institute_id}

    fields = json.loads(request.values.get("fields", {}))
    file_name = request.values.get("filename", "Student_Info.csv")
    # Filter query params
    search_text = request.values.get("searchText", None)
    hierarchy = request.values.get("hierarchy", "").split(",")
    gender = request.values.get("gender", None)
    category = request.values.get("category", None)
    if search_text:
        query["$or"] = [
            {"studentName": {"$regex": search_text}},
            {"studentId": {"$regex": search_text}},
        ]
    if gender:
        query["gender"] = gender
    if category:
        query["category"] = category
    query["accessTag.hierarchy"] = {"$in": hierarchy}
    query["active"] = True
    kwargs = {
        "projection": {"hierarchy": 0, "_id": 0, "active": 0},
    }
    students = mongo_db.find(settings_database, "studentInfo", query, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(f"Time taken to fetch the results from DB: {db_duration}")

    lookup = {}
    for student in students:
        student["meta"] = {}
        parent_hierarchy = student["accessTag"]["hierarchy"]
        while parent_hierarchy and parent_hierarchy != "root":
            if parent_hierarchy not in lookup:
                parent_node = current_app.arango_db.get_node(parent_hierarchy)
                lookup[parent_hierarchy] = parent_node
            parent_node = lookup[parent_hierarchy]
            student["meta"][parent_node["instituteLevelCode"]] = parent_node["hierarchyName"]
            parent_hierarchy = parent_node["parentCode"]
    db_duration = time.time() - start_time
    current_app.logger.info(f"Time taken till to fetch the results from Arango DB: {db_duration}")
    r = ReportsHelper(students, fields)

    csv_data = r.run_instructions()

    string_to_be_returned = StringIO()
    keys = fields.keys()
    dict_writer = csv.DictWriter(string_to_be_returned, keys)
    dict_writer.writeheader()
    dict_writer.writerows(csv_data)
    storage_provider = current_app.config["STORAGE_PROVIDER"]
    storage_helper = StorageHelper(storage_provider, storage_secrets)

    bucket_names = {
        "GCP": current_app.config["CLOUD_STORAGE_BUCKET"],
        "MINIO": current_app.config["MINIO_STORAGE_BUCKET"],
    }

    students_data = string_to_be_returned.getvalue()
    data = {
        "file_ext": ".csv",
        "file_name": file_name,
        "data": students_data.encode("utf-8"),
        "bucket_name": bucket_names[storage_provider],
        "content_type": "text/csv",
    }
    url = storage_helper.upload(data, get_public_url=True)
    return {"download_url": url}, 200, {}


def array_to_object(attr, key, value=None, rank=False):
    merge = {"$mergeObjects": [f"$$el.{value}" if value else "$$el"]}
    if rank:
        merge["$mergeObjects"].append({"rankAnalysis": "$rankAnalysis"})
    return {
        attr: {
            "$arrayToObject": {
                "$map": {
                    "input": f"${attr}",
                    "as": "el",
                    "in": {"k": f"$$el.{key}", "v": merge,},
                }
            }
        }
    }


def students_group_query(collection):
    group = {
        "_id": "$egnifyId",
        "tests": {
            "$push": {
                "testId": "$testId",
                "egnifyId": "$egnifyId",
                "hierarchyLevels": "$hierarchyLevels",
            }
        },
    }
    if collection == "masterresults":
        group["tests"]["$push"]["cwuAnalysis"] = "$cwuAnalysis"
        group["tests"]["$push"]["topicAnalysis"] = "$topicAnalysis"
        group["tests"]["$push"]["responseData"] = "$responseData"
    elif collection == "rankAnalysis":
        group["tests"]["$push"]["rankAnalysis"] = "$rankAnalysis"

    return group
