from flask import Blueprint, current_app, request
import importlib
from datetime import datetime, timezone
from rq_scheduler import Scheduler
import redis
from datetime import datetime
import utils.decorators as decorators
from rq import Queue

queue = None


blueprint = Blueprint("schedule", __name__, url_prefix="/schedule")


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global scheduler, queue
    redis_connection = redis.Redis.from_url(current_app.config["RQ_REDIS_URL"])
    scheduler = Scheduler(current_app.config["RQ_QUEUES"], connection=redis_connection)
    queue = Queue(current_app.config["RQ_QUEUES"], connection=redis_connection)


@blueprint.route("", methods=["POST"])
@decorators.make_egnify_response
def schedule_a_function():
    """

    :return:
    """
    data = request.json
    if dt := data.get("date"):
        data["dt"] = datetime.strptime(dt, "%y/%m/%d %H:%M:%S")
    else:
        data["dt"] = datetime.utcnow()
    task = schedule_task(data)
    resp = {"STATUS": "SUCCESS", "JOB_ID": task.id}
    return resp, 200, {}


def schedule_task(data):
    dt = data.get("dt", datetime.now(timezone.utc))
    module = importlib.import_module(data["module"])
    method = data["method"]
    depends_on = data.get("depends_on", None)
    task = scheduler.enqueue_at(
        dt,
        getattr(module, method),
        data["args"],
        depends_on=depends_on
    )
    return task

def cancel_a_scheduled_job(job_id):
    scheduler.cancel(job_id)
