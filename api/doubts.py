from api.models import doubts
from flask import Blueprint, current_app, request
from utils.decorators import make_egnify_response, roles_required
from .models import DoubtsSchema
import time
from utils.TimeHelper import TimeHelper
from datetime import datetime, timedelta, timezone
from dateutil import parser
from pytz import timezone as tz
import pymongo

# from notifications import pushnotification, util as meteoroid_util
blueprint = Blueprint("doubts", __name__, url_prefix="/doubt")
mongo_db = arango_db = settings_database = tenant_database = None


@blueprint.before_app_first_request
def init_bp():
    """
    """
    global mongo_db, arango_db, settings_database, tenant_database
    mongo_db = current_app.mongo_db
    arango_db = current_app.arango_db
    settings_database = current_app.config["SETTINGS_DATABASE"]
    tenant_database = current_app.config["TENANT_REGISTRY_DATABASE"]


@blueprint.route("/<string:institute_id>/<string:student_id>", methods=["POST"])
@roles_required(["STUDENT"])
@make_egnify_response
def create_doubt(institute_id, student_id):
    """Create doubt in Subject.
    ---
    post:
      description: doubt item for Subject
      responses:
        201:
          description: Return created documents
          content:
            application/json:
    """
    # try:
    response_data, status_code, extra_headers = {}, 201, {}
    collection_name = "doubts"
    post_data = request.get_json()  # read the json data from client request
    query = {"_id": student_id}
    student_data = mongo_db.find_one(settings_database, "studentInfo", query)
    if not student_data:
        return "Bad Args", 400, {}

    hierarchy = student_data["accessTag"]["hierarchy"]
    parent_nodes = arango_db.get_parent(hierarchy, height=20)
    node_hierachies = [v["id"] for v in parent_nodes["vertices"]]
    node_hierachies.append(hierarchy)
    # get the teacher who has the access to DOUBTS_MODULE
    query = {
        "instituteId": institute_id,
        "role": {"$in": ["DOUBT_SOLVER"]},
        "hierarchy.childCode": {"$in": node_hierachies},
        "subject":{"$in":[post_data['subject']]}
        
    }
    user = mongo_db.find_one(tenant_database, "users", query)
    print('user',user)
    if not user:
        return (
            "Teacher Not allocated to this subject to clarify doubts, please try another subject",
            400,
            extra_headers,
        )
    post_data["teacher"] = user["email"]
    post_data["instituteId"] = institute_id
    post_data["hierarchy"] = [hierarchy]
    post_data["egnifyId"] = student_id
    post_data["studentName"] = student_data["studentName"]
    if "classes" not in post_data:
        path = current_app.arango_db.get_path("root", hierarchy)
        klass_hierarchy_id = path[3]
        node = current_app.arango_db.get_node(klass_hierarchy_id)
        klass = node["hierarchyName"]
        institute = mongo_db.find_one(
            settings_database, "institutes", {"_id": institute_id}
        )
        post_data["classes"] = [
            x["_id"] for x in institute["systemHierarchies"] if x["name"] == klass
        ]
    result = DoubtsSchema().load(post_data)  # load for validations
    if result.errors:
        response_data["STATUS"] = "FAILURE"
        response_data["DATA"] = None
        response_data["errors"] = result.errors
        status_code = 400
        return response_data, status_code, extra_headers
    else:
        sanitized_data = result.data
        # convert to datetime type for mongo compatibility
        sanitized_data["startDate"] = (
            parser.parse(post_data["startDate"])
            .replace(tzinfo=timezone.utc)
            .astimezone(tz=tz("Asia/Kolkata"))
        )
        sanitized_data["messages"][0]["time"] = (
            parser.parse(post_data["messages"][0]["time"])
            .replace(tzinfo=timezone.utc)
            .astimezone(tz=tz("Asia/Kolkata"))
        )
        mongo_db.insert_one(settings_database, collection_name, sanitized_data)
        response_data["STATUS"] = "SUCCESS"
        response_data["message"] = "doubt created successfully"
        response_data["DATA"] = sanitized_data
        status_code = 201
        return response_data, status_code, extra_headers


@blueprint.route(
    "/<string:institute_id>/<string:student_id>/<string:doubt_id>", methods=["PUT"]
)
@roles_required(["STUDENT"])
@make_egnify_response
def update_doubt(institute_id, student_id, doubt_id):
    """
    updates the doubt 
    """
    response_data, status_code, extra_headers = {}, 201, {}
    post_data = request.get_json()
    result = DoubtsSchema().load(post_data)  # load for validations
    if result.errors:
        response_data = result.errors
        status_code = 400
        return response_data, status_code, extra_headers
    sanitized_data = result.data
    sanitized_data["_id"] = doubt_id
    # convert to datetime type for mongo compatibility
    sanitized_data["startDate"] = (
        parser.parse(post_data["startDate"])
        .replace(tzinfo=timezone.utc)
        .astimezone(tz=tz("Asia/Kolkata"))
    )
    query = {"_id": doubt_id}
    doubt = mongo_db.find_one_and_upsert(
        settings_database, "doubts", query, sanitized_data, upsert=False,
    )
    if not doubt:
        return "Invalid doubt_id", 400, {}
    response_data = {
        "STATUS": "SUCCESS",
        "DATA": doubt,
        "MESSAGE": "Updated doubt successfully",
    }
    return response_data, status_code, extra_headers


@blueprint.route(
    "/<string:institute_id>/message/<string:doubt_id>/send", methods=["POST"]
)
@roles_required(["STUDENT", "DOUBT_SOLVER"])
@make_egnify_response
def update_message(institute_id, doubt_id):
    """
    "updates the message sent by student or teacher"
    """
    post_data = request.get_json()
    message = post_data.get("message", None)
    status = post_data.get("status", None)
    time = message["time"]
    time = (
        parser.parse(time)
        .replace(tzinfo=timezone.utc)
        .astimezone(tz=tz("Asia/Kolkata"))
    )
    message["time"] = time
    if not message:
        return "Bad_Args", 400, {}
    query = {"_id": doubt_id}
    update = {"$push": {"messages": message}, "$set": {"status": status}}
    doc = (
        mongo_db.connection[settings_database]
        .get_collection("doubts")
        .update(query, update)
    )
    if not doc:
        return "Invalid doubt_id", 400, {}
    response_data = {
        "STATUS": "SUCCESS",
        "MESSAGE": "message sent successfully",
    }
    return response_data, 200, {}


@blueprint.route("/<string:institute_id>/<string:doubt_id>/clear", methods=["POST"])
@roles_required(["STUDENT"])
@make_egnify_response
def clear_doubt(institute_id, doubt_id):
    return_response = {}
    post_data = request.get_json()
    isClear = post_data.get("isClear", None)
    endDate = post_data.get("endDate", None)
    endDate = (
        parser.parse(endDate)
        .replace(tzinfo=timezone.utc)
        .astimezone(tz=tz("Asia/Kolkata"))
    )
    query = {"_id": doubt_id}
    doc = mongo_db.find_one_and_upsert(
        settings_database,
        "doubts",
        query,
        {"isClear": isClear, "endDate": endDate},
        upsert=False,
    )
    if not doc:
        return "Bad_Args", 400, {}
    return_response["STATUS"] = "SUCCESS"
    return return_response, 200, {}


@blueprint.route("/<string:institute_id>/<string:doubt_id>/solve", methods=["POST"])
@roles_required(["DOUBT_SOLVER"])
@make_egnify_response
def solve_doubt(institute_id, doubt_id):
    """
     update explanation for doubt doc
    """
    post_data = request.get_json()
    explanation = post_data.get("explanation", None)
    attachments = post_data.get("attachments", None)
    endDate = post_data.get("endDate", None)
    collection_name = "doubts"
    if not explanation or not endDate:
        return "Bad_Args", 400, {}
    endDate = datetime.now(timezone.utc)
    to_be_update = {
        "explanation": explanation,
        "endDate": endDate,
        "status": "resolved",
    }
    query = {
        "_id": doubt_id,
    }
    if attachments:
        to_be_update["attachments"] = attachments
    doc = mongo_db.find_one_and_upsert(
        settings_database, collection_name, query, to_be_update, upsert=False
    )
    if not doc:
        return "Invalid doubt_id", 400, {}
    return doc, 200, {}


@blueprint.route("/<string:institute_id>/<string:doubt_id>", methods=["DELETE"])
@roles_required(["STUDENT"])
@make_egnify_response
def delete_doubt(institute_id, doubt_id):
    """
    deletes the doubt 
    """
    query = {"_id": doubt_id}
    to_be_update = {"active": False}
    doubt_doc = mongo_db.find_one_and_upsert(
        settings_database, "doubts", query, to_be_update, upsert=False
    )
    if not doubt_doc:
        return "Bad_Args", 400, {}
    return doubt_doc, 200, {}


@blueprint.route("/<string:institute_id>/<string:doubt_id>", methods=["GET"])
@roles_required(["STUDENT", "DOUBT_SOLVER", "PARTNER_ADMIN"])
@make_egnify_response
def get_doubt(institute_id, doubt_id):
    """
    returns the doubt data
    """
    query = {"_id": doubt_id, "active": True}
    doc = mongo_db.find_one(settings_database, "doubts", query)
    if not doc:
        return "Bad_Arga or doubt not active", 400, {}
    return doc, 200, {}


@blueprint.route("/<string:institute_id>/all", methods=["GET"])
@roles_required(["STUDENT", "DOUBT_SOLVER", "PARTNER_ADMIN"])
@make_egnify_response
def get_doubts(institute_id):
    """
    returns the list of doubt item
    """
    start_time = time.time()
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 0, type=int)
    query = _build_doubts_query(institute_id)
    sort = request.values.get("sort", None)

    collection_name = "doubts"
    kwargs = {
        "key": "_id",
        "sort_key": "startDate",
        "skip": skip,
        "size": size,
        "ascending": False,
    }
    if sort and sort == "asc":
        kwargs["sort_key"] = ("messages.time",)
        kwargs["ascending"] = True
    tests = mongo_db.get_results(settings_database, collection_name, query, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return tests, 200, {}


@blueprint.route("/<string:hierarchy_id>/doubt/list", methods=["GET"])
@roles_required(["STUDENT", "DOUBT_SOLVER", "PARTNER_ADMIN"])
@make_egnify_response
def hierarchy_wise_doubt_list(hierarchy_id):
    """
    param: hierarchy_id
    return: list of doubts
    """
    status = request.values.get("status", None)
    parent_nodes = arango_db.get_parent(hierarchy_id, height=20)
    node_hierachies = [v["id"] for v in parent_nodes["vertices"]]
    node_hierachies.append(hierarchy_id)
    collection_name = "doubts"
    query = {
        "active": True,
        "hierarchy": {"$in": node_hierachies},
    }
    if status:
        query["status"] = status
    kwargs = {"key": "_id", "sort_key": "startDate", "ascending": True}
    start_time = time.time()
    doubts = mongo_db.get_results(settings_database, collection_name, query, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return doubts, 200, {}


@blueprint.route("/<string:institute_id>/count", methods=["GET"])
@roles_required(["STUDENT", "DOUBT_SOLVER", "PARTNER_ADMIN"])
@make_egnify_response
def get_doubts_count(institute_id):
    """"
        Return count of test items.
    """
    start_time = time.time()
    query = _build_doubts_query(institute_id)
    collection_name = "doubts"
    doubts_count = mongo_db.count(settings_database, collection_name, query)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return {"count": doubts_count}, 200, {}


@blueprint.route(
    "/<string:institute_id>/<string:doubt_id>/<any(bookmark, remove):type>",
    methods=["POST"],
)
@roles_required(["STUDENT", "DOUBT_SOLVER", "PARTNER_ADMIN"])
@make_egnify_response
def bookmark_doubt(institute_id, doubt_id, type):
    """
        params: "student" egnifyId, "teacher" email
        return: 
    """
    post_data = request.get_json()
    student_id = post_data.get("student", None)
    teacher = post_data.get("teacher", None)
    bookmark_flag = not (type == "remove")
    set_value = {}
    if not student_id and not teacher:
        return "Bad Args", 400, {}
    if student_id:
        set_value["bookmark_by_student"] = bookmark_flag
    if teacher:
        set_value["bookmark_by_teacher"] = bookmark_flag
    query = {
        "_id": doubt_id,
    }
    doc = mongo_db.find_one_and_upsert(
        settings_database, "doubts", query, set_value, upsert=False,
    )
    if not doc:
        return "Invalid doubt_id", 400, {}
    return {"MESSAGE": "Updated successfully", "DATA": None}, 200, {}


@blueprint.route("/<string:institute_id>/teacher/<string:email>", methods=["GET"])
@roles_required(["DOUBT_SOLVER", "PARTNER_ADMIN"])
@make_egnify_response
def get_doubts_by_teacher(institute_id, email):
    """
        returns the list of doubts assigned to teacher
    """
    query = {"teacher": email}
    projection = {field: 1 for field in ["_id", "teacher", "egnifyId"]}
    kwargs = {"key": "_id", "projection": projection}
    doubts = mongo_db.get_results(settings_database, "doubts", query, **kwargs)
    return doubts, 200, {}


@blueprint.route(
    "/<string:institute_id>/student/<string:student_id>/subject/list", methods=["GET"]
)
@roles_required(["STUDENT", "DOUBT_SOLVER", "PARTNER_ADMIN"])
@make_egnify_response
def get_doubts_by_student(institute_id, student_id):
    """
        returns the doubts list by student
    """
    query = {"egnifyId": student_id}
    doubts = list(
        mongo_db.connection[settings_database]
        .get_collection("doubts")
        .aggregate(
            [
                {"$match": query},
                {
                    "$group": {
                        "_id": "$subject",
                        "topics": {"$push": "$topic"},
                        # "doubts": {"$push": "$$ROOT"}
                    }
                },
            ]
        )
    )
    return doubts, 200, {}


@blueprint.route("/<string:institute_id>/students/list", methods=["GET"])
@roles_required(["DOUBT_SOLVER", "PARTNER_ADMIN"])
@make_egnify_response
def get_students_list_who_asked_doubts(institute_id):
    """"
        Return count of test items.
    """
    start_time = time.time()
    query = _build_doubts_query(institute_id)
    collection_name = "doubts"
    sort = request.values.get("sort", None)
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 20, type=int)    
    sort_query = {}
    if sort and sort == "asc":
        sort_query["time"] = pymongo.ASCENDING
        # sort_query["startDate"] = pymongo.ASCENDING
    else:
        sort_query["time"] = pymongo.DESCENDING
        # sort_query["startDate"] = pymongo.DESCENDING
    doubts = list(
        mongo_db.connection[settings_database]
        .get_collection(collection_name)
        .aggregate(
            [
                {"$match": query},
                {
                    "$group": {
                        "_id": "$egnifyId",
                        "count": {"$sum": {"$cond": [{"$eq":["$status", "pending"]}, 1,0]}},
                        "time": {"$last":"$messages.time"},
                     },
                },
                {"$sort": sort_query},
                {"$skip": skip},
                {"$limit": size}
            ]
        )
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    response_data = {"STATUS": "SUCCESS", "DATA": doubts}
    return response_data, 200, {}

@blueprint.route("/<string:institute_id>/students/count", methods=["GET"])
@roles_required(["DOUBT_SOLVER", "PARTNER_ADMIN"])
@make_egnify_response
def get_students_count_who_asked_doubts(institute_id):
    """"
        Return count of test items.
    """
    start_time = time.time()
    query = _build_doubts_query(institute_id)
    collection_name = "doubts"
    doubts_count = mongo_db.distinct(settings_database, collection_name, "egnifyId", query)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return {"count": doubts_count}, 200, {}


@blueprint.route("/<string:institute_id>/<string:doubt_id>/messages", methods=["POST"])
@roles_required(["STUDENT", "DOUBT_SOLVER", "PARTNER_ADMIN"])
@make_egnify_response
def is_messages_updated(institute_id, doubt_id):
    """
    returns the messages update status
    """
    post_data = request.get_json()
    pre_count = post_data.get("count", 0)
    print(pre_count)
    query = {"_id": doubt_id}
    doc = list(
        mongo_db.connection[settings_database]
        .get_collection("doubts")
        .aggregate(
            [{"$match": query}, {"$project": {"count": {"$size": "$messages"}}},]
        )
    )
    count = doc[0]["count"]
    is_updated = False
    if count > pre_count:
        is_updated = True
    response_data = {"STATUS": "SUCCESS", "DATA": is_updated, "count": count}
    return response_data, 200, {}


def _build_doubts_query(institute_id):
    query = {"active": True,"instituteId":institute_id}
    orientations = request.values.get("orientationId", None)
    subject = request.values.get("subject", None)
    topic = request.values.get("topic", None)
    classes = request.values.get("classes", None)
    status = request.values.get("status", None)
    startDate = request.values.get("startDate", None)
    endDate = request.values.get("endDate", None)
    teacher = request.values.get("teacher", None)
    student = request.values.get("student", None)
    bookmark_by_student = request.values.get("bookmark_by_student", None)
    bookmark_by_teacher = request.values.get("bookmark_by_teacher", None)
    search_text = request.values.get("searchText", None)
    isClear = request.values.get("isClear",None)

    if orientations:
        query["orientation"] = {"$in": orientations.split(",")}
    if subject:
        query["subject"] = {"$in": subject.split(",")}
    if topic:
        query["topic"] = {"$in": topic.split(",")}
    if classes:
        query["classes"] = {"$in": classes.split(",")}
    if status:
        query["status"] = status
    if startDate:
        startDate = parser.parse(startDate)
        query["startDate"] = {"$gte": startDate}
    if endDate:
        endDate = parser.parse(endDate)
        query["endDate"] = {"$lte": endDate}
    if teacher:
        teacher = teacher.split(",")
        query["teacher"] = {"$in": teacher}
    if student:
        student = student.split(",")
        query["egnifyId"] = {"$in": student}

    if bookmark_by_student:
        if bookmark_by_student == "bookmarked":
            query["bookmark_by_student"] = True
        else:
            query["bookmark_by_student"] = False
    if bookmark_by_teacher:
        if bookmark_by_teacher == "bookmarked":
            query["bookmark_by_teacher"] = True
        else:
            query["bookmark_by_teacher"] = False
    hierarchies = request.values.get("hierarchies", None)
    if hierarchies:
        hierarchies = hierarchies.split(",")
        query["hierarchy"] = {"$in": hierarchies}
    if search_text:
        query["$or"] = [
            {"studentName": {"$regex": search_text}},
            {"egnifyId": {"$regex": search_text}},
            {"topic": {"$regex": search_text}},
        ]
    if isClear:
        if isClear == 'yes':
            query['isClear'] = True
        else:
            query['isClear'] = False
    return query

