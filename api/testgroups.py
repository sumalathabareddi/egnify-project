from flask import Blueprint, current_app, request
from utils.decorators import (
    make_egnify_response,
    make_egnify_data_response,
    roles_required,
    authorization_required,
)

from .models.testgroups import TestGroupsSchema

blueprint = Blueprint("testgroups", __name__)

mongo_db = settings_database = None


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global mongo_db, settings_database
    mongo_db = current_app.mongo_db
    settings_database = current_app.config["SETTINGS_DATABASE"]


@blueprint.route("/<string:institute_id>/testgroups", methods=["POST"])
@roles_required(["SETTINGS_CREATOR"])
@authorization_required()
@make_egnify_response
def insert_test_groups(institute_id):
    """Add test groups.
    ---
    post:
      description: Add test groups
      responses:
        201:
          description: Return add test groups status
          content:
            application/json:
              schema: TestGroupsSchema
        400:
          description: Return validation errors
          content:
            application/json:
              schema: TestGroupsSchema
    """
    response_data, status_code, extra_headers = {}, 201, {}
    collection_name = "testgroups"
    post_data = request.get_json()
    testgroups = post_data.get("testgroups", [])
    result = TestGroupsSchema(many=True).load(testgroups)
    if result.errors:
        response_data["STATUS"] = "FAILURE"
        response_data["DATA"] = result.errors
        status_code = 400
    else:
        mongo_db.insert_many(settings_database, collection_name, result.data)
        response_data = {
            "STATUS": "SUCCESS",
            "DATA": None,
            "MESSAGE": "Created testgroups successfully",
        }

    return response_data, status_code, extra_headers


@blueprint.route("/<string:institute_id>/testgroups")
@roles_required(["SETTINGS_CREATOR", "SETTINGS_READER", "TEST_CREATOR", "TEST_VIEWER"])
@authorization_required()
@make_egnify_data_response
def get_testgroups(institute_id):
    """Get testgroups.
    ---
    get:
      description: Get list of testgroups
      responses:
        200:
          description: Return list of testgroups
          content:
            application/json:
              schema: TestGroupsSchema
    """
    response_data, status_code, extra_headers = (
        {},
        200,
        {},
    )
    collection_name = "testgroups"
    query = {"instituteId": institute_id}
    testgroups = mongo_db.find(settings_database, collection_name, query)
    if not testgroups:
        query = {"instituteId": "root"}
        testgroups = mongo_db.find(settings_database, collection_name, query)
    return testgroups, status_code, extra_headers


@blueprint.route("/<string:institute_id>/testgroups/<string:group_id>", methods=["PUT"])
@roles_required(["SETTINGS_CREATOR"])
@authorization_required()
@make_egnify_response
def update_test_group(institute_id, group_id):
    """Update testgroup.
    ---
    put:
      description: Update test group data
      responses:
        200:
          description: Return status of test group update
          content:
            application/json:
              schema: TestGroupsSchema
    """
    response_data, status_code, extra_headers = {}, 200, {}
    collection_name = "testgroups"
    post_data = request.get_json()
    query = {"instituteId": institute_id, "_id": group_id}
    mongo_db.upsert(settings_database, collection_name, query, post_data)

    response_data = {
        "STATUS": "SUCCESS",
        "MESSAGE": "Updated successfully",
        "DATA": None,
    }
    return response_data, status_code, extra_headers


@blueprint.route(
    "/<string:institute_id>/testgroups/<string:group_id>", methods=["DELETE"]
)
@roles_required(["SETTINGS_CREATOR"])
@authorization_required()
@make_egnify_response
def delete_test_group(institute_id, group_id):
    """Delete testgroup.
    ---
    get:
      description: Delete test group data
      responses:
        200:
          description: Return status of delete test group
          content:
            application/json:
              schema: TestGroupsSchema
    """
    response_data, status_code, extra_headers = {}, 200, {}
    collection_name = "testgroups"
    query = {"instituteId": institute_id, "_id": group_id}
    mongo_db.delete(settings_database, collection_name, query)

    response_data = {
        "STATUS": "SUCCESS",
        "MESSAGE": "Deleted successfully",
        "DATA": None,
    }
    return response_data, status_code, extra_headers
