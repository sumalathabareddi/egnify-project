import copy
import time
from collections import defaultdict
from datetime import datetime, timedelta, timezone

import utils.decorators as decorators
from db.HydraCacheMongoDb import HydraCacheMongoDb
from utils.egy_analytics import cache
from utils.EgnifyRole import EgnifyRole
from utils.TimeHelper import TimeHelper
from flask import Blueprint, current_app, request
from utils.StorageHelper import StorageHelper
from utils.TimeHelper import TimeHelper

from helper import script
from snapshot.StudentCSVReader import StudentCSVReader
from snapshot.StudentSnapshot import StudentSnapshot
from api.schedule import schedule_task, cancel_a_scheduled_job

from .models import TestSchema
from .models.colorschema import ColorSchema

blueprint = Blueprint("tests", __name__, url_prefix="/tests")

mongo_db = (
    arango_db
) = ga_database = settings_database = storage_secrets = cached_mongo_db = None
student_list_api = student_behaviour_api = None


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global mongo_db, ga_database, settings_database, storage_secrets, student_list_api
    global student_behaviour_api, cached_mongo_db, arango_db
    mongo_db = current_app.mongo_db
    arango_db = current_app.arango_db
    ga_database = current_app.config["GA_DATABASE"]
    test_database = current_app.config["TEST_DATABASE"]
    settings_database = current_app.config["SETTINGS_DATABASE"]
    cached_mongo_db = HydraCacheMongoDb(
        mongo_db, test_database, "tests", projection={"accessTag": 1, "testId": 1}
    )
    student_list_api = current_app.config.get("CHAINA_STUDENT_LIST_API", None)
    student_behaviour_api = current_app.config.get("CHAINA_STUDENT_BEHAVIOUR_API", None)
    storage_secrets = {}
    if current_app.config["STORAGE_PROVIDER"] == "GCP":
        storage_secrets["GCP_CREDENTIALS_FILE"] = current_app.config[
            "GCP_CREDENTIALS_FILE"
        ]
    if current_app.config["STORAGE_PROVIDER"] == "MINIO":
        storage_secrets["endpoint"] = current_app.config["MINIO_ENDPOINT"]
        storage_secrets["access_key"] = current_app.config["MINIO_ACCESS_KEY"]
        storage_secrets["secret_key"] = current_app.config["MINIO_SECRET_KEY"]
        storage_secrets["secure"] = True


def get_cached_mongo_db():
    """

    :return:
    """
    return cached_mongo_db


@blueprint.route("", methods=["GET"])
@decorators.roles_required(
    [
        "TEST_CREATOR",
        "REPORTS_VIEWER",
        "ANALYSIS_VIEWER",
        "LMS_PERFORMANCE_VIEWER",
        EgnifyRole.TEACHER,
        "STUDENT",
    ]
)
@decorators.make_egnify_response
def get_tests():
    """
        Return list of test items.
    """
    start_time = time.time()
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 0, type=int)
    query = _build_tests_query()
    collection_name = "tests"
    projection = {field: 0 for field in ["accessTag", "branches"]}
    kwargs = {
        "key": "testId",
        "sort_key": "startTime",
        "skip": skip,
        "size": size,
        "ascending": False,
        "projection": projection,
    }
    tests = mongo_db.get_results(ga_database, collection_name, query, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return tests, 200, {}


@blueprint.route("", methods=["POST"])
@decorators.roles_required(["TEST_CREATOR", "STUDENT"])
@decorators.make_egnify_response
def create_test():
    """

    :return:
    """
    response_data, status_code, extra_headers = {}, 200, {}
    collection_name = "tests"
    test_data = request.get_json()  # read the json data from client request
    result = TestSchema().load(test_data)  # load for validations
    if result.errors:
        response_data = result.errors
        status_code = 400
    else:
        sanitized_data = result.data
        sanitized_data["_id"] = sanitized_data["testId"]
        prepare_test_data(sanitized_data)
        if sanitized_data["modeOfConduct"] == "online":
            tasks = schedule_tasks(sanitized_data)
            sanitized_data["couchSync"] = {
                "jobId": tasks["couchSync"]["jobId"],
                "status": "scheduled",
            }
        sanitized_data["lowerSync"] = 5
        sanitized_data["higherSync"] = 10
        mongo_db.insert_many(ga_database, collection_name, [sanitized_data])
        response_data = sanitized_data
        status_code = 201

    return response_data, status_code, extra_headers


@blueprint.route("/<string:test_id>")
@decorators.roles_required(
    ["TEST_CREATOR", "REPORTS_VIEWER", "ANALYSIS_VIEWER", "STUDENT", EgnifyRole.TEACHER]
)
@decorators.make_egnify_response
def get_test_data(test_id):
    """
        Return data for test of `test_id`
    """
    start_time = time.time()
    query = {"testId": test_id, "active": True}
    collection_name = "tests"
    projection = {field: 0 for field in ["accessTag", "branches"]}
    kwargs = {
        "key": "testId",
        "projection": projection,
    }
    test_data = mongo_db.get_results(ga_database, collection_name, query, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return test_data, 200, {}


@blueprint.route("/<string:test_id>", methods=["DELETE"])
@decorators.roles_required(["TEST_CREATOR"])
@decorators.make_egnify_response
def archive_test(test_id):
    query = {"testId": test_id}
    to_be_updated = {"active": False}
    test_doc = mongo_db.find_one_and_upsert(
        ga_database, "tests", query, to_be_updated, upsert=False,
    )
    if not test_doc:
        return "Bad Args", 400, {}
    if "couchSync" in test_doc:
        cancel_a_scheduled_job(test_doc["couchSync"]["jobId"])
    return test_doc, 200, {}


@blueprint.route("/<string:hierarchy_id>/<string:test_id>")
@decorators.roles_required(
    ["TEST_CREATOR", "REPORTS_VIEWER", "ANALYSIS_VIEWER", "STUDENT"]
)
@decorators.authorization_required(
    cached_mongo_db=get_cached_mongo_db, back_propagation=True
)
@decorators.make_egnify_response
def get_test_data_for_hierarchy(hierarchy_id, test_id):
    """
        Return data for test of `test_id`
    """
    start_time = time.time()
    query = {"testId": test_id, "active": True}
    collection_name = "tests"
    projection = {field: 0 for field in ["accessTag", "branches"]}
    kwargs = {
        "key": "testId",
        "projection": projection,
    }
    test_data = mongo_db.get_results(ga_database, collection_name, query, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return test_data, 200, {}


@blueprint.route("/<string:test_id>", methods=["PUT"])
@decorators.roles_required(["TEST_CREATOR", "STUDENT"])
@decorators.make_egnify_response
def update_test(test_id):
    """

    :param test_id:
    :return:
    """
    response_data, status_code, extra_headers = {}, 200, {}
    collection_name = "tests"
    post_data = request.get_json()
    result = TestSchema().load(post_data)
    upload_errors = []
    for attr, errors in result.errors.items():
        if attr in post_data:
            upload_errors.append({attr: errors})
    if upload_errors:
        response_data = upload_errors
        status_code = 400
    else:
        sanitized_data = result.data
        post_data["startDate"] = sanitized_data["startDate"]
        post_data["endDate"] = sanitized_data["endDate"]
        prepare_test_data(post_data)
        if post_data["modeOfConduct"] == "online":
            tasks = schedule_tasks(post_data)
            post_data["couchSync"] = {
                "jobId": tasks["couchSync"]["jobId"],
                "status": "scheduled",
            }
            if post_data.get("isAutoGa", False):
                post_data["autoGa"] = {
                    "jobId": tasks["autoGa"]["jobId"],
                    "status": "scheduled",
                }
        post_data["lowerSync"] = 5
        post_data["higherSync"] = 10
        find_query = {"testId": test_id}
        upsertedTest = mongo_db.find_one_and_upsert(
            ga_database, collection_name, find_query, post_data, new=False
        )
        if "couchSync" in upsertedTest:
            cancel_a_scheduled_job(upsertedTest["couchSync"]["jobId"])
        if "autoGa" in upsertedTest:
            cancel_a_scheduled_job(upsertedTest["autoGa"]["jobId"])

        response_data = {
            "STATUS": "SUCCESS",
            "DATA": None,
            "MESSAGE": "Updated test successfully",
        }
    return response_data, status_code, extra_headers


@blueprint.route("/count")
@decorators.roles_required(["TEST_CREATOR", "REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.make_egnify_response
def get_tests_count():
    """"
        Return count of test items.
    """
    start_time = time.time()
    query = _build_tests_query()
    collection_name = "tests"
    tests_count = mongo_db.count(ga_database, collection_name, query)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return {"count": tests_count}, 200, {}


@blueprint.route("/<string:test_id>/status")
@decorators.roles_required(["TEST_CREATOR", "REPORTS_VIEWER", "ANALYSIS_VIEWER"])
# @decorators.authorization_required(
#     cached_mongo_db=get_cached_mongo_db, back_propagation=True
# )
@decorators.make_egnify_response
def get_test_status(test_id):
    """
        Return status and gaStatus for test.
    """
    start_time = time.time()
    query = {"testId": test_id, "active": True}
    collection_name = "tests"
    projection = {field: 1 for field in ["status", "gaStatus", "testId"]}
    kwargs = {
        "key": "testId",
        "projection": projection,
    }
    test_status_data = mongo_db.get_results(
        ga_database, collection_name, query, **kwargs
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return test_status_data, 200, {}


@blueprint.route("/<test_id>/stats", methods=["GET", "POST"])
@decorators.roles_required(["TEST_CREATOR", "TEST_VIEWER"])
@decorators.authorization_required(
    cached_mongo_db=get_cached_mongo_db, back_propagation=True
)
@decorators.make_egnify_response
def get_test_stats(test_id):
    """
    Returns test taking numbers
    """
    coll = "testStudentSnapshot"
    # Used for filtering results for user
    hierarchies = request.values.get("hierarchy", "").split(",")
    leaf_nodes = []
    for hierarchy in hierarchies:
        nodes = cache.get(f"leaf_nodes_{hierarchy}")
        if not nodes:
            nodes = arango_db.get_leaf_nodes(hierarchy)
            cache.set(f"leaf_nodes_{hierarchy}", nodes, 1800)
        leaf_nodes.extend([node["id"] for node in nodes])
    # Query the the test with id and leaf hierarchies
    query = {"testId": test_id, "hierarchyLevels": {"$in": leaf_nodes}}
    online_query = copy.deepcopy(query)
    online_query.update({"mode": "online"})
    completed_query = copy.deepcopy(query)
    completed_query.update({"status": "completed"})

    count = mongo_db.count(ga_database, coll, query)
    online = mongo_db.count(ga_database, coll, online_query)
    completed = mongo_db.count(ga_database, coll, completed_query)
    resp = {
        "STATUS": "SUCCESS",
        "DATA": {"total": count, "started": online, "submitted": completed},
    }
    return resp, 200, {}


@blueprint.route("/<test_id>/attendance", methods=["GET", "POST"])
@decorators.roles_required(["REPORTS_VIEWER", "TESTS_CREATOR", "TESTS_VIEWER"])
@decorators.authorization_required(
    cached_mongo_db=get_cached_mongo_db, back_propagation=True
)
@decorators.make_egnify_response
def get_test_attendance(test_id):
    start_time, coll = time.time(), "testStudentSnapshot"
    hierarchy = request.values.get("hierarchy", "")
    status = request.values.get("status")
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 20, type=int)
    search_text = request.values.get("searchText", "")
    hierarchies = hierarchy.split(",")
    query = {"testId": test_id, "hierarchyLevels": {"$in": hierarchies}}
    if status and status == "completed":
        query["status"] = status
    if status and status == "inprogress":
        query["mode"] = "online"
        query["status"] = {"$ne": "completed"}
    if status and status == "absent":
        query["mode"] = {"$exists": False}
    kwargs = {
        "skip": skip,
        "size": size,
    }
    if search_text:
        query["$or"] = [
            {"studentName": {"$regex": search_text, "$options": "i"}},
            {"studentId": {"$regex": search_text, "$options": "i"}},
        ]
    data = mongo_db.find(ga_database, coll, query, **kwargs)
    count = mongo_db.count(ga_database, coll, query)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {coll}: {db_duration}"
    )
    resp = {"STATUS": "SUCCESS", "DATA": {"students": data, "count": count}}
    return resp, 200, {}


@blueprint.route("/<string:test_id>/snapshot", methods=["POST"])
@decorators.roles_required(["TEST_CREATOR", "STUDENT"])
# @decorators.authorization_required(
#     cached_mongo_db=get_cached_mongo_db, back_propagation=True
# )
@decorators.make_egnify_response
def create_test_snapshot(test_id):
    """
        Returns the test snapshot creation status.
    """
    tests_collection_name = "tests"
    query = {"testId": test_id}
    # # clean the previous data if exists
    # mongo_db.delete(ga_database, "uploadprogress", query)
    # mongo_db.delete(ga_database, "testStudentSnapshot", query)
    # fetch the hierarchies for which test has been created.
    test_data = mongo_db.find(ga_database, tests_collection_name, query, key="testId")
    test_hierarchies = test_data[test_id]["accessTag"]["hierarchy"]
    # fetch the leaf level nodes for student snapshot creation
    leaf_nodes = []
    for each_hierarchy in test_hierarchies:
        hierarchy_leaf_nodes = arango_db.get_leaf_nodes(each_hierarchy)
        hierarchy_leaf_nodes = [node["id"] for node in hierarchy_leaf_nodes]
        leaf_nodes.extend(hierarchy_leaf_nodes)
    # use the test least level nodes to fetch students

    start_time = time.time()
    students_query = {"accessTag.hierarchy": {"$in": leaf_nodes}, "active": True}
    students_coll_name = "studentInfo"
    students_kwargs = {"key": "_id"}
    students = mongo_db.find(
        settings_database, students_coll_name, students_query, **students_kwargs
    )

    current_app.logger.info(
        f"Time taken to read from database for students {time.time() - start_time}"
    )
    result_upload_progress = defaultdict(
        lambda: {
            "total": 0,
            "resultsUploaded": 0,
            "missingConfirmed": 0,
            "hierarchy": None,
            "testId": None,
        }
    )
    start_time = time.time()

    lookup = {}
    # create snapshot for all the students
    snapshot_list = []
    for student_info in students.values():
        student_snapshot = StudentSnapshot(
            [], None, [], test_id, test_data[test_id]["testName"], None, None
        )
        student_snapshot.fill_student_details(student_info, False)
        hierarchy_levels = []
        parent_hierarchy = student_info["accessTag"]["hierarchy"]
        while parent_hierarchy and parent_hierarchy != "root":
            hierarchy_levels.append(parent_hierarchy)
            if parent_hierarchy not in lookup:
                parent_node = arango_db.get_node(parent_hierarchy)
                lookup[parent_hierarchy] = parent_node
            parent_node = lookup[parent_hierarchy]
            result_upload_progress[parent_hierarchy]["total"] += 1
            result_upload_progress[parent_hierarchy][
                "instituteLevelCode"
            ] = parent_node["instituteLevelCode"]
            result_upload_progress[parent_hierarchy]["level"] = parent_node["level"]
            result_upload_progress[parent_hierarchy]["hierarchy"] = parent_hierarchy
            result_upload_progress[parent_hierarchy]["testId"] = test_id
            result_upload_progress[parent_hierarchy]["name"] = parent_node[
                "hierarchyName"
            ]
            parent_hierarchy = parent_node["parentCode"]
        student_snapshot.hierarchyLevels = hierarchy_levels
        snapshot_list.append(vars(student_snapshot))
    current_app.logger.info(
        f"Time taken to create student snapshot list {time.time() - start_time}"
    )
    start_time = time.time()
    mongo_db.insert_many_in_process(
        ga_database, "uploadprogress", list(result_upload_progress.values())
    )
    mongo_db.wait_for_termination()
    new_list=[]
    snapshot_collection = "testStudentSnapshot"
    in_db = mongo_db.find(ga_database,snapshot_collection,query)
    print(len(in_db))
    for index, student in enumerate(in_db,0):
        if student['studentId'] != snapshot_list[index]['studentId']:
            new_list.append(snapshot_list[index])

    print(len(new_list))
        
    # mongo_db.insert_many_in_process(ga_database, snapshot_collection, snapshot_list)
    # mongo_db.wait_for_termination()
    current_app.logger.info(
        f"Time taken to write to database {time.time() - start_time}"
    )
    return_response = {
        "STATUS": "SUCCESS",
        "DATA": None,
        "MESSAGE": f"Created snapshot for {len(snapshot_list)} students",
    }
    return return_response, 200, {}


@blueprint.route("/<string:test_id>/file", methods=["POST"])
@decorators.roles_required(["TEST_CREATOR"])
# @decorators.authorization_required(
#     cached_mongo_db=get_cached_mongo_db, back_propagation=True
# )
@decorators.make_egnify_response
def test_result_upload(test_id):
    """
        Upload the students response csv to cloud storage.
    """
    start_time = time.time()
    uploaded_file = request.files.get("file")
    uploaded_file_string = uploaded_file.read()
    bucket_names = {
        "GCP": current_app.config["CLOUD_STORAGE_BUCKET"],
        "MINIO": current_app.config["MINIO_STORAGE_BUCKET"],
    }
    storage_provider = current_app.config["STORAGE_PROVIDER"]
    data = {
        "file_ext": ".csv",
        "file_name": uploaded_file.filename,
        "data": uploaded_file_string,
        "content_type": uploaded_file.content_type,
        "bucket_name": bucket_names[storage_provider],
    }

    storage_helper = StorageHelper(storage_provider, storage_secrets)

    url = storage_helper.upload(data, get_public_url=True)

    gcp_duration = time.time() - start_time
    current_app.logger.info(
        "Time taken put file in cloud storage: {}".format(gcp_duration)
    )
    resp = {"STATUS": "SUCCESS", "url": url}
    return resp, 200, {}


@blueprint.route("/<string:student_id>/<string:test_id>/review", methods=["GET"])
@decorators.roles_required(["ANALYSIS_VIEWER", "REPORTS_VIEWER", "STUDENT"])
@decorators.make_egnify_response
def test_review(student_id, test_id):
    """
        Return raw response of a test for a student.
    """
    start_time = time.time()
    _id = "_".join([test_id, student_id])
    query = {"_id": _id}
    collection_name = "behaviourData"
    projection = {"behaviourData": 1}
    test_status_data = mongo_db.find_one(
        ga_database, collection_name, query, projection
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    custom_headers = {"Cache-Control": "private, max-age=86400"}
    return test_status_data, 200, custom_headers


@blueprint.route("<string:test_id>/resultupload", methods=["POST"])
@decorators.roles_required(["TEST_CREATOR"])
@decorators.make_egnify_response
def result_upload_for_test(test_id):
    """

    :param test_id:
    :return:
    """
    data = request.get_json()
    # fetch the user access hierarchies
    user_access_hierarchies = request.values.get("hierarchy", "")
    user_access_hierarchies = user_access_hierarchies.split(",")
    if not user_access_hierarchies:
        return {}, 400, {}
    # fetch the hierarchies test has been created on
    tests_collection_name = "tests"
    query = {"testId": test_id}
    test_data = mongo_db.find(ga_database, tests_collection_name, query, key="testId")
    test_hierarchies = test_data[test_id]["accessTag"]["hierarchy"]
    # prepare the common leaf nodes to fetch students list
    user_leaf_nodes = []
    for each_user_access_hierarchy in user_access_hierarchies:
        hierarchy_leaf_nodes = arango_db.get_leaf_nodes(each_user_access_hierarchy)
        hierarchy_leaf_nodes = [node["id"] for node in hierarchy_leaf_nodes]
        user_leaf_nodes.extend(hierarchy_leaf_nodes)
    test_leaf_nodes = []
    for each_test_hierarchy in test_hierarchies:
        hierarchy_leaf_nodes = arango_db.get_leaf_nodes(each_test_hierarchy)
        hierarchy_leaf_nodes = [node["id"] for node in hierarchy_leaf_nodes]
        test_leaf_nodes.extend(hierarchy_leaf_nodes)
    common_leaf_nodes = list(set(user_leaf_nodes) & set(test_leaf_nodes))

    return_response = read_csv_file_to_snapshot(
        data, common_leaf_nodes, test_data[test_id]
    )
    return return_response, 200, {}


@blueprint.route("<string:test_id>/missing", methods=["GET"])
@decorators.roles_required(["TEST_CREATOR"])
@decorators.make_egnify_response
def get_missing_students(test_id):
    """

    :param test_id:
    :return:
    """
    collection_name = "testStudentSnapshot"
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 10, type=int)
    query = {"testId": test_id, "syncStatus": False}
    kwargs = {
        "projection": {attr: 1 for attr in ["studentId", "egnifyId", "studentName"]},
        "skip": skip,
        "size": size,
    }
    students = mongo_db.find(ga_database, collection_name, query, **kwargs)
    return students, 200, {}


@blueprint.route("<string:test_id>/missing/count", methods=["GET"])
@decorators.roles_required(["TEST_CREATOR"])
@decorators.make_egnify_response
def get_missing_student_count(test_id):
    """

    :param test_id:
    :return:
    """
    collection_name = "testStudentSnapshot"

    query = {"testId": test_id, "syncStatus": False}
    missing_count = mongo_db.count(ga_database, collection_name, query)
    return_response = {"count": missing_count}
    return return_response, 200, {}


@blueprint.route("<string:test_id>/missing/confirm", methods=["POST"])
@decorators.roles_required(["TEST_CREATOR"])
@decorators.make_egnify_response
def confirm_missing_students_count(test_id):
    """

    :param test_id:
    :return:
    """
    collection_name = "uploadprogress"

    upload_progress_query = {"testId": test_id}
    upload_progress = mongo_db.find(
        ga_database, collection_name, upload_progress_query, key="hierarchy"
    )
    hierarchies_progress = upload_progress.values()
    for each_item in hierarchies_progress:
        each_item["missingConfirmed"] = (
            each_item["total"] - each_item["resultsUploaded"]
        )
    mongo_db.update_many_in_process(ga_database, collection_name, hierarchies_progress)
    mongo_db.wait_for_termination()
    return_response = {
        "STATUS": "SUCCESS",
        "DATA": None,
        "MESSAGE": "Confirmed successfully",
    }
    return return_response, 200, {}


@blueprint.route("<string:test_id>/uploadprogress", methods=["GET"])
@decorators.roles_required(["TEST_CREATOR"])
@decorators.make_egnify_response
def get_test_result_upload_progress(test_id):
    """

    :param test_id:
    :return:
    """
    collection_name = "uploadprogress"
    query = {"testId": test_id}
    upload_progress = mongo_db.find(ga_database, collection_name, query)
    return upload_progress, 200, {}


@blueprint.route(
    "<string:test_id>/institutes/<string:institute_id>/colorschema", methods=["GET"]
)
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_required(
    cached_mongo_db=get_cached_mongo_db, back_propagation=True
)
@decorators.make_egnify_response
def color_schema(test_id, institute_id):
    response_data, status_code, extra_headers = {}, 200, {}
    collection_name = "testColorSchema"
    query = {"testId": test_id, "instituteId": institute_id}
    color_schema = mongo_db.find_one(ga_database, collection_name, query)
    response_data["STATUS"] = "SUCCESS"
    response_data["DATA"] = color_schema
    return response_data, status_code, extra_headers


@blueprint.route(
    "<string:test_id>/institutes/<string:institute_id>/colorschema", methods=["PUT"]
)
@decorators.roles_required(["REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_required(
    cached_mongo_db=get_cached_mongo_db, back_propagation=True
)
@decorators.make_egnify_response
def update_color_schema(test_id, institute_id):
    response_data, status_code, extra_headers = {}, 200, {}
    collection_name = "testColorSchema"
    query = {"testId": test_id, "instituteId": institute_id}
    data = request.get_json()
    result = ColorSchema().load(data)
    if result.errors:
        status_code = 400
        response_data["STATUS"] = "FAILURE"
        response_data["DATA"] = result.errors
    else:
        response_data["STATUS"] = "SUCCESS"
        response_data["MESSAGE"] = "Updated color schema successfully"
    mongo_db.find_one_and_upsert(ga_database, collection_name, query, data)
    return response_data, status_code, extra_headers


@blueprint.route(
    "<string:test_id>/institutes/<string:institute_id>/online", methods=["GET"]
)
@decorators.roles_required(["TEST_CREATOR"])
@decorators.authorization_required(
    cached_mongo_db=get_cached_mongo_db, back_propagation=True
)
@decorators.make_egnify_response
def test_online_students(test_id, institute_id):
    response_data, status_code, extra_headers = {}, 200, {}
    collection_name = "testStudentSnapshot"
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 10, type=int)
    kwargs = {
        "skip": skip,
        "size": size,
        "query": {"testId": test_id, "instituteId": institute_id, "mode": "online"},
        "projection": {"studentId": 1},
    }
    data = mongo_db.find(ga_database, collection_name, **kwargs)
    response_data["STATUS"] = "SUCCESS"
    response_data["DATA"] = data
    return response_data, status_code, extra_headers


@blueprint.route(
    "<string:test_id>/institutes/<string:institute_id>/online/count", methods=["GET"]
)
@decorators.roles_required(["TEST_CREATOR"])
@decorators.authorization_required(
    cached_mongo_db=get_cached_mongo_db, back_propagation=True
)
@decorators.make_egnify_response
def test_online_students_count(test_id, institute_id):
    response_data, status_code, extra_headers = {}, 200, {}
    collection_name = "testStudentSnapshot"
    query = {"testId": test_id, "instituteId": institute_id, "mode": "online"}
    count = mongo_db.count(ga_database, collection_name, query)
    response_data["STATUS"] = "SUCCESS"
    response_data["DATA"] = {"count": count}
    return response_data, status_code, extra_headers


def read_csv_file_to_snapshot(data, common_leaf_nodes, test_info):
    """

    :param data:
    :param common_leaf_nodes:
    :return:
    """
    collection_name = "testStudentSnapshot"
    file_url = data["file_url"]
    test_id = data["test_id"]
    test_name = data["test_name"]
    include_invalid = data["include_invalid"]

    total_questions = data.get("markingSchema", {}).get("totalQuestions", 0)
    egnify_multi_question_type = False
    for subject in data.get("markingSchema", {}).get("subjects", []):
        for mark in subject.get("marks", {}):
            if (
                mark.get("egnifyQuestionType", "") == "Matrix match type"
                or mark.get("egnifyQuestionType", "") == "Matrix list type"
            ):
                egnify_multi_question_type = True

    student = StudentCSVReader(test_id)
    student.read_csv_file(
        file_url, test_name, total_questions, egnify_multi_question_type
    )  # read csv file and raise validation exceptions if any

    return_response = {}
    start_time = time.time()
    test_query = {"testId": test_id}
    students_query = {
        "hierarchy": {"$in": common_leaf_nodes},
        "startTime": {"lt": test_info["endTime"]},
        "endTime": {"$gt": test_info["startTime"]},
    }
    kwargs = {
        "key": "studentId",
    }
    current_app.logger.info(f"Completed csv reading {time.time() - start_time}")
    student_ids_from_snapshot = mongo_db.find(
        ga_database, "hierarchy-ledger", students_query, **kwargs
    )  # dict of {studentId: studentSnapshot}
    current_app.logger.info(
        f"Completed reading students from snapshot {time.time() - start_time}"
    )
    unknown_students = (
        []
    )  # list of all students that are not part of test student snapshot
    students_in_csv = student.students
    for each_student_id in students_in_csv.keys():
        if each_student_id not in student_ids_from_snapshot:
            unknown_students.append(each_student_id)
    # now separate illegal students, invalid students
    students_from_settings = mongo_db.read_by_field(
        settings_database, "studentInfo", "studentId", unknown_students
    )
    illegal_students, invalid_students = [], []
    for each_unknown_student in unknown_students:
        if each_unknown_student in students_from_settings:
            invalid_students.append(students_from_settings[each_unknown_student])
        else:
            illegal_students.append(each_unknown_student)
    students_to_consider = (
        students_from_settings if include_invalid else student_ids_from_snapshot
    )

    missing_students = []
    present_students = []
    upload_progress_query = {"testId": test_id}
    upload_progress = mongo_db.find(
        ga_database, "uploadprogress", upload_progress_query, key="hierarchy"
    )

    lookup = {}
    for each_student_id, student_info in students_to_consider.items():
        if each_student_id not in student.students:
            missing_students.append(each_student_id)
        else:
            if not student_info["responseData"]["questionResponse"]:
                parent_hierarchy = student_info["accessTag"]["hierarchy"]
                while parent_hierarchy and parent_hierarchy != "root":
                    # update the result upload numbers
                    if parent_hierarchy not in lookup:
                        parent_node = arango_db.get_node(parent_hierarchy)
                        lookup[parent_hierarchy] = parent_node
                    parent_node = lookup[parent_hierarchy]
                    upload_progress[parent_hierarchy]["resultsUploaded"] += 1
                    parent_hierarchy = parent_node["parentCode"]

            present_students.append(each_student_id)
    mongo_db.update_many_in_process(
        ga_database, "uploadprogress", list(upload_progress.values())
    )
    mongo_db.wait_for_termination()
    current_app.logger.info(
        f"Completed updating upload progresses {time.time() - start_time}"
    )
    # student_ids_from_db = mongo_db.read_by_field(
    #     settings_database, "studentInfo", "studentId", list(student.students.keys())
    # )  # fetch student's info from db
    current_app.logger.info(
        "Time taken to read from DB {}".format(time.time() - start_time)
    )
    student.fill_db_with_csv(students_to_consider)
    to_be_updated = {
        "csvUrls.{}".format(student.file_tag): {
            "name": student.file_name,
            "url": file_url,
        },
        "resultsUploaded": len(student.students_as_list),
        "regenerate": True,
    }
    mongo_db.upsert(ga_database, "tests", test_query, to_be_updated)
    mongo_db.upsert_many_in_process(
        ga_database, collection_name, student.students_as_list
    )
    current_app.logger.info(f"number of missing students: {len(missing_students)}")
    current_app.logger.info(f"number of present students: {len(present_students)}")

    mongo_db.wait_for_termination()
    if student_list_api and student_behaviour_api and student.other_testId:
        results = script.get_behaviour(
            student_list_api,
            student_behaviour_api,
            test_id,
            student.other_testId,
            student.online_student_list,
        )
        if results and isinstance(results, list):
            mongo_db.upsert_many_in_process(
                ga_database, collection_name, results, upsert=False
            )
            mongo_db.wait_for_termination()
        else:
            current_app.logger.info("No Behaviour")
    else:
        current_app.logger.info("No Behaviour")
    current_app.logger.info(f"Number of invalid students {len(invalid_students)}")
    invalid_data = get_invalid_students(invalid_students)
    return_response["DATA"] = {
        "invalid": invalid_data,
        "illegal": illegal_students,
    }
    return_response["STATUS"] = "SUCCESS"
    return return_response


def get_invalid_students(students):
    """

    :param students:
    :param level:
    :return:
    """

    data = defaultdict(list)
    for student in students:
        hierarchy = student["accessTag"]["hierarchy"]
        student = {
            "Student ID": student["studentId"],
            "Student Name": student["studentName"],
        }
        data[hierarchy].append(student)
    return data


def prepare_test_data(sanitized_data):
    """

    :param sanitized_data:
    """
    sanitized_data["accessTag"] = {"hierarchy": sanitized_data["hierarchy"]}
    start_time_hours, start_time_minutes = map(
        int, sanitized_data["startTime"].split(":")
    )  # "02:00" => hours = 2, minutes = 0

    end_time_hours, end_time_minutes = map(
        int, sanitized_data["endTime"].split(":")
    )  # "05:00" => hours = 5, minutes = 0

    test_start_date = sanitized_data["startDate"]
    start_time = datetime(
        test_start_date.year, test_start_date.month, test_start_date.day
    ) + timedelta(
        hours=start_time_hours, minutes=start_time_minutes
    )  # convert to datetime type for mongo compatibility
    test_start_date = datetime(
        test_start_date.year, test_start_date.month, test_start_date.day
    )  # convert to datetime type for mongo compatibility
    sanitized_data["startDate"] = TimeHelper.to_utc(test_start_date, "Asia/Kolkata")
    sanitized_data["startTime"] = TimeHelper.to_utc(start_time, "Asia/Kolkata")

    test_end_date = sanitized_data["endDate"]
    end_time = datetime(
        test_end_date.year, test_end_date.month, test_end_date.day
    ) + timedelta(
        hours=end_time_hours, minutes=end_time_minutes
    )  # convert to datetime type for mongo compatibility
    test_end_date = datetime(
        test_end_date.year, test_end_date.month, test_end_date.day
    )  # convert to datetime type for mongo compatibility

    sanitized_data["endDate"] = TimeHelper.to_utc(test_end_date, "Asia/Kolkata")
    sanitized_data["endTime"] = TimeHelper.to_utc(end_time, "Asia/Kolkata")


@blueprint.route("/<string:hierarchy_id>/test/list", methods=["GET"])
@decorators.roles_required(["STUDENT", "REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_required(
    cached_mongo_db=get_cached_mongo_db, back_propagation=True
)
@decorators.make_egnify_response
def hierarchy_wise_test_list(hierarchy_id):
    """
    :param hierarchy_id:
    :return:
    """
    parent_nodes = arango_db.get_parent(hierarchy_id, height=20)
    hierachies = list(map(lambda x: x["id"], parent_nodes["vertices"]))
    hierachies.append(hierarchy_id)
    status = request.values.get("status", None)
    test_category_type = request.values.get("testCategoryType")
    syllabus_type = request.values.get("syllabusType")
    query = {
        "accessTag.hierarchy": {"$in": hierachies},
        "active": True,
        "status": {"$ne": "draft"},
        "testFormatType": "ADMIN_TEST_TYPE",
        "testCategoryType": test_category_type,
        "syllabusType": syllabus_type,
    }
    if status:
        if status == "inprogress":
            query["startTime"] = {"$lte": datetime.now(timezone.utc)}
            query["endTime"] = {"$gte": datetime.now(timezone.utc)}
        elif status == "upcoming":
            query["startTime"] = {"$gte": datetime.now(timezone.utc)}
        elif status == "completed":
            query["endTime"] = {"$lt": datetime.now(timezone.utc)}
    collection_name = "tests"
    projection = {field: 0 for field in ["accessTag"]}
    kwargs = {
        "key": "testId",
        "sort_key": "startTime",
        "ascending": True,
        "projection": projection,
    }
    start_time = time.time()
    tests = mongo_db.get_results(ga_database, collection_name, query, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return tests, 200, {}


def schedule_tasks(test):
    tasks = {}
    end_time = test["endTime"]
    duration = test["duration"]
    grace = test.get("gracePeriod", 0)
    if grace:
        date = test["startTime"] + timedelta(minutes=duration + grace)
    else:
        date = end_time + timedelta(minutes=duration)
    # Schedule couch to mongo sync
    data = {
        "module": "couch_to_mongo",
        "method": "on_schedule",
        "dt": date,
        "args": {
            "test_id": test["testId"],
            "questionPaperId": "QP" + test["testId"],
            "test_name": test["testName"],
            "tie_breaking_list": [],
            "studentId": None,
        },
    }
    couch_to_mongo_task = schedule_task(data)
    tasks["couchSync"] = {"jobId": couch_to_mongo_task.id}

    if test.get("isAutoGa", False):
        query = {"_id": test["markingSchemaId"]}
        marking_schema = mongo_db.find_one(settings_database, "markingschemas", query)
        data = {
            "module": current_app.config["ANALYSIS_FILE"],
            "method": "hello_gcs_generic",
            "dt": date,
            "depends_on": couch_to_mongo_task.id,
            "args": {
                "test_id": test["testId"],
                "questionPaperId": test["questionPaperId"],
                "test_name": test["testName"],
                "marking_schema": marking_schema,
            },
        }

        ga_task = schedule_task(data)
        tasks["autoGa"] = {"jobId": ga_task.id}

    return tasks


def _build_tests_query():
    status = request.values.get("status", None)
    ga_status = request.values.get("gaStatus", None)
    textbook = request.values.get("textbook", None)
    search_text = request.values.get("searchText", None)
    mode_of_conduct = request.values.get("modeOfConduct", None)
    hierarchies = request.values.get("hierarchy", None)
    test_type = request.values.get("type", "ADMIN_TEST_TYPE")
    topic = request.values.get("topic", None)
    query = {"active": True, "testFormatType": test_type, "testCategoryType": None}
    orientations = request.values.get("orientations", None)
    patterns = request.values.get("patterns", None)
    classes = request.values.get("classes", None)
    start_time = request.values.get("startTime", None)
    end_time = request.values.get("endTime", None)

    if hierarchies:
        childrens = []
        hierarchies = hierarchies.split(",")
        for hierarchy in hierarchies:
            if status and status == "completed":
                parent_nodes = arango_db.get_parent(hierarchy, height=20)
                node_hierachies = [v["id"] for v in parent_nodes["vertices"]]
                childrens = childrens + node_hierachies
            children_nodes = arango_db.get_children(hierarchy, depth=20)
            node_hierachies = [v["id"] for v in children_nodes["vertices"]]
            childrens = childrens + node_hierachies
        query["accessTag.hierarchy"] = {"$in": childrens}

    if orientations:
        query["orientationId"] = {"$in": orientations.split(",")}
    if patterns:
        query["patternId"] = {"$in": patterns.split(",")}
    if classes:
        query["classes"] = {"$in": classes.split(",")}

    if start_time:
        start_time = TimeHelper.to_utc(
            datetime.strptime(start_time, "%Y-%m-%d %H:%M"), "Asia/Kolkata"
        )
    if end_time:
        end_time = TimeHelper.to_utc(
            datetime.strptime(end_time, "%Y-%m-%d %H:%M"), "Asia/Kolkata"
        )

    if status:
        if status == "completed":
            # Status itself doesn't give the test stage, when it is
            # combined with time range we get it's state. For a test
            # to be in completed stage it should not be in draft stage
            # and endTime should have been passed.
            if start_time:
                query["startTime"] = {"$gt": start_time}
            if end_time:
                query["endTime"] = {"$lte": end_time}
            else:
                query["endTime"] = {"$lte": datetime.now(timezone.utc)}
            query["status"] = {"$ne": "draft"}
        elif status == "draft":
            # If assessment is being prepared, it's status will be
            # draft and we can use the same.
            query["status"] = "draft"
            if start_time:
                query["startTime"] = {"$gt": start_time}
            if end_time:
                query["endTime"] = {"$lte": end_time}
        elif status == "upcoming":
            # Backend doesn't have any "upcoming" status, assessments that
            # are yet be started and paper is published are upcoming ones.
            if start_time:
                query["startTime"] = {"$gt": start_time}
            else:
                query["startTime"] = {"$gt": datetime.now(timezone.utc)}
            if end_time:
                query["endTime"] = {"$lte": end_time}
            query["status"] = {"$ne": "draft"}
        elif status == "inprogress":
            # Inprogress tests are the ones which are still being taken, i.e
            # It's start time is less than current time and end time hasn't passed
            # yet.
            inner_query = []
            if start_time and end_time:
                inner_query.append({"startTime": {"$gte": start_time}})
                inner_query.append({"endTime": {"$lte": end_time}})
            elif start_time:
                inner_query.append({"startTime": {"$gte": start_time}})
                inner_query.append({"endTime": {"$gte": datetime.now(timezone.utc)}})
            else:
                inner_query.append({"startTime": {"$lte": datetime.now(timezone.utc)}})
                inner_query.append({"endTime": {"$gte": datetime.now(timezone.utc)}})
            query["$and"] = inner_query
            query["status"] = {"$ne": "draft"}

    if ga_status:
        query["gaStatus"] = ga_status
    if topic:
        query["topic"] = topic
    if textbook:
        query["mapping.textbook.code"] = textbook
    if search_text:
        search_query = {
            "$or": [
                {"testName": {"$regex": search_text, "$options": "i"}},
                {"testId": {"$regex": search_text, "$options": "i"}},
            ]
        }
        query["$and"] = query.get("$and", [])
        query["$and"].append(search_query)

    if mode_of_conduct:
        query["modeOfConduct"] = mode_of_conduct
    return query

