import csv
import json
import random
import time
from collections import namedtuple
from datetime import datetime, timezone
from random import randint

import redis
import requests
import utils.decorators as decorators
from aggregation import Aggregation
from bson.objectid import ObjectId
from db.HydraCacheMongoDb import HydraCacheMongoDb
from flask import Blueprint, current_app, request
from iteround import saferound
from pymongo.errors import DuplicateKeyError
from scipy.optimize import lsq_linear
from utils.EgnifyException import MultipleValidationExceptions, ValidationException
from utils.egy_analytics import cache
from aggregation import Aggregation
from utils import deep_defaultdict
from rq import Queue
# from multiprocessing import Queue
import students_bulk_creation

from bulkread.BulkRead import StudentsBulkRead, StudentsBulkReadV2

from .models.students import StudentSchema

blueprint = Blueprint("students", __name__, url_prefix="/students")

mongo_db = (
    arango_db
) = (
    settings_database
) = ga_database = cached_mongo_db = student_mongo_db = rankguru = nova = None

queue = None

def get_cached_mongo_db():
    """

    :return:
    """
    return cached_mongo_db


def get_student_cached_mongo_db():
    """

    :return:
    """
    return student_mongo_db


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global mongo_db, arango_db, settings_database, ga_database
    global cached_mongo_db, student_mongo_db, rankguru, nova, queue
    mongo_db = current_app.mongo_db
    arango_db = current_app.arango_db
    redis_connection = redis.Redis.from_url(current_app.config["RQ_REDIS_URL"])
    queue = Queue(current_app.config["RQ_QUEUES"], connection=redis_connection)
    settings_database = current_app.config["SETTINGS_DATABASE"]
    ga_database = current_app.config["GA_DATABASE"]
    cached_mongo_db = HydraCacheMongoDb(
        mongo_db, ga_database, current_app.config["MASTER_RESULTS_TABLE"]
    )
    student_mongo_db = HydraCacheMongoDb(mongo_db, settings_database, "studentInfo")
    rankguru = current_app.config.get("IS_RANKGURU", False)
    nova = not rankguru


@blueprint.route("", methods=["POST"])
@decorators.roles_required(["SETTINGS_CREATOR"])
@decorators.authorization_required()
@decorators.make_egnify_response
def add_student():
    response_data, status_code, extra_headers = {}, 201, {}
    post_data = request.json
    res = StudentSchema().load(post_data)
    if res.errors:
        response_data["STATUS"] = "FAILURE"
        response_data["ERRORS"] = res.errors
        status_code = 400
        return response_data, status_code, extra_headers
    student = res.data
    student["_id"] = student["egnifyId"]
    try:
        mongo_db.insert_one(settings_database, "studentInfo", student)
    except DuplicateKeyError:
        response_data["STATUS"] = "FAILURE"
        response_data["MESSAGE"] = "Duplicate record in the system"
        status_code = 400
        return response_data, status_code, extra_headers
    else:
        hierarchy = [{"childCode": student["accessTag"]["hierarchy"]}]
        password = student.get("password")
        if not password:
            password = str(randint(10000, 99999))
        user_doc = {
                "_id": student["_id"],
                "role": ["STUDENT", "LMS_OTP_ACCESS"],
                "password": password,
                "active": True,
                "hierarchy": hierarchy,
                "email": student["email"].lower(),
                "hostname": current_app.config["HOST_NAME"],
                "instituteId": student["instituteId"],
                "studentId": student["studentId"].upper(),
                "studentName": student["studentName"],
                "username": student["studentName"],
                "egnifyId": student["egnifyId"],
            }
        headers = {
            "Content-type": "application/json",
            "Accept": "text/plain",
            "accesscontroltoken": request.headers.get("accesscontroltoken"),
            "Authorization": request.headers.get("Authorization"),
        }
        sso_resp = requests.post(
            f"{current_app.config['SSO_BASE_URL']}/api/v1/users/add",
            data=json.dumps(user_doc),
            headers=headers,
        )
        if sso_resp.ok:
            move_students([student])
            response_data["STATUS"] = "SUCCESS"
            response_data["MESSAGE"] = f"Created student with password {password}"
            response_data["DATA"] = student
        else:
            mongo_db.delete(settings_database, "studentInfo", {"_id": student["_id"]})
            response_data["STATUS"] = "FAILURE"
            status_code = 500
        return response_data, status_code, extra_headers


def move_students(students):
    collection_name = "hierarchy-ledger"
    ids = [x["egnifyId"] for x in students]
    query = {"egnifyId": {"$in": ids}, "isCurrentHierarchy": True}
    update = {
        "$set": {"isCurrentHierarchy": False, "endTime": datetime.now(timezone.utc)}
    }
    coll = mongo_db.connection[settings_database].get_collection(collection_name)
    coll.update_many(query, update)
    cache_keys, new_docs = [], []
    for student in students:
        doc = {
            "_id": str(ObjectId()),
            "startTime": datetime.now(timezone.utc),
            "hierarchy": student["accessTag"]["hierarchy"],
            "isCurrentHierarchy": True,
            "egnifyId": student["egnifyId"],
            "studentId": student["studentId"],
        }
        new_docs.append(doc)
        cache_keys.append(f"view//students/{student['egnifyId']}")
        cache_keys.append(student["egnifyId"])
        cache_keys.append(f"user_token_{student['egnifyId']}")
    cache.delete_many(*cache_keys)
    mongo_db.insert_many(settings_database, collection_name, new_docs)


@blueprint.route("/<string:student_id>", methods=["PUT"])
@decorators.roles_required(["SETTINGS_CREATOR"])
@decorators.authorization_on_student(cached_mongo_db=get_cached_mongo_db)
@decorators.make_egnify_response
def update_student_info(student_id):
    response_data, status_code, extra_headers = {}, 200, {}
    post_data = request.json
    query = {"_id": student_id}
    student = [
        {
            "egnifyId": student_id,
            "hierarchy": [{"childCode": post_data["accessTag"]["hierarchy"]}],
        }
    ]
    old_doc = mongo_db.find_one_and_upsert(
        settings_database, "studentInfo", query, post_data, upsert=False, new=False
    )
    if old_doc["accessTag"]["hierarchy"] != post_data["accessTag"]["hierarchy"]:
        headers = {
            "Content-type": "application/json",
            "Accept": "text/plain",
            "accesscontroltoken": request.headers.get("accesscontroltoken"),
            "Authorization": request.headers.get("Authorization"),
        }
        sso_resp = requests.put(
            f"{current_app.config['SSO_BASE_URL']}/api/v1/users/students/{student_id}",
            data=json.dumps(student),
            headers=headers,
        )
        if sso_resp.ok:
            move_students([post_data])
            response_data["STATUS"] = "SUCCESS"
            response_data["MESSAGE"] = "Updated user successfully"
        else:
            response_data["STATUS"] = "FAILURE"
            status_code = 500
    return response_data, status_code, extra_headers


@blueprint.route("/<string:institute_id>/bulkupdate", methods=["POST"])
@decorators.roles_required(["SETTINGS_CREATOR"])
@decorators.authorization_required()
@decorators.make_egnify_response
def bulk_update_students(institute_id):
    response_data, status_code, extra_headers = {}, 200, {}
    file_url = request.values.get("fileUrl")
    if not file_url:
        response_data["STATUS"] = "FAILURE"
        response_data["MESSAGE"] = "Expected file url for bulk update"
        status_code = 400
        return response_data, status_code, extra_headers
    with requests.Session() as s:
        downloaded_data = s.get(file_url)
        decoded_data = downloaded_data.content.decode("utf-8")
        csv_reader = csv.DictReader(decoded_data.splitlines(), delimiter=",")
        headers = csv_reader.fieldnames
        csv_data = list(csv_reader)

        sbv2 = StudentsBulkReadV2(csv_data, headers, institute_id)
        errors = sbv2.validate()
        if errors:
            raise MultipleValidationExceptions(errors)

        sheet_students = sbv2.read_students()
        students, student_user_docs, lookup = [], [], {}
        for row, student in enumerate(sheet_students, 1):
            # Get the corresponding hierarchy node
            query = {
                "viewId": student["accessTag"]["hierarchy"],
                "instituteId": institute_id,
            }
            # Check in lookup table, add if not found.
            if query["viewId"] not in lookup:
                node = list(arango_db.v_find(query))
                lookup[query["viewId"]] = node
            hierarchy_node = lookup[query["viewId"]]
            # Invalid hierarchy node
            if not hierarchy_node:
                errors.append(ValidationException("E403", row, None))
            else:
                student["accessTag"]["hierarchy"] = hierarchy_node[0]["id"]
                students.append(student)
                student_user_docs.append(
                    {
                        "_id": student["_id"],
                        "egnifyId": student["egnifyId"],
                        "hierarchy.childCode": hierarchy_node[0]["id"],
                    }
                )
        if errors:
            raise MultipleValidationExceptions(errors)
        mongo_db.upsert_many_in_process(settings_database, "studentInfo", students)
        mongo_db.wait_for_termination()

        headers = {
            "Content-type": "application/json",
            "Accept": "text/plain",
            "accesscontroltoken": request.headers.get("accesscontroltoken"),
            "Authorization": request.headers.get("Authorization"),
        }
        requests.post(
            f"{current_app.config['SSO_BASE_URL']}/api/v1/users/{institute_id}/bulkupdate",
            data=json.dumps(student_user_docs),
            headers=headers,
        )

        move_students(student_user_docs)
        response_data = {"STATUS": "SUCCESS", "DATA": None}
    return response_data, status_code, extra_headers


@blueprint.route("/<string:student_id>")
@decorators.roles_required(
    [
        "STUDENT",
        "REPORTS_VIEWER",
        "ANALYSIS_VIEWER",
        "SETTINGS_CREATOR",
        "SETTINGS_READER",
    ]
)
@decorators.authorization_on_student(cached_mongo_db=get_student_cached_mongo_db)
@cache.cached(timeout=86400)
@decorators.make_egnify_data_response
def get_student_info(student_id):
    """
        Return student information.
    """
    start_time = time.time()
    collection_name = "studentInfo"
    project_list = ["studentName", "accessTag", "studentId", "egnifyId"]
    kwargs = {
        "obj": [student_id],
        "projection": {field: 1 for field in project_list},
    }
    student = mongo_db.read_by_id(settings_database, collection_name, **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return student[student_id], 200, {"Cache-Control": "private, max-age=3600"}


@blueprint.route("/<string:student_id>", methods=["DELETE"])
@decorators.roles_required(["SETTINGS_READER", "SETTINGS_CREATOR"])
@decorators.make_egnify_response
def delete_student(student_id):
    response_data, status_code, extra_headers = {}, 200, {}
    update = {"active": False}
    mongo_db.find_one_and_upsert(
        settings_database, "studentInfo", {"egnifyId": student_id}, update, upsert=False
    )
    response_data["STATUS"] = "SUCCESS"
    return response_data, status_code, extra_headers


@blueprint.route("/<string:student_id>/tests")
@decorators.roles_required(["STUDENT", "REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_on_student(cached_mongo_db=get_cached_mongo_db)
@decorators.make_egnify_data_response
def get_student_tests(student_id):
    """
        Return list test ids for student
    """
    start_time = time.time()

    test_category_type = request.values.get("testCategoryType")
    syllabus_type = request.values.get("syllabusType")

    collection_name = current_app.config["MASTER_RESULTS_TABLE"]
    project_list = ["studentId", "egnifyId", "studentName", "testId"]
    aggregation = Aggregation()
    query = {"egnifyId": student_id}
    lookup_query = {
        "from": "tests",
        "localField": "testId",
        "foreignField": "testId",
        "as": "test",
    }
    match_query = {"test.testFormatType": "ADMIN_TEST_TYPE"}
    if test_category_type:
        match_query["test.testCategoryType"] = test_category_type
        match_query["test.syllabusType"] = syllabus_type
    else:
        match_query["test.couchSync.status"] = "completed"


    projection = {field: 1 for field in project_list}
    aggregation.match(query).lookup(lookup_query).unwind("$test").match(
        match_query
    ).project(projection)

    # Run the pipeline
    student_tests = list(
        mongo_db.connection[ga_database]
        .get_collection(collection_name)
        .aggregate(aggregation.pipeline, allowDiskUse=True)
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    custom_headers = {"Cache-Control": "private, max-age=600"}
    return student_tests, 200, custom_headers


@blueprint.route("/<string:student_id>/practice/tests")
@decorators.roles_required(["STUDENT"])
@decorators.authorization_on_student(cached_mongo_db=get_cached_mongo_db)
@decorators.make_egnify_data_response
def get_student_practice_tests(student_id):
    """
        Return list test ids for student
    """
    skip = request.values.get("skip", 0, type=int)
    size = request.values.get("size", 20, type=int)
    status = request.values.get("status", None)
    testType = request.values.get("testType", None)
    practiceType = request.values.get("practiceType", None)
    klass = request.values.get("class", None)
    start_time = time.time()
    testFormatType = request.values.get("testFormatType", "PRACTICE_TEST_TYPE")
    query = {
        "active": True,
        "egnifyId": student_id,
        "testFormatType": testFormatType,
    }
    if status:
        query["status"] = status
    if testType:
        query["testType"] = testType
    if practiceType:
        query["practiceType"] = practiceType
    if klass:
        query["class"] = klass

    project_list = ["egnifyId", "testId"]
    kwargs = {
        "query": query,
        "projection": {field: 1 for field in project_list},
        "key": "testId",
        "sort_key": "startTime",
        "skip": skip,
        "size": size,
        "ascending": False,
    }
    tests = mongo_db.get_results(ga_database, "tests", **kwargs)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from tests: {db_duration}"
    )
    return tests, 200, {}


@blueprint.route("/<string:student_id>/practice/count")
@decorators.roles_required(["STUDENT"])
@decorators.make_egnify_response
def get_tests_count(student_id):
    """"
        Return count of test items.
    """
    status = request.values.get("status", None)
    testType = request.values.get("testType", None)
    practiceType = request.values.get("practiceType", None)
    klass = request.values.get("class", None)
    start_time = time.time()
    testFormatType = request.values.get("testFormatType", "PRACTICE_TEST_TYPE")
    query = {
        "active": True,
        "egnifyId": student_id,
        "testFormatType": testFormatType,
    }
    if status:
        query["status"] = status
    if testType:
        query["testType"] = testType
    if practiceType:
        query["practiceType"] = practiceType
    if klass:
        query["class"] = klass
    collection_name = "tests"
    tests_count = mongo_db.count(ga_database, collection_name, query)
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return {"count": tests_count}, 200, {}


@blueprint.route("/<string:hierarchy_id>/<string:student_id>/tests/all")
@decorators.roles_required(["STUDENT", "REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_required(cached_mongo_db=get_cached_mongo_db)
@decorators.make_egnify_response
def get_student_all_tests(hierarchy_id, student_id):
    """
        Return list test ids for student
    """
    start_time = time.time()
    collection_name = "rankAnalysis"
    tests = request.values.get("tests", None)
    _id_values = []
    if tests:
        for test in tests.split(","):
            _id_values.append("_".join([hierarchy_id, test, student_id]))
    else:
        return {}, 400, {}
    tests = mongo_db.read_by_id(
        ga_database, collection_name, _id_values, projection={"hierarchyLevels": 0}
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    custom_headers = {"Cache-Control": "private, max-age=3600"}
    return tests, 200, custom_headers



@blueprint.route("/<string:student_id>/tests/snapshot")
@decorators.roles_required(["STUDENT", "LMS_PERFORMANCE_VIEWER"])
@decorators.make_egnify_response
def get_student_test_snapshot_info(student_id):
    """

    :param student_id:
    :return:
    """
    status = request.values.get("status", "completed")
    textbook = request.values.get("textbook", None)
    start_time = time.time()
    collection_name = "testStudentSnapshot"
    projection_fields = ["_id", "testId", "studentId", "egnifyId", "status"]
    projection = {attr: 1 for attr in projection_fields}
    query = {"status": status}
    if rankguru:
        query["studentId"] = student_id
        query["textbook"] = textbook
    else:
        query["egnifyId"] = student_id

    student = mongo_db.get_results(
        ga_database, collection_name, query, "_id", projection=projection,
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return student, 200, {}


@blueprint.route("/<string:student_id>/test/<string:test_id>")
@decorators.roles_required(
    [
        "STUDENT",
        "REPORTS_VIEWER",
        "ANALYSIS_VIEWER",
        "LMS_PERFORMANCE_VIEWER",
        "CMS_PERFORMANCE_VIEWER",
    ]
)
@decorators.authorization_on_student(cached_mongo_db=get_cached_mongo_db)
@cache.cached(timeout=86400)
@decorators.make_egnify_response
def get_student_test_info(student_id, test_id):
    """
        Returns the student test details for given test.
        Includes CWU Analysis, Response data, Topic Analysis, Student meta data.
    """
    _id = "_".join([test_id, student_id])
    start_time = time.time()
    collection_name = current_app.config["MASTER_RESULTS_TABLE"]
    student = mongo_db.read_by_id(
        ga_database,
        collection_name,
        [_id],
        projection={"hierarchyLevels": 0, "rankOrder": 0},
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    custom_headers = {"Cache-Control": "private, max-age=14400"}
    return student, 200, custom_headers


@blueprint.route("/<string:student_id>/test/<string:test_id>/snapshot")
@decorators.roles_required(["STUDENT", "REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_on_student(cached_mongo_db=get_cached_mongo_db)
@cache.cached(timeout=86400, query_string=True)
@decorators.make_egnify_response
def get_student_test_snapshot(student_id, test_id):
    """
        Returns the student test snapshot data.
    """
    _id = "_".join([test_id, student_id])
    start_time = time.time()
    collection_name = "testStudentSnapshot"
    no_projection = [
        "_header",
        "accessTag",
        "fileUrl",
        "resultUploadSourceFormat",
        "egnifyId",
    ]
    projection = {attr: 0 for attr in no_projection}
    student_data = mongo_db.read_by_id(
        ga_database, collection_name, [_id], projection=projection
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    custom_headers = {"Cache-Control": "private, max-age=14400"}
    return student_data, 200, custom_headers


@blueprint.route("/<string:student_id>/test/<string:test_id>/snapshot/status")
@decorators.roles_required(["STUDENT", "REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.authorization_on_student(cached_mongo_db=get_cached_mongo_db)
@decorators.make_egnify_response
def get_student_test_snapshot_status(student_id, test_id):
    """
        Returns the student test snapshot status data.
    """
    _id = "_".join([test_id, student_id])
    start_time = time.time()
    collection_name = "testStudentSnapshot"
    projection = [
        "mode",
        "status",
        "endTime",
        "egnifyId",
        "accessTag",
        "syncStatus",
        "instituteId",
    ]
    projection = {attr: 1 for attr in projection}
    student_data = mongo_db.read_by_id(
        ga_database, collection_name, [_id], projection=projection
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return student_data, 200, {}


@blueprint.route("/<institute_id>/list", methods=["POST"])
@decorators.roles_required(["SETTINGS_READER", "SETTINGS_CREATOR"])
@decorators.make_egnify_response
def list_students(institute_id):
    """

    :param institute_id:
    :return:
    """
    response_data, status_code, extra_headers = {}, 200, {}
    collection_name = "studentInfo"
    query = {"instituteId": institute_id}

    post_data = request.get_json()

    search_text = post_data.get("searchText", None)
    hierarchy = post_data.get("hierarchy", [])
    skip = post_data.get("skip", 0)
    size = post_data.get("size", 20)
    gender = post_data.get("gender", None)
    category = post_data.get("category", None)
    if search_text:
        query["$or"] = [
            {"studentName": {"$regex": search_text, "$options": "i"}},
            {"studentId": {"$regex": search_text, "$options": "i"}},
        ]
    if gender:
        query["gender"] = gender
    if category:
        query["category"] = category
    query["accessTag.hierarchy"] = {"$in": hierarchy}
    query["active"] = True
    kwargs = {
        "skip": skip,
        "size": size,
        "projection": {"hierarchy": 0, "_id": 0, "active": 0},
    }
    students = mongo_db.find(settings_database, collection_name, query, **kwargs)
    ids = [x["egnifyId"] for x in students]
    users_query = {"_id": {"$in": ids}}
    kwargs = {"key": "_id", "projection": {"active": 1, "_id": 1}}
    user_docs = mongo_db.get_results("tenantregistry", "users", users_query, **kwargs)
    for student in students:
        student.update(user_docs[student["egnifyId"]])

    total = mongo_db.count(settings_database, collection_name, query)
    response_data = {
        "STATUS": "SUCCESS",
        "MESSAGE": "",
        "DATA": {"students": students, "total": total},
    }
    return response_data, status_code, extra_headers


@blueprint.route("/<institute_id>/list/count", methods=["POST"])
@decorators.roles_required(["SETTINGS_READER", "SETTINGS_CREATOR"])
@decorators.make_egnify_response
def list_students_count(institute_id):
    response_data, status_code, extra_headers = {}, 200, {}
    collection_name = "studentInfo"
    query = {"instituteId": institute_id}

    post_data = request.get_json()

    search_text = post_data.get("searchText", None)
    hierarchy = post_data.get("hierarchy", [])
    gender = post_data.get("gender", None)
    category = post_data.get("category", None)
    if search_text:
        query["$or"] = [
            {"studentName": {"$regex": search_text, "$options": "i"}},
            {"studentId": {"$regex": search_text, "$options": "i"}},
        ]
    if gender:
        query["gender"] = gender
    if category:
        query["category"] = category
    query["accessTag.hierarchy"] = {"$in": hierarchy}
    query["active"] = True
    total = list(
        mongo_db.connection[settings_database]
        .get_collection(collection_name)
        .aggregate(
            [
                {"$match": query},
                {"$project": {"accessTag.hierarchy": 1}},
                {"$group": {"_id": "$accessTag.hierarchy", "count": {"$sum": 1}}},
            ]
        )
    )
    response_data = {
        "STATUS": "SUCCESS",
        "MESSAGE": "",
        "DATA": {"total": total},
    }
    return response_data, status_code, extra_headers


@blueprint.route("/<string:institute_id>/bulkcreate", methods=["POST"])
@decorators.roles_required(["SETTINGS_CREATOR"])
@decorators.make_egnify_response
def bulk_create_students(institute_id):
    data = request.json
    return_response = {}
    job = queue.enqueue(
        students_bulk_creation.bulk_create_students, job_timeout="30m", args=(data,)
    )
    return_response["STATUS"] = "SUCCESS"
    return_response["JOB_ID"] = job.get_id()
    return return_response, 200, {}


@blueprint.route("/<institute_id>/archive", methods=["POST"])
@decorators.roles_required(["SETTINGS_READER", "SETTINGS_CREATOR"])
@decorators.make_egnify_response
def bulk_archive_students(institute_id):
    response_data, status_code, extra_headers = {}, 200, {}
    collection_name = "studentInfo"

    post_data = request.get_json()
    student_ids = post_data.get("studentIds", [])
    ops = []
    for student_id in student_ids:
        op = {'_id': student_id, 'active': False}
        ops.append(op)
    mongo_db.upsert_many_in_process(settings_database, collection_name, ops, upsert=False)
    mongo_db.wait_for_termination()
    return response_data, status_code, extra_headers


@blueprint.route("/<student_id>/<any(block, unblock):typ>", methods=["POST"])
@decorators.roles_required(["SETTINGS_READER", "SETTINGS_CREATOR"])
@decorators.make_egnify_response
def toggle_student_status(student_id, typ):
    active_flag = not (typ == "block")
    set_value = {"active": active_flag}
    mongo_db.find_one_and_upsert(
        "tenantregistry", "users", {"_id": student_id}, set_value, upsert=False,
    )
    if not active_flag:
        key = f"user_token_{student_id}"
        if token_string := cache.get(key):
            cache.delete(token_string)
    return {"MESSAGE": "Updated successfully", "DATA": None}, 200, {}


@blueprint.route("/<string:student_id>/test/<string:test_id>/create", methods=["POST"])
@decorators.make_egnify_response
def create_student_test_info(student_id, test_id):
    """
        Returns the list of questions assigned to the student for this parctice test
    """
    post_data = request.get_json()
    marking_schema_id = post_data["markingSchema"]
    topic = post_data.get("topic", None)
    sub_topic = post_data.get("subTopic", None)
    question_paper_id = post_data.get("questionPaperId", f"QP{test_id}")
    print(question_paper_id)
    # difficulty = post_data.get("difficulty", 5)
    # Questions = namedtuple("Questions", ["Easy", "Medium", "Hard"])
    collection_name = "questions"
    marking_schema_cached_mongdb = HydraCacheMongoDb(
        mongo_db, settings_database, "markingschemas"
    )
    marking_schema = marking_schema_cached_mongdb.read_by_id(marking_schema_id)[
        marking_schema_id
    ]
    # For now not considering weightage into consideration but will implement them.
    # In the below array we need to add more conditions based on weightage
    questions = [[1, 1, 1], [1, 2, 3]]
    and_query = []
    if topic:
        and_query.append({"topic": {"$in": topic}})
    if sub_topic:
        and_query.append({"subTopic": {"$in": sub_topic}})

    questions_list = []
    practice_chapters = deep_defaultdict(set)
    for subject in marking_schema["subjects"]:
        for question_type in subject["marks"]:

            # number_of_questions = [
            #     question_type["numberOfQuestions"],
            #     ((5 * difficulty) + question_type["numberOfQuestions"]),
            # ]

            settings_cached_mongo_db = HydraCacheMongoDb(
                mongo_db, settings_database, "subjecttaxonomies"
            )
            subject_object = settings_cached_mongo_db.read_by_id(subject["subject"])
            subject_name = subject_object[subject["subject"]]["name"]
            # easy_maximum_questions = get_count_of_questions(
            #     query_list=and_query,
            #     difficulty="Easy",
            #     subject=subject_name,
            #     question_type=question_type["egnifyQuestionType"],
            # )
            # medium_maximum_questions = get_count_of_questions(
            #     query_list=and_query,
            #     difficulty="Medium",
            #     subject=subject_name,
            #     question_type=question_type["egnifyQuestionType"],
            # )
            # hard_maximum_questions = get_count_of_questions(
            #     query_list=and_query,
            #     difficulty="Hard",
            #     subject=subject_name,
            #     question_type=question_type["egnifyQuestionType"],
            # )
            # nnls_response = lsq_linear(
            #     questions,
            #     number_of_questions,
            #     (
            #         [0.1, 0.1, 0.1],
            #         [
            #             easy_maximum_questions,
            #             medium_maximum_questions,
            #             hard_maximum_questions,
            #         ],
            #     ),
            # )["x"]
            # nnls_response_list = nnls_response.tolist()
            # nnls_response_list = saferound(nnls_response_list, places=0)
            # questions_to_select = Questions(*nnls_response_list)
            # current_app.logger.info(
            #     f"Approximated Easy questions : "
            #     f"{questions_to_select.Easy}, Medium : {questions_to_select.Medium} "
            #     f", Hard: {questions_to_select.Hard}"
            # )
            total_questions = get_questions_from_mongo(
                and_query,
                None,
                int(question_type["numberOfQuestions"]),
                subject_name,
                question_type["egnifyQuestionType"],
            )
            # medium_questions = get_questions_from_mongo(
            #     and_query,
            #     "Medium",
            #     int(questions_to_select.Medium),
            #     subject_name,
            #     question_type["egnifyQuestionType"],
            # )
            # hard_questions = get_questions_from_mongo(
            #     and_query,
            #     "Hard",
            #     int(questions_to_select.Hard),
            #     subject_name,
            #     question_type["egnifyQuestionType"],
            # )
            questions_to_return = list(total_questions)
            # for easy in easy_questions:
            #     questions_to_return.append(easy)
            # for medium in medium_questions:
            #     questions_to_return.append(medium)
            # for hard in hard_questions:
            #     questions_to_return.append(hard)
            random.shuffle(questions_to_return)
            q_list_size = len(questions_to_return)
            end_index = question_type["numberOfQuestions"]
            if q_list_size < end_index:
                end_index = q_list_size
            start_index = question_type["start"]
            for index in range(
                start_index, start_index + end_index
            ):
                q = questions_to_return[index - start_index]
                questions_list.append(
                    {
                        "_id": "_".join([str(index), question_paper_id]),
                        "questionPaperId": question_paper_id,
                        "qno": str(index),
                        "questionNumberId": q["questionNumberId"],
                    }
                )
                sub, topic = q.get("subject"), q.get("topic")
                if sub and topic:
                    practice_chapters[sub].add(topic)
    mongo_db.upsert_many_in_process(
        current_app.config["TEST_DATABASE"], collection_name, questions_list
    )
    syllabus = []
    for sub, chapters in practice_chapters.items():
        item = {
            "subject": sub,
            "topics": list(chapters),
        }
        syllabus.append(item)
    set_value = {"syllabus": syllabus}
    mongo_db.find_one_and_upsert(
        current_app.config["TEST_DATABASE"],
        "tests",
        {"_id": test_id},
        set_value,
        upsert=False,
    )
    mongo_db.wait_for_termination()
    start_time = time.time()
    query = {"questionPaperId": question_paper_id}
    projection = {field: 1 for field in ["qno", "questionNumberId"]}
    collection_name = "questions"
    kwargs = {
        "key": "qno",
        "projection": projection,
    }
    questions = mongo_db.get_results(
        current_app.config["TEST_DATABASE"], collection_name, query, **kwargs
    )
    db_duration = time.time() - start_time
    current_app.logger.info(
        f"Time taken to fetch the results from {collection_name}: {db_duration}"
    )
    return questions, 200, {}

@blueprint.route("/<string:hierarchy_id>/student/<string:student_id>/test/attendance")
@decorators.roles_required(["STUDENT", "REPORTS_VIEWER", "ANALYSIS_VIEWER"])
@decorators.make_egnify_response
def get_student_attendance_for_tests(hierarchy_id,student_id):

    """
        Returns the syncStatus of student testStudentsnapshotData for all tests in given hierarchy
        if syncStauts is true student addtended the test 
    """
    coll = "testStudentSnapshot"
    # To get the tests at parent level iam getting parrent nodes id's
    parent_node = arango_db.get_parent(hierarchy_id,height=10) #if didn't mention the height it will give only 2 nodes by default
    hierarchies = []
    for vertex in parent_node["vertices"]:
        hierarchies.append(vertex["id"])
    hierarchies.append(hierarchy_id)
    # i am getting all the test ids which were completed at parent level 
    query = {
        "accessTag.hierarchy": {"$in":hierarchies},
        "active": True,
        "status":"completed",
        "testFormatType": "ADMIN_TEST_TYPE",
        "endTime":{"$lt": datetime.now(timezone.utc)}
    }        
    collection_name = "tests"
    projection = {field: 1 for field in ["testId","startTime"]}
    kwargs = {
        "sort_key": "startTime",
        "ascending": True,
        "projection": projection,
    }
    tests = mongo_db.get_results(ga_database, collection_name, query, **kwargs)
    test_ids = [test["testId"] for test in tests]
    query = {
        "testId":{"$in":test_ids},
        "studentId":student_id
    }
    kwargs = {
        "projection": {attr: 1 for attr in ["studentId", "egnifyId", "studentName", "syncStatus","testName","testId"]},
    }
    data = mongo_db.find(ga_database,coll,query,**kwargs)
    return_response = {
        "STATUS": "SUCCESS",
        "DATA": data   
    }
    return return_response, 200, {}

def get_questions_from_mongo(
    query_list, difficulty, number_of_questions, subject, question_type
):
    """

    :param query_list:
    :param difficulty:
    :param number_of_questions:
    :param subject:
    :param question_type:
    :return:
    """
    duplicate_query_list = query_list.copy()
    duplicate_query_list.append({"parsed": {"$ne": True}})
    duplicate_query_list.append({"hide": {"$ne": True}})
    # duplicate_query_list.append({"difficulty": difficulty})
    duplicate_query_list.append({"subject": subject})
    duplicate_query_list.append({"questionType": question_type})
    questions = (
        mongo_db.connection[current_app.config["QUESTION_BANK_DB"]]
        .get_collection("questions-list")
        .aggregate(
            [
                {"$match": {"$and": duplicate_query_list}},
                {"$sample": {"size": number_of_questions}},
                {"$project": {"questionNumberId": 1, "subject": 1, "topic": 1}},
            ]
        )
    )
    return questions


def get_count_of_questions(query_list, difficulty, subject, question_type):
    """

    :param query_list:
    :param difficulty:
    :param subject:
    :param question_type:
    """
    duplicate_query_list = query_list.copy()
    duplicate_query_list.append({"parsed": {"$ne": True}})
    duplicate_query_list.append({"hide": {"$ne": True}})
    # duplicate_query_list.append({"difficulty": difficulty})
    duplicate_query_list.append({"subject": subject})
    duplicate_query_list.append({"questionType": question_type})
    count = mongo_db.count(
        current_app.config["QUESTION_BANK_DB"],
        "questions-list",
        {"$and": duplicate_query_list},
    )
    return count
