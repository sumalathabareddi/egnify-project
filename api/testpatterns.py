from flask import Blueprint, current_app, request
from utils.decorators import (
    make_egnify_response,
    make_egnify_data_response,
    roles_required,
    authorization_required,
)
from utils.egy_analytics import cache

from .models import TestPatternSchema
from .models.subjecttaxonomies import SubjectTaxonomySchema

blueprint = Blueprint("testpatterns", __name__)

mongo_db = settings_database = None


@blueprint.before_app_first_request
def init_bp():
    """

    """
    global mongo_db, settings_database
    mongo_db = current_app.mongo_db
    settings_database = current_app.config["SETTINGS_DATABASE"]


@blueprint.route("/testpatterns", methods=["POST"])
@roles_required(["PARTNER_ADMIN"])
@make_egnify_response
def insert_test_patterns():
    """Add test patterns.
    ---
    post:
      description: Add test patterns
      responses:
        201:
          description: Return add test patterns status
          content:
            application/json:
              schema: TestPatternSchema
        400:
          description: Return validation errors
          content:
            application/json:
              schema: TestPatternSchema
    """
    response_data, status_code, extra_headers = {}, 201, {}
    collection_name = "testpatterns"
    if request.is_json:
        post_data = request.get_json()
        testpatterns = post_data.get("testpatterns", [])
        result = TestPatternSchema(many=True).load(testpatterns)
        if result.errors:
            response_data["STATUS"] = "FAILURE"
            response_data["DATA"] = result.errors
            status_code = 400
        else:
            mongo_db.insert_many(settings_database, collection_name, result.data)
            response_data["STATUS"] = "SUCCESS"
            response_data["MESSAGE"] = "Created testpatterns successfully"
    else:
        response_data["STATUS"] = "FAILURE"
        response_data["MESSAGE"] = "Invalid mimetype, expected application/json"
        status_code = 400

    return response_data, status_code, extra_headers


@blueprint.route("/<string:institute_id>/testpatterns/<string:orientation_id>")
@roles_required(
    [
        "SETTINGS_READER",
        "SETTINGS_CREATOR",
        "TEST_VIEWER",
        "TEST_CREATOR",
        "ANALYSIS_VIEWER",
        "REPORTS_VIEWER",
        "STUDENT",
    ]
)
@authorization_required()
@cache.cached(timeout=604800)
@make_egnify_data_response
def get_testpatterns(institute_id, orientation_id):
    """GET testpatterns for orientation.
    ---
    get:
      description: Get list of test patterns for orientation
      responses:
        200:
          description: Return test patterns list
          content:
            application/json:
              schema: TestPatternSchema
    """
    collection_name = "testpatterns"
    query = {"orientationId": orientation_id, "active": True}
    testpatterns = mongo_db.find(settings_database, collection_name, query)
    for pattern in testpatterns:
        pattern[
            "schemasUrl"
        ] = f"/{institute_id}/markingschemas/{orientation_id}/pattern/{pattern['_id']}"

    return testpatterns, 200, {"Cache-Control": "private, max-age=14400"}


@blueprint.route("/subjects", methods=["POST"])
@make_egnify_response
def add_subjects():
    """Add subjects.
    ---
    post:
      description: Add subjects
      responses:
        201:
          description: Return add subjects status
          content:
            application/json:
              schema: SubjectTaxonomySchema
        400:
          description: Return validation errors
          content:
            application/json:
              schema: SubjectTaxonomySchema
    """
    response_data, status_code, extra_headers = {}, 201, {}
    collection_name = "subjecttaxonomies"
    post_data = request.get_json()
    subjecttaxonomies = post_data.get("subjecttaxonomies", [])
    result = SubjectTaxonomySchema(many=True).load(subjecttaxonomies)
    if result.errors:
        response_data["STATUS"] = "FAILURE"
        response_data["DATA"] = result.errors
        status_code = 400
    else:
        mongo_db.insert_many(settings_database, collection_name, result.data)
        response_data["STATUS"] = "SUCCESS"
        response_data["MESSAGE"] = "Created subjecttaxonomies successfully"

    return response_data, status_code, extra_headers


@blueprint.route("/subjects")
# @cache.cached(timeout=604800)
@make_egnify_data_response
def get_subjects():
    """Get subjects.
    ---
    get:
      description: Get list of subjects
      responses:
        200:
          description: Return list of subjects
          content:
            application/json:
              schema: SubjectTaxonomySchema
    """
    collection_name = "subjecttaxonomies"
    query = {}
    subjecttaxonomies = mongo_db.find(settings_database, collection_name, query)
    return subjecttaxonomies, 200, {"Cache-Control": "private, max-age=14400"}
