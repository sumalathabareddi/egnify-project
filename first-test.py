
from collections import OrderedDict
import base64
import csv

from api.models import InstituteSchema, InstituteHierarchiesSchema

from db.HydraMongoDb import HydraMongoDb
import time
from pytz import timezone as tz
from datetime import timezone
from utils.TimeHelper import TimeHelper



from app import create_app

application = create_app()
mongo_db =  HydraMongoDb(application.config['MONGO_URI'])
settings_database = application.config["SETTINGS_DATABASE"]
tenant_registry = application.config["TENANT_REGISTRY_DATABASE"]
test_database = application.config["TEST_DATABASE"]
data = []
def get_test():
    institutes = mongo_db.find(settings_database,"institutes",{})
    for i in institutes:
        print(f"{i['_id']}::: {i['instituteName']}")
        test_ids = mongo_db.distinct_field(test_database,"testStudentSnapshot","testId",{"instituteId":i["_id"]})
        # print(test_ids)
        # test_ids = [x["testId"] for x in test_ids]
        print(len(test_ids))
        collection_name = "tests"
        projection = {field: 1 for field in ["testName", "testId", "startTime", "endTime"]}
        kwargs = {
            # "key": "testId",
            "sort_key": "startTime",
            "ascending": True,
            "projection": projection,
        }
        tests = mongo_db.get_results(test_database, collection_name, {"_id":{"$in":test_ids},"status":{"$ne":"draft"},"active":True,"testFormatType":"ADMIN_TEST_TYPE"}, **kwargs)
        print(f'{i["_id"]}:::{len(tests)}')
        if tests:
            test = tests[0]
            doc = {
                "instituteName": i["instituteName"],
                "testId": test['_id'],
                "testName": test['testName'],
                "startTime": test['startTime'].replace(tzinfo=timezone.utc).astimezone(tz=tz('Asia/Kolkata')).strftime("%d/%m/%Y %I:%M%p"),
                "endTime": test['endTime'].replace(tzinfo=timezone.utc).astimezone(tz=tz('Asia/Kolkata')).strftime("%d/%m/%Y %I:%M%p")
            }
            data.append(doc)
    with open('./instittue_test_data.csv', mode='w') as csv_file:
            fieldnames = ['instituteName','testId','testName','startTime', 'endTime']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(data)

get_test()