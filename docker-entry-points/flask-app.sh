#!/bin/sh
gunicorn --log-level=info --timeout=600 --workers=1 --threads=50 --bind 0.0.0.0:$PORT main
