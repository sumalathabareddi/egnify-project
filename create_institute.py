
from collections import OrderedDict
import base64

from api.models import InstituteSchema, InstituteHierarchiesSchema

from db.HydraArangoDb import HydraArangoDb
from db.HydraMongoDb import HydraMongoDb
from utils import md5
from bson.objectid import ObjectId
from os import urandom
from hashlib import pbkdf2_hmac
import time



from app import create_app

application = create_app()
mongo_db =  HydraMongoDb(application.config['MONGO_URI'])
arango_db = HydraArangoDb(
    application.config["ARANGODB_URI"],
    application.config["ARANGODB_USERNAME"],
    application.config["ARANGODB_PASSWORD"],
)
settings_database = application.config["SETTINGS_DATABASE"]
tenant_registry = application.config["TENANT_REGISTRY_DATABASE"]
hostname = application.config["HOST_NAME"]

def get_time():
      return int(time.time() * 1000)


institue = {
    "instituteName": "Urbane College",
    "academicSchedule": {
        "end": {
            "month": "6"
        },
        "start": {
            "month": "5"
        }
    },
    "url" : "https://getranks.in",
    "subdomain" : "getranks.in",
    "hierarchy": [
      {
        "level": 1,
        "parent": "null",
        "name": "Orientation",
        "code": "1612974687403"
      },
      {
        "level": 2,
        "parent": "1612974687403",
        "name": "Class",
        "code": "1612974708383"
      },
      {
        "level": 3,
        "parent": "1612974708383",
        "name": "State",
        "code": "1612974724979"
      },
      {
        "level": 4,
        "parent": "1612974724979",
        "name": "City",
        "code": "1612974754385"
      },
      {
        "level": 5,
        "parent": "1612974754385",
        "name": "Campus",
        "code": "1612974774987"
      },
      {
        "level": 6,
        "parent": "1612974774987",
        "name": "Section",
        "code": "1612974795112"
      }
    ],
    "systemHierarchies":[
      {
        "_id":"6014126ede011288a812802f",
        "name":"Class 8",
        "level":2
      },
      {
        "_id":"5eca99cc378a08e9212330d9",
        "name":"Class 9",
       "level":2
      },
      {
        "_id":"5eca99cf378a08e9212330da",
        "name":"Class 10",
       "level":2
      },
      {
        "level": 2,
        "name": "Class 11",
        "_id": "5e15800af83f08a5a5711e4a"
      },
      {
        "level": 2,
        "name": "Class 12",
        "_id": "5e15800ff83f08a5a5711e4b"
      },
      {
        "level": 2,
        "name": "Class 13",
        "_id": "5e158015f83f08a5a5711e4c"
      },
      {
        "level": 1,
        "name": "Engineering",
        "_id": "5e157ffef83f08a5a5711e48"
      },
      {
        "level": 1,
        "name": "Medical",
        "_id": "5e158004f83f08a5a5711e49"
      },
      {
        "level":1,
        "name":"School",
        "_id":"60141371de011288a8128030"
      }
    ],
    "email":"urbanecollegehyd@gmail.com",
    "adminName":"Priyatham",
    "adminPhone":"96404 03484",
    "location":"Hyderabad"
}

institue["hierarchy"][0]["code"] = str(get_time())
institue["hierarchy"][1]["code"] = str(get_time()+1)
institue["hierarchy"][1]["parent"] = institue["hierarchy"][0]["code"]
institue["hierarchy"][2]["code"] = str(get_time()+2)
institue["hierarchy"][2]["parent"] =institue["hierarchy"][1]["code"]
institue["hierarchy"][3]["code"] = str(get_time()+3)
institue["hierarchy"][3]["parent"] = institue["hierarchy"][2]["code"]
institue["hierarchy"][4]["code"] = str(get_time()+4)
institue["hierarchy"][4]["parent"] = institue["hierarchy"][3]["code"]
institue["hierarchy"][5]["code"]= str(get_time()+5)
institue["hierarchy"][5]["parent"] = institue["hierarchy"][4]["code"]



def make_hierarchies(institute_doc):
    """Orientation hierarchy nodes for institute"""
    _id = md5(institute_doc["_id"])
    orientations = [
        x["name"] for x in institute_doc["systemHierarchies"] if x["level"] == 1
    ]
    level_code = None
    for item in institute_doc["hierarchy"]:
        if item["name"] == "Orientation":
            level_code = item["code"]
    hierarchy = []
    for orientation in orientations:
        doc = {
            "isLeafNode": False,
            "instituteId": institute_doc["_id"],
            "instituteLevelCode": level_code,
            "hierarchyName": orientation,
            "_id": md5(institute_doc["_id"] + orientation),
            "parentCode": _id,
            "level": 1,
            "hierarchyTuple": [institute_doc["instituteName"], orientation],
        }
        if not arango_db.has_node(doc["_id"]):
            arango_db.create_node(doc["_id"], doc)
        arango_db.create_edge(doc["parentCode"], doc["_id"])
        child_code = {"childCode": doc["_id"], "levelCode": level_code, "level": 1}
        hierarchy.append(child_code)
    return hierarchy
def create_institue(data):
    response_data  = {}
    collection_name = "institutes"
    institue['email'] = data['email']
    institue['adminName'] = data['adminName']
    institue['adminPhone'] = data['adminPhone']
    institue['instituteName'] = data['instituteName']
    institue['logoUrl'] = data['logoUrl']
    institute_data = institue
    result = InstituteSchema().load(institute_data)
    if result.errors:
        response_data["STATUS"] = "FAILURE"
        response_data["DATA"] = result.errors
        print(f'{response_data}')
        status_code = 400
    else:
        sanitized_data = OrderedDict(result.data)
        mongo_db.insert_one(settings_database, collection_name, sanitized_data)
        response_data["STATUS"] = "SUCCESS"
        response_data["DATA"] = sanitized_data
        # Add institute to graph
        _id = md5(sanitized_data["_id"])
        initial_institute_hierarchy = {
            "instituteId": sanitized_data["_id"],
            "instituteLevelCode": "",
            "hierarchyName": sanitized_data["instituteName"],
            "_id": _id,
            "parentCode": "root",
            "level": 0,
        }
        result = InstituteHierarchiesSchema().load(initial_institute_hierarchy)
        init_h = result.data
        if not arango_db.has_node(init_h["_id"]):
            arango_db.create_node(init_h["_id"], init_h)
        arango_db.create_edge(init_h["parentCode"], init_h["_id"])
        user_hierarchy = make_hierarchies(sanitized_data)
        print(f'{user_hierarchy}')
        response_data["hierarchy"] = user_hierarchy
        salt = urandom(16)
        base64_salt = base64.b64encode(salt)
        password = "egnify"
        byte_password = password.encode()
        pass_hash = pbkdf2_hmac("sha1", byte_password, salt, 10000, 64)
        base64_pass = base64.b64encode(pass_hash)
        doc = {
            "_id": str(ObjectId()),
            "role": ["SETTINGS_CREATOR","TEST_CREATOR","REPORTS_VIEWER","ANALYSIS_VIEWER","TEACHER","PAPER_CREATOR","PAPER_EDITOR"],
            "forgotPassSecureHash": "",
            "passwordChange": True,
            "active": True,
            "hierarchy": user_hierarchy,
            "email": institute_data["email"],
            "password": base64_pass.decode(),
            "hostname": hostname,
            "instituteId": sanitized_data["_id"],
            "username": institute_data["email"].split('@')[0].upper(),
            "salt": base64_salt.decode(),
            "forgotPassSecureHashExp":"",
        }
        mongo_db.insert_one(tenant_registry, "users", doc)
        print(f'_id:::{sanitized_data["_id"]}')

data = {
    'instituteName':'KLN Junior College',
    'location':'Miryalaguda',
    'email':'klnjr1121@gmail.com',
    'adminName':'KIRAN KUMAR BIKUMALLA',
    'adminPhone':'9866739047',
    'logoUrl':''
}        

create_institue(data)
