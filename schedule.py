from flask import Blueprint, current_app, request
import importlib
from datetime import datetime, timezone
from rq_scheduler import Scheduler
import redis
from datetime import datetime
import utils.decorators as decorators
from rq import Queue
from app import create_app

application = create_app()

queue = None

redis_connection = redis.Redis.from_url(application.config["RQ_REDIS_URL"])
scheduler = Scheduler(application.config["RQ_QUEUES"], connection=redis_connection)
queue = Queue(application.config["RQ_QUEUES"], connection=redis_connection)

   




def schedule_task(data):
    dt = data.get("dt", datetime.now(timezone.utc))
    module = importlib.import_module(data["module"])
    method = data["method"]
    depends_on = data.get("depends_on", None)
    task = scheduler.enqueue_at(
        dt,
        getattr(module, method),
        data["args"],
        depends_on=depends_on
    )
    return task

def cancel_a_scheduled_job(job_id):
    scheduler.cancel(job_id)


date = datetime.now(timezone.utc)
data = {
    "module": "generate_analysis",
    "method": "hello_world",
    "dt": date,
    "args": {"subject":'subject',"body": 'body'},
}
print(date)
schedule_task(data)