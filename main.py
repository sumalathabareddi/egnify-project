import logging
import os
import sys
import time

from auth.Authentication import Authentication
from flask import request
from utils import EgnifyException
from utils.decorators import make_egnify_response
from utils.error_handler import response_data

from app import create_app

application = create_app()


@application.before_request
def validate_request():
    """Authenticates request before forwarding it to api routes.
    Makes a call to SSO server to authenticate the request,
    raises NotAuthorizedException if authentication fails.
    """
    endpoint = request.endpoint
    exclude_list = [
        "check_my_health",
        "swagger_ui.show",
        "static",
        "rq_dashboard.overview",
        "rq_dashboard.static",
        "rq_dashboard.list_queues",
        "rq_dashboard.list_workers",
        "rq_dashboard.list_jobs",
        "rq_dashboard.list_instances",
        "rq_scheduler_dashboard.overview",
        "rq_scheduler_dashboard.static",
        "rq_scheduler_dashboard.scheduler_status",
        "rq_scheduler_dashboard.list_jobs",
        "rq_scheduler_dashboard.list_queues",
    ]  # rq_dashboard and rq_scheduler_dashboard shouldn't be in this list.
    if request.method != "OPTIONS" and endpoint not in exclude_list:
        sso_url = application.config["SSO_URL"]
        if not sso_url:
            application.logger.critical("SSO_URL is not set")
            sys.exit(1)
        start_time = time.time()
        access_control_token = request.headers.get("accesscontroltoken")
        authorization = request.headers.get("Authorization")
        if not access_control_token or not authorization:
            raise EgnifyException.NotAuthorizedException
        auth_resp = Authentication(sso_url).authenticate(
            access_control_token, authorization
        )
        sso_time = time.time() - start_time
        application.logger.info(
            f"Time taken to get response from SSO server: {sso_time}"
        )
        application.logger.info(
            f"Authentication status from SSO server: {auth_resp}"
        )
        if not auth_resp.is_authenticated:
            raise EgnifyException.NotAuthorizedException


@application.errorhandler(EgnifyException.NotFoundException)
@make_egnify_response
def handle_404_errors(exception):
    """

    :param exception:
    :return:
    """
    return response_data["404"], 404, {}


@application.errorhandler(EgnifyException.NotAuthorizedException)
@make_egnify_response
def handle_401_errors(exception):
    """

    :param exception:
    :return:
    """
    return response_data["401"], 401, {}


@application.errorhandler(EgnifyException.NoContentException)
@make_egnify_response
def handle_201_errors(exception):
    """

    :param exception:
    :return:
    """
    return response_data["201"], 201, {}


@application.errorhandler(EgnifyException.NoContentDataException)
@make_egnify_response
def handle_204_data_errors(exception):
    """

    :param exception:
    :return:
    """
    return response_data["204"], 204, {}


@application.errorhandler(EgnifyException.ServerException)
@make_egnify_response
def handle_500_errors(exception):
    """

    :param exception:
    :return:
    """
    return response_data["500"], 500, {}


@application.errorhandler(EgnifyException.MultipleValidationExceptions)
@make_egnify_response
def handle_validation_errors(exception):
    """

    :param exception:
    :return:
    """
    return exception.value, 400, {}


@application.errorhandler(EgnifyException.NoAccessException)
@make_egnify_response
def handle_403_validation_errors(exception):
    """

    :param exception:
    :return:
    """
    return response_data["403"], 403, {}


@application.route("/health")
@make_egnify_response
def check_my_health():
    """

    :return:
    """
    message = "App is running. Thanks for checking"
    return message, 200, {}


if __name__ == "__main__":
    port = os.environ["PORT"]
    application.run("0.0.0.0", port)
    application.logger.setLevel(2)
else:
    gunicorn_logger = logging.getLogger("gunicorn.error")
    application.logger.handlers = gunicorn_logger.handlers
    application.logger.setLevel(gunicorn_logger.level)
