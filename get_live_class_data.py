import base64
import csv
from datetime import datetime, timedelta
from hashlib import pbkdf2_hmac
from os import urandom
from random import randint
from flask import helpers
from pytz import timezone as tz
from datetime import timezone
from utils.TimeHelper import TimeHelper

import redis
from db.HydraArangoDb import HydraArangoDb
from db.HydraMongoDb import HydraMongoDb
from rq import Queue




from api.models.students import StudentSchema
from app import create_app
from bulkread.BulkRead import StudentsBulkRead, StudentsBulkReadV2

application = create_app()

mongo_db = HydraMongoDb(application.config['MONGO_URI'])
# mongo_db = HydraMongoDb("mongodb://root:5eqcMZQCPXtG8MvU@localhost:27019/?authenticationDatabase=admin")
arango_db = HydraArangoDb(
    application.config["ARANGODB_URI"],
    application.config["ARANGODB_USERNAME"],
    application.config["ARANGODB_PASSWORD"],
)
settings_database = application.config["SETTINGS_DATABASE"]
tenant_registry = application.config["TENANT_REGISTRY_DATABASE"]
ga_database = application.config["GA_DATABASE"]
hostname = application.config["HOST_NAME"]
# hostname = 'getranks.in'
redis_connection = redis.Redis.from_url(application.config["RQ_REDIS_URL"])
queue = Queue(application.config["RQ_QUEUES"], connection=redis_connection)
print(hostname)
docs = []
ids = set()
tests = mongo_db.find(ga_database,"tests",{"active":True})
print(len(tests))
for test in tests:
    duration = test.get("duration", 0)
    grace = test.get("gracePeriod", 0)
    if grace:
        date = test["startTime"] + timedelta(minutes=duration + grace)
    else:
        date = test["endTime"] + timedelta(minutes=duration)
    docs.append({"_id":test["_id"],"gaTime":date})
mongo_db.upsert_many_in_process(ga_database, "tests", docs)
mongo_db.wait_for_termination()
# for _id in ids:
#     if not arango_db.has_node(_id):
#         docs.append({"id":_id})
#         doc = mongo_db.connection[ga_database].get_collection("tests").update_many(
#             {}, {"$pull":{"hierarchy":_id, "accessTag.hierarchy":_id}},
#         )
#         print(doc)
        


# with open('./hierarchy_live_data.csv', mode='w') as csv_file:
#         fieldnames = ['id']
#         writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
#         writer.writeheader()
#         writer.writerows(docs) 
print(len(docs))

        
        

# today = "2021-07-31 00:00"
# today = TimeHelper.to_utc(
#             datetime.strptime(today, "%Y-%m-%d %H:%M"), "Asia/Kolkata"
#         )
# print(today)
# tomorrow = today + timedelta(days=7)
# print(tomorrow)
# query = {"startTime":{"$gte":today},"endTime": {"$lte": tomorrow }}
# live_class = mongo_db.find(settings_database,"learnschedule",query)
# print(len(live_class))
# for klass in live_class:
#     iId = klass['instituteId']
#     i_data = mongo_db.find_one(settings_database,"institutes",{"_id":iId})
#     _id = klass['_id']
#     students = mongo_db.find(ga_database,"scheduleStudentSnapshot",{"scheduleId":_id})
#     print(len(students))                       
#     for s in students:
#         doc = {
#            "instituteId": iId,
#            "instituteName": i_data['instituteName'],
#            "scheduleId":_id,
#            "type":klass['type'],
#            "scheduleStartTime": klass['startTime'].replace(tzinfo=timezone.utc).astimezone(tz=tz('Asia/Kolkata')).strftime("%d/%m/%Y %I:%M%p"),
#            "scheduleEndTime":klass['endTime'].replace(tzinfo=timezone.utc).astimezone(tz=tz('Asia/Kolkata')).strftime("%d/%m/%Y %I:%M%p"),
#            "studentId": s["studentId"],
#            "studentName": s['studentName'],
#            "email": s['email']
#         }
#         if s.get('joined_at',None):
#             doc["joined_at"]=s['joined_at'].replace(tzinfo=timezone.utc).astimezone(tz=tz('Asia/Kolkata')).strftime("%d/%m/%Y %I:%M%p")
#         else:
#             doc["joined_at"] ="N/A"

#         print(doc['joined_at'])
#         docs.append(doc)
# print(len(docs))
# with open('./students_live_data.csv', mode='w') as csv_file:
#         fieldnames = ['instituteId','instituteName','scheduleId','type','scheduleStartTime','scheduleEndTime','studentId','studentName','email','joined_at']
#         writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
#         writer.writeheader()
#         writer.writerows(docs)            
            
        