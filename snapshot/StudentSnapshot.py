from collections import OrderedDict
from utils.EgnifyException import ValidationException


class StudentSnapshot:
    """

    """

    def __init__(self, header, _index, row, testId, testName, fileUrl, fileType):
        self._id = ""
        self.studentId = ""
        self.instituteId = ""
        self.egnifyId = ""
        self.testId = testId
        self.testName = testName
        self.resultUploadSourceFormat = fileType
        self.fileUrl = fileUrl
        self.responseData = {"questionResponse": {}}
        self._header = header
        self.accessTag = {}
        self.populate_student_response(_index, row)

    def fill_student_details(self, studentFromDb, syncStatus):
        """

        :param studentFromDb:
        :param syncStatus:
        """
        self.studentId = studentFromDb["studentId"]
        self.instituteId = studentFromDb["instituteId"]
        self.egnifyId = studentFromDb["egnifyId"]
        self.accessTag = studentFromDb["accessTag"]
        self.studentName = studentFromDb["studentName"]
        self.syncStatus = syncStatus
        self._id = self.testId + "_" + self.egnifyId

    def populate_student_response(self, _index, row):
        """

        :param _index:
        :param row:
        """
        column_index = 1
        for column_header, data in zip(self._header, row):
            if column_header == "Student Id":
                if not data:
                    raise ValidationException("E301", _index, column_index)
                elif not data.isalnum():
                    raise ValidationException("E401", _index, column_index)
                self.studentId = data
            elif column_header == "Test Id" or column_header == "Mode":
                continue
            else:
                if not data:
                    raise ValidationException("E302", _index, column_index)
                elif data in ("C", "W", "U", "P", "ADD", "P1", "P2", "P3"):
                    self.responseData["questionResponse"][column_header] = data
                else:
                    raise ValidationException("E402", _index, column_index)
            column_index = column_index + 1
