import codecs
import csv
import re
from collections import defaultdict
from urllib.request import urlopen

from utils.EgnifyException import (
    MultipleValidationExceptions,
    ServerException,
    ValidationException,
)

from snapshot.StudentSnapshot import StudentSnapshot


class StudentCSVReader:
    """

    """

    def __init__(self, test_id):
        self.test_id = test_id
        self.students = {}
        self.file_name = ""
        self.file_tag = ""
        self.students_unknown = []
        self.students_as_list = []
        self.students_missing = {}
        self.errors = []
        self.other_testId = None
        self.online_student_list = []

    def read_csv_file(
        self, file_name, testName, totalQuestions, egnify_multi_question_type=False
    ):
        """

        :param file_name:
        :param testName:
        :param totalQuestions:
        :param egnify_multi_question_type:
        """
        response = urlopen(file_name)
        reader = csv.reader(
            codecs.iterdecode(response.read().splitlines(), "utf-8"), delimiter=","
        )
        response_headers = response.info()
        self.file_name, self.file_tag = self.get_file_info(response_headers)
        csv_header = next(reader, None)
        header_index = 0  # this gets incremented for each question
        test_id_index = None  # used for online test
        question_regex = re.compile(r"Q[0-9]+")
        multi_question_regex = re.compile(r"Q[0-9]+[a-d]")
        for each_col in csv_header:
            # chaina online test headers
            if each_col == "Test Id" or each_col == "Mode":
                if each_col == "Test Id":
                    test_id_index = csv_header.index("Test Id")
                continue
            elif not question_regex.match(each_col) and each_col != "Student Id":
                if egnify_multi_question_type:
                    if not multi_question_regex.match(each_col):
                        self.errors.append(ValidationException("E201", 1, header_index))
                    if each_col != ("Q{}d".format(header_index)):
                        # Decrement the index as this matrix type is the same question
                        header_index -= 1
                else:
                    self.errors.append(ValidationException("E201", 1, (header_index)))
            header_index += 1
        if header_index != (totalQuestions + 1):  # +1 here because of Student Id
            self.errors.append(ValidationException("E201", 1, (header_index)))
        no_data_exists = True
        _index = 1
        for row in reader:
            no_data_exists = False
            if test_id_index:
                provided_test_id = row[test_id_index]
                flag = row[-1]
                if flag == "On":
                    self.online_student_list.append(row[0])
                if provided_test_id != "":
                    self.other_testId = provided_test_id

            if len(csv_header) != len(row):
                self.errors.append(ValidationException("E201", _index, 1))
            try:
                student_snapshot = StudentSnapshot(
                    csv_header, _index, row, self.test_id, testName, file_name, "csv"
                )
                self.students[student_snapshot.studentId] = student_snapshot
            except ValidationException as ve:
                self.errors.append(ve)
            _index = _index + 1
        if no_data_exists:
            raise ServerException("Empty file, No data exists to read")
        if self.errors:
            raise MultipleValidationExceptions(self.errors)

    def get_hierarchy_map(self, studentIdsFromDb={}):
        """

        :param studentIdsFromDb:
        :return:
        """
        hierarchy_map = defaultdict(list)
        for studentId in self.students.keys():
            if studentId in studentIdsFromDb:
                hierarchy_id = studentIdsFromDb[studentId]["hierarchy"][0]["childCode"]
                hierarchy_map[hierarchy_id].append(studentId)
            else:
                self.students_unknown.append(studentId)
        return hierarchy_map

    def fill_db_with_csv(self, studentIdsFromDb={}):
        """

        :param studentIdsFromDb:
        """
        for studentId, student in self.students.items():
            if studentId in studentIdsFromDb:
                if not isinstance(student, dict):  # not sure why is this here?
                    studentFromDb = studentIdsFromDb[studentId]
                    student.fill_student_details(studentFromDb, True)
                    self.students_as_list.append(vars(student))
            else:
                self.students_missing[studentId] = student

    def get_file_info(self, response_headers):
        file_name, file_tag = "Untitled", "Untitled"
        if response_headers.get("X-Amz-Meta-Name"):
            file_name = response_headers.get("X-Amz-Meta-Name")
            file_tag = response_headers.get("X-Amz-Meta-Tag")
        elif response_headers.get("x-goog-meta-name"):
            file_name = response_headers.get("x-goog-meta-name")
            file_tag = response_headers.get("x-goog-meta-tag")
        return file_name, file_tag
