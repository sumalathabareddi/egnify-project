from collections import OrderedDict
from utils.EgnifyException import ValidationException


class StudentScheduleSnapshot:
    """

    """

    def __init__(self, scheduleId, scheduleName):
        self._id = ""
        self.studentId = ""
        self.instituteId = ""
        self.egnifyId = ""
        self.email = ""
        self.scheduleId = scheduleId
        self.scheduleName = scheduleName
        self.join_time = ""
        self.leave_time = ""
        self.duration = None
        self.accessTag = {}
        

    def fill_student_details(self, studentFromDb, joined):
        """

        :param studentFromDb:
        :param joined:
        """
        self.studentId = studentFromDb["studentId"]
        self.instituteId = studentFromDb["instituteId"]
        self.egnifyId = studentFromDb["egnifyId"]
        self.accessTag = studentFromDb["accessTag"]
        self.studentName = studentFromDb["studentName"]
        self.email = studentFromDb["email"]
        self.joined = joined
        self._id = self.scheduleId + "_" + self.egnifyId
        print('_id::',self._id)
