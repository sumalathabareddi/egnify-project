
import csv
import requests
import json
from db.HydraMongoDb import HydraMongoDb
from os import urandom
import base64
from hashlib import pbkdf2_hmac

mongo_url = "35.154.37.69"
mongo_db = HydraMongoDb(mongo_url)

admin_login_url = "https://accounts.rankguru.com/auth/local"
student_url = "https://api.rankguru.com/consumer/student/create"

headers = {
            "Content-type": "application/json",
            "Accept": "text/plain",
}
user_doc = {
    "email": 'contentadmin@egnify.com',
    "rememberMe": False,
    "hostname":"cms.rankguru.com",
    "password":'rankgurucmsadmin2021',
    "forceLogin": True
}
res = {}
failure_docs = []
login_resp = requests.post(
            admin_login_url,
            data=json.dumps(user_doc),
            headers=headers, 
            )
if login_resp.ok:
    res = login_resp.json()
    # body  = {"email":"STU113","rememberMe":false,"hostname":"rankguru.com","password":"4143"}
    # res = {"token":"","accessControlToken":""}
    with open('./Dummy New Logins - Dummy New Logins.csv') as csv_file:

        csv_reader = csv.DictReader(csv_file, delimiter=",")
        csv_data = list(csv_reader)
        p_docs = []
        for row in csv_data:
            s_doc = {
                'token':'208b9605-b7f3-4d15-b609-d95eefabb53e',
                'admission_no':row['user ID'],
                'country':'India',
                'class':row['Class'],
                'state':row['State'],
                'city':row['City'],
                'branch':row['Branch'],
                'section':'A',
                'orientation':row['Orientation'],
                'masked_contact':f'xxxxxx{row["user ID"]}',
                'student_name':row['user ID'],
                'digital_content':'yes',
            }
            headers = {
                "Content-type": "application/json",
                "Accept": "text/plain",
                "accesscontroltoken": res.get("accessControlToken"),
                "Authorization": res.get("token"),
            }
            sso_resp = requests.post(
                student_url,
                data=json.dumps(s_doc),
                headers=headers,
            )
            if sso_resp.ok:
                salt = urandom(16)
                base64_salt = base64.b64encode(salt)
                # password = str(randint(10000, 99999))
                password = row['Password']
                # print('password::',password)
                byte_password = password.encode()
                pass_hash = pbkdf2_hmac("sha1", byte_password, salt, 10000, 64)
                base64_pass = base64.b64encode(pass_hash)
                user_docs = []
                doc = {
                    "studentId":row['user ID'],
                    "password": base64_pass.decode(),
                    "salt": base64_salt.decode(),
                    "dummy":True
                }
                # print(doc)
                p_docs.append(doc)
                # user = mongo_db.find_one('tenantregistry-lms-prod','users',{'studentId':row["user ID"]})
                # print(user['studentId'])
                print('success')
                print(sso_resp)
                
            else:
                print(f'student::create {row["user ID"]}')
                failure_docs.append(row)
                print(sso_resp.text)
                print('failure')
        doc = mongo_db._upsert_many(
        connection=None,
        db_name='tenantregistry-lms-prod',
        collection_name="users",
        obj=p_docs,
        id_field="studentId",
        upsert=False,
        )
else:
    print('admin Login') 
    print(login_resp)

with open('./user_students.csv', mode='w') as csv_file:
        fieldnames = ['State','City','Branch','Orientation','Class','user ID','Password']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(failure_docs)