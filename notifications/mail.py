import requests
from app import create_app

application = create_app()

METEOROID_URL = application.config.get("METEOROID")

def sendEmail(to_emails, subject,text_data,headers):

    create_url = f"{METEOROID_URL}/email/send"
    from_email = dict(Email="support@egnify.com", Name="Egnify")
    data ={
       "from": from_email,
       "to":to_emails,
       "subject":subject,
       "text":text_data,
       "html": None
    }
    resp = requests.post(
        create_url, json=data, headers=headers
    )
    if resp:
        return resp.json()

    return False
