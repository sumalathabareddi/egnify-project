import requests
from app import create_app

application = create_app()

METEOROID_URL = application.config.get("METEOROID")


def sendPushNotification(bulk_notification, headers):

    create_url = f"{METEOROID_URL}/notification/create/bulk"

    notification_resp = requests.put(
        create_url, json=bulk_notification, headers=headers
    )

    if notification_resp.ok:
        return notification_resp.json()

    return False


def cancelPushNotification(notification_id, headers):

    cancel_url = f"{METEOROID_URL}/{notification_id}"
    cancel_resp = requests.delete(cancel_url, headers=headers)
    if cancel_resp.ok:
        return "success"

    return False
