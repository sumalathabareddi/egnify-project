import datetime
import time

from app import create_app
from db.HydraMongoDb import HydraMongoDb
from db.HydraArangoDb import HydraArangoDb

application = create_app()

settings_database = application.config["SETTINGS_DATABASE"]


def get_student_ids_for_hierarchies(hierarchies):
    leaf_nodes = []
    for each_hierarchy in hierarchies:
        hierarchy_leaf_nodes = application.arango_db.get_leaf_nodes(each_hierarchy)
        hierarchy_leaf_nodes = [node["id"] for node in hierarchy_leaf_nodes]
        leaf_nodes.extend(hierarchy_leaf_nodes)
    # use the test least level nodes to fetch students
    students_query = {"accessTag.hierarchy": {"$in": leaf_nodes}}
    students_coll_name = "studentInfo"
    students_kwargs = {"projection": {"studentId": 1, "_id": 0}}
    students = application.mongo_db.find(
        settings_database, students_coll_name, students_query, **students_kwargs
    )
    student_ids = []
    for studentInfo in students:
        student_ids.append(studentInfo["studentId"])
    return ",".join(student_ids)


def date_time_utc(date_time_string, delta_min=0, delta_hr=0, delta_sec=0):
    # date_object = datetime.datetime.strptime(date_time_string, "%Y-%m-%d %H:%M:%S")
    trigger_date = date_time_string + datetime.timedelta(
        hours=delta_hr, minutes=delta_min, seconds=delta_sec
    )
    required_format = trigger_date.strftime("%Y/%m/%d %H:%M:%S")
    return required_format
