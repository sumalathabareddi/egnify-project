import base64
import csv
from datetime import datetime
from hashlib import pbkdf2_hmac
from os import urandom
from random import randint
from flask import helpers

import redis
from db.HydraArangoDb import HydraArangoDb
from db.HydraMongoDb import HydraMongoDb
from rq import Queue




from api.models.students import StudentSchema
from app import create_app
from bulkread.BulkRead import StudentsBulkRead, StudentsBulkReadV2

application = create_app()

mongo_db = HydraMongoDb(application.config['MONGO_URI'])
# mongo_db = HydraMongoDb("mongodb://root:5eqcMZQCPXtG8MvU@localhost:27019/?authenticationDatabase=admin")
arango_db = HydraArangoDb(
    application.config["ARANGODB_URI"],
    application.config["ARANGODB_USERNAME"],
    application.config["ARANGODB_PASSWORD"],
)
settings_database = application.config["SETTINGS_DATABASE"]
tenant_registry = application.config["TENANT_REGISTRY_DATABASE"]
ga_database = application.config["GA_DATABASE"]
hostname = application.config["HOST_NAME"]
# hostname = 'getranks.in'
redis_connection = redis.Redis.from_url(application.config["RQ_REDIS_URL"])
queue = Queue(application.config["RQ_QUEUES"], connection=redis_connection)
print(hostname)
docs = []
with open('./Kakatiya Junior College_students_sample.csv') as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=",")
    csv_data = list(csv_reader)
    print(len(csv_data))
    sids = [f"5f803cbf6eb2bf3b8dbeb04f_{student['Student Id']}" for student in csv_data]
    print(len(sids))
    mongo_db.delete(tenant_registry,"users",{"_id":{"$in":sids}})
    mongo_db.delete(settings_database,"studentInfo",{"_id":{"$in":sids}})
# ids = set()
# marking_schemas = mongo_db.find(settings_database,"markingschemas",{"active":True})
# new_schemas = []
# for schema in marking_schemas:
#     print(f"{schema['_id']}")
#     subjects = schema.get('subjects',[])
#     end = 0
#     for subject in subjects:
#         print(subject['subjectName'])
#         marks = subject['marks']
#         tieBreaker = subject['tieBreaker']
#         print(f'tb:{tieBreaker}')
#         totalQuestions = subject['totalQuestions']
#         print(f"t::{totalQuestions}")
#         for index, mark in enumerate(marks,0):
#             noOfQs = mark.get("numberOfQuestions")
#             # print(f'noQ:{noOfQs}')
#             if mark.get("start",None):
#                 print('ok')
#             else:
#                 start = end+1
#                 end = (start + noOfQs) -1 
#                 ids.add(schema["_id"])
#                 print(f"{start}::{end}")
#                 mark['start'] = start
#                 mark['end'] = end
#     if schema["_id"] in ids:
#         print(f"{schema['subjects'][0]['marks']}")
        
#     # if not subjects:
#     #     print(schema["_id"])
# print(len(ids))
# # mongo_db.update_many_in_process(
# #     settings_database, "markingschemas", marking_schemas, id_field="_id",
# # )
# # mongo_db.wait_for_termination()
            
            
        