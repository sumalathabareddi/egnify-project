import functools
import multiprocessing
import time
from multiprocessing import Pool
from munch import DefaultMunch
import rq
import re
from rq import Queue
import traceback
import redis
from itertools import combinations
from datetime import datetime, timezone, timedelta
from pytz import timezone as tmz


from app import create_app
from db.HydraMongoDb import HydraMongoDb
from db.HydraArangoDb import HydraArangoDb
from hierarchy.HierarchyResult import HierarchyResult
from legacy.MasterResult import MasterResult
from marks.Question import Question
from marks.Subject import Subject
from marks.SubTopic import SubTopic
from marks.Test import Test
from marks.Topic import Topic
from utils import md5
from cwu.calculator import CWUCalculator
from cwu.questions import keys_for_paper

from notifications import pushnotification

application = create_app()

mongodb = HydraMongoDb(application.config["MONGO_URI"])
ga_database = application.config["GA_DATABASE"]
questions_database = application.config["QUESTION_BANK_DB"]
arango_db = HydraArangoDb(
    application.config["ARANGODB_URI"],
    application.config["ARANGODB_USERNAME"],
    application.config["ARANGODB_PASSWORD"],
)
settings_database = application.config["SETTINGS_DATABASE"]
master_results_table = application.config["MASTER_RESULTS_TABLE"]
rankguru = application.config["IS_RANKGURU"]
redis_connection = redis.Redis.from_url(application.config["RQ_REDIS_URL"])
queue = Queue(application.config["RQ_QUEUES"], connection=redis_connection)


# This is for testing purpose in local environment
class CurrentJob:
    """

    """

    def __init__(self):
        self.meta = {}

    def save_meta(self):
        """

        """
        print(self.meta)


def re_order_marking_schema_subjects(marking_schema, test_subjects):
    """
    :param marking_schema:
    :param test_subjects:
    :return:
    """
    modified_schema = marking_schema
    marking_schema_subjects = marking_schema.get("subjects", [])
    prev_start = 0
    new_order = []
    for subject in test_subjects:
        subject_code = subject["subjectCode"]
        subject_name = subject["subject"]
        subject_index = -1
        for index in range(len(marking_schema_subjects)):
            if marking_schema_subjects[index]["subject"] == subject_code:
                subject_index = index
                break
        data = marking_schema_subjects[subject_index]
        data["start"] = prev_start + 1
        data["end"] = data["totalQuestions"] + prev_start
        prev_start = data["end"]
        data["subjectName"] = subject_name
        data["subjectCode"] = subject_code
        start = data["start"]
        for section in data["marks"]:
            section["start"] = start
            start += section["numberOfQuestions"]
            section["end"] = start - 1

        new_order.append(data)
    modified_schema["subjects"] = new_order
    return modified_schema


def process_marking_schema(marking_schema):
    """

    :param marking_schema:
    """
    question_marks = {}
    is_partial = marking_schema.get("is_partial", False)
    for subject in marking_schema.get("subjects", []):
        for marks in subject.get("marks", []):
            if marks["egnifyQuestionType"] == "Matrix match type":
                sub_q = marks["numberOfSubQuestions"] // marks["numberOfQuestions"]
                for question_number in range(marks["start"], marks["end"] + 1):
                    for sub_item in range(97, 97 + sub_q):
                        question_marks[f"Q{question_number}{chr(sub_item)}"] = {
                            "C": marks.get("C", 0),
                            "W": marks.get("W", 0),
                            "ADD": marks.get("ADD", 0),
                            "P": marks.get("P", 0) if is_partial else marks.get("W", 0),
                            "U": marks.get("U", 0),
                        }
            else:
                for question_number in range(marks["start"], marks["end"] + 1):
                    question_marks[f"Q{question_number}"] = {
                        "C": marks.get("C", 0),
                        "W": marks.get("W", 0),
                        "ADD": marks.get("ADD", 0),
                        "P": marks.get("P", 0) if is_partial else marks.get("W", 0),
                        "U": marks.get("U", 0),
                    }
    return question_marks


def get_tie_breaking_list(marking_schema):
    """

    :param marking_schema:
    """
    tie_breaking_order = []
    subject_sorted = sorted(
        marking_schema.get("subjects", []), key=lambda sub: sub["tieBreaker"]
    )
    subject_sorted = [
        sorted_dict["subjectCode"]
        for sorted_dict in subject_sorted
        if "subject" in sorted_dict
    ]
    subjects = mongodb.read_by_id(
        settings_database, "subjecttaxonomies", subject_sorted
    )
    for subject in subject_sorted:
        tie_breaking_order.append(subjects[subject].get("name", None))
    return tie_breaking_order


def hello_gcs_generic(data):
    """

    :param data:
    """
    ticks = time.time()
    test_id = data.get("test_id", None)
    test_name = data.get("test_name", None)
    test_ids = data.get("test_ids", None)
    question_paper_id = data.get("question_paper_id", [f"QP{test_id}"])[0]
    previous_tests = data.get("previous_tests", None)
    marking_schema = data.get("marking_schema", None)
    test_from_db = mongodb.read_by_id(ga_database, "tests", [test_id])
    marking_schema = re_order_marking_schema_subjects(
        marking_schema, test_from_db[test_id]["subjects"]
    )
    current_job = rq.get_current_job()
    re_generate_analysis = data.get("regenerate", False)
    manager = multiprocessing.Manager()
    error_queue = manager.list()
    if test_ids:
        test_id = "_".join(test_ids)
        if len(test_ids) > 2:
            test_1 = "_".join(test_ids[1:])
            test_1_exists = mongodb.find_one(
                ga_database,
                "hierarchicalAnalysis",
                {"test_id": test_1},
                projection={"_id": 1},
            )
            test_ids = [test_ids[0], test_1] if test_1_exists else test_ids

    if re_generate_analysis:
        delete_generated_test_records(test_id)

    re_eval_cwu(test_id, test_from_db)
    tie_breaking_list = get_tie_breaking_list(marking_schema)
    print('tie::',tie_breaking_list)
    hierarchyDetails = getHierarchyDetails()
    if test_ids:
        results = calulate_master_results_for_multiple_test(
            ticks, test_ids, error_queue, current_job=current_job
        )
    else:
        results = calulate_master_results_for_single_test(
            ticks,
            test_name,
            tie_breaking_list,
            test_id,
            question_paper_id,
            marking_schema,
            error_queue,
            current_job=current_job,
        )

    master_results = get_master_results(results)
    mongodb.insert_many(ga_database, master_results_table, master_results)
    ticks5 = time.time()
    length_of_results = len(results)
    hierarchy = HierarchyResult()
    print('result::##########',results)
    hierarchy.caliculate_hierarical_analysis(results)
    mongodb.wait_for_termination()
    hierarchy.aggregate_results(test_id, hierarchyDetails, error_queue)
    ticks4 = time.time()
    print("time taken to caliculate hierarchy " + str(ticks4 - ticks5))
    current_job.meta["step3"] = ticks4 - ticks5
    current_job.save_meta()
    if error_queue:
        print(error_queue)
        raise Exception("Exception in running analysis")
    hierarchy_result = list(hierarchy.hierarchyResultMap.values())
    mongodb.insert_many(ga_database, "hierarchicalAnalysis", hierarchy_result)
    hierarchy.aggregate_results_for_subject(test_id)
    mongodb.insert_many(
        ga_database, "rankAnalysis", list(hierarchy.hierarchyRankList.values())
    )
    mongodb.insert_many(
        ga_database, "markAnalysis", list(hierarchy.hierarchyMarksList.values())
    )
    mongodb.wait_for_termination()
    print("time taken to finish writing " + str(time.time() - ticks4))
    mongodb.upsert(
        ga_database,
        "tests",
        {"testId": test_id},
        {
            "status": "completed",
            "gaStatus": "finished",
            "totalStudents": length_of_results,
            "autoGa.status": "completed",
            "regenerate": False,
        },
        upsert=False,
    )
    if test_from_db[test_id].get("is_parsed", False):
        q_percent = {}
        # Get the analysis for questions
        node = None
        for item in hierarchy_result:
            if item["parentCode"] == "root":
                node = item
                break
        if node:
            for qno, q_data in node["questionMap"].items():
                wrong = q_data.get("W", 0) / node["numberOfStudents"]
                q_percent[qno] = wrong
        questions = mongodb.find(
            ga_database, "questions", {"questionPaperId": f"QP{test_id}"}, key="qno"
        )

        update_docs = []
        for qno, q_doc in questions.items():
            q_type = q_doc["questionType"]
            if q_type == "Matrix match type":
                sub_questions = [f"Q{qno}{sub}" for sub in ["a", "b", "c", "d"]]
                percent = sum(q_percent[sub_q] for sub_q in sub_questions) / 4
            else:
                percent = q_percent.get(f"Q{qno}")
            question_number_id = q_doc["questionNumberId"]
            difficulty = None
            if percent >= 0.8:
                difficulty = "Hard"
            elif percent < 0.8 and percent >= 0.5:
                difficulty = "Medium"
            elif percent < 0.5 and percent >= 0.0:
                difficulty = "Easy"
            if difficulty:
                update_docs.append(
                    {"_id": question_number_id, "difficulty": difficulty}
                )
        mongodb.upsert_many_in_process(ga_database, "questions-list", update_docs)

    print("Updated the test table as GA finished")
    current_job.meta["step4"] = time.time() - ticks4
    current_job.save_meta()

    ##Notification Alert
    # IST = tmz("Asia/Kolkata")

    # test_start_time = test_from_db[test_id]["startTime"]
    # human_readable_date = (
    #     test_start_time.replace(tzinfo=timezone.utc)
    #     .astimezone(tz=IST)
    #     .strftime("%a %d %b %I:%M %p")
    # )

    # header = (
    #     f"Results announced for test {test_name}, conducted on {human_readable_date}."
    # )
    # total_marks = marking_schema["totalMarks"]
    # request_headers = data.get("headers")
    # bulk_notification = []
    # for i in range(len(master_results)):
    #     marks = master_results[i]["cwuAnalysis"]["overall"]["marks"]
    #     total_marks_obtained = (
    #         marks["C"] + marks["W"] + marks["U"] + marks["ADD"] + marks["P"]
    #     )
    #     body = f"Total marks obtained {total_marks_obtained} out of {total_marks}.Tap here to see the detailed analysis with rank."
    #     notification_obj = {
    #         "header": header,
    #         "body": body,
    #         "studentIds": master_results[i]["studentId"],
    #         "details": body,
    #     }
    #     bulk_notification.append(notification_obj)
    # pushnotification.sendPushNotification(
    #     {"bulk_array": bulk_notification}, request_headers
    # )

    # print("Notification Scheduled.")

    if previous_tests:
        jobs_list = []
        for i in range(2, len(previous_tests) + 1):
            for combination_test in combinations(previous_tests, i):
                new_data = data
                new_data["previous_tests"] = None
                new_data["test_ids"] = [t for t in combination_test]
                new_data["test_id"] = None
                job = queue.enqueue(
                    hello_gcs_generic, job_timeout="60m", args=(new_data,)
                )
                current_job.meta["step5"] = jobs_list.append(job.get_id())
                current_job.save_meta()


def re_eval_cwu(test_id, test):
    query = {"testId": test_id}
    paper_edit_docs = mongodb.find(ga_database, "paper-edit-entries", query, key="qno")
    update_ops = []
    if paper_edit_docs:
        additional_questions = [
            key
            for key, value in paper_edit_docs.items()
            if value.get("hasAdditionalMarks", False)
        ]
        question_paper_id = test[test_id]["questionPaperId"][0]
        question_keys = keys_for_paper(
            question_paper_id, rankguru, mongodb, settings_database, questions_database
        )
        calc = CWUCalculator(question_keys, rankguru)
        response_docs = mongodb.find(ga_database, "behavourData", query)
        for response_doc in response_docs:
            behaviour_data = response_doc["behavourData"]
            raw_response = {
                x["_id"]: x.get("response", []) for x in behaviour_data.values()
            }
            cwu = calc.student_result(raw_response)
            for each_add_qn in additional_questions:
                cwu[each_add_qn] = "ADD"
            op = {"_id": response_doc["_id"], "responseData.questionResponse": cwu}
            update_ops.append(op)
    if update_ops:
        mongodb.upsert_many_in_process(ga_database, "testStudentSnapshot", update_ops)
        mongodb.wait_for_termination()


def calulate_master_results_for_multiple_test(
    ticks, test_ids, error_queue, current_job
):
    """

    :param ticks:
    :param test_ids:
    :param error_queue:
    :param current_job:
    :return:
    """
    query = {"testId": {}}
    query["testId"]["$in"] = test_ids
    print("Reading data from mongo {}".format(mongodb.ip_address))
    unique_students = mongodb.distinct_field(
        ga_database, master_results_table, "studentId", query
    )
    print("Read data from mongo {}".format(mongodb.ip_address))
    print(f"Total unique students in the DB {len(unique_students)}")
    ticks3 = time.time()
    current_job.meta["step1"] = ticks3 - ticks
    current_job.save_meta()
    unique_test_id = "_".join(test_ids)
    length_of_tests = len(test_ids)
    # This should run as thread as Processing time vs Copy time is low
    myPool = Pool(multiprocessing.cpu_count())
    aggregated_master_results = myPool.map(
        functools.partial(
            caliculate_master_results_for_multi_test,
            unique_test_id=unique_test_id,
            test_list=test_ids,
            error_queue=error_queue,
        ),
        divide_chunks(
            unique_students,
            int(50000 / (length_of_tests * multiprocessing.cpu_count())),
        ),
    )
    myPool.close()
    myPool.join()
    ticks1 = time.time()
    print("Time taken to caliculate " + str(ticks1 - ticks3))
    current_job.meta["step2"] = ticks1 - ticks3
    current_job.save_meta()
    aggregated_master_results_as_list = {}
    for aggregated_master_result in aggregated_master_results:
        aggregated_master_results_as_list.update(aggregated_master_result)
    aggregated_master_results_as_list = list(aggregated_master_results_as_list.values())
    return aggregated_master_results_as_list


def calulate_master_results_for_single_test(
    ticks,
    test_name,
    tie_breaking_list,
    test_id,
    question_paper_id,
    marking_schema,
    error_queue,
    current_job,
):
    """

    :param marking_schema:
    :param ticks:
    :param test_name:
    :param tie_breaking_list:
    :param test_id:
    :param error_queue:
    :param current_job:
    :return:
    """
    testStudentSnapshot = mongodb.find(
        ga_database,
        "testStudentSnapshot",
        {"testId": test_id, "syncStatus": True},
        "studentId",
    )
    behaviour = mongodb.find(
        ga_database, "behaviourData", {"testId": test_id}, "studentId"
    )
    rows = []
    for student_id, doc in testStudentSnapshot.items():
        raw_response = {}
        student_behviour = behaviour.get(student_id, None)
        if student_behviour:
            raw_response = {
                key: value.get("response", [])
                for key, value in student_behviour["behaviourData"].items()
                if key != "metaData"
            }
        rows.append([student_id, doc, raw_response])
    tie_breaking_list.insert(0, "overall")
    test = Test(test_id, test_name, tie_breaking_list)
    ticks3 = time.time()
    current_job.meta["step1"] = ticks3 - ticks
    current_job.save_meta()
    print("time taken to fetch the records from DB " + str(ticks3 - ticks))
    questions = get_mongodb_question_details(
        mongodb, ga_database, test_id, question_paper_id, marking_schema, rankguru
    )
    myPool = Pool(multiprocessing.cpu_count())
    results = myPool.map(
        functools.partial(
            caliculate_marks_for_a_student,
            test_obj=test,
            marking_schema=marking_schema,
            questions=questions,
            error_queue=error_queue,
        ),
        rows,
        2 * multiprocessing.cpu_count(),
    )
    myPool.close()
    myPool.join()
    ticks1 = time.time()
    print("Time taken to caliculate " + str(ticks1 - ticks3))
    current_job.meta["step2"] = ticks1 - ticks3
    current_job.save_meta()
    return results


def get_master_results(results):
    """

    :param results:
    :return:
    """
    masterResults = []
    for result in results:
        if result:
            masterDict = result.__dict__
            masterDict["_id"] = result.id
            masterResults.append(masterDict)
    return masterResults


def getHierarchyDetails():
    """
    :return:
    """
    hierarchies = {}
    details = arango_db.get_children("root", depth=20)
    vertices = details.get("vertices", [])
    for vertex in vertices:
        hierarchies[vertex["id"]] = vertex
    return hierarchies


def get_master_restults_for_multi_test(db, database_to_use, query):
    """

    :param db:
    :param database_to_use:
    :param query:
    :return:
    """
    total_master_records = db.get_all_records(
        database_to_use, master_results_table, query, process=True
    )
    total_master_records_map = {}
    for master_record in total_master_records:
        master_record_object = MasterResult.get_object_from_dict(master_record)
        total_master_records_map[master_record_object.id] = master_record_object
    return total_master_records_map


def divide_chunks(list_to_split, size):
    """

    :param list_to_split:
    :param size:
    """
    # looping till length l
    for i in range(0, len(list_to_split), size):
        yield list_to_split[i : i + size]


def caliculate_master_results_for_multi_test(
    student_list, unique_test_id, test_list, error_queue
):
    """

    :param student_list:
    :param unique_test_id:
    :param test_list:
    :param error_queue:
    :return:
    """
    try:
        aggregated_master_result = {}
        query = {}
        query["testId"] = {}
        query["studentId"] = {}
        query["testId"]["$in"] = test_list
        query["studentId"]["$in"] = student_list
        ticks = time.time()
        master_results_per_student = get_master_restults_for_multi_test(
            mongodb, ga_database, query
        )
        ticks1 = time.time()
        print("Master results individual query " + str(ticks1 - ticks))
        for id, master_results_per_test in master_results_per_student.items():
            if master_results_per_test.studentId in aggregated_master_result:
                aggregated_master_result_per_student = aggregated_master_result[
                    master_results_per_test.studentId
                ]
                aggregated_master_result_per_student.add(master_results_per_test)
            else:
                aggregated_master_result_per_student = master_results_per_test
                aggregated_master_result_per_student.testId = unique_test_id
                aggregated_master_result_per_student.id = "_".join(
                    [unique_test_id, master_results_per_test.studentId]
                )
            aggregated_master_result[
                master_results_per_test.studentId
            ] = aggregated_master_result_per_student
        return aggregated_master_result
    except Exception as e:
        track = traceback.format_exc()
        print(track)
        error_queue.append(e)


def caliculate_marks_for_a_student(
    row, test_obj, marking_schema, questions, error_queue
):
    """
    :param marking_schema:
    :param row:
    :param test_obj:
    :param error_queue:
    :return:
    """

    def replace_alpha(q):
        return re.sub(r"[a-z]", r"", q)

    try:
        is_partial = marking_schema.get("is_partial", False)
        undefined = "Not_Mapped"
        behaviour = False
        marking_schema_per_quesion = process_marking_schema(marking_schema)
        temporary_questions = DefaultMunch.fromDict(
            row[1]
        ).responseData.questionResponse
        questions_behaviour = row[1].get("behaviourData", {})
        new_test = test_obj.clone()
        new_test.studentId = row[0]

        for ques, mark_schema in marking_schema_per_quesion.items():
            answer = temporary_questions.get(ques)
            question_json = questions.get(replace_alpha(ques))
            question_json.update(mark_schema)
            question_json["P1"] = question_json.get("P", 0)
            question_json["P2"] = 2 * question_json.get("P", 0)
            question_json["P3"] = 3 * question_json.get("P", 0)
            question_json[
                "questionNumberId"
            ] = f"{question_json['questionNumberId']}{ques}"
            if answer in ["P1", "P2", "P3"]:
                if is_partial:
                    answer_copy = "P"
                else:
                    answer_copy = "W"
            else:
                answer_copy = answer
            question_key = replace_alpha(ques.replace("Q", ""))
            resp_lst = row[2].get(question_key, [])
            selection = {}
            if isinstance(resp_lst, list):
                selection = {str(key).replace(".", "_"): 1 for key in resp_lst}
            question = DefaultMunch.fromDict(question_json, undefined)
            subject = new_test.subjects.get(question.subject, Subject(question.subject))
            print('subject::',question.subject)
            topic = subject.topics.get(question.topic, Topic(question.topic))
            subTopic = topic.subTopics.get(
                question.subTopic, SubTopic(question.subTopic)
            )
            questionObj = Question(
                question.questionNumberId,
                ques,
                answer_copy,
                question.difficulty,
                question[answer],
                question.C,
                selection,
            )
            if questions_behaviour and isinstance(
                questions_behaviour.get(ques, None), dict
            ):
                questionObj.timespent = questions_behaviour[ques].get(
                    "totalTimeSpent", 0
                )
                behaviour = True
                questionObj.datetime = questions_behaviour[ques].get("datetime", None)
                questionObj.visit_count = questions_behaviour[ques].get("visitCount", 0)
                question_time = questions_behaviour[ques].get("time", None)
                if question_time:
                    questionObj.timespent = sum(question_time)
                    questionObj.visit_count = len(question_time)
            subTopic.questions[question.questionNumberId] = questionObj
            topic.subTopics[question.subTopic] = subTopic
            subject.topics[question.topic] = topic
            new_test.subjects[question.subject] = subject
        new_test.caliculate_marks()
        return MasterResult(
            new_test, DefaultMunch.fromDict(row[1]), behaviour=behaviour
        )
    except Exception as e:
        track = traceback.format_exc()
        print(track)
        error_queue.append(e)
        # traceback.print_stack()


def get_mongodb_question_details(
    mongodb, database_to_use, test_id, question_paper_id, marking_schema, rankguru=False
):
    """

    :param mongodb:
    :param database_to_use:
    :param test_id:
    :param rankguru:
    :return:
    """
    if not rankguru:
        # TODO: Get question paper Id or get it passed
        # For now QPtestId is quesiton paper ID
        results = {}
        results_cursor = list(
            mongodb.connection[database_to_use]
            .get_collection("questions")
            .aggregate(
                [
                    {"$match": {"questionPaperId": question_paper_id}},
                    {
                        "$lookup": {
                            "from": "questions-list",
                            "localField": "questionNumberId",
                            "foreignField": "_id",
                            "as": "question",
                        }
                    },
                    {"$unwind": {"path": "$question"}},
                ]
            )
        )
        # Use `Unmapped` if there is no concept mapping for question
        for each_question in results_cursor:
            if not each_question["question"]["topic"]:
                each_question["question"]["topic"] = "Unmapped"
            if not each_question["question"]["subTopic"]:
                each_question["question"]["subTopic"] = "Unmapped"

        for question in results_cursor:
            question_no = question["qno"]
            question["question"]["topic"] = question["question"]["topic"].replace(
                ".", ""
            )
            question["question"]["subTopic"] = question["question"]["subTopic"].replace(
                ".", ""
            )
            results[f"Q{question_no}"] = question["question"]
    else:
        results = {}
        test_data = mongodb.find_one(database_to_use, "tests", {"testId": test_id})
        questions_data = mongodb.find(
            database_to_use,
            "questions",
            {"questionPaperId": test_data["questionPaperId"]},
            "qno",
        )
        subjects = test_data["markingSchema"]["subjects"]
        subjects_map = {}
        first_subject = None
        for subject in subjects:
            subjects_map[subject["subject"]] = subject["marks"][0]
            if not first_subject:
                first_subject = subject["subject"]
        for key, question_map in questions_data.items():
            subject = question_map["subject"]
            question_map.update(subjects_map.get(subject, subjects_map[first_subject]))
            question_map["topic"] = question_map.get("concept_name", "unidentified")
            question_map["topicCode"] = md5(
                question_map["topic"].encode("utf-8")
            ).hexdigest()
            question_map["qid"] = question_map["questionNumberId"]
            question_map["questionNumber"] = question_map["qno"]
            results[key] = question_map
    return results


def delete_generated_test_records(test_id):
    """

    :param test_id:
    """
    mongodb.delete(ga_database, "rankAnalysis", {"testId": test_id})
    mongodb.delete(ga_database, master_results_table, {"testId": test_id})
    mongodb.delete(ga_database, "markAnalysis", {"testId": test_id})
    mongodb.delete(ga_database, "hierarchicalAnalysis", {"testId": test_id})
    mongodb.upsert(
        ga_database, "tests", {"testId": test_id}, {"gaStatus": ""}, upsert=False
    )


def clean_test_records(job, exc_type, exc_value, traceback):
    """

    :param job:
    :param exc_type:
    :param exc_value:
    :param traceback:
    """
    (method_args,) = job.args
    testId = method_args["test_id"]
    print("Deleting records for the testId {} ".format(testId))
    delete_generated_test_records(testId)

def hello_world(data):
    print('hello world')
