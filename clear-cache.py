
import csv
import requests
import json
from db.HydraMongoDb import HydraMongoDb
from app import create_app

application = create_app()

mongo_db = HydraMongoDb(application.config['MONGO_URI'])

login_url = "https://account.getranks.in/auth/local"
cache_url = "https://rest.getranks.in/util/clear/cache"
tenant_registry = application.config["TENANT_REGISTRY_DATABASE"]
settings_database = application.config["SETTINGS_DATABASE"]
tenant_registry = application.config["TENANT_REGISTRY_DATABASE"]
ga_database = application.config["GA_DATABASE"]
hostname = application.config["HOST_NAME"]

headers = {
            "Content-type": "application/json",
            "Accept": "text/plain",
}
user_doc = {"email":"anjan@egnify.com","password":"egnify","rememberMe":False,"hostname":"getranks.in"}
res = {}
login_resp = requests.post(
            login_url,
            data=json.dumps(user_doc),
            headers=headers, 
            )
if login_resp.ok:
    res = login_resp.json()
    # body  = {"email":"STU113","rememberMe":false,"hostname":"rankguru.com","password":"4143"}
    # res = {"token":"","accessControlToken":""}
    # with open('./Geetham -27-07-2021.csv') as csv_file:

    #     csv_reader = csv.DictReader(csv_file, delimiter=",")
    #     csv_data = list(csv_reader)

    mschemas = mongo_db.find(tenant_registry,"users",{"studentId":{"$exists":False},"active":True,"instituteId":"5fa4ca3a3a1f08a5a709d9be","zoom":{"$exists":False}})
    # mschemas = mongo_db.find(settings_database,"markingschemas",{"active":True})
    for schema in mschemas:
        # u_doc = {
        #     'redis_key':f"view//{schema.get('instituteId','root')}/markingschema/{schema['_id']}"
        # }
        u_doc = {
            'redis_key':f"5fa4ca3a3a1f08a5a709d9be_{schema['email']}_zoom_key"
        }
        headers = {
            "Content-type": "application/json",
            "Accept": "text/plain",
            "accesscontroltoken": res.get("accessControlToken"),
            "Authorization": res.get("token"),
        }
        sso_resp = requests.post(
            cache_url,
            data=json.dumps(u_doc),
            headers=headers,
        )
        if sso_resp.ok:
            print(u_doc['redis_key'])
            print('success')
            print(sso_resp.text)
            
        else:
            print(sso_resp)
            print('failure')
else: 
    print(login_resp)