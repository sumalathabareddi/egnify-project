import requests
import csv
from bulkread.BulkRead import HierarchiesBulkRead
from app import create_app
from db.HydraMongoDb import HydraMongoDb

application = create_app()

settings_database = application.config["SETTINGS_DATABASE"]
mongo_db = HydraMongoDb(application.config['MONGO_URI'])
docs =[]
csv_headers = []
csv_data_1 = []
programe = set()
sections = set()
with open('./Sections Delete List.csv') as csv_file:
        csv_reader1 = csv.reader(csv_file, delimiter=",")
        csv_headers = next(csv_reader1)
        csv_data_1 = list(csv_reader1)
        for row in csv_data_1:
                programe.add(row[4])
                sections.add(row[5])
        print(len(programe))
        print(len(sections))
        institute_info = mongo_db.find_one(
                settings_database, "institutes", {"_id": "608d0036e369128d6947c4dc"}
        )
        system_hierarchies = mongo_db.find(settings_database, "systemhierarchies", query={})
        hbr = HierarchiesBulkRead(institute_info,csv_headers,csv_data_1,system_hierarchies,0)
        err = hbr.validate()
        if err:
                print(err)
        else: 
                docs = hbr.create_hierarchies()
                new_docs =[]
                for doc in docs:
                        size = len(doc['hierarchyTuple']) 
                        if doc['hierarchyName'] in programe and size == 5:
                                print(doc['hierarchyTuple'])
                                new_docs.append({"id": doc["_id"]})
                with open('./hierarchies_1.csv', mode='w') as csv_file:
                        fieldnames = ["id"]
                        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
                        writer.writeheader()
                        writer.writerows(new_docs)  
