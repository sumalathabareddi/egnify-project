
import csv
import requests
import json

login_url = "https://accounts.rankguru.com/auth/local"

# body  = {"email":"STU113","rememberMe":false,"hostname":"rankguru.com","password":"4143"}
# res = {"token":"","accessControlToken":""}
change_password_url = "https://accounts.rankguru.com/api/v1/users/password"

with open('./Dummy USR Logins 2021.csv') as csv_file:

    csv_reader = csv.DictReader(csv_file, delimiter=",")
    csv_data = list(csv_reader)

    for row in csv_data:
        headers = {
            "Content-type": "application/json",
            "Accept": "text/plain",
        }
        user_doc = {
            "email": row['userid'],
            "rememberMe": False,
            "hostname":"rankguru.com",
            "password":row['Password']
        }
        pass_doc ={
            'oldPassword': row['Password'],
            'newPassword': row['New Password']
        }
        login_resp = requests.post(
            login_url,
            data=json.dumps(user_doc),
            headers=headers, 
            )
        if login_resp.ok:
            res = login_resp.json()
            # print(login_resp.json())
            headers = {
                "Content-type": "application/json",
                "Accept": "text/plain",
                "accesscontroltoken": res.get("accessControlToken"),
                "Authorization": res.get("token"),
            }
            sso_resp = requests.put(
                change_password_url,
                data=json.dumps(pass_doc),
                headers=headers,
            )
            if sso_resp.ok:
                print(user_doc['email'])
                print('success')
                print(sso_resp)
                
            else:
                print(sso_resp)
                print('failure')
        else: 
            print(login_resp)