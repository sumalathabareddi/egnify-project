import base64
import csv
from datetime import datetime
from hashlib import pbkdf2_hmac
from os import urandom
from random import randint
from flask import helpers


import redis
import requests
import rq
from db.HydraArangoDb import HydraArangoDb
from db.HydraMongoDb import HydraMongoDb
from rq import Queue
from utils.EgnifyException import MultipleValidationExceptions, ValidationException



from api.models.students import StudentSchema
from app import create_app
from bulkread.BulkRead import StudentsBulkRead, StudentsBulkReadV2
from utils.StorageHelper import StorageHelper


application = create_app()

mongo_db = HydraMongoDb(application.config['MONGO_URI'])
arango_db = HydraArangoDb(
    application.config["ARANGODB_URI"],
    application.config["ARANGODB_USERNAME"],
    application.config["ARANGODB_PASSWORD"],
)
settings_database = application.config["SETTINGS_DATABASE"]
tenant_registry = application.config["TENANT_REGISTRY_DATABASE"]
hostname = application.config["HOST_NAME"]
redis_connection = redis.Redis.from_url(application.config["RQ_REDIS_URL"])
queue = Queue(application.config["RQ_QUEUES"], connection=redis_connection)
storage_provider = application.config["STORAGE_PROVIDER"]
storage_secrets = {}
storage_secrets["GCP_CREDENTIALS_FILE"] = application.config["GCP_CREDENTIALS_FILE"]
bucket_name = application.config["CLOUD_STORAGE_BUCKET"]


def format_data(file_path,id_prefix):
    docs=[]
    with open(file_path) as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=",")
        csv_data = list(csv_reader)
        for row in csv_data:
            if id_prefix:
                row['Student Id'] = f'{id_prefix}{row["Student Id"]}'.upper()
            else:
                row['Student Id'] = f'{row["Student Id"]}'.upper()
            if not row['Email']:
                row['Email'] = f'{row["Student Id"]}@getranks.in'.lower()
            row['Orientation'] = f'{row["Orientation"]}'.strip()
            if row['Class'] =='Class LONG TERM':
                row['Class'] = 'Class 13'
            else:
                row['Class'] = f'{row["Class"]}'.strip()
            row['State'] = f'{row["State"]}'.strip()
            row['City'] = f'{row["City"]}'.strip()
            row['Campus'] = f'{row["Campus"]}'.strip()
            row['Section'] = f'{row["Section"]}'.strip()
            docs.append(row)


    with open('./students.csv', mode='w') as csv_file:
        fieldnames = ['Student Id','Student Name','Father Name','Phone','Email','DOB','Gender','Category','Orientation','Class','State','City','Campus','Section']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(docs)


def upload_file(file_path):
    with open(file_path) as uploaded_file:
        uploaded_file_string = uploaded_file.read()
        uploaded_file_string =uploaded_file_string.encode('utf-8')
        storage_helper = StorageHelper(storage_provider, storage_secrets)
        extention = 'csv'
        data = {
            "file_ext": f".{extention}",
            "file_name": file_path.split('/')[1],
            "data": uploaded_file_string,
            "content_type": 'text/csv',
            "bucket_name": bucket_name,
            "content_disposition": "inline",
        }

        storage_helper = StorageHelper(storage_provider, storage_secrets)

        url = storage_helper.upload(data, get_public_url=True)
        print(url)
        return url


def create_accounts(students):
    passwords = {}
    user_docs = []
    pwd_docs =[]
    total_students = len(students)
    for index, student in enumerate(students, 1):
        # Create salt and password
        salt = urandom(16)
        base64_salt = base64.b64encode(salt)
        password = str(randint(10000, 99999))
        # password = student['Password']
        byte_password = password.encode()
        pass_hash = pbkdf2_hmac("sha1", byte_password, salt, 10000, 64)
        base64_pass = base64.b64encode(pass_hash)
        passwords[student["email"]] = password
        # Create account doc
        student_hierarchy = {
            "childCode": student["accessTag"]["hierarchy"],
        }
        doc = {
            "_id": student["_id"],
            "role": ["STUDENT", "LMS_OTP_ACCESS"],
            "forgotPassSecureHash": "",
            "passwordChange": True,
            "active": True,
            "hierarchy": [student_hierarchy],
            "email": student["email"],
            "password": base64_pass.decode(),
            "hostname": hostname,
            "instituteId": student["instituteId"],
            "studentId": student["studentId"],
            "studentName": student["studentName"],
            "username": student["studentName"],
            "egnifyId": student["egnifyId"],
            "salt": base64_salt.decode(),
            "forgotPassSecureHashExp": str(datetime.utcnow()),
        }
        p_doc ={
            "Student Name":student["studentName"],
            "Orientation":student["Orientation"],
            "Class":student["Class"],
            "State":student["State"],
            "City":student["City"],
            "Campus":student["Campus"],
            "Section":student["Section"],
            "Email":student["email"],
            "Student Id":student["studentId"],
            "Password": password
        }
        pwd_docs.append(p_doc)
        user_docs.append(doc)

    with open('./students.csv', mode='w') as csv_file:
        fieldnames = ['Student Name','Orientation','Class','State','City','Campus','Section','Student Id','Email','Password']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(pwd_docs)
    print(f'{len(user_docs)}')
    mongo_db.upsert_many_in_process(tenant_registry, "users", user_docs)
    mongo_db.wait_for_termination()
    print('done::::::::')

def bulk_create_students_nova(data):
    print(f'data::{data}')
    format_data(data['filePath'],data['id_prefix'])
    print('data format completed!')
    data['fileUrl'] = upload_file('./students.csv')
    print('file  uploaded')
    url = data["fileUrl"]
    institute_id = data["instituteId"]
    print(hostname)
    collection_name = "studentInfo"
    institute_details = mongo_db.find_one(
        settings_database, "institutes", {"_id": institute_id}
    )
    with requests.Session() as s:
        downloaded_data = s.get(url)
        decoded_data = downloaded_data.content.decode("utf-8")
        csv_reader = csv.reader(decoded_data.splitlines(), delimiter=",")
        csv_headers = next(csv_reader)
        csv_data = list(csv_reader)
        print("Completed reading students csv data")
        system_hierarchies = mongo_db.find(
            settings_database, "systemhierarchies", query={}
        )
        sbr = StudentsBulkRead(
            csv_headers, csv_data, institute_details, system_hierarchies
        )
        errors = sbr.validate()
        if errors:
            print(f'error:::validation::{errors}')
            raise MultipleValidationExceptions(errors)
        docs = sbr.create_students()
        result = StudentSchema(many=True).load(docs)
        print(result.errors)
        if result.errors:
            print(f'error:::schema:::{result.errors}')
        else:
            errors, students = (
                [],
                [],
            )
            for row, student in enumerate(result.data, 1):
                """
                    Make sure node with student["accessTag"]["hierarchy"] exists,
                    raise error if it doesn't exists
                """
                if not arango_db.has_node(student["accessTag"]["hierarchy"]):
                    errors.append(ValidationException("E403", row, None))
                else:
                    students.append(student)
            if errors:
                print(f'error:::{errors}')
                raise MultipleValidationExceptions(errors)
            else:
                print('completed reading::')
                # mongo_db.upsert_many_in_process(settings_database, collection_name, students)
                # mongo_db.wait_for_termination()
                # for key,doc in enumerate(students):
                #     doc["Orientation"] = csv_data[key][8]
                #     doc["Class"] = csv_data[key][9]
                #     doc["State"] = csv_data[key][10]
                #     doc["City"] = csv_data[key][11]
                #     doc["Campus"] = csv_data[key][12]
                #     doc['Section'] = csv_data[key][13]
                # create_accounts(students)
                

data = {
    "filePath":'./MS - MEHD - New Students - 01.07.2021.csv',
    'id_prefix': '',    
    "instituteId":"5fa4ca3a3a1f08a5a709d9be"
}

# bulk_create_students_nova(data)


def generate_password(_id):
    salt = urandom(16)
    base64_salt = base64.b64encode(salt)
    # password = str(randint(10000, 99999))
    password = '9242'
    print('password::',password)
    byte_password = password.encode()
    pass_hash = pbkdf2_hmac("sha1", byte_password, salt, 10000, 64)
    base64_pass = base64.b64encode(pass_hash)
    user_docs = []
    doc = {
        "email": _id,
        "password": base64_pass.decode(),
        "salt": base64_salt.decode()
    }
    print(doc)
    user_docs.append(doc)
    # print('docs::',user_docs)
    doc = mongo_db._upsert_many(
        connection=None,
        db_name=tenant_registry,
        collection_name="users",
        obj=user_docs,
        id_field="email",
        upsert=False,
    )


# generate_password("teacher101@getranks.in")


def bulk_update_passwords():
    user_docs=[]
    pwd_docs = []
    s_docs = []
    with open('./MS College - New Students-12.csv') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=",")
        csv_data = list(csv_reader)
        for student in csv_data:
            
            _id = f'5fa4ca3a3a1f08a5a709d9be_{student["Student Id"].upper()}'
            print('_id::',_id)
            salt = urandom(16)
            base64_salt = base64.b64encode(salt)
            # password = str(randint(10000, 99999))
            password = str(student['Password'])
            print('password::',password)
            byte_password = password.encode()
            pass_hash = pbkdf2_hmac("sha1", byte_password, salt, 10000, 64)
            base64_pass = base64.b64encode(pass_hash)
            doc = {
                "_id": _id,
                "password": base64_pass.decode(),
                "salt": base64_salt.decode()
            }
            user_docs.append(doc)
            student['Password'] = password
            pwd_docs.append(student)            
        doc = mongo_db._upsert_many(
            connection=None,
            db_name=tenant_registry,
            collection_name="users",
            obj=user_docs,
            id_field="_id",
            upsert=False,
        )

    # with open('./students.csv', mode='w') as csv_file:
    #     fieldnames = ['Student Id','Name','Password','Orientation','Class','State','City','Campus','Section']
    #     writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    #     writer.writeheader()
    #     writer.writerows(pwd_docs)

bulk_update_passwords()

def bulk_update_email():
    docs = []
    with open('./New Zoom ids MLE IOE.csv') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=",")
        csv_data = list(csv_reader)
        for student in csv_data:
            doc ={
                    "email": student['email'].lower(),
                    "subject": student['subject'].split(","),
                    "username": student['name']
                }
            docs.append(doc)
        print(len(docs))
        doc = mongo_db._upsert_many(
        connection=None,
        db_name=tenant_registry,
        collection_name="users",
        obj=docs,
        id_field="email",
        upsert=False,
        )
    print(doc)
# bulk_update_email()           

def get_ids():
    with open('./MS Deletioon List_07_08_2021.csv') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=",")
        csv_data = list(csv_reader)
        docs = []
        lookup = {}
        for student in csv_data:
            query = {
                    "hierarchyName": student["Section"],
                    "instituteId": "5fa4ca3a3a1f08a5a709d9be",
            }
            if query['hierarchyName'] not in lookup:
                print(query['hierarchyName'])
                node = list(arango_db.find(query))
                if not node:
                    lookup[query["hierarchyName"]] = {}
                else:
                    lookup[query["hierarchyName"]] = node[0]
            hierarchy = lookup[query["hierarchyName"]]
            doc = {}
            if hierarchy:
                if(hierarchy['level']==5 or hierarchy['level']==6 ):
                    doc['id'] = hierarchy['id']
                    docs.append(doc)
    with open('./ms_hierarchy_ids_4.csv', mode='w') as csv_file:
        fieldnames = ['id']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(docs)
     
# get_ids()        
            
  
def update_username():
    docs=[]
    users = mongo_db.find(tenant_registry,"users",{"instituteId":'608d0036e369128d6947c4dc',"role":'LEARN_SCHEDULE_CREATOR',"studentId":{"$exists":False},"username":""})
    for student in users:
        if student:
            doc ={
                "_id":student["_id"],
                "username": student['email'].split("@")[0]
            }
            docs.append(doc)
        else:
            print(_id)
        # print(docs)
    doc = mongo_db._upsert_many(
        connection=None,
        db_name="users",
        collection_name="users",
        obj=docs,
        id_field="_id",
        upsert=False,
    )
    print(doc) 
# update_username()             

def get_users():
    docs = []
    no =[]
    with open('./Student_Info (2).csv') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=",")
        csv_data = list(csv_reader)
        for student in csv_data:
            _id = f'5fa4ca3a3a1f08a5a709d9be_{student["Student ID"].upper()}'
            user = mongo_db.find_one(tenant_registry,"users",{"_id":_id})
            if user:
                print('ok')
            else:
                no.append({"_id":_id})
    with open('./no_students.csv', mode='w') as csv_file:
        fieldnames = ['id']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(no)       

# get_users()