
import csv
import requests
import json
from db.HydraMongoDb import HydraMongoDb
from os import urandom
import base64
from hashlib import pbkdf2_hmac
from app import create_app
import json

application = create_app()

mongo_db = HydraMongoDb(application.config['MONGO_URI'])
test_database = application.config["TEST_DATABASE"]

admin_login_url = "https://account.getranks.in/auth/local"
test_url = "https://rest.getranks.in/tests?hierarchy=0ec8b37ddf370b168572721970090f30,5d59d69909dbf7a25fc8bae4efe53ba1,09ddea9889a46e63a62e0158c55ba30b&status=completed&startTime=2021-08-05+00:00&endTime=2021-08-12+00:00&skip=0&size=10"

headers = {
            "Content-type": "application/json",
            "Accept": "text/plain",
}
user_doc = {
    "email": 'msioemeh@gmail.com',
    "rememberMe": False,
    "hostname":"getranks.in",
    "password":'egnify'
}
res = {}
snap_docs = []
login_resp = requests.post(
            admin_login_url,
            data=json.dumps(user_doc),
            headers=headers, 
            )
if login_resp.ok:
    res = login_resp.json()  
    headers = {
        "Content-type": "application/json",
        "Accept": "text/plain",
        "accesscontroltoken": res.get("accessControlToken"),
        "Authorization": res.get("token"),
    }
    test_resp = requests.get(
        test_url,
        headers=headers,
    )
    if test_resp.ok:
        test_list = test_resp.text
        test_list = json.loads(test_list)
        print(test_list)
        print(len(test_list.values()))
        for test in test_list.values():
            print(test)
            test_id = test['_id']
            students = mongo_db.find(test_database,'testStudentSnapshot',{'testId':test_id,'syncStatus':True})
            for s in students:
                print(s['studentId'])
                doc = {
                    'testId': s['testId'],
                    'StudentId':s['studentId'],
                    'device': s.get('deviceType',None),
                    'version': s.get('version',None)
                }
                snap_docs.append(doc)
        print('success')
        # print(test_resp)
        
    else:
        print(test_resp.text)
        print('failure')
else:
    print('admin Login') 
    print(login_resp)

with open('./students_test_data.csv', mode='w') as csv_file:
        fieldnames = ['testId','StudentId','device','version']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(snap_docs)