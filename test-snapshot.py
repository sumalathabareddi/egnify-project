from db.HydraArangoDb import HydraArangoDb
from db.HydraMongoDb import HydraMongoDb
from bson.objectid import ObjectId

from app import create_app

application = create_app()

# mongo_db = HydraMongoDb(application.config['MONGO_URI'])
mongo_db = HydraMongoDb("mongodb://root:5eqcMZQCPXtG8MvU@localhost:27018/?authenticationDatabase=admin")
arango_db = HydraArangoDb(
    application.config["ARANGODB_URI"],
    application.config["ARANGODB_USERNAME"],
    application.config["ARANGODB_PASSWORD"],
)
settings_database = application.config["SETTINGS_DATABASE"]
tenant_registry = application.config["TENANT_REGISTRY_DATABASE"]
test_database = application.config["TEST_DATABASE"]
question_db = application.config["QUESTION_BANK_DB"]

# hostname = application.config["HOST_NAME"]
hostname = 'getranks.in'

def get_test(test_id):

    query = {
            "testId": test_id
        }
    docs = mongo_db.find_one(test_database,"tests",query)
    print('docs:',docs["couchSync"])
    if "couchSync" in docs:
        print('test not found!', docs["couchSync"])

    else:
        print(f'{docs}')
        
    
def get_users(test_id):
    query = {
        'questionPaperId':f"QP{test_id}",
        'testId':test_id
    }
    users_assigned = mongo_db.get_results(
        question_db,
        "question_papers",
        query,
        key="userId",
        projection={"testId": 1, "questionPaperId": 1, "userId": 1},
    )

    print(f'{users_assigned}')
    for key,user in users_assigned.items():
        print('userId::',key)




def get_keys(question_paper_id):
    
    query = {"questionPaperId": question_paper_id}
    results = list(
            mongo_db.connection[test_database]
            .get_collection("questions")
            .aggregate(
                [
                    {"$match": query},
                    {
                        "$lookup": {
                            "from": "questions-list",
                            "localField": "questionNumberId",
                            "foreignField": "_id",
                            "as": "question",
                        }
                    },
                    # {"$sample": {"size": 1}},
                    {"$unwind": {"path": "$question"}},
                ]
            )
        )
    print('len',len(results))
    for each_q in results:
        q = each_q["question"]
        if "key" in q:
            print('key',q["key"])
            # print('qno',each_q["qno"])
            # print('_id',q["_id"])
        else:
            print('_id',q["answer"])
        


        
        





    
# get_test("1607909031071")
#get_users("1607909031071")

get_keys("QP1607771457568") 

# get_keys("QP1607832292965")#practies test
