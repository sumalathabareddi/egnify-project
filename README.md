## Introduction
This project is GetRanks backend written in `python`. This repo contains the code for API's and worker.

## Prerequisites
Make sure to install `python3` on your host machine. Since version `3.3` python ships with inbuilt `venv` which can be used to create virtual enviroments. Read more about it [here](https://docs.python.org/3/library/venv.html).


### Installing deps
* Install internal egnify-analytics dependency. Run `python -m pip install git+ssh://git@gitlab.com/Lyra-egnify/egnify-analytics.git@master`. Note: You can set the branch/tag/commit of which you wish to install as a package.
* Install the external dependencies. Run `python -m pip install -r requirements.txt`.

### Setting up env var for local setup
* `export PORT=9090`
* `export APPLICATION_SETTINGS=config/development.cfg`
* `export GOOGLE_APPLICATION_CREDENTIALS=Egnify-Product-9de40bf5e2a8.json`
* `export AWS_DEFAULT_REGION=us-west-2`

### Worker

Worker process is [rq](https://python-rq.org/) job queue that listens to the Redis queue and performs the submitted jobs. To start run `python worker.py`.

### Flask API Service
Flask API service exposes API's. To start run `python main.py`.
