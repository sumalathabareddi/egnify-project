# Use analytics as base image
FROM registry.gitlab.com/lyra-egnify/egnify-analytics/master:latest

LABEL maintainer = "Vikram Somavaram"

# Set work directory
WORKDIR /usr/src/app

# Copy all the code base to the image
COPY . .

# Copy GCP credentials file
COPY Egnify-Product-9de40bf5e2a8.json /usr/local/Egnify-Product-9de40bf5e2a8.json

# Install required packages
RUN pip install -r requirements.txt

# Run the application
ENTRYPOINT [ "sh", "/usr/src/app/docker-entry-points/flask-app.sh" ]
