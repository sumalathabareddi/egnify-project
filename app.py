import os

from utils.egy_analytics import cache
from db.HydraCouchDb import HydraCouchDb
from db.HydraMongoDb import HydraMongoDb
from db.HydraArangoDb import HydraArangoDb

# from db.HydraFirestore import HydraFirestore
from flask import Flask
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint
from werkzeug.utils import find_modules, import_string
import rq_dashboard
import rq_scheduler_dashboard


def register_blueprints(application):
    """Registers all blueprints found in api module.
    Uses werkzeug.utils find_modules and import_string to register blueprints.
    """
    for name in find_modules("api"):
        mod = import_string(name)
        if hasattr(mod, "blueprint"):
            application.register_blueprint(mod.blueprint)


def register_db(application):
    """Registers DB resources as attributes for flask app object.

    This approach is better because it will be expensive to recreate new service connections on every request.

    Whenever you want to use these resources just import current_app from flask application contexts and use current_app.db_resource.
    """
    application.mongo_db = HydraMongoDb(application.config["MONGO_URI"])
    application.question_bank_mongo_db = HydraMongoDb(
        application.config["QUESTION_BANK_MONGO_URI"]
    )
    application.couch_db = HydraCouchDb(application.config["COUCHDB_URI"])
    application.arango_db = HydraArangoDb(
        application.config["ARANGODB_URI"],
        application.config["ARANGODB_USERNAME"],
        application.config["ARANGODB_PASSWORD"],
    )
    # application.firestore = HydraFirestore(
    #     application.config["FIREBASE_SERVICE_ACCOUNT_FILE_PATH"]
    # )


def register_swagger_blueprint(application):
    """Registers swagger api documentation blueprint.
    """
    SWAGGER_URL = "/swagger"
    API_URL = "/static/swagger.yml"
    swaggerui_blueprint = get_swaggerui_blueprint(
        SWAGGER_URL,
        API_URL,
        config={"app_name": "Nova Rest Services", "swagger.pretty.print": True},
    )
    application.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)


def register_rq_dashboard_blueprint(application):
    """Registers rq dashboard blueprint.
    """
    import warnings

    warnings.warn("This is public at the moment. Please take some action on this.")
    application.config.from_object(rq_dashboard.default_settings)
    application.register_blueprint(rq_dashboard.blueprint, url_prefix="/rq-dashboard")


def register_rq_scheduler_dashboard_blueprint(application):
    """Registers rq scheduler dashboard blueprint.
    """
    import warnings

    warnings.warn("This is public at the moment. Please take some action on this.")
    application.config.from_object(rq_scheduler_dashboard.default_settings)
    application.register_blueprint(
        rq_scheduler_dashboard.blueprint, url_prefix="/rq-scheduler-dashboard"
    )


def create_app():
    """Creates application with given settings.

    This greatly helps in testing. You can have instances of the application with different settings to test every case.

    This is reading the configuration from env var, but to fully utilize benefits of this pattern we need to pass config as a param.
    # TODO: Pass config as a param.
    """
    os.environ["MARSHMALLOW_SCHEMA_DEFAULT_JIT"] = "toastedmarshmallow.Jit"
    SENTRY_DSN=os.getenv('SENTRY_DSN')
    if SENTRY_DSN:
        import sentry_sdk
        from sentry_sdk.integrations.flask import FlaskIntegration

        sentry_sdk.init(
            dsn=SENTRY_DSN,
            integrations=[FlaskIntegration()]
        )
    application = Flask(__name__, template_folder="templates")
    application.config.from_envvar("APPLICATION_SETTINGS")
    for env_var in os.environ:
        application.config[env_var] = os.getenv(env_var)
    if application.config["RQ_REDIS_URL"]:
        application.config["RQ_DASHBOARD_REDIS_URL"] = application.config[
            "RQ_REDIS_URL"
        ]
        application.config["RQ_SCHEDULER_DASHBOARD_REDIS_URL"] = application.config[
            "RQ_REDIS_URL"
        ]

    cache.init_app(application)
    CORS(application, max_age=3600)
    register_blueprints(application)
    register_db(application)
    register_swagger_blueprint(application)
    register_rq_dashboard_blueprint(application)
    register_rq_scheduler_dashboard_blueprint(application)
    # Add root node to graph if it doesn't exist already
    root = {
        "active": True,
        "isLeadNode": False,
        "instituteId": "",
        "instituteLevelCode": "",
        "hierarchyName": "root",
        "_id": "root",
        "parentCode": "",
        "level": 0,
    }
    if not application.arango_db.has_node(root["_id"]):
        application.arango_db.create_node(root["_id"], root)

    return application
