import redis
from rq import Worker, Queue, Connection
from app import create_app
import importlib

application = create_app()

listen = application.config["RQ_LISTEN_QUEUES"]
conn = redis.Redis.from_url(application.config["RQ_REDIS_URL"])

if __name__ == "__main__":
    with Connection(conn):
        qs = map(Queue, listen)
        generate_analysis = importlib.import_module(application.config["ANALYSIS_FILE"])
        worker = Worker(
            qs,
            exception_handlers=[generate_analysis.clean_test_records],
            disable_default_exception_handler=False,
        )
        worker.work()
