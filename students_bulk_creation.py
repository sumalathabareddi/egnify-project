import base64
import csv
import copy
from datetime import datetime
from hashlib import pbkdf2_hmac
from os import urandom
from random import randint

import redis
import requests
import rq
from db.HydraArangoDb import HydraArangoDb
from db.HydraMongoDb import HydraMongoDb
from rq import Queue

from api.models.students import StudentSchema
from app import create_app
from bulkread.BulkRead import StudentsBulkRead, StudentsBulkReadV2

application = create_app()

mongo_db = HydraMongoDb(application.config["MONGO_URI"])
arango_db = HydraArangoDb(
    application.config["ARANGODB_URI"],
    application.config["ARANGODB_USERNAME"],
    application.config["ARANGODB_PASSWORD"],
)
settings_database = application.config["SETTINGS_DATABASE"]
tenant_registry = application.config["TENANT_REGISTRY_DATABASE"]
hostname = application.config["HOST_NAME"]
redis_connection = redis.Redis.from_url(application.config["RQ_REDIS_URL"])
queue = Queue(application.config["RQ_QUEUES"], connection=redis_connection)


def create_accounts(students, headers, current_job):
    resp_docs = []
    user_docs = []
    total_students = len(students)
    headers.append('Password')
    for index, student in enumerate(students, 1):
        # Create salt and password
        salt = urandom(16)
        base64_salt = base64.b64encode(salt)
        password = str(randint(10000, 99999))
        byte_password = password.encode()
        pass_hash = pbkdf2_hmac("sha1", byte_password, salt, 10000, 64)
        base64_pass = base64.b64encode(pass_hash)
        resp_doc = copy.deepcopy(student)
        resp_doc["password"] = password
        student.pop("hierarchyId", None)
        resp_docs.append(resp_doc)
        # Create account doc
        student_hierarchy = {
            "childCode": student["accessTag"]["hierarchy"],
        }
        doc = {
            "_id": student["_id"],
            "role": ["STUDENT", "LMS_OTP_ACCESS"],
            "forgotPassSecureHash": "",
            "passwordChange": True,
            "active": True,
            "hierarchy": [student_hierarchy],
            "email": student["email"].lower(),
            "password": base64_pass.decode(),
            "hostname": hostname,
            "instituteId": student["instituteId"],
            "studentId": student["studentId"],
            "studentName": student["studentName"],
            "username": student["studentName"],
            "egnifyId": student["egnifyId"],
            "salt": base64_salt.decode(),
            "forgotPassSecureHashExp": str(datetime.utcnow()),
        }

        user_docs.append(doc)
        current_job.meta["creating"] = index
        current_job.meta["total"] = total_students
        current_job.save_meta()

    current_job.meta["step3"] = "Wrinting to database"
    current_job.save_meta()
    mongo_db.insert_many(settings_database, "studentInfo", students)
    mongo_db.insert_many(tenant_registry, "users", user_docs)
    current_job.meta["step4"] = "Completed"
    current_job.meta["filedata"] = {"headers": headers,  "rows": resp_docs}
    current_job.save_meta()


def bulk_create_students(data):
    url = data["fileUrl"]
    institute_id = data["instituteId"]
    current_job = rq.get_current_job()
    with requests.Session() as s:
        downloaded_data = s.get(url)
        decoded_data = downloaded_data.content.decode("utf-8")

        csv_reader = csv.DictReader(decoded_data.splitlines(), delimiter=",")
        headers = csv_reader.fieldnames
        csv_data = list(csv_reader)
        current_job.meta["step1"] = "Completed reading students csv data"
        current_job.save_meta()
        sbr = StudentsBulkReadV2(csv_data, headers, institute_id)
        errors = sbr.validate()
        if errors:
            current_job.meta["errors"] = errors
            current_job.save_meta()
            return
        docs = sbr.read_students()
        result = StudentSchema(many=True).load(docs)
        if result.errors:
            current_job.meta["errors"] = errors
            current_job.save_meta()
            return
        else:
            errors, students = [], []
            lookup = {}
            for row, student in enumerate(result.data, 1):
                # Make sure node with student["accessTag"]["hierarchy"] exists,
                # Raise error if it doesn't exists
                query = {
                    "viewId": student["accessTag"]["hierarchy"],
                    "instituteId": institute_id,
                }
                student["hierarchyId"] = student["accessTag"]["hierarchy"]
                # Maintain a lookup for prev visited hierarchies
                if query["viewId"] not in lookup:
                    node = list(arango_db.find(query))
                    lookup[query["viewId"]] = node
                hierarchy = lookup[query["viewId"]]
                if not hierarchy:
                    errors.append(f"Coudn't find given hierarchy id in row {row}")
                else:
                    student["accessTag"]["hierarchy"] = hierarchy[0]["id"]
                    students.append(student)
            if errors:
                current_job.meta["errors"] = errors
                current_job.save_meta()
                return
            ids = [x["_id"] for x in students]
            query = {"_id": {"$in":  ids}}
            conflict_students = mongo_db.find(
                tenant_registry, "users", query, projection={"studentId": 1}
            )
            if conflict_students:
                conflict_ids = [x["studentId"] for x in conflict_students]
                current_job.meta["errors"] = [f"Student Ids already present {conflict_ids}"]
                current_job.save_meta()
                return
            current_job.meta["step2"] = "Validated the students data"
            current_job.save_meta()
            create_accounts(students, headers, current_job)
