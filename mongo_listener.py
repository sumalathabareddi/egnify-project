from db.HydraMongoDb import HydraMongoDb
from app import create_app
from api.models.calender import CalenderSchedule

application = create_app()
mongodb = HydraMongoDb(application.config["MONGO_URI"])
ga_database = application.config["GA_DATABASE"]
settings_database = application.config["SETTINGS_DATABASE"]
ga_mongo_db = mongodb.connection[ga_database]
# settings_mongo_db = mongodb.connection[settings_database]
type_obj = {
    'tests':'test',
    'assignments':'assignment',
    'learnschedule': 'live'
}
print('suma')
with ga_mongo_db.watch([],full_document="updateLookup") as stream:
    for change_stream in stream:
        print(change_stream)
        if change_stream:
            op_type = change_stream.get("operationType",None)
            print('opType::',op_type)
            full_document =  change_stream.get("fullDocument")
            db_info = change_stream.get('ns')
            coll = db_info.get("coll")
            collections = ["assignments","tests","learnschedule"]
            collection_name = "schedules"
            if coll in collections:
                schedule_doc = {
                    "type": type_obj.get(coll),
                    "schedule_id": full_document["_id"],
                    "name": full_document.get('name',None),
                    "active": full_document.get('active',None),
                    "hierarchy": full_document.get("hierarchy",None),
                    "startTime": full_document.get('startTime',None),
                    "endTime": full_document.get('endTime',None)
                }
                if coll == "tests":
                    schedule_doc['name'] = full_document.get("testName")
                if coll == "assignments":
                    schedule_doc['startTime'] = full_document.get('start_time',None)
                    schedule_doc['hierarchy'] = full_document.get('hierarchies',None)
                if op_type == "insert":
                    mongodb.insert_one(ga_database,collection_name,schedule_doc) 
                elif op_type == 'update':             
                    query = {"schedule_id": schedule_doc["schedule_id"]}
                    mongodb.find_one_and_upsert(ga_database,collection_name,query,schedule_doc,upsert=False) 
                elif op_type == "delete":
                    mongodb.delete(ga_database,collection_name,{"schedule_id":full_document["_id"]})               
            
with settings_mongo_db.watch([],full_document="updateLookup") as stream:
    for change_stream in stream:
        # print(change_stream)
        if change_stream:
            op_type = change_stream.get("operationType",None)
            print('opType::',op_type)
            full_document =  change_stream.get("fullDocument")
            db_info = change_stream.get('ns')
            coll = db_info.get("coll")
            collections = ["learnschedule"]
            collection_name = "schedules"
            if coll in collections:
                schedule_doc = {
                    "type": type_obj.get(coll),
                    "schedule_id": full_document["_id"],
                    "name": full_document.get('name',None),
                    "active": full_document.get('active',None),
                    "hierarchy": full_document.get("hierarchy",None),
                    "startTime": full_document.get('startTime',None),
                    "endTime": full_document.get('endTime',None)
                }
                if op_type == "insert":
                    mongodb.insert_one(ga_database,collection_name,schedule_doc) 
                elif op_type == 'update':             
                    query = {"schedule_id": schedule_doc["schedule_id"]}
                    mongodb.find_one_and_upsert(ga_database,collection_name,query,schedule_doc,upsert=False) 
                elif op_type == "delete":
                    mongodb.delete(ga_database,collection_name,{"schedule_id":full_document["_id"]})     