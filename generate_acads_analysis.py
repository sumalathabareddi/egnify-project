import multiprocessing
import time

import rq

from app import create_app
from db.HydraMongoDb import HydraMongoDb
from hierarchy.HierarchyResult import HierarchyResult
from legacy.acadsGA import generateAcadsGAObjects

application = create_app()


def getHierarchyDetails(db, database_to_use, testId):
    """

    :param db:
    :param database_to_use:
    :param testId:
    :return:
    """
    details = db.get_results(
        database_to_use,
        "institutehierarchies",
        {},
        "childCode",
        0,
        {
            "childCode": 1,
            "child": 1,
            "level": 1,
            "isLeafNode": 1,
            "parent": 1,
            "parentCode": 1,
            "active": 1,
            "_id": 0,
        },
    )
    return details


def get_master_results(results):
    """

    :param results:
    :return:
    """
    masterResults = []
    for result in results:
        masterDict = result.__dict__
        masterDict["_id"] = result.id
        masterResults.append(masterDict)
    return masterResults


mongodb = HydraMongoDb(application.config["MONGO_URI"])
ga_database = application.config["GA_DATABASE"]
master_results_table = application.config["MASTER_RESULTS_TABLE"]


def hello_gcs_generic(data):
    """

    :param data:
    """
    ticks = time.time()
    testId = data["test_id"]
    current_job = rq.get_current_job()
    regenerate = data.get("regenerate", False)
    if regenerate:
        delete_generated_test_records(testId)
    manager = multiprocessing.Manager()
    error_queue = manager.list()
    results = generateAcadsGAObjects(mongodb, testId)
    ticks1 = time.time()
    current_job.meta["step1"] = ticks1 - ticks
    current_job.save_meta()
    hierarchy = HierarchyResult()
    hierarchy.caliculate_hierarical_analysis(results)
    ticks2 = time.time()
    current_job.meta["step2"] = ticks2 - ticks1
    current_job.save_meta()
    hierarchyDetails = getHierarchyDetails(mongodb, ga_database, "")
    hierarchy.aggregate_results(testId, hierarchyDetails, error_queue)
    ticks3 = time.time()
    if error_queue:
        print(error_queue)
        raise Exception("Exception in running analysis")
    current_job.meta["step3"] = ticks3 - ticks2
    current_job.save_meta()
    if error_queue:
        print(error_queue)
        raise Exception("Exception in running analysis")
    mongodb.insert_many_in_process(
        ga_database, master_results_table, get_master_results(results)
    )
    mongodb.insert_many_in_process(
        ga_database, "hierarchicalAnalysis", list(hierarchy.hierarchyResultMap.values())
    )
    mongodb.insert_many_in_process(
        ga_database, "rankAnalysis", list(hierarchy.hierarchyRankList.values())
    )
    mongodb.insert_many_in_process(
        ga_database, "markAnalysis", list(hierarchy.hierarchyMarksList.values())
    )
    mongodb.wait_for_termination()
    ticks4 = time.time()
    current_job.meta["step4"] = ticks4 - ticks3
    current_job.save_meta()


def delete_generated_test_records(test_id):
    """

    :param test_id:
    """
    mongodb.delete(ga_database, "rankAnalysis", {"testId": test_id})
    mongodb.delete(ga_database, "markAnalysis", {"testId": test_id})
    mongodb.delete(ga_database, "hierarchicalAnalysis", {"testId": test_id})


def clean_test_records(job, exc_type, exc_value, traceback):
    """

    :param job:
    :param exc_type:
    :param exc_value:
    :param traceback:
    """
    (method_args,) = job.args
    testId = method_args["test_id"]
    delete_generated_test_records(testId)
