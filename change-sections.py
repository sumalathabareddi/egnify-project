import base64
import csv
from datetime import datetime
from hashlib import pbkdf2_hmac
from os import urandom
from random import randint
from flask import helpers
from utils.egy_analytics import cache


import redis
import requests
import rq
from db.HydraArangoDb import HydraArangoDb
from db.HydraMongoDb import HydraMongoDb
from rq import Queue
from utils.EgnifyException import MultipleValidationExceptions, ValidationException



from api.models.students import StudentSchema
from app import create_app
from bulkread.BulkRead import StudentsBulkRead, StudentsBulkReadV2
from utils.StorageHelper import StorageHelper
import itertools
from datetime import datetime, timezone
from bson.objectid import ObjectId



application = create_app()

mongo_db = HydraMongoDb(application.config['MONGO_URI'])
arango_db = HydraArangoDb(
    application.config["ARANGODB_URI"],
    application.config["ARANGODB_USERNAME"],
    application.config["ARANGODB_PASSWORD"],
)
settings_database = application.config["SETTINGS_DATABASE"]
tenant_registry = application.config["TENANT_REGISTRY_DATABASE"]
hostname = application.config["HOST_NAME"]
redis_connection = redis.Redis.from_url(application.config["RQ_REDIS_URL"])
queue = Queue(application.config["RQ_QUEUES"], connection=redis_connection)
storage_provider = application.config["STORAGE_PROVIDER"]
storage_secrets = {}
storage_secrets["GCP_CREDENTIALS_FILE"] = application.config["GCP_CREDENTIALS_FILE"]
bucket_name = application.config["CLOUD_STORAGE_BUCKET"]


def format_data(file_path,id_prefix):
    docs=[]
    with open(file_path) as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=",")
        csv_data = list(csv_reader)
        for row in csv_data:
            if id_prefix:
                row['Student Id'] = f'{id_prefix}{row["Student Id"]}'.upper()
            else:
                row['Student Id'] = f'{row["Student Id"]}'.upper()
            if "MS.ACADEMY" in row['Student Id']:
                row['Email'] = row["Student Id"].lower()
            else: 
                row['Email'] = f'{row["Student Id"]}@getranks.in'.lower()
            row['Orientation'] = f'{row["Orientation"]}'.strip()
            if row['Class'] =='Class LONG TERM':
                row['Class'] = 'Class 13'
            else:
                row['Class'] = f'{row["Class"]}'.strip()
            row['State'] = f'{row["State"]}'.strip()
            row['City'] = f'{row["City"]}'.strip()
            row['Campus'] = f'{row["Campus"]}'.strip()
            row['Section'] = f'{row["Section"]}'.strip()
            print(row['Email'])
            docs.append(row)


    with open('./students.csv', mode='w') as csv_file:
        fieldnames = ['Student Id','Student Name','Father Name','Phone','Email','DOB','Gender','Category','Orientation','Class','State','City','Campus','Section']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(docs)


def upload_file(file_path):
    with open(file_path) as uploaded_file:
        uploaded_file_string = uploaded_file.read()
        uploaded_file_string =uploaded_file_string.encode('utf-8')
        storage_helper = StorageHelper(storage_provider, storage_secrets)
        extention = 'csv'
        data = {
            "file_ext": f".{extention}",
            "file_name": file_path.split('/')[1],
            "data": uploaded_file_string,
            "content_type": 'text/csv',
            "bucket_name": bucket_name,
            "content_disposition": "inline",
        }

        storage_helper = StorageHelper(storage_provider, storage_secrets)

        url = storage_helper.upload(data, get_public_url=True)
        print(url)
        return url
def move_students(students):
    collection_name = "hierarchy-ledger"
    ids = [x["egnifyId"] for x in students]
    query = {"egnifyId": {"$in": ids}, "isCurrentHierarchy": True}
    update = {
        "$set": {"isCurrentHierarchy": False, "endTime": datetime.now(timezone.utc)}
    }
    coll = mongo_db.connection[settings_database].get_collection(collection_name)
    coll.update_many(query, update)
    cache_keys, new_docs = [], []
    for student in students:
        doc = {
            "_id": str(ObjectId()),
            "startTime": datetime.now(timezone.utc),
            "hierarchy": student["accessTag"]["hierarchy"],
            "isCurrentHierarchy": True,
            "egnifyId": student["egnifyId"],
            "studentId": student["studentId"],
        }
        new_docs.append(doc)
        cache_keys.append(f"view//students/{student['egnifyId']}")
        cache_keys.append(student["egnifyId"])
        cache_keys.append(f"user_token_{student['egnifyId']}")
    # cache.delete_many(*cache_keys)
    mongo_db.insert_many(settings_database, collection_name, new_docs)
    print('done')


def create_accounts(students):
    total_students = len(students)
    user_docs=[]
    for index, student in enumerate(students, 1):
        # Create account doc
        student_hierarchy = {
            "childCode": student["accessTag"]["hierarchy"],
        }
        doc = {
            "_id": student["_id"],
            "hierarchy": [student_hierarchy]
        }
        user_docs.append(doc)
    print(f'{len(user_docs)}')
    doc = mongo_db._upsert_many(
        connection=None,
        db_name=tenant_registry,
        collection_name="users",
        obj=user_docs,
        id_field="_id",
        upsert=False,
    )
    print('done::::::::')
    move_students(students)

def bulk_create_students_nova(data):
    print(f'data::{data}')
    format_data(data['filePath'],data['id_prefix'])
    print('data format completed!')
    data['fileUrl'] = upload_file('./students.csv')
    print('file  uploaded')
    url = data["fileUrl"]
    institute_id = data["instituteId"]
    print(hostname)
    collection_name = "studentInfo"
    institute_details = mongo_db.find_one(
        settings_database, "institutes", {"_id": institute_id}
    )
    with requests.Session() as s:
        downloaded_data = s.get(url)
        decoded_data = downloaded_data.content.decode("utf-8")
        csv_reader = csv.reader(decoded_data.splitlines(), delimiter=",")
        csv_headers = next(csv_reader)
        csv_data = list(csv_reader)
        print("Completed reading students csv data")
        system_hierarchies = mongo_db.find(
            settings_database, "systemhierarchies", query={}
        )
        sbr = StudentsBulkRead(
            csv_headers, csv_data, institute_details, system_hierarchies
        )
        errors = sbr.validate()
        if errors:
            print(f'error:::validation::{errors}')
            raise MultipleValidationExceptions(errors)
        docs = sbr.create_students()
        # result = StudentSchema(many=True).load(docs)
        result = []
        if result:
            print(f'error:::schema:::{result.errors}')
        else:
            errors, students = (
                [],
                [],
            )
            lookup = {}
            for row, student in enumerate(docs, 1):
                """
                    Make sure node with student["accessTag"]["hierarchy"] exists,
                    raise error if it doesn't exists
                """
                query = {
                    "id": student["accessTag"]["hierarchy"],
                    "instituteId": institute_id,
                }
                if query['id'] not in lookup:
                    print(query['id'])
                    node = list(arango_db.find(query))
                    if not node:
                        lookup[query["id"]] = {}
                    else:
                        lookup[query["id"]] = node[0]
                hierarchy = lookup[query["id"]]
                if not hierarchy:
                    errors.append(f"Coudn't find given hierarchy id in row {row}")
                else:
                    students.append(student)
            if errors:
                print(f'error:::{errors}')
                raise MultipleValidationExceptions(errors)
            else:
                print('completed reading::')
                doc = mongo_db._upsert_many(
                        connection=None,
                        db_name=settings_database,
                        collection_name="studentInfo",
                        obj=students,
                        id_field="_id",
                        upsert=False,
                    )
                for key,doc in enumerate(students):
                    doc["Orientation"] = csv_data[key][8]
                    doc["Class"] = csv_data[key][9]
                    doc["State"] = csv_data[key][10]
                    doc["City"] = csv_data[key][11]
                    doc["Campus"] = csv_data[key][12]
                    doc['Section'] = csv_data[key][13]
                create_accounts(students)
                

data = {
    "filePath":'./Final - MS - Change Hierarchies  details-07_08_2021_2.csv',
    'id_prefix': '',    
    "instituteId":"5fa4ca3a3a1f08a5a709d9be"
}

bulk_create_students_nova(data)


def add_role():
    
    query = {}
    institutes = mongo_db.find(settings_database, "institutes", query)
    emails = [x['email'] for x in institutes]
    print(len(emails))
    update = {
        "$push": {"role": "PARTNER_ADMIN"}
    }
    for email in emails:
        print(email)
        res= mongo_db.connection[tenant_registry].get_collection("users").update(
            {"email": email}, update
        )
        print(res)
# add_role()
      