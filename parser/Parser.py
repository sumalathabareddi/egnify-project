import json
from dataclasses import dataclass

import requests
from bson.objectid import ObjectId
from utils.EgnifyException import MultipleValidationExceptions, ValidationException
from api.models import Questions, QuestionsList
from utils import md5


@dataclass
class RequestValidator:
    file_url: str = None
    marking_schema: dict = None
    test_id: str = None
    question_paperid: str = None
    question_reg: str = None
    option_reg: str = None
    ext_name: str = None
    test_details: dict = None

    def validate(self):
        list_of_errors = []
        if not self.marking_schema:
            list_of_errors.append(
                ValidationException("400", "marking_schema", None)
            )  # No marking schema
        if not self.file_url:
            list_of_errors.append(
                ValidationException("400", "file_url", None)
            )  # No Url
        if not self.test_id:
            list_of_errors.append(
                ValidationException("400", "test_id", None)
            )  # No TestId
        if not self.question_paperid:
            list_of_errors.append(
                ValidationException("400", "question_paperid", None)
            )  # No Paper Id
        if not self.question_reg or not self.option_reg:  # No Mandatory Fields
            list_of_errors.append(
                ValidationException(
                    "400",
                    "question_reg, option_reg are mandatory fields, one of them is",
                    None,
                )
            )
        if len(list_of_errors):
            raise MultipleValidationExceptions(list_of_errors)


def convert_doc_to_pdf(
    req_form_data,
    url,
    file,
    file_size,
    parser_url,
):
    args = [
        {
            "file": {
                "link": url,
                "options": {
                    "filename": file.filename,
                    "contentType": file.content_type,
                    "knownLength": file_size,
                },
            },
            "markingSchema": req_form_data["markingSchema"],
            "subjects": req_form_data["subjects"],
            "question_reg": req_form_data.get("question_reg"),
            "option_reg": req_form_data.get("option_reg"),
            "no_of_questions": req_form_data.get("no_of_questions"),
            "column1_reg": req_form_data.get("column1_reg", None),
            "column2_reg": req_form_data.get("column2_reg", None),
        }
    ]

    result = requests.post("{}/docxparser/parse".format(parser_url), json=args)
    if not result.ok:
        resp={
            "reason":result.reason,
            "status_code":result.status_code
        }
        return resp
    resp = result.json()
    return resp


def update_test_details(
    question_paper_id,
    req_form_data,
    url,
    extention,
    converted_url=None,
    is_parsed=False,
):
    update_obj = {}
    update_obj["questionNumberFormat"] = req_form_data.get("question_reg")
    update_obj["optionNumberFormat"] = req_form_data.get("option_reg")
    update_obj["questionPaperId"] = [question_paper_id]
    update_obj["is_parsed"] = is_parsed

    if extention == "pdf":
        update_obj["questionPaperUrl"] = url
    else:
        update_obj["docxurl"] = url
        update_obj["questionPaperUrl"] = converted_url

    return update_obj


def create_questions_un_mapped(
    question_paper_id, req_form_data, test_details, questions=None, question_paper=None
):
    marking_schema = json.loads(req_form_data["markingSchema"])
    subjects = test_details["subjects"]
    if questions:
        # Parsed Questions
        questions = {item["qno"]: item for item in questions}
        questions_list = []
        questions_details_list = []
        for key, value in questions.items():
            string = value["question"] + "".join(
                [str(option["optionText"]) for option in value["options"]]
            )
            obj = {
                "questionNumberId": md5(string),
                "qno": str(key),
                "questionPaperId": question_paper_id,
                "parsed": True,
            }
            obj["_id"] = obj["qno"] + "_" + obj["questionPaperId"]
            matrix_exists = value.get("matrix", {})
            if matrix_exists:
                questionTypeMetaData = {"matrix": matrix_exists}
            else:
                questionTypeMetaData = value.get("questionTypeMetaData", {})
            obj["subject"] = value["subject"]
            obj["questionType"] = value["questionType"]

            questions_list_obj = {
                "questionNumberId": obj["questionNumberId"],
                "_id": obj["questionNumberId"],
                "question": value["question"],
                "questionType": value["questionType"],
                "questionTypeMetaData": questionTypeMetaData,
                "options": [option["optionText"] for option in value["options"]],
                "subject": value["subject"],
                "parsed": True,
                "hide": True,
            }

            questions_result = Questions().load(obj)
            if questions_result.errors:
                raise MultipleValidationExceptions(
                    [ValidationException("400", questions_result.errors, None)]
                )
            else:
                questions_list.append(Questions().load(obj).data)

            questions_list_result = QuestionsList().load(questions_list_obj)
            if questions_list_result.errors:
                raise MultipleValidationExceptions(
                    [ValidationException("400", questions_list_result.errors, None)]
                )
            else:
                questions_details_list.append(
                    QuestionsList().load(questions_list_obj).data
                )

        return questions_list, True, questions_details_list, True

    else:
        totalQuestions = marking_schema["totalQuestions"]
        questions_error = []
        questions_list_error = []
        questions_list = []
        questions_details_list = []
        for i in range(1, totalQuestions + 1):
            obj = {}
            obj["questionNumberId"] = str(ObjectId())
            obj["qno"] = str(i)
            obj["questionPaperId"] = question_paper_id
            obj["_id"] = obj["qno"] + "_" + obj["questionPaperId"]

            questions_list_obj = {}
            questions_list_obj["questionNumberId"] = questions_list_obj["_id"] = obj[
                "questionNumberId"
            ]

            derive_from_schema(
                questions_list_obj, i, marking_schema["subjects"], subjects
            )
            obj["subject"] = questions_list_obj["subject"]
            obj["questionType"] = questions_list_obj["questionType"]
            questions_result = Questions().load(obj)
            if questions_result.errors:
                questions_error.append(
                    ValidationException("400", questions_result.errors, None)
                )
            else:
                questions_list.append(Questions().load(obj).data)

            questions_list_result = QuestionsList().load(questions_list_obj)
            if questions_list_result.errors:
                questions_list_error.append(
                    ValidationException("400", questions_list_result.errors, None)
                )
            else:
                questions_details_list.append(
                    QuestionsList().load(questions_list_obj).data
                )
        if questions_error:
            raise MultipleValidationExceptions(questions_error)
        if questions_list_error:
            raise MultipleValidationExceptions(questions_list_error)

        return questions_list, False, questions_details_list, False


def derive_from_schema(questions_list_obj, qno, subjects, subject_sequence):
    for index, subject in enumerate(subjects):
        marks = subject["marks"]
        for mark in marks:
            if qno >= mark["start"] and qno <= mark["end"]:
                questions_list_obj["questionType"] = mark["egnifyQuestionType"]
                questions_list_obj["subject"] = subject_sequence[index]["subject"]
