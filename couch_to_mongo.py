from datetime import datetime, timedelta

from db.HydraMongoDb import HydraMongoDb
from db.HydraCouchDb import HydraCouchDb
from app import create_app
import generate_analysis

application = create_app()
mongodb = HydraMongoDb(application.config["MONGO_URI"])
settings_database = application.config["SETTINGS_DATABASE"]
ga_database = application.config["GA_DATABASE"]
couchDB = HydraCouchDb(application.config["COUCHDB_URI"])
rankguru = application.config.get("IS_RANKGURU", False)
nova = application.config.get("IS_NOVA", False)
questions_database = application.config["QUESTION_BANK_DB"]
test_database = application.config["TEST_DATABASE"]


def on_schedule(data):
    """

    :param data:
    """

    testId = data["test_id"]
    questionPaperId = data["questionPaperId"]
    test_name = data["test_name"]
    tie_breaking_list = data.get("tie_breaking_list", [])

    query = {"testId": testId, "studentId": {"$ne": "null"}}

    if nova:
        query["mode"] = "online"
        query["syncStatus"] = False
    students = []
    no_data = []
    print('query::',query)
    studentDetails = mongodb.get_results(
        ga_database, "testStudentSnapshot", query, key="_id"
    )
    print('steudentDetails::',len(studentDetails))
    for student in studentDetails:
        print('_id::',studentDetails[student]['studentId'])
        try:
            # behaviourData = couchDB.get_elements(
            #     studentDetails[student]["databaseUrl"], object=False
            # )
            b_query={"testId":testId,"studentId":studentDetails[student]['studentId']}
            behaviourData = mongodb.find_one(ga_database,"behaviourData",b_query)['behaviourData']
            students.append(studentDetails[student]["studentId"])
            studentDetails[student]["behaviourData"] = behaviourData
            # behaviourData = couchDB.get_elements(
            #     studentDetails[student]["databaseUrl"], object=False
            # )
            # print('behaviourData::',behaviourData)
            students.append(studentDetails[student]["studentId"])
            studentDetails[student]["behaviourData"] = behaviourData
        except Exception as e:
            print(f'exp:::{e}')
            no_data.append(student)

    for student in no_data:
        del studentDetails[student]
    if no_data:
        mongodb.upsert(
            ga_database,
            "testStudentSnapshot",
            {"_id": {"$in": no_data}},
            {"mode": ""},
            upsert=False,
            multi=True,
            update_operator="$unset",
        )
    print(students)
    print('studentDetails::',studentDetails)
    behaviour_docs = list(studentDetails.values())
    if behaviour_docs:
        mongodb.upsert_many_in_process(ga_database, "behaviourData", behaviour_docs)

    question_keys = getQuestionKeys(questionPaperId)
    
    # print(question_keys)

    if rankguru:
        studentsInfo = mongodb.get_results(
            settings_database,
            "studentInfo",
            {"studentId": {"$in": students}},
            key="studentId",
            projection={
                "hierarchyLevels": 1,
                "hierarchy": 1,
                "_id": 0,
                "studentId": 1,
                "studentName": 1,
            },
        )
        for student in studentsInfo:
            for hierarchy in studentsInfo[student]["hierarchy"]:
                for level in studentsInfo[student]["hierarchyLevels"]:
                    if (
                        studentsInfo[student]["hierarchyLevels"][level]
                        == hierarchy["child"]
                    ):
                        studentsInfo[student]["hierarchyLevels"][level] = hierarchy[
                            "childCode"
                        ]
            del studentsInfo[student]["hierarchy"]

        for _id in studentDetails:
            print(_id)
            student_response = {}
            studentDetails[_id]["hierarchyLevels"] = studentsInfo[
                studentDetails[_id]["studentId"]
            ]["hierarchyLevels"]
            studentDetails[_id]["studentName"] = studentsInfo[
                studentDetails[_id]["studentId"]
            ]["studentName"]

            for response in studentDetails[_id]["behaviourData"]:
                if response != "metaData":
                    student_response[response] = studentDetails[_id]["behaviourData"][
                        response
                    ]["response"]
                    studentDetails[_id]["behaviourData"][response] = {
                        "time": studentDetails[_id]["behaviourData"][response]["time"],
                        "datetime": studentDetails[_id]["behaviourData"][response][
                            "dateTime"
                        ],
                    }

            studentDetails[_id]["responseData"] = {}
            studentDetails[_id]["responseData"]["questionResponse"] = calculateCWU(
                question_keys, student_response
            )
            studentDetails[_id]["syncStatus"] = "completed"

        mongodb.update_many_in_process(
            settings_database,
            "testStudentSnapshot",
            list(studentDetails.values()),
            id_field="_id",
        )

        mongodb.wait_for_termination()

        # CWU CALCULATION DONE HERE
        # GA Starts Here

        params_for_ga = {
            "test_id": testId,
            "test_name": test_name,
            "tie_breaking_list": tie_breaking_list,
        }
        generate_analysis.hello_gcs_generic(params_for_ga)
        # GA Ends Here
    else:
        test_data = mongodb.find_one(ga_database, "tests", {"testId": testId})
        upload_progress_query = {"testId": testId}
        upload_progress = mongodb.find(
            ga_database, "uploadprogress", upload_progress_query, key="hierarchy"
        )
        for _id in studentDetails:
            print(_id)
            student_response = {}
            behaviourData = {}
            metaData = studentDetails[_id]["behaviourData"].get("metaData", None)
            # print('metaData',metaData)
            for response in studentDetails[_id]["behaviourData"]:
                if response != "metaData":
                    Qkey = f"Q{response}"
                    student_response_data = studentDetails[_id]["behaviourData"][
                        response
                    ]
                    student_response[response] = student_response_data.get(
                        "response", []
                    )
                    behaviourData[Qkey] = {
                        "time": student_response_data.get("time", []),
                        "datetime": student_response_data.get("dateTime", None),
                    }
            studentDetails[_id]["behaviourData"] = behaviourData

            for hierarchy in studentDetails[_id]["hierarchyLevels"]:
                if hierarchy in upload_progress:
                    earlier_count = upload_progress[hierarchy]["resultsUploaded"]
                    upload_progress[hierarchy]["resultsUploaded"] = earlier_count + 1

            studentDetails[_id]["responseData"] = {}
            studentDetails[_id]["responseData"]["questionResponse"] = calculateCWU(
                question_keys, student_response
            )
            studentDetails[_id]["syncStatus"] = True
            studentDetails[_id]["status"] = "completed"
            if metaData:
                studentDetails[_id]["startedAt"] = metaData["startTime"]
                started_at = metaData["startTime"]
                if started_at:
                    endtime = datetime.strptime(
                        started_at, "%Y-%m-%dT%H:%M:%S.%fZ"
                    ) + timedelta(minutes=test_data["duration"])
                    studentDetails[_id]["endTime"] = endtime
        snapshot_docs = list(studentDetails.values())
        if snapshot_docs:
            mongodb.update_many_in_process(
                ga_database, "testStudentSnapshot", snapshot_docs, id_field="_id",
            )
        test_collection = "tests"
        mongodb.find_one_and_upsert(
            test_database,
            test_collection,
            {"testId": testId},
            {"couchSync.status": "completed"},
            upsert=False,
        )
        mongodb.update_many_in_process(
            ga_database, "uploadprogress", list(upload_progress.values())
        )
        mongodb.wait_for_termination()


def getQuestionKeys(questionPaperId):
    """

    :param questionPaperId:
    :param student_response:
    :return:
    """
    if rankguru:
        questions_keys = mongodb.get_results(
            settings_database,
            "questions",
            {"questionPaperId": questionPaperId},
            key="qno",
            projection={"key": 1, "qno": 1, "_id": 0, "q_type": 1},
        )
    else:
        questions_keys = mongodb.get_results(
            questions_database,
            "questions",
            {"questionPaperId": questionPaperId},
            key="qno",
            projection={"qno": 1, "_id": 0, "questionNumberId": 1},
        )

        question_numberids = []

        for qno in questions_keys:
            question_numberids.append(questions_keys[qno]["questionNumberId"])

        questions = mongodb.get_results(
            questions_database,
            "questions-list",
            {"questionNumberId": {"$in": question_numberids}},
            key="questionNumberId",
            projection={
                "key": 1,
                "questionNumberId": 1,
                "_id": 0,
                "questionType": 1,
                "answer": 1,
                "range":1
            },
        )
        for qno in questions_keys:
            questionNumberId = questions_keys[qno]["questionNumberId"]
            if (
                "range" in questions[questionNumberId]
                and questions[questionNumberId]["range"]
            ):
                questions_keys[qno]["key"] = questions[questionNumberId]["range"]
            elif (
                "key" in questions[questionNumberId]
                and questions[questionNumberId]["key"]
            ):
                questions_keys[qno]["key"] = questions[questionNumberId]["key"]
            else:
                questions_keys[qno]["key"] = [questions[questionNumberId]["answer"]]

            questions_keys[qno]["q_type"] = questions[questionNumberId]["questionType"]

    return questions_keys


def calculateCWU(questions_keys, student_response):
    """

    :param student_response:
    :param question_key:
    :return:
    """
    cwu = {}
    for key in questions_keys:
        qKey = f"Q{key}"
        student_response[key] = student_response.get(key, [])
        data = hadleQuestionTypes(
            student_response[key],
            questions_keys[key]["key"],
            questions_keys[key]["q_type"],
            questions_keys[key]["qno"],
        )
        if questions_keys[key]["q_type"] == "Matrix match type":
            for col in data:
                if not rankguru:
                    cwu[f"{qKey}{col}"] = data[col]
                else:
                    cwu[f"{key}{col}"] = data[col]
        else:
            if not rankguru:
                cwu[qKey] = data
            else:
                cwu[key] = data
    return cwu


def hadleQuestionTypes(user_key, question_key, q_type, qno):
    print(f'{user_key}::{qno}::{q_type}')
    if "" in user_key or "-" in user_key:
        return "U"
    if q_type == "Numeric type":
        print(f'qno::{qno}:: :: key:::{question_key}')
        user_key = [float(k) for k in user_key]
        question_key = [float(k) for k in question_key]

    elif q_type == "Matrix match type":
        res = {}
        answer_key = {}

        for resp in question_key:
            option = resp["sub"]
            answer = [x.upper() for x in resp["answer"]]
            answer_key[option.lower()] = answer

        user_response = {}

        for response in user_key:
            if response:
                for key in response:
                    user_response[key.lower()] = [x.upper() for x in response[key]]

        for key in answer_key:
            res[key] = "U"
            if key in user_response and user_response[key] and len(user_response[key]):
                res[key] = comparetwoKeys(user_response[key], answer_key[key], q_type)
        return res

    return comparetwoKeys(user_key, question_key, q_type)


def comparetwoKeys(user_key, question_key, q_type):
    if not user_key:
        return "U"

    correctMatchCount = 0

    # user_key = set(user_key)
    # question_key = set(question_key)

    # Special case for anser range only for Numeric type questions.

    if q_type == "Numeric type" and len(question_key) == 2:
        if user_key[0] >= question_key[0] and user_key[0] <= question_key[1]:
            return "C"
        else:
            return "W"

    if len(question_key) < len(user_key):
        return "W"

    if user_key == question_key:
        return "C"

    for key in user_key:
        if key in question_key:
            correctMatchCount = correctMatchCount + 1
        else:
            return "W"

    return "P" + str(correctMatchCount)



data = {
"questionPaperId": "QP1626520041332", 
"studentId": None, 
"test_id": "1626520041332", 
"test_name": "SR JEE MAINS WEEKEND TEST 18-07", 
"tie_breaking_list": []
}
on_schedule(data)
