from locust import HttpLocust, TaskSequence, TaskSet, seq_task, task, exception
from random import randrange
import json
import os
import inspect
import random
import time
import csv

# import requests

pollTime = 60  # number of seconds before pollTimeout
csvCType = "application/vnd.ms-excel"
Student_credentials = "./automated-tests/jeetlogin.csv"
csv_file = "./automated-tests/mocktest30k1.csv"
Hid = "Egni_u001_l11-l21-l32"

credentials = []
with open("./automated-tests/jeetlogin.csv") as Student_credentials:
    csv_reader = csv.reader(Student_credentials, delimiter=",")
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            line_count += 1
        else:
            temp = {
                "studentId": row[0],
                "password": row[1],
            }
            credentials.append(temp)
            line_count += 1


class china(TaskSequence):
    authToken = ""
    authTokenIndex = "token"
    ACToken = ""
    ACTokenIndex = "accessControlToken"
    # loginHost = "luke.dev.lms.egnify.io"
    # testHost = "https://accounts.dev.lms.egnify.io"
    # resultHost = "https://api.dev.lms.egnify.io"
    loginHost = "ui.qa.china.egnify.io"
    testHost = "https://accounts.qa.china.egnify.io"
    resultHost = "https://rest.qa.china.egnify.io"
    resultAPI = "/tests"
    QUploadAPI = ""
    GCSUploadAPI = ""
    currTestId = None
    QURL = None

    def getToken(self):
        """

        :return:
        """
        for i in credentials:
            return {"authorization": self.authToken, "accesscontroltoken": self.ACToken}

    @seq_task(1)
    def login(self):
        """

        """
        for student in credentials:
            email = student["studentId"].lower()
            body = {
                "email": email,
                "password": student["password"],
                "hostname": self.loginHost,
            }
            # print(body)
        with self.client.post(
            self.testHost + "/auth/local", body, name="login", catch_response=True
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                print(content)
                if content[self.authTokenIndex] != None:
                    self.authToken = str(content[self.authTokenIndex])
                    self.ACToken = str(content[self.ACTokenIndex])
                    response.success()

            else:
                response.failure(
                    "Unable to login"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\nEnding User Locust"
                )
                raise exception.StopLocust()

    @seq_task(2)
    def fetchTests(self):
        """

        """
        with self.client.get(
            self.resultHost + self.resultAPI,
            name="fetchTests",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
                # print(content)
            else:
                response.failure(
                    "Unable to fetch subjects"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(3)
    def getTestsCount(self):
        """

        """
        with self.client.get(
            self.resultHost + "/tests/count",
            name="getTestsCount",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
                # print(content)
            else:
                response.failure(
                    "Unable to fetch subjects"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(4)
    def getTestData(self):
        """

        """
        with self.client.get(
            self.resultHost + "/tests/000031",
            name="getTestData",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
                # print(content)
            else:
                response.failure(
                    "Unable to fetch subjects"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(5)
    def getTestStatus(self):
        """

        """
        with self.client.get(
            self.resultHost + "/tests/000031/status",
            name="getTestStatus",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
                # print(content)
            else:
                response.failure(
                    "Unable to fetch subjects"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(6)
    def getFileUrl(self):
        """

        """
        GCSFileDict = {"file": (csv_file.split("/")[1], open(csv_file, "rb"), csvCType)}
        with self.client.post(
            self.resultHost + "/tests/000040/file",
            name="getFileUrl",
            files=GCSFileDict,
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    self.randomURL = content["url"]
                    response.success()
                print("-----------")
                print(self.randomURL)
                print(content)
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(7)
    def getSnapshot(self):
        """

        """
        postdata = {
            "file_url": str(self.randomURL),
            "test_id": "000040",
            "test_name": "JEE MAINS - 20",
            "verify": True,
            "markingSchema": {"totalQuestions": 90},
        }
        headers = self.getToken()
        headers["content-type"] = "application/json"
        print(postdata)
        with self.client.post(
            self.resultHost + "/tests/000040/snapshot",
            name="getsnapshot",
            data=json.dumps(postdata),
            headers=headers,
            catch_response=True,
        ) as response:
            print(response)
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(8)
    def getMarkAnalysisforTest(self):
        """

        """
        with self.client.get(
            self.resultHost + "/analysis/" + Hid + "/test/000031/mark",
            name="getMarkAnalysisforTest",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(9)
    def getRankkAnalysisforStduentTest(self):
        """

        """
        with self.client.get(
            self.resultHost
            + "/analysis/"
            + Hid
            + "/student/189288994/test/000031/rank",
            name="getRankkAnalysisforStduentTest",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(10)
    def getHierarchialAnalysisforTest(self):
        """

        """
        with self.client.get(
            self.resultHost + "/analysis/" + Hid + "/test/000031/data",
            name="getRankAnalysisforStudentTest",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(11)
    def getRankHierarchialAnalysisforTest(self):
        """

        """
        with self.client.get(
            self.resultHost + "/analysis/" + Hid + "/test/000031/rank",
            name="getRankAnalysisforStudentTest",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(12)
    def getRankHierarchialAnalysisforTest_post(self):
        """

        """
        with self.client.post(
            self.resultHost + "/analysis/" + Hid + "/test/000031/rank",
            name="getRankAnalysisforStudentTest_post",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(13)
    def getRankHierarchialAnalysisforhiearchyandTest(self):
        """

        """
        with self.client.get(
            self.resultHost + "/analysis/" + Hid + "/test/000031/rank/count",
            name="getRankAnalysisforhierarchyandtest",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(14)
    def getRankHierarchialAnalysisforhiearchyandTest_post(self):
        """

        """
        with self.client.post(
            self.resultHost + "/analysis/" + Hid + "/test/000031/rank/count",
            name="getRankAnalysisforhierarchyandtest_post",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(15)
    def getCWUAnalysisforhiearchyandTest(self):
        """

        """
        with self.client.get(
            self.resultHost + "/analysis/" + Hid + "/test/000031/cwu",
            name="getCWUAnalysisforhierarchyandtest",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(16)
    def getCWUAnalysisforhiearchyandTest_post(self):
        """

        """
        with self.client.post(
            self.resultHost + "/analysis/" + Hid + "/test/000031/cwu",
            name="getCWUAnalysisforhierarchyandtest_post",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(17)
    def geterrorAnalysisforhiearchyandTest(self):
        """

        """
        with self.client.get(
            self.resultHost + "/analysis/" + Hid + "/test/000031/error",
            name="geterrorAnalysisforhierarchyandtest",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(18)
    def geterrorAnalysisforhiearchyandTest_post(self):
        """

        """
        with self.client.post(
            self.resultHost + "/analysis/" + Hid + "/test/000031/error",
            name="geterrorAnalysisforhierarchyandtest_post",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(19)
    def geterrorAnalysisforhiearchyandTestandquestion(self):
        """

        """
        with self.client.get(
            self.resultHost + "/analysis/" + Hid + "/test/000031/error/Q6/count",
            name="geterrorAnalysisforquestion",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(20)
    def getuniquestudentscount(self):
        """

        """
        with self.client.get(
            self.resultHost + "/analysis/" + Hid + "/students/count?testIds=000031",
            name="getuniquestudentscount",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(21)
    def getuniquestudentscount_post(self):
        """

        """
        with self.client.post(
            self.resultHost + "/analysis/" + Hid + "/students/count?testIds=000031",
            name="getuniquestudentscount_post",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(22)
    def getuniquestudentscount_get(self):
        """

        """
        with self.client.get(
            self.resultHost + "/analysis/" + Hid + "/test/000031/error/Q6",
            name="getuniquestudentscount_get",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(23)
    def getStudentdata(self):
        """

        """
        with self.client.get(
            self.resultHost + "/students/189288994",
            name="getStudentdata",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(24)
    def getStudentTests(self):
        """

        """
        with self.client.get(
            self.resultHost + "/students/189288994/tests",
            name="getStudentTests",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(25)
    def getStudenttestdetails(self):
        """

        """
        with self.client.get(
            self.resultHost + "/students/189288994/test/000031",
            name="getStudenttestdetails",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(26)
    def getHierarchies(self):
        """

        """
        with self.client.get(
            self.resultHost + "/hierarchies",
            name="gethierarchies",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(27)
    def getHierarchiesbylevel(self):
        """

        """
        with self.client.get(
            self.resultHost + "/hierarchies/5",
            name="gethierarchiesandlevel",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()

    @seq_task(28)
    def getHierarchiesbyTestandlevel(self):
        """

        """
        with self.client.get(
            self.resultHost + "/hierarchies/000031/5",
            name="gethierarchiesTNL",
            headers=self.getToken(),
            catch_response=True,
        ) as response:
            if response.content != None and response.status_code == 200:
                content = json.loads(response.content.decode("utf8"))
                if response.content != None and response.status_code == 200:
                    response.success()
                    exit(0)
            else:
                response.failure(
                    "Unable to fetch Url"
                    + str(response.status_code)
                    + ": "
                    + str(response.text)
                    + "\n ... Ending User Locust"
                )
                raise exception.StopLocust()


class chinatime(HttpLocust):
    task_set = china
    min_wait = 1000
    max_wait = 3000
