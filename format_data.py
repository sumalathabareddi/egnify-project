import requests
import csv

docs =[]
with open('./GOUTHAMI JUNIOR COLLEGE_students_sample_1.csv') as csv_file:

    csv_reader = csv.DictReader(csv_file, delimiter=",")
    # csv_headers = next(csv_reader)
    # print(f'{csv_headers}')
    csv_data = list(csv_reader)

    for row in csv_data:
        row['Student Id'] = row["Student Id"].strip()
        row['Email'] = f'{row["Student Id"]}@getranks.in'.lower()
        row['Student Name'] = row['Student Name'].strip()
        # row['Orientation'] = f'{row["Orientation"]}'.strip()
        # if row['Class'] =='Class LONG TERM':
        #     row['Class'] = 'Class 13'
        # else:
        #     row['Class'] = f'{row["Class"]}'.strip()
        # row['State'] = f'{row["State"]}'.strip()
        # row['City'] = f'{row["City"]}'.strip()
        # row['Campus'] = f'{row["Campus"]}'.strip()
        # row['Section'] = f'{row["Section"]}'.strip()
        docs.append(row)


with open('./students.csv', mode='w') as csv_file:
        fieldnames = ['Student Id','Student Name','Father Name','Phone','Email','DOB','Gender','Category','Hierarchy Id']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(docs)